 
$.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
    var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

    if (token) {
        return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
    }
});

$( document ).ajaxError(function( event, jqxhr, settings, exception ) {
    if ( jqxhr.status== 500 ) {
         console.log('Uncaught Error.\n' + jqxhr.responseText);
    }
});


 $(document).ready(function(){
        window.setInterval(function(){
          //checkMessages();
          //checkNewMessagesCount();
        }, 2000);
    });

    $('#message-list').on('scroll', function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
            console.log('end reach up');
        }
        else if($(this).scrollTop() <= 0)
        {
            console.log('Top reached');
            loadOldMessages();
        }
    
    });


    function loadOldMessages(){
        var ref_message_id = $(".last_message_id").first().val();
        console.log("oldest id: "+ref_message_id);
        var user_id = $('#partner_user_id').val();
        console.log(user_id);
        $.ajax({url: "/admin/messenger/old-convo-messages/"+user_id+"?ref_message_id="+ref_message_id, success: function(result){
            $(".chatbox").prepend(result);
            $('#chatbox').emoticonize();
        }});
    }
  
    function loadMessages(user_id,contact_name){
        
        $('.chat-container').show();
        $('.chat-partner').text(contact_name);
        $('#employees-chat').addClass('animated fadeOutRight');
        $('#employees-chat').hide();

        $(".chatbox").html('');
        $('#partner_user_id').val(user_id);
        var last_message_id = $(".last_message_id").last().val();
        console.log(last_message_id);
        $.ajax({url: "/admin/messenger/load-convo/"+user_id,
        success: function(result){
            $(".chatbox").append(result);
            scrollDown();
            $('#chatbox').emoticonize();
        }});

        
    }

    function loadNewMessages(user_id){

        //$("#chatbox").html('');

        $('#partner_user_id').val(user_id);
        var last_message_id = $(".last_message_id").last().val();
        console.log(last_message_id);
        $.ajax({url: "/admin/messenger/convo/"+user_id+"?last_message_id="+last_message_id, success: function(result){
            $(".chatbox").append(result);
            scrollDown();
            $('#chatbox').emoticonize();
        }});

        
    }

    $('#chat-activator').click(function(){
        $('#employees-chat').addClass('animated fadeInLeft');
        $('#employees-chat').removeClass('animated fadeOutRight');
        $('#employees-chat').toggle();

    });
    
    function closeChatBox(){
        $('.chat-container').hide();
        $('#partner_user_id').val('');
    }



    $('.sendMesage').click(function(){

        var new_message = $('.new-message').val();
        var partner_user_id = $('#partner_user_id').val();
        $.ajax({
            url: '/admin/messenger/new-message',
            type: 'POST',
            data: {new_message:new_message,partner_user_id:partner_user_id} ,
            success: function (response) {
                //your success code
                console.log("Message has been sent!");
                $('.new-message').val('');
                loadNewMessages(partner_user_id);
                $('#chatbox').emoticonize();
                scrollDown();
            },
            error: function () {
                //your error code
                alert("This page session expires!, This is due to the page is being idle for a few munites.")
                location.reload();
            }
        });




    });

    $(".new-message").keyup(function (e) {
        if (e.keyCode == 13) {
            var new_message = $('.new-message').val();
            var partner_user_id = $('#partner_user_id').val();
            $.ajax({
                url: '/admin/messenger/new-message',
                type: 'POST',
                data: {new_message:new_message,partner_user_id:partner_user_id} ,
                success: function (response) {
                    //your success code
                    console.log("Message has been sent!");
                    $('.new-message').val('');
                    loadNewMessages(partner_user_id);
                    
                    scrollDown();
                },
                error: function () {
                    //your error code
                    alert("This page session expires!, This is due to the page is being idle for a few munites.")
                    location.reload();
                }
            });
        }
    });

    function checkMessages(){
        var partner_user_id = $('#partner_user_id').val();
        var last_message_id = $(".last_message_id").last().val();

        if(partner_user_id == "" || last_message_id == ""){
            console.log("User not specified");
            return;
        }
        if(partner_user_id != undefined){
            var last_message_id = $(".last_message_id").last().val();
            console.log(last_message_id);
            $.ajax({url: "/admin/messenger/convo/"+partner_user_id+"?last_message_id="+last_message_id, success: function(result){
                var last_mes_id_result = $(result).find('.last_message_id').val();
                console.log(last_mes_id_result);

                if(last_message_id < last_mes_id_result){
                    var snd = new Audio("/assets/music/newmessage.mp3"); 
                    snd.play();
                    $(".chatbox").append(result);
                    scrollDown();
                }else{
                    console.log("No new Messages");
                }
                
                
            }});
        }

        
        
    }


    function checkNewMessagesCount(){

       

        $.ajax({url: "/admin/messenger/new-messages", success: function(result){
            
            console.log("result: "+result);
            
            if(result != ""){

                if ($(".chat-container").is(':hidden')) {
                    var snd = new Audio("/assets/music/newmessage.mp3"); 
                    snd.play();
                }
                
                $('#new-message-count').text(result['count']);
                
                console.log("passed!");
            }

        }});
        

        
        
    }


function loadDropDownMessages(){
    //alert("loading");
    $.ajax({url: "/admin/messenger/load-dropdown-messages", success: function(result){
            
        console.log("result: "+result);
        $('.conversations').html(result);
        $('#new-message-count').text('');
    }});
}

function scrollDown(){
    //$(".message-list").animate({ scrollTop: $('.message-list').prop("scrollHeight")}, 100);
    $("#message-list").scrollTop($("#message-list")[0].scrollHeight);
}



$(document).ready(function(){
    $('#billingMenu').click(function(){
        $('#academicsMenu').removeClass('active'); 
    });
    $('#academicsMenu').click(function(){
        $('#billingMenu').removeClass('active'); 
    });
});
$(document).ready(function(){
    $('.msg_box').hide();
    $('.chat_body').slideToggle('slow');
    $('.chat_head').click(function(){
        $('.chat_body').slideToggle('slow');
    });
    $('.msg_head').click(function(){
        $('.msg_wrap').slideToggle('slow');
    });
    
    $('.close').click(function(){
        $('.msg_box').hide();
    });
    
    $('.user').click(function(){

        $('.msg_wrap').show();
        $('.msg_box').show();
    });
    
    $('textarea').keypress(
    function(e){
        if (e.keyCode == 13) {
            e.preventDefault();
            var msg = $(this).val();
            $(this).val('');
            if(msg!='')
            $('<div class="msg_b">'+msg+'</div>').insertBefore('.msg_push');
            $('.msg_body').scrollTop($('.msg_body')[0].scrollHeight);
        }
    });

    $('#chatBox').keypress(
    function(e){
        $('#chatBox').removeClass('hide-chat-box');
    });
    
});