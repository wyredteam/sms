@extends('sms.main.index')

@section('css_filtered')
@include('admin.csslinks.css_crud')
<link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
    <style>
        .modal-lar{
            width:90%;
        }
        .select2{
            z-index:100 !important;
        }

    </style>
@stop

@section('content')



<div class="col-md-12">
  <div class="portlet box wyred">
    <div class="portlet-title">
      <div class="caption">

        <i class="fa fa-desktop text-white"></i> Student Billing
      </div>
      <div class="tools">
         
      </div>
    </div>
    <div class="portlet-body" style="padding:20px">
      <div class="row">
       
        <div class="col-md-6">
         <div class="form-group">
           <label>Student Status </label>
             <select class="form-control input-sm" id="status">
                 @foreach($status as $stat)
                     <option value="{{$stat->student_status_id}}">{{$stat->student_status}}</option>
                 @endforeach
             </select>
          </div>
           <div class="form-group">
               <form id="generateAssessment">
           <label>Search Student Name <i class="fa fa-search"></i></label>
               <select class="form-control input-sm student select2" name="student_id">
                   <option></option>
                   @foreach($students as $student)
                       <option value="{{$student->student_id}}">{{$student->last_name}}, {{$student->first_name}} </option>
                   @endforeach
               </select>
                   </form>
          </div>
       </div>
       <div class="col-md-6">
           <div class="well">
               <address>
                   <h4><b><i class="fa fa-info-circle"></i> Student Information</b></h4>

                   <br>
                   <strong>School Year: {{$sy->sy_from}} - {{$sy->sy_to}}</strong><br>
               </address>
               <h4 class="text-warning status"></h4>
               <address>
                   <strong>Student ID:</strong>
                   <span class="id_num"></span><br>
                   <strong>Full Name:</strong>
                   <span class="full_name"></span><br>
                   <strong>Grade Level:</strong>
                   <span class="grade_level"></span><br>
                   <strong>Section Name:</strong>
                   <span class="section"></span><br>
                   <strong>Start Time:</strong>
                   <span class="start"></span><br>
                   <strong>End Time:</strong>
                   <span class="end"></span><br>
                   <strong class="text-danger">Previous Balance:</strong><br>
                   <div class="row">
                       <table class="table" id="overallbalance">
                           <thead>
                           <tr>
                               <th>School Year</th>
                               <th>Fees</th>
                               <th>Balance</th>
                           </tr>
                           </thead>
                           <tbody>

                           </tbody>
                       </table>
                   </div>
               </address>
           </div>
       
       </div>
    
        <div class="col-md-12">
           <div class="tabbable-line">
                  <ul class="nav nav-tabs ">
                    <li class="active">
                      <a href="#tab_15_1" data-toggle="tab">
                      <i class="fa fa-desktop"></i> Payments </a>
                    </li>
                    <li>
                      <a href="#studentDiscount" data-toggle="tab">
                      <i class="fa fa-dollar"></i> My Discount </a>
                    </li>
                    <li>
                      <a href="#tab_15_2" data-toggle="tab">
                      <i class="fa fa-list-alt"></i> Promissories Record </a>
                    </li>
                    <li>
                        <a href="#tab_15_3" data-toggle="tab">
                       <i class="fa fa-copy"></i>  Student Ledger </a>
                    </li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_15_1">

                       <!-- new tab -->
                       <div class="col-md-12">
                        <div class="well">
                         <div class="col-md-12" style="padding-top:10px">
                            <a href="#" class="btn blue-madison btn-sm pull-right" data-toggle="modal" data-target="#new-payment">New Payments <i class="fa fa-cube"></i></a>
                             <a href="#" class="btn blue-madison btn-sm pull-right" data-toggle="modal" data-target="#prev-bal">Payments for Balance <i class="fa fa-cube"></i></a>
                             <button class="btn blue-madison wyredModalCallback generate" data-toggle="modal" data-url="/sms/registrar/generate-billing" data-form="generateAssessment" data-target="#wyredSaveModal" disabled> Generate Assessment
                                 <i class="fa fa-cube" ></i>
                             </button>
                        </div>
                         <div style="background:#FFFFFF;padding:10px">
                          <ul class="nav nav-tabs">
                            <li class="active">
                              <a href="#assessment" data-toggle="tab">
                              Assessment </a>
                            </li>
                            <li>
                              <a href="#non-assessment" data-toggle="tab">
                              Non-Assessment </a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <div class="tab-pane active" id="assessment">
                               <table id="payments_record" class="table table-striped" >
                                <thead>
                                  <tr>
                                      <th class="text-center">Date</th>
                                      <th class="text-center">Fee</th>
                                      <th class="text-center">OR No</th>
                                      <th class="text-center">Amount</th>
                                      <th class="text-center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>

                                </tbody>
                              </table>
                            </div>
                            <div class="tab-pane active" id="non-assessment">
                                <table id="non-assessment" class="table table-striped " >
                                    <thead>
                                    <tr>
                                        <th class="text-center">Date</th>
                                        <th class="text-center">Fee</th>
                                        <th class="text-center">OR No</th>
                                        <th class="text-center">Amount</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                          </div>
                          </div>
                        </div>
                      </div>
                          <!-- end new tab -->
                     
            
                    </div>
                    <div class="tab-pane" id="studentDiscount">
                    <div class="col-md-12">
                      <div class="well">
                      <div class="col-md-12" style="padding-top:10px">
                            <button class="btn green-meadow btn-sm pull-right" data-toggle="modal" id="adding-discount" data-target="#add-discount"><i class="fa fa-plus"></i> Add Discount</button>
                       </div>
                       <div style="background:#FFFFFF;padding:20px">
                        <table id="get-discount" class="table table-striped " >
                              <thead>
                                <tr>
                                    <th class="text-center">School Year</th>
                                    <th class="text-center">Fees</th>
                                    <th class="text-center">Discount Percentage</th>
                                    <th class="text-center">Discount Amount</th>
                                    <th class="text-center">Description</th>
                                    <th class="text-center">Action</th>
                                </tr>
                              </thead>
                              <tbody>

                              </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    </div>
                    <div class="tab-pane" id="tab_15_2">
                     <div class="well">
                      <div class="col-md-12" style="padding-top:10px">
                            <a class="btn green-meadow btn-sm pull-right" href="#" data-toggle="modal" data-target="#promissories"><i class="fa fa-plus"></i> Add New Promissory</a>
                       </div>
                       <div style="background:#FFFFFF;padding:20px">
                         <table id="get-promissory" class="table table-striped" >
                              <thead>
                                <tr>
                                    <th>Fees</th>
                                    <th>Amount</th>
                                    <th>Promised Date</th>
                                    <th>Reason</th>
                                    <th class="text-center">Action</th>
                                </tr>
                              </thead>
                              <tbody>

                              </tbody>
                        </table>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane" id="tab_15_3">
                           <div class="well">
                      <div class="col-md-12" style="padding-top:10px">
                            <a class="btn blue-madison btn-sm pull-right" href="" id="print-ledger" target="_blank"><i class="fa fa-print"></i>
                              Print Student Ledger</a>
                       </div>
                       <div style="background:#FFFFFF;padding:20px">
                          <table id="ledger_record" class="table table-striped" >
                              <thead>
                              <tr>
                                  <th>School Year</th>
                                  <th>Date</th>
                                  <th>Fees</th>
                                  <th>OR</th>
                                  <th>Payments</th>
                                  <th>Balance</th>
                              </tr>
                              </thead>
                              <tbody>

                              </tbody>
                          </table>

                          </div>

                      </div>
                    
                  </div>
                </div>
      
   
     </div>
   
   
  </div>
</div>


      <!-- add promise -->
      <div class="modal fade draggable-modal mo-z drag-me" id="promissories" tabindex="-1" role="basic" aria-hidden="true">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h4 class="modal-title">Add Promissory</h4>
                  </div>
                  <div class="modal-body">
                      <form id="add-promissory">
                          <div class="row">

                              <div class="col-md-12">
                                  <table id="edit-payments" class="table table-striped table-bordered" >
                                      <tr>
                                          <th>Fees</th>
                                          <th>Amount</th>
                                          <th>Promised Date</th>
                                          <th>Reason</th>
                                      </tr>
                                      <tr>
                                          <input type="hidden" name="student_id" id="promised_student_id">
                                          <td><select name="fees_id" id="promised_fees" class="form-control fees_info" required></select>
                                              <option></option>
                                          </td>
                                          <td><input type="text" name="promised_amount" class="form-control mask-money" required></td>
                                          <td><input type="text" name="promised_date" class="form-control promised_date" required></td>
                                          <td><input type="text" name="reason" class="form-control" ></td>
                                      </tr>
                                  </table>
                              </div>
                          </div>
                      </form>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn default" data-dismiss="modal">Close</button>
                      <button class="btn btn-info wyredModalCallback" data-toggle="modal" data-url="/sms/billing/add-promissory" data-form="add-promissory" data-target="#wyredSaveModal">Save</button>
                  </div>
              </div>
              <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
      </div>
      <!-- add Promise -->





        <!-- Previous Balance Payment -->


        <!-- END Portlet PORTLET-->
        <div class="modal fade draggable-modal mo-z drag-me" id="prev-bal" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog modal-lar">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Fees</h4>
                    </div>
                    <div class="modal-body">
                        <form id="PrevPayment">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>OR No</label>
                                        <input type="text" class="form-control input-sm" name="or_num" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input type="text" class="form-control input-sm" value="{{$date_now}}" name="date_now" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div style="overflow: auto; height: 400px; width: 100%;">
                                        <table id="previous_balance" class="table table-striped " >
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>School Year</th>
                                                <th>Fees</th>
                                                <th>Balance</th>
                                                <th>Amount</th>
                                                <th width="10%">Payment Type</th>
                                                <th width="20%">Check No.</th>
                                                <th>Check Date</th>
                                                <th>Bank</th>
                                            </tr>
                                            </thead>
                                            <tbody id="balance_record" class="balance_record">

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button class="btn btn-info wyredModalCallback" data-toggle="modal" data-url="/sms/billing/save-payment" data-form="PrevPayment" data-target="#wyredSaveModal">Save Fees</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <!-- Previous Balance Payment -->



        <!-- END Portlet PORTLET-->
<div class="modal fade draggable-modal mo-z drag-me" id="new-payment" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-lar">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Fees</h4>
      </div>
      <div class="modal-body">
        <form id="payment">
        <div class="row">
          <div class="col-md-4">
              <div class="form-group">
               <label>OR No</label>
               <input type="text" class="form-control input-sm" name="or_num" required>
              </div>
          </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Date</label>
                    <input type="text" class="form-control input-sm" value="{{$date_now}}" name="date_now" required>
                </div>
            </div>
           <div class="col-md-12">
           <div style="overflow: auto; height: 400px; width: 100%;">
           <table id="payments" class="table table-striped " >
                <thead>
                  <tr>
                      <td></td>
                      <th>Fees</th>
                      <th>Amount</th>
                      <th width="10%">Total Payment</th>
                      <th>Balance</th>
                      <th width="15%">Amount Paid</th>
                      <th width="10%">Payment Type</th>
                      <th width="20%">Check No.</th>
                      <th>Check Date</th>
                      <th>Bank</th>
                  </tr>
                </thead>
                <tbody id="fees_body" class="fees_body">

                </tbody>
               <tr>
                   <td colspan="5"> Total Payment</td>
                   <td><input type="text" id="total_payment" class="form-control mask-money" disabled></td>
                   <td colspan="4"></td>
               </tr>
          </table>
        </div>
        </div>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button class="btn btn-info wyredModalCallback" data-toggle="modal" data-url="/sms/billing/save-payment" data-form="payment" data-target="#wyredSaveModal">Save Fees</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


   <!-- Edit Payment-->
      <div class="modal fade draggable-modal mo-z drag-me" id="edit-payment" tabindex="-1" role="basic" aria-hidden="true">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h4 class="modal-title">Edit Payment</h4>
                  </div>
                  <div class="modal-body">
                      <form id="editPayments">
                          <div class="row">

                              <div class="col-md-12">
                                  <table id="edit-payments" class="table table-striped table-bordered" >
                                      <tr>
                                          <th>Date</th>
                                          <th>Fee</th>
                                          <th>OR No</th>
                                          <th>Amount</th>
                                      </tr>
                                      <tr>
                                          <input type="hidden" name="payment_id" id="payment_id">
                                          <td><input type="text" name="payment_date" id="payment_date" class="form-control"></td>
                                          <td><input type="text" name="payment_fee" id="payment_fee" class="form-control" disabled></td>
                                          <td><input type="text" name="payment_or_no" id="payment_or_no" class="form-control"></td>
                                          <td><input type="text" name="payment_amount" id="payment_amount" class="form-control mask-money"></td>
                                      </tr>
                                  </table>
                              </div>
                          </div>
                      </form>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn default" data-dismiss="modal">Close</button>
                      <button class="btn btn-info wyredModalCallback" data-toggle="modal" data-url="/sms/billing/edit-payment" data-form="editPayments" data-target="#wyredSaveModal">Edit</button>
                  </div>
              </div>
              <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
      </div>
    <!-- Edit Payment -->

      <!-- Add discount -->
      <div class="modal fade draggable-modal mo-z drag-me" id="add-discount" tabindex="-1" role="basic" aria-hidden="true">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h4 class="modal-title">Discount Module</h4>
                  </div>
                  <div class="modal-body">
                      <form id="discounts">
                          <div class="row">

                              <div class="col-md-12">
                                  <table id="add-discount" class="table table-striped table-bordered" >
                                      <tr>
                                          <th>Fee</th>
                                          <th>Fee Amount</th>
                                          <th>Percentage</th>
                                          <th>Amount</th>
                                          <th>Discount Description</th>
                                          <th>Discounted Amount</th>
                                      </tr>
                                      <tr>
                                          <input type="hidden" name="discount_id" id="discount_id">
                                          <input type="hidden" name="student_id" id="discount_student_id">
                                          <td><select name="discount_fees" id="discount_fees" class="form-control fees_info" required></select>
                                              <option></option>
                                          </td>
                                          <td><input type="text" name="fee_amount" class="form-control clear" id="fee_amount" disabled></td>
                                          <td><input type="text" name="discount_percentage" class="form-control discounts" id="discount_percentage" required disabled>%</td>
                                          <td><input type="text" name="discount_amount" class="form-control discounts" id="discount_amount" required disabled></td>
                                          <td><input type="text" name="discount_description" class="form-control discounts" id="discount_description" required disabled></td>
                                          <td><input type="text" name="discounted_amount" class="form-control clear" id="discounted_amount" disabled></td>
                                      </tr>
                                  </table>
                              </div>
                          </div>
                      </form>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn default" data-dismiss="modal">Close</button>
                      <button class="btn btn-info wyredModalCallback" data-toggle="modal" data-url="/sms/billing/save-discount" data-form="discounts" data-target="#wyredSaveModal">Add Discount</button>
                  </div>
              </div>
              <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
      </div>
      <!--add discount -->



@stop
@section('js_filtered')
@include('admin.jslinks.js_crud')
@include('admin.jslinks.js_datatables')
<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Clock picker -->
<script src="/assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="/assets/admin/pages/scripts/table-advanced.js"></script>



<script>
    Number.prototype.format = function(n, x) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
    };

    $(document).ready(function(){

     $(".drag-me").draggable({
         handle: ".modal-header"
     });

     $('#customizeSched').hide();
     $('#setEmpFlexySched').hide();

     $('#fixedOption').change(function(){
         if(this.checked == true){
             $('#customizeSched').hide();
             $('#fixedSched').show();
         }else {
             $('#customizeSched').show();
             $('#fixedSched').hide();
         }
     });

     $('#customizeOption').change(function(){
         if(this.checked == true){
             $('#customizeSched').show();
             $('#fixedSched').hide();
         }else {
             $('#customizeSched').hide();
             $('#fixedSched').show();
         }
     });

     $('#schedType').change(function(){
         if(this.value == '1'){
             $('#setEmpFlexySched').show();
         }else if(this.value == '2'){
             $('#setEmpFlexySched').hide();
         }
     });

     $('.clockpicker').clockpicker({
         defaultTime: 'value',
         minuteStep: 1,
         disableFocus: true,
         template: 'dropdown',
         showMeridian:false
     });

     $('.promised_date').datepicker({
         startView: 2,
         todayBtn: "linked",
         keyboardNavigation: false,
         forceParse: false,
         autoclose: true
     });

     $('.default .input-group.date').datepicker({
         format: "mm/dd"
     });


 });

 $('.student').change(function(){
     var student_id = this.value;

            $('#print-ledger').attr('href','/sms/billing/print-ledger?student_id='+student_id);
             $('#discount_student_id').val(student_id);
             $('.fees_content').remove();
             $('.discount-data').remove();
             $('.promissory-data').remove();
             $('.ledger').remove();
             $('#promised_student_id').val(student_id);
             getStudentInfo(student_id);
             getAssessment(student_id);
             getPayments(student_id);
             getDiscount(student_id);
             getPromissory(student_id);
             getLedger(student_id);
            getOverallBalance(student_id);
 });


 $('#wyredSaveModal').on('hidden.bs.modal',function(){
     $('.fees_input').attr("disabled", "disabled");
     $('.fees_checker').prop('checked', false);
     $('.dis').attr('disabled', true);
     $('.bank-check').attr('disabled',true);
 });

 $('#new-payment, #edit-payment').on('hidden.bs.modal',function(){
     var student_id = $('.student').val();
     $('.fees_content').remove();
     $('.ledger').remove();
     getPayments(student_id);
     getAssessment(student_id);
     getLedger(student_id);
     getOverallBalance(student_id);
 });
    $('#wyredDeleteModal').on('hidden.bs.modal',function(){
        var student_id = $('.student').val();
        $('.fees_content').remove();
        $('.promissory-data').remove();
        $('.discount-data').remove();
        getDiscount(student_id);
        getPayments(student_id);
        getAssessment(student_id);
        getPromissory(student_id);
        getOverallBalance(student_id);
    });
    $('#promissories').on('hidden.bs.modal',function(){
        var student_id = $('.student').val();
        $('.promissory-data').remove();
        getPromissory(student_id);
    });
    $('#add-discount').on('hidden.bs.modal',function(){
        var student_id = $('.student').val();
        $('.discount-data').remove();
        $('.fees_content').remove();
        getDiscount(student_id);
        getPayments(student_id);
        getAssessment(student_id);
    });
function getDiscount(student_id){
    $.get('/sms/billing/get-discount?student_id='+student_id,function(data){
        $(data).each(function(){
            var sy = this.get_school_year.sy_from+" - "+this.get_school_year.sy_to;

            $(this.get_discount).each(function(){

            $('#get-discount > tbody:last-child').append("<tr class='discount-data d"+this.discount_id+"'><td>"+sy+"</td><td>"+this.get_fees.title+"</td><td>"+this.percentage+"%</td><td>"+this.amount+"</td><td>"+this.description+"</td><td><a href='#' data-toggle='modal' data-target='#wyredDeleteModal' data-id='"+this.discount_id+"' data-url='/softdelete/cancel-discount' onclick='softDeleteCallback(this)'  class='btn red-sunglo btn-sm btn-block'><i class='fa fa-trash'></i> Cancel</a></td></tr>");

            });
        });
    })
}

    function getPromissory(student_id){
        $.get('/sms/billing/get-promissory?student_id='+student_id,function(data){
            $(data).each(function(){
                $('#get-promissory > tbody:last-child').append("<tr class='promissory-data p"+this.pomissory_id+"'><td>"+this.get_fees.title+"</td><td>"+this.amount+"</td><td>"+this.promised_date+"</td><td>"+this.reason+"</td><td><a href='#' data-toggle='modal' data-target='#wyredDeleteModal' data-id='"+this.promissory_id+"' data-url='/softdelete/cancel-promissory' onclick='softDeleteCallback(this)'  class='btn red-sunglo btn-sm btn-block'><i class='fa fa-trash'></i> Cancel</a></td></tr>");
            });
        })
    }
 function getAssessment(student_id){
     $('#total_payment').val('');
     $('.fees_info').empty();
     $('.fees_info').append("<option></option>");
     $.get("/sms/get-assessments?student_id="+student_id, function(data){
         $(data[0].get_student_bill).each(function(){
             var balance = this.get_billing.bal;
             $('#payments > tbody:last-child').append("<tr class='fees_content text-center'><td><input class='fees_check"+this.get_billing.fees_id+" fees_checker' type='checkbox' onclick=enableThis('"+this.get_billing.fees_id+"') name='fees[]' value='"+this.get_billing.fees_id+"-"+this.account_id+"'></td><td>"+this.get_billing.get_fees.title+"</td><td>"+this.get_billing.dis_amount+"</td><td>"+(this.get_billing.total).format(2,3)+"</td><td>"+(this.get_billing.bal).format(2,3)+"</td><td><input type='text'  id='"+this.get_billing.fees_id+"' name='payment[]' onkeyup='totalPayment("+this.get_billing.fees_id+")' class='form-control fees_input mask-money payment_amount' disabled></td><td><select onchange='checkPayment("+this.get_billing.fees_id+")' name='payment_option[]' class='form-control dis payment_option "+this.get_billing.fees_id+"' disabled></select></td><td><input type='text' class='form-control dis p"+this.get_billing.fees_id+"' name='check_no[]' disabled> <input type='hidden' value='' name='check_no[]' id='i"+this.get_billing.fees_id+"' class='dis' disabled></td><td><input type='text' name='check_date[]' class='form-control bank-check' id='c"+this.get_billing.fees_id+"' disabled><input type='hidden'  name='check_date[]' class='check"+this.get_billing.fees_id+" bank-check' disabled></td><td><input type='text' name='bank[]' class='form-control bank-check' id='b"+this.get_billing.fees_id+"' disabled><input type='hidden' class='bank"+this.get_billing.fees_id+" bank-check' name='bank[]' disabled></td></tr>");
             $('.mask-money').maskMoney();
             if(balance <= 0){
                 $('.fees_check'+this.get_billing.fees_id).attr('disabled',true);
             }
             $('#b'+this.get_billing.fees_id).autosuggest({
                 data_url :"/autosuggest/getbank",
                 data_display : 'bank'
             });
             $('.fees_info').append("<option value='"+this.get_billing.fees_id+"'>"+this.get_billing.get_fees.title+"</option>");
         });
         $(data[1]).each(function(){
             $('.payment_option').append("<option value='"+this.payment_option_id+"'>"+this.payment_option+"</option>");
         });
     });
 }

 function totalPayment(fees_id){
        var payment = 0;
        var amount = 0;
     $('.payment_amount').each(function(){
         if($(this).is(':disabled')){
         }
         else{
             amount = $(this).val();
             amount = amount.replace(',','');
             payment = payment + Number(amount);
         }
     });

    $('#total_payment').val((payment).format(2,3));
 }
 function getPayments(student_id){
      $('.records').remove();
     $.get("/sms/billing/get-payments?student_id="+student_id, function(data){
            

            $(data[0]).each(function(){
                $('#payments_record > tbody:last-child').append("<tr class='records text-center'><td>"+this.date_of_payment+"</td><td class='text-left'>"+this.payment_option+"-"+this.title+"</td><td>"+this.or_no+"</td><td>"+this.amount+"</td><td><button class='btn blue-madison btn-sm' data-toggle='modal' onclick='editPayment("+this.payment_id+")' data-target='#edit-payment'><i class='fa fa-pencil'></i> Edit</button><a href='#' data-toggle='modal' data-target='#wyredDeleteModal' data-id='"+this.payment_id+"' data-url='/softdelete/cancel-payment' onclick='softDeleteCallback(this)'  class='btn red-sunglo btn-sm pull-right'><i class='fa fa-trash'></i> Cancel</a></td></tr>")
            });
            $('#payments_record > tbody:last-child').append("<tr class='records text-center'><td></td><td></td><td> <b>TOTAL Balance :</b></td><td>"+(data[2]).format(2,3)+"</td><td></td></tr>");
         $(data[1]).each(function(){

             $('#non-assessment > tbody:last-child').append("<tr class='records text-center'><td>"+this.date_of_payment+"</td><td class='text-left'>"+this.payment_option+"-"+this.title+"</td><td>"+this.or_no+"</td><td>"+this.amount+"</td><td><button class='btn blue-madison btn-sm' data-toggle='modal' onclick='editPayment("+this.payment_id+")' data-target='#edit-payment'><i class='fa fa-pencil'></i> Edit</button><a href='#' data-toggle='modal' data-target='#wyredDeleteModal' data-id='"+this.payment_id+"' data-url='/softdelete/cancel-payment' onclick='softDeleteCallback(this)'  class='btn red-sunglo btn-sm pull-right'><i class='fa fa-trash'></i> Cancel</a></td></tr>")
         });
     });
 }


    $('#discount_fees').change(function(){
        var fees_id = this.value;
        var student_id = $('.student').val();
        $.get("/sms/billing/get-fee-amount?fees_id="+fees_id+"&student_id="+student_id, function(data){
            $('#fee_amount').val(data);
        });
        if(fees_id != ''){
            $(".discounts").attr('disabled',false);
        }
        else{
            $(".discounts").attr('disabled',true);
            $(".discounts").val("");
            $('#fee_amount').val("");
        }
    });

    $('#adding-discount').click(function(){
        $('.discounts').val('');
        $('.clear').val('');
    });

    function getLedger(student_id){
        $.get("/sms/billing/get-ledger?student_id="+student_id, function(data){
            $(data).each(function(){
                $('#ledger_record > tbody:last-child').append("<tr class='ledger'><td>"+this.get_school_year.sy_from+" - "+this.get_school_year.sy_to+"</td><td></td><td>TOTAL AMOUNT DUE</td><td></td><td></td><td>"+this.amount_due+"</td></tr>");
                    $(this.get_payments).each(function(){
                        if(this.get_fees.get_category.title == 'Assessment') {
                            $('#ledger_record > tbody:last-child').append("<tr class='ledger'><td></td><td>" + this.date_of_payment + "</td><td>"+this.get_payment_option.payment_option+"-" + this.get_fees.title + "</td><td>" + this.or_no + "</td><td>" + this.amount + "</td><td>"+this.bal+"</td></tr>");
                        }
                    });
            });
        });
    }

 $('#discount_percentage').on('keyup',function(){
     var percentage = this.value;
     var amount     = $('#fee_amount').val();
     var percent = percentage / 100;
     var overall = amount * percent;
     var discounted = amount - overall;
             $('#discounted_amount').val(discounted);
             $('#discount_amount').val(overall);
 });
    $('#discount_amount').on('keyup',function(){
        var discount_amount = this.value;
        var amount     = $('#fee_amount').val();
        var overall = (discount_amount / amount) * 100;
        var dis        = (overall/100) * amount
        var discounted = amount - dis;
                $('#discounted_amount').val(discounted);
                $('#discount_percentage').val(overall);
    });

function enableThis(fees){
    if($('#'+fees).prop('disabled')){
        $('#' + fees).removeAttr('disabled');
        var amount1 = $('#'+fees).val();
        amount1 = amount1.replace(',','');
        amount1 = Number(amount1);
        var totalAmount1 = $('#total_payment').val();
        totalAmount1 = totalAmount1.replace(',','');
        totalAmount1 = Number(totalAmount1);
        var total1 = totalAmount1 + amount1;
        $('#total_payment').val((total1).format(2,3));


        $('.'+fees).prop('disabled',false);
        if($('.'+fees).val()== '2'){
            $('.p'+fees).prop('disabled',false);
            $('#i' + fees).attr('disabled', true);
            $('#b' + fees).attr('disabled', false);
            $('#c' + fees).attr('disabled', false);
            $('.bank'+fees).attr('disabled', true);
            $('.check'+fees).attr('disabled',true);

        }
        else{
            $('.p' + fees).attr("disabled", "disabled");
            $('#i' + fees).attr('disabled', false);
            $('#b' + fees).attr('disabled', true);
            $('#c' + fees).attr('disabled', true);
            $('.bank'+fees).attr('disabled', false);
            $('.check'+fees).attr('disabled', false);
        }

    }
    else {
        $('#' + fees).attr("disabled", "disabled");
        $('.' + fees).attr("disabled", "disabled");
        $('.p' + fees).attr("disabled", "disabled");
        $('#i' + fees).attr('disabled', true);
        $('#b' + fees).attr('disabled', true);
        $('#c' + fees).attr('disabled', true);
        $('.bank'+fees).attr('disabled', true);
        $('.check'+fees).attr('disabled',true);
        var amount = $('#'+fees).val();
        amount = amount.replace(',','');
        amount = Number(amount);
        var totalAmount = $('#total_payment').val();
        totalAmount = totalAmount.replace(',','');
        totalAmount = Number(totalAmount);
        var total = totalAmount - amount;
        $('#total_payment').val((total).format(2,3));
    }
}

    function checkPayment(payment){
        if($('.'+payment).val() == '2'){
            $('.p'+payment).prop('disabled',false);
            $('#i' + payment).attr('disabled', true);
            $('#b' + payment).attr('disabled', false);
            $('#c' + payment).attr('disabled', false);
            $('.bank'+payment).attr('disabled', true);
            $('.check'+payment).attr('disabled',true);
        }
        else{
            $('.p' + payment).attr("disabled", "disabled");
            $('#i' + payment).attr('disabled', false);
            $('#b' + payment).attr('disabled', true);
            $('#c' + payment).attr('disabled', true);
            $('.bank'+payment).attr('disabled', false);
            $('.check'+payment).attr('disabled',false);
        }

    }

    function getOverallBalance(student_id){
        $('.overallbal').remove();
        $('.prev-class').remove();
        $.get("/sms/billing/get-overall-balance?student_id="+student_id, function(data) {
            $(data).each(function(){
                var sy = this.get_school_year.sy_from+" - "+this.get_school_year.sy_to;
                var account = this.account_id;
                if(this.overall_bal > 0){
                    $(this.get_student_bill).each(function(){

                        if(this.balance > 0) {
                            $('#overallbalance tbody:last-child').append("<tr class='overallbal'><td>" + sy + "</td><td>"+this.get_billing.get_fees.title+"</td><td>"+this.balance+"</td></tr>");

                            $('#previous_balance tbody:last-child').append("<tr class='prev-class'><td><input type='checkbox' onclick='prevBal("+this.get_billing.get_fees.fees_id+")' name='fees[]' value='"+this.get_billing.get_fees.fees_id+"-"+account+"' ></td><td>"+sy+"</td><td>"+this.get_billing.get_fees.title+"</td><td>"+this.balance+"</td><td><input type='text' class='form-control' id='aprev"+this.get_billing.get_fees.fees_id+"' name='payment[]' disabled></td><td><select name='payment_option[]' class='form-control' id='pprev"+this.get_billing.get_fees.fees_id+"' disabled> <option value='1'>CASH</option> <option value='2'>CHECK</option> </select></td><td><input type='text' class='form-control' name='check_no[]' id='cprev"+this.get_billing.get_fees.fees_id+"' disabled></td><td><input type='text' class='form-control' id='dprev"+this.get_billing.get_fees.fees_id+"' name='check_date[]' disabled></td><td><input type='text' class='form-control' name='bank[]' id='bprev"+this.get_billing.get_fees.fees_id+"' disabled></td></tr>");
                        }
                    });
                }
            });

        });
    }

    function prevBal(account){

       if($('#pprev'+account).prop('disabled')){

           $('#pprev'+account).attr('disabled',false);
           $('#aprev'+account).attr('disabled',false);
           $('#dprev'+account).attr('disabled',false);
           $('#cprev'+account).attr('disabled',false);
           $('#bprev'+account).attr('disabled',false);
       }
        else{
           $('#pprev'+account).attr('disabled',true);
           $('#aprev'+account).attr('disabled',true);
           $('#dprev'+account).attr('disabled',true);
           $('#cprev'+account).attr('disabled',true);
           $('#bprev'+account).attr('disabled',true);
       }


    }

 function getStudentInfo(student_id){

     $.get("/sms/billing/get-student-info?student_id="+student_id, function(data){
         if(data != "") {
             $(".id_num").html(data.students.student_id);
             $(".full_name").html(data.students.first_name + " " + data.students.last_name);
             $(".grade_level").html(data.get_schedule.rf_section.get_grade_level.grade_level);
             $(".section").html(data.get_schedule.rf_section.section_name);
             $(".status").html('');
             $(".start").html(data.get_schedule.start_time);
             $(".end").html(data.get_schedule.end_time);
             $(".generate").removeAttr("disabled");
         }
         else{

             $(".status").html("STUDENT NOT ENROLLED");
             $(".id_num").html('');
             $(".full_name").html('');
             $(".grade_level").html('');
             $(".section").html('');
             $(".start").html('');
             $(".end").html('');
             $(".generate").attr('disabled',true);

         }
     });
 }
    function editPayment(payment_id){
        $.get("/sms/billing/get-payment-id?payment_id="+payment_id, function(data){
            $('#payment_date').val(data.date_of_payment);
            $('#payment_fee').val(data.get_fees.title);
            $('#payment_or_no').val(data.or_no);
            $('#payment_amount').val(data.amount);
            $('#payment_id').val(data.payment_id);

        });
    }
</script>

    
@stop
