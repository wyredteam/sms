@extends('sms.main.index')

@section('css_filtered')
    @include('admin.csslinks.css_crud')
    <link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
    <style>
        .bills{
            background: #5e88d4;
        }
        .select2{
            z-index: 9999999;
        }
    </style>
@stop
@section('content')



    <div class="col-md-8">
        <div class="portlet box wyred">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-desktop"></i> Expenses
                </div>
                <div class="tools">
                    <div class="pull-right">
                        <button class="btn red-sunglo btn-sm btn-block" data-toggle="modal" id="add-new-fees" data-target="#add-other-income"><i class="fa fa-plus"></i>Add Particulars</button>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">

                    <div class="col-md-12">
                        <div style="height: 425px;overflow-y: scroll;">
                            <div class="">
                                <table id="gradeFeesTable" class="table table-striped table-hover table-responsive" >
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Fees</th>
                                        <th>Category</th>
                                        {{--<th>Action</th>--}}
                                        {{--<th>Action</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                    <div class="col-md-12" style="margin-top:40px;">
                        <h3>Expenses </h3>
                        <div class="table-responsive">
                            <table id="other-payments" class="table table-striped table-hover" >
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Name</th>
                                    <th>Fees</th>
                                    <th>Payment Type</th>
                                    <th>Voucher #</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <th></th>
                                    <th></th>
                                </tr>

                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="portlet box wyred">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info-circle text-white"></i>Voucher
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-xs-12">
                        <form id="ExpensesForm">
                            <table id="payable" class="table">
                                <thead>
                                </thead>
                                <tbody>

                                </tbody>
                                <tr>
                                    <td>Date:</td>
                                    <td><input type="text" name="date" value="<?php echo date("Y-m-d"); ?>" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>Name:</td>
                                    <td><input type="text" name="name" class="form-control" required></td>
                                </tr>
                                <tr>
                                    <td>Check Voucher #:</td>
                                    <td><input type="text" name="voucher_no" class="form-control" required></td>
                                </tr>
                                <tr>
                                    <td>Voucher Type:</td>
                                    <td><select name="payment_type" id="payment-type" class="form-control">
                                            <option value="1">CASH</option>
                                            <option value="2">CHECK</option>
                                        </select></td>
                                </tr>
                                <tr class="checks">
                                    <td>Check #:</td>
                                    <td><input type="text" name="check_no" id="check_no" class="form-control" disabled></td>
                                </tr>
                                <tr class="checks">
                                    <td>Check Bank:</td>
                                    <td><input type="text" name="bank" id="bank" class="form-control" disabled></td>
                                </tr>
                                <tr class="checks">
                                    <td>Check Date:</td>
                                    <td><input type="text" id="check_date" class="form-control" disabled></td>
                                </tr>
                                <tr>
                                    <th colspan="2" class="text-center"><b>PARTICULARS</b></th>
                                </tr>
                                <tr>
                                    <td>Total Amount:</td>
                                    <td><input type="text" class="form-control mask-money" name="total-expenses" id="total-expenses" disabled></td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <div class="col-xs-12">
                        <button class="btn blue-madison btn-block wyredModalCallback" data-toggle="modal"  data-url="/sms/setup/billing/save-expenses" data-form="ExpensesForm" data-target="#wyredSaveModal"><i class="fa fa-money"></i> Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Portlet PORTLET-->
    <div class="modal fade draggable-modal mo-z drag-me" id="add-other-income"  role="basic" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">
                        <b><label>School Year: {{$sy->sy_from}} - {{$sy->sy_to}}</label></b>
                    </h4>
                </div>
                <div class="modal-body">
                    <form id="setupFee">
                        <div class="form-group">
                            <label>Reciept Account</label>
                            <input type="hidden" name="fee_id" id="fee_id" class="clear">
                            <select class="form-control select2" id="account" name="account">
                                @foreach($accounts as $account)
                                    <option value="{{$account->account_code}}">{{$account->account_desc}} - ({{$account->getAccountType->acccount_type}})</option>
                                @endforeach
                            </select>
                            <small>
                                Note: If you want to add new Charts of Account. Please proceed to Pythagoras Accounting Management System. or click this icon <a href="{{substr_replace(Request::root(), "", -2)}}84" target="_blank"><i class="fa fa-mail-forward"></i></a>
                            </small>
                        </div>
                        <div class="form-group">
                            <label>Fee Categories</label>
                            <select class="form-control input-sm" id="category" name="category">
                                @foreach($categories as $category)
                                    <option value="{{$category->fee_categories_id}}">{{$category->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Fee Title</label>
                            <input type="text" class="form-control input-sm clear" id="title" name="title">
                        </div>
                        <div class="form-group">
                            <label>Fee Description</label>
                            <textarea rows="3" class="form-control input-sm clear" id="description" name="description"></textarea>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button class="btn blue-madison wyredModalCallback" data-toggle="modal" data-url="/sms/setup/billing/save-fees" data-form="setupFee" data-target="#wyredSaveModal">Save Fees</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@stop
@section('js_filtered')
    @include('admin.jslinks.js_crud')
    @include('admin.jslinks.js_datatables')
    <script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Clock picker -->
    <script src="/assets/js/plugins/clockpicker/clockpicker.js"></script>
    <script src="/assets/admin/pages/scripts/table-advanced.js"></script>


    <script>
        Number.prototype.format = function(n, x) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
            return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
        };
        $(document).ready(function(){
            gradeFeesTableFunc();
            paymentTableFunc();
            $(".drag-me").draggable({
                handle: ".modal-header"
            });
            $('.checks').fadeOut();
            $('#customizeSched').hide();
            $('#setEmpFlexySched').hide();

            $('#fixedOption').change(function(){
                if(this.checked == true){
                    $('#customizeSched').hide();
                    $('#fixedSched').show();
                }else {
                    $('#customizeSched').show();
                    $('#fixedSched').hide();
                }
            });

            $('#customizeOption').change(function(){
                if(this.checked == true){
                    $('#customizeSched').show();
                    $('#fixedSched').hide();
                }else {
                    $('#customizeSched').hide();
                    $('#fixedSched').show();
                }
            });

            $('#schedType').change(function(){
                if(this.value == '1'){
                    $('#setEmpFlexySched').show();
                }else if(this.value == '2'){
                    $('#setEmpFlexySched').hide();
                }
            });

            $('.clockpicker').clockpicker({
                defaultTime: 'value',
                minuteStep: 1,
                disableFocus: true,
                template: 'dropdown',
                showMeridian:false
            });

            $('.customize .input-group.date').datepicker({
                startView: 2,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });

            $('.default .input-group.date').datepicker({
                format: "mm/dd"
            });


        });

        $('#wyredSaveModal,#wyredDeleteModal').on('hidden.bs.modal',function(){
            paymentTableFunc();
            gradeFeesTableFunc();
        });

        $('#payment-type').change(function(){
            var type = $('#payment-type').val();
            if(type == 1){
                $('.checks').fadeOut('slow');
                $("#check_no").attr('disabled',true);
                $("#bank").attr('disabled',true);
                $("#check_date").attr('disabled',true);
            }
            else{
                $('.checks').fadeIn('slow');
                $("#check_no").attr('disabled',false);
                $("#bank").attr('disabled',false);
                $("#check_date").attr('disabled',false);
            }
        });
        function payable(row_id){

            if($('#s'+subjectTableData[row_id].fees_id).is(':checked')){

                $('#payable > tbody:last-child').append('<tr class="bills w'+subjectTableData[row_id].fees_id+'"><td><input type="hidden" name="fees_id[]" value="'+subjectTableData[row_id].fees_id+'">'+subjectTableData[row_id].title+'</td><td><input type="text" class="form-control expenses-amount mask-money" onkeyup="addTotal()" name="amount[]"></td></tr>');


            }
            else{
                $('.w'+subjectTableData[row_id].fees_id).remove();

            }
            $('.w'+subjectTableData[row_id].fees_id).animate({"background-color": "#BCBFEF"});

        }
        function addTotal(){
            var payment = 0;
            var amount = 0;
            $('.expenses-amount').each(function(){

                    amount = $(this).val();
                    amount = amount.replace(',','');
                    payment = payment + Number(amount);

            });

            $('#total-expenses').val((payment).format(2,3));
        }
        function gradeFeesTableFunc(){

            $('#gradeFeesTable').dataTable().fnClearTable();
            $("#gradeFeesTable").dataTable().fnDestroy();

            var subjectTable = $('#gradeFeesTable').DataTable({
                responsive: true,
                bAutoWidth:false,

                "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                    nRow.setAttribute('data-id',aData.row_id);
                    nRow.setAttribute('class','ref_provider_info_class');
                },

                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (data) {
                            subjectTableData = data;
                            console.log(subjectTableData);
                            fnCallback(subjectTableData);
                        }
                    });
                },

                "sAjaxSource": "/sms/setup/billing/get-expenses",
                "sAjaxDataProp": "",
                "iDisplayLength": 10,
                "scrollCollapse": false,
                "paging":         true,
                "searching": true,

                "columns": [

                    { sDefaultContent: "" ,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html('<input type="checkbox" id="s'+oData.fees_id+'"  onchange="payable('+oData.row_id+')" name="amount">');
                        }
                    },
                    { "mData": "title", sDefaultContent: ""},
                    { "mData": "description", sDefaultContent: ""},


//                            { sDefaultContent: "" ,
//                                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
//                                    $(nTd).html('<a href="#" onclick="softDeleteCallback(this)" data-toggle="modal" data-target="#wyredDeleteModal" data-id="'+oData.billing_id+'" data-url="/softdelete/delete-billing" class="btn red-sunglo btn-sm btn-block"><i class="fa fa-trash"></i> REMOVE</a>');
//                                }
//                            },
//
//                            { sDefaultContent: "" ,
//                                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
//                                    $(nTd).html('<a href="#" data-toggle="modal" onclick="editFees('+oData.row_id+')" class="btn blue-madison btn-sm btn-block"><i class="fa fa-pencil-square"></i> EDIT</a>');
//                                }
//                            },
                ]
            });

        }

        function paymentTableFunc(){

            $('#other-payments').dataTable().fnClearTable();
            $("#other-payments").dataTable().fnDestroy();

            var subjectTable = $('#other-payments').DataTable({
                responsive: true,
                bAutoWidth:false,

                "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                    nRow.setAttribute('data-id',aData.row_id);
                    nRow.setAttribute('class','ref_provider_info_class');
                },

                "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (data) {
                            expTableData = data;
                            console.log(expTableData);
                            fnCallback(expTableData);
                        }
                    });
                },

                "sAjaxSource": "/sms/setup/billing/get-expenses-report",
                "sAjaxDataProp": "",
                "iDisplayLength": 10,
                "scrollCollapse": false,
                "paging":         true,
                "searching": true,

                "columns": [


                    { "mData": "date", sDefaultContent: ""},
                    { "mData": "name", sDefaultContent: ""},
                    { "mData": "get_fees.title", sDefaultContent: ""},
                    { "mData": "get_payment_option.payment_option", sDefaultContent: ""},
                    { "mData": "voucher_no", sDefaultContent: ""},
                    { "mData": "amount", sDefaultContent: ""},

                    { sDefaultContent: "" ,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html('<a href="#" onclick="softDeleteCallback(this)" data-toggle="modal" data-target="#wyredDeleteModal" data-id="'+oData.expenses_id+'" data-url="/softdelete/delete-expenses" class="btn red-sunglo btn-sm btn-block"><i class="fa fa-trash"></i> Cancel</a>');
                        }
                    },

                    { sDefaultContent: "" ,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html('<a href="#" data-toggle="modal" onclick="editFees('+oData.row_id+')" class="btn blue-madison btn-sm btn-block"><i class="fa fa-pencil-square"></i> EDIT</a>');
                        }
                    },
                ]
            });

        }


    </script>


@stop
