@extends('sms.main.index')

@section('css_filtered')
    @include('admin.csslinks.css_crud')
    <link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<style>
    .bills{
        background: #5d91dd;
    }
</style>
@stop
@section('content')



    <div class="col-md-8">
        <div class="portlet box wyred">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-desktop"></i> Other Income
                </div>
                <div class="tools">
                <div class="pull-right">
                    <button class="btn red-sunglo btn-sm btn-block" data-toggle="modal" id="add-new-fees" data-target="#add-other-income"><i class="fa fa-plus"></i>Add Other Income</button>
                </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">

                    <div class="col-md-12">
                        <div style="height: 425px;overflow-y: scroll;">
                            <div class="table-responsive">
                                <table id="gradeFeesTable" class="table table-striped table-hover" >
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Grade Level</th>
                                        <th>Fees</th>
                                        <th>Category</th>
                                        <th>Amount</th>
                                        {{--<th>Action</th>--}}
                                        {{--<th>Action</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                    <div class="col-md-12" style="margin-top:40px;">
                            <h3>Collection</h3>
                        <div class="table-responsive">
                            <table id="other-payments" class="table table-striped table-hover" >
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Payer's Name</th>
                                    <th>Fees</th>
                                    <th>Payment Type</th>
                                    <th>Or</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <th></th>
                                    <th></th>
                                </tr>

                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>

            </div>
        </div>
        </div>
        <div class="col-md-4" style="margin-top:20px;">
            <div class="portlet box wyred">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info-circle text-white"></i>Payment
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                    <div class="col-xs-12">
                    <form id="IncomeForm">
                    <table id="payable" class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="caption">Fees</th>
                            <th class="caption">Amount</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tr>
                            <td>Date:</td>
                            <td><input type="text" name="date" value="<?php echo date("Y-m-d"); ?>" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Payer's Name:</td>
                            <td><input type="text" name="payer" class="form-control" required></td>
                        </tr>
                        <tr>
                            <td>OR Number:</td>
                            <td><input type="text" name="or_no" class="form-control" required></td>
                        </tr>
                        <tr>
                            <td>Pay Type:</td>
                            <td><select name="payment_type" id="payment-type" class="form-control">
                                    <option value="1">CASH</option>
                                    <option value="2">CHECK</option>
                                </select></td>
                        </tr>
                        <tr class="checks">
                            <td>Check #:</td>
                            <td><input type="text" name="check_no" id="check_no" class="form-control" disabled></td>
                        </tr>
                        <tr class="checks">
                            <td>Check Bank:</td>
                            <td><input type="text" name="bank" id="bank" class="form-control" disabled></td>
                        </tr>
                        <tr class="checks">
                            <td>Check Date:</td>
                            <td><input type="text" id="check_date" class="form-control" disabled></td>
                        </tr>
                        <tr>
                            <td>Amount:</td>
                            <td><input type="text" class="form-control mask-money" name="total-payment" id="total-payment" required></td>
                        </tr>
                    </table>
                    </form>
                    </div>
                    <div class="col-xs-12">
                            <button class="btn blue-madison btn-block wyredModalCallback" data-toggle="modal"  data-url="/sms/setup/billing/other-payments" data-form="IncomeForm" data-target="#wyredSaveModal"><i class="fa fa-money"></i> Pay</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Portlet PORTLET-->
        <div class="modal fade draggable-modal mo-z drag-me" id="add-other-income" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">
                                <b><label>School Year: {{$sy->sy_from}} - {{$sy->sy_to}}</label></b>
                            </h4>
                    </div>
                    <div class="modal-body">
                            <form id="addGradeFees">
                                <div class="form-group">
                                    <label>Fees</label>
                                    <select class="form-control input-sm" name="fees" id="fee">
                                        <option></option>
                                        <option value="add-fees">-----------------------------------------------------------</option>
                                        <option value="add-fees" class="text-red"><b>+ Add Fees</b></option>
                                        <option></option>
                                        @foreach($fees as $fee)
                                            <option value="{{$fee->fees_id}}">{{$fee->title}} - {{$fee->description}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Amount</label>
                                    <input type="text" class="form-control input-sm mask-money" id="amount_fee" name="amount">
                                </div>
                            </form>
                        </div>

                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button class="btn btn-info wyredModalCallback" data-toggle="modal" data-url="/sms/setup/billing/save-grade-fees" data-form="addGradeFees" data-target="#wyredSaveModal">Save Fees</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        @stop
        @section('js_filtered')
            @include('admin.jslinks.js_crud')
            @include('admin.jslinks.js_datatables')
            <script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
            <!-- Clock picker -->
            <script src="/assets/js/plugins/clockpicker/clockpicker.js"></script>
            <script src="/assets/admin/pages/scripts/table-advanced.js"></script>


            <script>
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };
                $(document).ready(function(){
                    gradeFeesTableFunc();
                    paymentTableFunc();
                    $(".drag-me").draggable({
                        handle: ".modal-header"
                    });
                    $('.checks').fadeOut();
                    $('#customizeSched').hide();
                    $('#setEmpFlexySched').hide();

                    $('#fixedOption').change(function(){
                        if(this.checked == true){
                            $('#customizeSched').hide();
                            $('#fixedSched').show();
                        }else {
                            $('#customizeSched').show();
                            $('#fixedSched').hide();
                        }
                    });

                    $('#customizeOption').change(function(){
                        if(this.checked == true){
                            $('#customizeSched').show();
                            $('#fixedSched').hide();
                        }else {
                            $('#customizeSched').hide();
                            $('#fixedSched').show();
                        }
                    });

                    $('#schedType').change(function(){
                        if(this.value == '1'){
                            $('#setEmpFlexySched').show();
                        }else if(this.value == '2'){
                            $('#setEmpFlexySched').hide();
                        }
                    });

                    $('.clockpicker').clockpicker({
                        defaultTime: 'value',
                        minuteStep: 1,
                        disableFocus: true,
                        template: 'dropdown',
                        showMeridian:false
                    });

                    $('.customize .input-group.date').datepicker({
                        startView: 2,
                        todayBtn: "linked",
                        keyboardNavigation: false,
                        forceParse: false,
                        autoclose: true
                    });

                    $('.default .input-group.date').datepicker({
                        format: "mm/dd"
                    });


                });

                $('#wyredSaveModal,#wyredDeleteModal').on('hidden.bs.modal',function(){
                   paymentTableFunc();
                });

        $('#payment-type').change(function(){
            var type = $('#payment-type').val();
            if(type == 1){
                $('.checks').fadeOut('slow');
                $("#check_no").attr('disabled',true);
                $("#bank").attr('disabled',true);
                $("#check_date").attr('disabled',true);
            }
            else{
                $('.checks').fadeIn('slow');
                $("#check_no").attr('disabled',false);
                $("#bank").attr('disabled',false);
                $("#check_date").attr('disabled',false);
            }
        });
        function payable(row_id){

            var total = $('#total-payment').val();
            total = total.replace(',','');
            total = Number(total);
                if($('#s'+subjectTableData[row_id].billing_id).is(':checked')){
                    $('#payable > tbody:last-child').append('<tr class="bills w'+subjectTableData[row_id].billing_id+'"><td><input type="hidden" name="fees_id[]" value="'+subjectTableData[row_id].get_fees.fees_id+'"><input type="hidden" name="amount[]" value="'+subjectTableData[row_id].amount+'">'+subjectTableData[row_id].get_fees.title+'</td><td>'+subjectTableData[row_id].amount+'</td></tr>');
                total += Number(subjectTableData[row_id].amount);

                }
                else{
                    $('.w'+subjectTableData[row_id].billing_id).remove();
                    total = total - Number(subjectTableData[row_id].amount);
             }
            $('.w'+subjectTableData[row_id].billing_id).animate({"background-color": "#ffffff"});
            $('#total-payment').val((total).format(2,3));
        }
                function gradeFeesTableFunc(){

                    $('#gradeFeesTable').dataTable().fnClearTable();
                    $("#gradeFeesTable").dataTable().fnDestroy();

                    var subjectTable = $('#gradeFeesTable').DataTable({
                        responsive: true,
                        bAutoWidth:false,

                        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                            nRow.setAttribute('data-id',aData.row_id);
                            nRow.setAttribute('class','ref_provider_info_class');
                        },

                        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                            var grade_id = 'null';
                            oSettings.jqXHR = $.ajax( {
                                "dataType": 'json',
                                "type": "GET",
                                "url": sSource + grade_id,
                                "data": aoData,
                                "success": function (data) {
                                    subjectTableData = data;
                                    console.log(subjectTableData);
                                    fnCallback(subjectTableData);
                                }
                            });
                        },

                        "sAjaxSource": "/sms/setup/billing/get-grade-fees?grade_id=",
                        "sAjaxDataProp": "",
                        "iDisplayLength": 10,
                        "scrollCollapse": false,
                        "paging":         true,
                        "searching": true,

                        "columns": [

                            { sDefaultContent: "" ,
                                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                    $(nTd).html('<input type="checkbox" id="s'+oData.billing_id+'"  onchange="payable('+oData.row_id+')" name="amount">');
                                }
                            },
                            { "mData": "get_grade.grade_level", sDefaultContent: ""},
                            { "mData": "get_fees.title", sDefaultContent: ""},
                            { "mData": "get_fees.get_category.title", sDefaultContent: ""},
                            { "mData": "amount", sDefaultContent: ""},

//                            { sDefaultContent: "" ,
//                                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
//                                    $(nTd).html('<a href="#" onclick="softDeleteCallback(this)" data-toggle="modal" data-target="#wyredDeleteModal" data-id="'+oData.billing_id+'" data-url="/softdelete/delete-billing" class="btn red-sunglo btn-sm btn-block"><i class="fa fa-trash"></i> REMOVE</a>');
//                                }
//                            },
//
//                            { sDefaultContent: "" ,
//                                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
//                                    $(nTd).html('<a href="#" data-toggle="modal" onclick="editFees('+oData.row_id+')" class="btn blue-madison btn-sm btn-block"><i class="fa fa-pencil-square"></i> EDIT</a>');
//                                }
//                            },
                        ]
                    });

                }

                function paymentTableFunc(){

                    $('#other-payments').dataTable().fnClearTable();
                    $("#other-payments").dataTable().fnDestroy();

                    var subjectTable = $('#other-payments').DataTable({
                        responsive: true,
                        bAutoWidth:false,

                        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                            nRow.setAttribute('data-id',aData.row_id);
                            nRow.setAttribute('class','ref_provider_info_class');
                        },

                        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                            oSettings.jqXHR = $.ajax( {
                                "dataType": 'json',
                                "type": "GET",
                                "url": sSource,
                                "data": aoData,
                                "success": function (data) {
                                    paymentTableData = data;
                                    console.log(paymentTableData);
                                    fnCallback(paymentTableData);
                                }
                            });
                        },

                        "sAjaxSource": "/sms/setup/billing/get-other-payments",
                        "sAjaxDataProp": "",
                        "iDisplayLength": 10,
                        "scrollCollapse": false,
                        "paging":         true,
                        "searching": true,

                        "columns": [


                            { "mData": "date_of_payment", sDefaultContent: ""},
                            { "mData": "payers_name", sDefaultContent: ""},
                            { "mData": "get_fees.title", sDefaultContent: ""},
                            { "mData": "get_payment_option.payment_option", sDefaultContent: ""},
                            { "mData": "or_no", sDefaultContent: ""},
                            { "mData": "amount", sDefaultContent: ""},

                            { sDefaultContent: "" ,
                                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                    $(nTd).html('<a href="#" onclick="softDeleteCallback(this)" data-toggle="modal" data-target="#wyredDeleteModal" data-id="'+oData.other_payment_id+'" data-url="/softdelete/delete-other-payment" class="btn red-sunglo btn-sm btn-block"><i class="fa fa-trash"></i> Cancel</a>');
                                }
                            },

                            { sDefaultContent: "" ,
                                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                    $(nTd).html('<a href="#" data-toggle="modal" onclick="editFees('+oData.row_id+')" class="btn blue-madison btn-sm btn-block"><i class="fa fa-pencil-square"></i> EDIT</a>');
                                }
                            },
                        ]
                    });

                }


            </script>


@stop