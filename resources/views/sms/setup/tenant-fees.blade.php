@extends('sms.main.index')

@section('css_filtered')
@include('admin.csslinks.css_crud')
<link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
@stop

@section('content')



<div class="col-md-8">
  <div class="portlet box wyred">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-list text-white"></i>Grade Level Fees List
    </div>
    <div class="tools">
    
    </div>
  </div>
  <div class="portlet-body">
    <div class="row">
     
   
    <div class="col-md-12">
            <div style="height: 425px;overflow-y: scroll;">
                <div class="table-responsive">

                    <table id="gradeFeesTable" class="table table-striped table-hover" >
                        <thead>
                        <tr>
                            <th>Grade Level</th>
                            <th>Fees</th>
                            <th>Category</th>
                            <th>Amount</th>
                            <th>Action</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
      <div class="col-md-12">
       <div class="pull-right" style="padding-right:10px;padding-top:10px;">
        <button class="btn red-sunglo" data-toggle="modal" data-target="#recall-fees"><i class="fa fa-reply"></i> Recall Previous SY Account's</button>
       </div>
      </div>
   </div>
  </div>
 </div>
</div>

<div class="col-md-4">
<div class="portlet box wyred">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-info-circle text-white"></i> Grade Fee's Information
    </div>
    <div class="tools">
      
    </div>
  </div>
  <div class="portlet-body">
    <div class="row">
    <div class="col-md-12">
    <form id="addGradeFees">
   
      <div class="form-group">
       <label>Tenant Name</label>
       <input type="hidden" name="billing_id" id="billing_id" class="clear">
         <select class="form-control input-sm" id="gradeType" name="gradeType" onchange="changeGradeLevel()" id="gradeType">
              
        </select>
      </div>
       <div class="form-group">
        <label>Fees</label>
          <select class="form-control input-sm" name="fees" id="fee">
          <option></option>
          <option value="add-fees">-----------------------------------------------------------</option>
          <option value="add-fees" class="text-red"><b>+ Add Fees</b></option>
          <option></option>
       
          
        </select>
        </div>
       <div class="form-group">
         <label>Amount</label>
           <input type="text" class="form-control input-sm mask-money" id="amount_fee" name="amount">
       </div>
       </form>
       </div>  
      <div class="col-md-12">
      <button class="btn blue-madison btn-block wyredModalCallback" data-toggle="modal"  data-url="/sms/setup/billing/save-grade-fees" data-form="addGradeFees" data-target="#wyredSaveModal"><i class="fa fa-reply"></i> Setup Fee</button>
      </div>
   </div>
  </div>
 </div>
</div>


@stop
@section('js_filtered')
@include('admin.jslinks.js_crud')
@include('admin.jslinks.js_datatables')
<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Clock picker -->
<script src="/assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="/assets/admin/pages/scripts/table-advanced.js"></script>


<script>
 $(document).ready(function(){

     $('#setupMenu').addClass('active');
     $('#billingMenu').addClass('active');
     $('#feesMenu').addClass('active');

     feesTableFunc();


     $(".drag-me").draggable({
       handle: ".modal-header"
      });

    $('#customizeSched').hide();
    $('#setEmpFlexySched').hide();

    $('#fixedOption').change(function(){
      if(this.checked == true){
         $('#customizeSched').hide();
         $('#fixedSched').show();
      }else {
         $('#customizeSched').show();
         $('#fixedSched').hide();
      }
    });

     $('#customizeOption').change(function(){
      if(this.checked == true){
         $('#customizeSched').show();
         $('#fixedSched').hide();
      }else {
         $('#customizeSched').hide();
         $('#fixedSched').show();
      }
    });

    $('#schedType').change(function(){
      if(this.value == '1'){
         $('#setEmpFlexySched').show();
      }else if(this.value == '2'){
        $('#setEmpFlexySched').hide();
      }
    });

      $('.clockpicker').clockpicker({
        defaultTime: 'value',
        minuteStep: 1,
        disableFocus: true,
        template: 'dropdown',
        showMeridian:false
    });

    $('.customize .input-group.date').datepicker({
      startView: 2,
      todayBtn: "linked",
      keyboardNavigation: false,
      forceParse: false,
      autoclose: true
    });

    $('.default .input-group.date').datepicker({
    format: "mm/dd"
    });
  

 });
  $('#wyredDeleteModal').on('hidden.bs.modal',function(){
        feesTableFunc();
      $('#fee_id').val('');
  });
 $('#add-fees').on('hidden.bs.modal',function(){
        feesTableFunc();
     
  });
 $('#add-new-fees').click(function(){
     $('#fee_id').val('');
     $('#category').val('');
     $('#title').val('');
     $('#description').val('');
 });
 function editFee(row_id){
        
        $('#account').val(feeTableData[row_id].get_account.account_code);
        $('#category').val(feeTableData[row_id].get_category.fee_categories_id);
        $('#title').val(feeTableData[row_id].title);
        $('#description').val(feeTableData[row_id].description);
        $('#fee_id').val(feeTableData[row_id].fees_id);

    }
  function feesTableFunc(){
      
      $('#feesTable').dataTable().fnClearTable();
      $("#feesTable").dataTable().fnDestroy();

          var feeTable = $('#feesTable').DataTable({
          responsive: true,
          bAutoWidth:false,

          "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            nRow.setAttribute('data-id',aData.row_id);
            nRow.setAttribute('class','ref_provider_info_class');
          },

          "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            oSettings.jqXHR = $.ajax( {
              "dataType": 'json',
              "type": "GET",
              "url": sSource,
              "data": aoData,
              "success": function (data) {
                feeTableData = data;
                console.log(feeTableData);
                fnCallback(feeTableData);           
              }
            });
          },
                     
          "sAjaxSource": "/sms/setup/billing/get-fees",
          "sAjaxDataProp": "",
          "iDisplayLength": 10,
          "scrollCollapse": false,
          "paging":         true,
          "searching": true,

          "columns": [
             

               { "mData": "get_account.account_desc", sDefaultContent: ""},
               { "mData": "get_category.title", sDefaultContent: ""},
               { "mData": "title", sDefaultContent: ""},
               { "mData": "description", sDefaultContent: ""},

                { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a href="#" onclick="softDeleteCallback(this)" data-toggle="modal" data-target="#wyredDeleteModal" data-id="'+oData.fees_id+'" data-url="/softdelete/delete-fees" class="btn btn-danger btn-block btn-sm"><i class="fa fa-trash"></i> REMOVE</a>');
                  }
                },  

               { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a href="#" data-toggle="modal" data-target="#add-fees" onclick="editFee('+oData.row_id+')" class="btn btn-info btn-block btn-sm"><i class="fa fa-pencil-square"></i> EDIT</a>');
                  }
                },  
          ]
      });

    }
</script>

    
@stop
