<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Student Information System And Acounting System (SIAS)</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="shortcut icon" href="/system/img/SMIicon.ico">
<link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
<style type="text/css">
	/* Paste this css to your style sheet file or under head tag */
	/* This only works with JavaScript, 
	if it's not present, don't show loader */
	.no-js #loader { display: none;  }
	.js #loader { display: block; position: absolute; left: 100px; top: 0; }
	.se-pre-con {
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url(/assets/img/ring.svg) center no-repeat #fff;
	}
</style>
@include('admin.csslinks.css_links')


@yield('css_filtered')

</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo ">

 <div class="se-pre-con"></div>

@include('admin.bodywrapper.body_wrapper')


@include('admin.jslinks.js_initial')
@yield('js_filtered')


<script src="/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
<script src="/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>

<script src="/assets/css/custom/preloader.css"></script>

<script>
jQuery(document).ready(function() {    
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features
});

$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
</script>

</body>
</html>

