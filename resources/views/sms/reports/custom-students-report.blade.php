<!DOCTYPE html>
<html>
  <head>
    <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/css/custom/rhitsReports.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="http://{{$_SERVER['HTTP_HOST']}}/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
          border-bottom: 1px solid #DDDDDD;
          vertical-align: bottom;

         }
         body{
          padding-top:20px;
         }
         table{
          width: 100%;
          border:1px solid #000000;
          border-collapse: collapse;
         }
         th,td{
          border: 1px solid #000000;
         }
         .white{
          background-color:white !important; 
         }

         .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
          background-color: #F5F5F6;
          border-bottom-width: 1px;

        }
        .padding{
          padding-left: 10px;
        }
        .holder{
          border: 1px solid #DDDDDD;
          margin-bottom: 20px;
          padding:20px;
        }

        .bordered{
          border: 1px solid #DDDDDD;
        }
        td{
          text-transform: uppercase;
	 font-size:10px;
        }
    </style>
    <title>Student List Report</title>
  </head>


<body>


  <div class="page-num">Page 1</div>
  <div class="body-legal col-100" >
    <div class ="header col-md-12">
      <div class="col-md-12">
          <h4 class="t-center bold">SCHOOL OF THE MORNING STAR</h4>
          <h5 class="t-center bold">BUTUAN CITY</h5>
          <p class="t-center"></p>
          <p class="t-center"><i></i></p>
          <h2 class="text-center">STUDENT LIST</h2>
      </div>
    </div>

    <div class="col-md-12" style="margin-bottom: 10px;">
      <div class="col-xs-6">
        <h5>School Year: {{$headers['school_year']}}</h5>
        <h5>Student Status : {{$headers['student_status']}}</h5>
        <h5>Grade Type : {{$headers['grade_type']}}</h5>
      </div>
      <div class="col-xs-6">
        <h5>Grade Level : {{$headers['grade_level']}}</h5>
        <h5>Section: {{$headers['sectionName']}}</h5>
        <h5>Total Student Lists Found: {{count($students)}}</h5>
      </div>

      <div class="col-xs-12">
        
            
            
                  <table>
                  <tr class="bg-primary">
                    <th class="text-center" width="30"></th>
                  <!---  <th class="text-center" width="180">Id. Number</th> -->
                    <th class="text-center">Student</th>
		    <th class="text-center">Birthday</th>
		  <!---  <th class="text-center">Gender</th>
                    <th class="text-center">Father's Name</th>
                    <th class="text-center">Mother's Name</th>
                    <th class="text-center">Student Address</th> -->
                  </tr>
                  <?php $counter=1; ?>
                @foreach($students as $students)                    

                  <?php
                    $student_id = $students->student_id;
                    $final_student_id = sprintf("%06d",$student_id);
                  ?>
                  <tr>
                    <td>{{$counter}}.</td>
                   <!--- <td>{{$students->lrn}}</td> -->
                    <td class="text-left padding">{{$students->last_name}},  {{$students->midde_name}} {{$students->first_name}} {{$students->name_extension}}</td>
			<td class=" text-left padding">{{$students->birthday}}</td>
                    <?php 
                        $parents = explode(",", $students->parents_name);
                    ?>
		    <?php 
			if($students->gender == "Male"){
				$gender = "M";			
			}else{
				$gender = "F";
			}
		    ?>
		   <!--- <td class="text-left padding">{{$gender}}</td>
                    <td class="text-left padding">{{$parents[0]}}</td>
                    <td class="text-left padding">{{$parents[1] or ""}}</td>
                    <td class="text-left padding">{{$students->home_address}}</td> -->
                  </tr>
                  <?php $counter++; ?>
                @endforeach
                  </table>
      </div>
    </div>

</body>

</html>

