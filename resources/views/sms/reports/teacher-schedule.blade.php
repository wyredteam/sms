<!DOCTYPE html>
<html>
<head>
    <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/css/custom/rhitsReports.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="http://{{$_SERVER['HTTP_HOST']}}/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: bottom;

        }
        .tables{
            border:1px solid #000000;
            border-collapse: collapse;
            width: 100%;
        }
        .payments{
            position:relative;
            margin-top:20px;
            top:0px;
        }
        body{
            padding-top:20px;
        }
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
            margin-top:20px;
            border: 1px solid #000000;
        }



        .tables th,td{
            border:1px solid #000000;
            padding-left:10px ;
            text-align: left;
        }
        .tables th{
            padding:8px;
        }
        .center{
            text-align: center !important;
        }


        .border{
            border:1px solid #000000;
        }



    </style>
    <title>Teacher Schedule</title>
</head>


<body>


<div class="page-num">Page 1</div>
<div class="body-letter col-100" >
    <div class ="header col-md-12">
        <div class="col-md-12">
            <h3 class="t-center bold">SCHOOL OF THE MORNING STAR</h3>
            <h4 class="t-center bold">BUTUAN CITY</h4>
            <p class="t-center"></p>
            <p class="t-center"><i></i></p>
            <h3 class="text-center">Teacher Schedule</h3>
        </div>
    </div>
    <div class="col-md-12" style="margin-bottom: 10px;">
        <table class="tables">
            <tr>
                <td>Name:</td>
                <td colspan="5">{{$teacher->first_name}} {{$teacher->last_name}}</td>
            </tr>
            <tr>
                <td colspan="6">Advisory:</td>
            </tr>
            <tr>
                <td>Section:</td>
                <td>{{$sched->RfSection->section_name}}</td>
                <td>Start Time:</td>
                <td>{{$sched->start_time}}</td>
                <td>End Time</td>
                <td>{{$sched->end_time}}</td>
            </tr>
        </table>

        <table class="tables">
            <tr>
                <th class="center" colspan="5">SCHEDULE</th>
            </tr>
            <tr class="center">
                <th class="center">Day</th>
                <th class="center">Start Time</th>
                <th class="center">End Time</th>
                <th class="center">Subject</th>
                <th class="center">Section</th>
            </tr>
            @foreach($schedule as $schedules)
                <tr>
                    <td>{{$schedules->weekdays}}</td>
                    <td class="center">{{$schedules->start_time}}</td>
                    <td class="center">{{$schedules->end_time}}</td>
                    <td class="center">{{$schedules->subject_name}}</td>
                    <td class="center">{{$schedules->section_name}}</td>
                </tr>
             @endforeach
        </table>
    </div>
</div>
</body>

</html>

