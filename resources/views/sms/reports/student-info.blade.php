<!DOCTYPE html>
<html>
<head>
    <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/css/custom/rhitsReports.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="http://{{$_SERVER['HTTP_HOST']}}/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: bottom;

        }
        table{
            width: 80%;
        }
        .payments{
            position:relative;
            margin-top:20px;
            top:0px;
        }
        body{
            padding-top:10px;
        }
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
            margin-top:20px;
        }
        th,td{
            border: 1px solid #000000;
            text-align: left;
            padding-left:10px;
            padding-top:2px;
            /*padding-bottom: 2px;*/
            font-size: 15px;
            text-transform: capitalize;

        }
        .paddings{
            padding-top:5px;
            padding-bottom: 5px;
            font-size: 14px;
        }
        .statement{
            position:relative;
            float: left;
            width: 50%;
        }
        .payment-sched{
            position: relative;
            float: left;
            width: 45%;
            left:40px;
        }
        .white{
            background-color:white !important;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #F5F5F6;
            border-bottom-width: 1px;

        }
        .border{
            border:1px solid #000000;
        }
        .billing{
            height:620px;
            margin-bottom: 30px;
        }
        .padding{
            padding-left: 10px;
        }
        .holder{
            border: 1px solid #DDDDDD;
            margin-bottom: 20px;
            padding:20px;
        }

        .bordered{
            border: 1px solid #DDDDDD;
        }
        .no-border table,td,th{
            border: 1px solid #ffffff;
        }
        .border-top{
            /*border-top: 1px solid #000000 !important;*/
            border-top: 1px;
            border-top: 1px solid ;
            border-top: medium solid #414446;
        }


    </style>
    <title>Student Assessment</title>
</head>


<body>


<div class="page-num">Page 1</div>
<div class="body-letter col-100" >
    <div class ="header col-md-12">
        <div class="col-md-12">
            <h4 class="t-center bold">SCHOOL OF THE MORNING STAR</h4>
            <h5 class="t-center bold">BUTUAN CITY</h5>
            <p class="t-center"></p>
            <p class="t-center"><i></i></p>
            <h4 class="text-center">Student Information</h4>
        </div>
    </div>
    <div class="col-md-12" style="margin-bottom: 10px;">
        <table class="" style="padding-left: 15px; padding-right: 15px;">
            <tr>
                <th colspan="10"><b>PERSONAL INFORMATION</b></th>
            </tr>
            <tr>
                <td>Last Name:</td>
                <th class="paddings">{{$student->last_name}}</th>
             </tr>
            <tr>
                <td width="20%">First Name:</td>
                <th class="paddings">{{$student->first_name}}</th>
            </tr>
            <tr>
                <td width="20%">Middle Name:</td>
                <th class="paddings">{{$student->middle_name}}</th>
            </tr>
            <tr>
                <td>Suffix:</td>
                <th class="paddings">{{$student->name_extension}}</th>
            </tr>
            <tr>
                <td>Nick Name:</td>
                <th class="paddings">{{$student->nick_name}}</th>
            </tr>
            <tr>
                <td>Gender:</td>
                <th class="paddings">{{$student->gender}}</th>
            </tr>
            <tr>
                <td>Birth Place:</td>
                <th class="paddings">{{$student->birthplace}}</th>
            </tr>
            <tr>
                <td>Birth Day:</td>
                <th class="paddings">{{$student->birthday}}</th>
            </tr>
            <tr>
                <td>Cellphone #:</td>
                <th class="paddings">{{$student->cp_no}}</th>
            </tr>
            <tr>
                <td>Landline #:</td>
                <th class="paddings">{{$student->tel_no}}</th>
            </tr>
            <tr>
                <td>Home Address:</td>
                <th class="paddings">{{$student->home_address}}</th>
            </tr>
            <tr>
                <th class="bg-primary" colspan="10">PARENT INFORMATION</th>
            </tr>
        </table>
            @foreach($student->Parents_Students as $parents)

            <table>
                <tr> @if($parents->parental_type_id == '1') <th colspan="2" class="bg-info">Mother</th>
                    @elseif($parents->parental_type_id == '2') <th colspan="2" class="bg-info">Father</th>
                @endif
                </tr>
            @if($parents->parental_type_id != '3')
                <tr>
                    <td width="20%">Name:</td>
                    <th>{{$parents->Parents->parents_name}}</th>
                </tr>
                    <tr>
                    <td>Religion:</td>
                    <th>{{$parents->Parents->Religion->religion_name}}</th>
                    </tr>
                    <tr>
                    <td>Birthday:</td>
                    <th>{{$parents->Parents->dob}}</th>
                    </tr>
                    <tr>
                    <td>Nationality:</td>
                    <th>{{$parents->Parents->Nationality->nationality_name}}</th>
                </tr>
                <tr>
                    <td>Occupation:</td>
                    <th>{{$parents->Parents->Occupation->designation_name}}</th>
                </tr>
                    <tr>
                    <td>Name of Employer:</td>
                    <th>{{$parents->Parents->firm_employer_name}}</th>
                    </tr>
                    <tr>
                    <td>Tel No.(RESIDENCE):</td>
                    <th>{{$parents->Parents->residence_tel}}</th>
                    </tr>
                    <tr>
                    <td>Tel No. (OFFICE):</td>
                    <th>{{$parents->Parents->home_tel}}</th>
                </tr>
                <tr>
                    <td>Home Address:</td>
                    <th colspan="8">{{$parents->Parents->home_address}}</th>
                </tr>
                @endif
                @if($parents->parental_type_id == '3'&& $parents->Parents->parents_name != '')
                <tr>
                    <th colspan="8" class="bg-info">Guardian</th>
                </tr>
                    <tr>
                        <td>Name:</td>
                        <th colspan="7">{{$parents->Parents->parents_name}}</th>
                    </tr>
                @endif
            @endforeach
        </table>
    </div>

</body>

</html>

