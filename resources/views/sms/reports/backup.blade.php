 <td colspan="5" >
                    <table style="margin-bottom:30px;" class="table table-responsive table-bordered">
                             <?php $bal_due = 0; $bal=0; ?>
                            @foreach($bill->getStudentBill as $student_bill)
                                    @if($student_bill->getBilling->getFees->getCategory->title == 'Assessment')
                                <tr style="margin-left:130px">
                                    <td>{{$student_bill->getBilling->getFees->title}}</td>
                                    <td class="text-right">
                                    @if($student_bill->balance_due < 0)
                                        P 0.00
                                    @else
                                        {{number_format($student_bill->balance_due,2,'.',',')}}
                                    @endif

                                    </td>

                                    <td class="text-right">
                                    @if($student_bill->bal < 0)
                                        P 0.00
                                    @else
                                        {{number_format($student_bill->bal,2,'.',',')}}
                                    @endif
                                    

                                    


                                    </td>
                                <?php $bal_due = $bal_due + $student_bill->balance_due; $bal = $bal+$student_bill->bal; ?>
                                </tr>
                                    @endif
                            @endforeach
                    </td>
                    </tr>
                    </table >