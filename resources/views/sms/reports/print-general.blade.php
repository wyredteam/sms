<!DOCTYPE html>
<html>
<head>
    <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/css/custom/rhitsReports.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="http://{{$_SERVER['HTTP_HOST']}}/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: bottom;

        }
        table{
            width: 80%;
        }
        .payments{
            position:relative;
            margin-top:20px;
            top:0px;
        }
        body{
            padding:30px;

        }
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
            margin-top:20px;
        }
        th,td{
            border: 1px solid #000000;
            text-align: left;
            padding-left:10px;
            padding-top:2px;
            /*padding-bottom: 2px;*/
            font-size: 15px;
            text-transform: capitalize;

        }
        .paddings{
            padding-top:5px;
            padding-bottom: 5px;
            font-size: 14px;
        }
        .statement{
            position:relative;
            float: left;
            width: 50%;
        }
        .payment-sched{
            position: relative;
            float: left;
            width: 45%;
            left:40px;
        }
        .white{
            background-color:white !important;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #F5F5F6;
            border-bottom-width: 1px;

        }
        .border{
            border:1px solid #000000;
        }
        .billing{
            height:620px;
            margin-bottom: 30px;
        }
        .padding{
            padding-left: 10px;
        }
        .holder{
            border: 1px solid #DDDDDD;
            margin-bottom: 20px;
            padding:20px;
        }

        .bordered{
            border: 1px solid #DDDDDD;
        }
        .no-border table,td,th{
            border: 1px solid #ffffff;
        }
        .border-top{
            /*border-top: 1px solid #000000 !important;*/
            border-top: 1px;
            border-top: 1px solid ;
            border-top: medium solid #414446;
        }


    </style>
    <title>General Report</title>
</head>


<body>


<div class="page-num">Page 1</div>
<div class="body-letter col-100" >
    <div class ="header col-md-12">
        <div class="col-md-12">
            <h4 class="t-center bold">SCHOOL OF THE MORNING STAR</h4>
            <h5 class="t-center bold">BUTUAN CITY</h5>
            <h4 class="text-center">Income Statement from {{date('F d, Y', strtotime($from))}} to {{date('F d, Y', strtotime($to))}}</h4>
        </div>
    </div>
    <div class="col-md-12" style="margin-bottom: 10px;">
        <div style="background:#FFFFFF;padding:20px;" >
            <div class="col-xs-12">
            </div>
            <h4>INCOME</h4>
            <table id="general_income" class="table table-bordered" >
                <thead>
                <tr>
                    <th>Account</th>
                    <th class="text-right">Amount</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($acc_income as $income)
                        @if($income->amount > 0)
                            <tr>
                                <td>{{$income->account_desc}}</td>
                                <td class="text-right">P{{$income->amount}}</td>
                            </tr>
                        @endif
                    @endforeach
                <tr class="bg-info">
                    <td class="text-center">TOTAL</td>
                    <td class="text-right">P{{$total_income}}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div style="background:#FFFFFF;padding:20px;margin-top:0px">
            <h4>EXPENSES</h4>
            <table id="general_expenses" class="table table-bordered" >
                <thead>
                <tr>
                    <th>Account</th>
                    <th class="text-right">Amount</th>
                </tr>
                </thead>
                <tbody>
                @foreach($acc_expenses as $expenses)
                    @if($expenses->amount > 0)
                        <tr>
                            <td>{{$expenses->account_desc}}</td>
                            <td class="text-right">P{{$expenses->amount}}</td>
                        </tr>
                    @endif
                @endforeach
                <tr class="bg-info">
                    <td class="text-center">TOTAL</td>
                    <td class="text-right">P{{$total_expenses}}</td>
                </tr>
                </tbody>
            </table>
		
		
        </div>
    </div>

</body>

</html>

