@extends('sms.main.index')

@section('css_filtered')
    @include('admin.csslinks.css_crud')
    <link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
@stop

@section('content')



    <div class="col-md-12">
        <div class="portlet box wyred">
            <div class="portlet-title">
                <div class="caption">

                    <i class="fa fa-desktop text-white"></i> Generate Monthly Billing

                </div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-5" style="margin-bottom:20px;">
                        <form method="get" id="formData" action="/sms/reports/generate-payment-list" target="_blank">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label>Generate List:</label>
                            </div>
                            <div class="form-group">
                                <label for="">Select Fees:</label>
                                <select name="fees" id="fees" class="form-control select2">
                                    @foreach($fees as $fee)
                                        <option value="{{$fee->fees_id}}">{{$fee->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Grade Type</label>
                                <select class="form-control input-sm" id="gradeType" name="gradeType" onchange="changeGradeLevel()" id="gradeType" required>
                                    <option></option>
                                    @foreach($gradeType as $keyVal)
                                        <option value="{{$keyVal->grade_type_id}}">{{$keyVal->grade_type}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Grade Level :</label>
                                <select class="form-control input-sm gradelevel"  name="grade_level" data-id="grade_level_id" data-name="grade_level" data-url="/select-binder/get-gradeLevel" id="gradeLevels" required>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Section :</label>
                                <select class="form-control input-sm sectionName edit_section_id" name="section_name"  data-id="section_id" data-name="section_name" data-url="/select-binder/get-sectionName" required>
                                    <option ></option>
                                </select>
                            </div>
                        </form>
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-block" id="generate">Generate List</button>
                        </div>
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary btn-block" target="_blank" id="print-list"><i class="fa fa-print"></i> Print</a>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <table id="feesTable" class="table table-striped table-hover" >
                            <thead>
                            <tr>
                                <th width="20%">Grade Level</th>
                                <th>Name</th>
                                <th>Fees</th>
                                <th>Amount</th>

                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        @stop
        @section('js_filtered')
            @include('admin.jslinks.js_crud')
            @include('admin.jslinks.js_datatables')
            <script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
            <!-- Clock picker -->
            <script src="/assets/admin/pages/scripts/table-advanced.js"></script>
            <script type="text/javascript">
                function changeGradeLevel(){

                    var selValue = $('#gradeType').val();



                    if(selValue == 1){
                        $('#grade_level').prop('disabled',true);
                        $('#section').prop('disabled',true);
                    }
                    else {
                        $('#grade_level').prop('disabled',false);
                        $('#section').prop('disabled',false);
                        $('#gradeLevels').select_binder(selValue);
                    }
                }
                $('.gradelevel').change(function(){
                    var selValue = $(this).val();
                    $('.sectionName').select_binder(selValue);

                });

                $('#generate').click(function(){
                    var gradeType = $('#gradeType').val();
                    var grade_level = $('.gradelevel').val();
                    var section = $('.sectionName').val();
                    var fees = $('#fees').val();
                    if(gradeType == 1){
                        grade_level = 'All';
                        section = 'All';
                        gradeType = 'All'

                    }

                    feesTableFunc(grade_level,gradeType,section,fees);

                });

                function generateBalances(){
                    var data = $('#formData').serialize();
                    window.open('/sms/billing/generate-balances?'+data, 'name');
                }
                function feesTableFunc(grade_level,gradeType,section,fees){

                    var urls = 'grade_type='+gradeType+'&grade_level='+grade_level+'&section='+section+'&fees='+fees;
                    $('#print-list').attr('href','/sms/setup/billing/print-payments-list?'+urls);

                    $('#feesTable').dataTable().fnClearTable();
                    $("#feesTable").dataTable().fnDestroy();

                    var feeTable = $('#feesTable').DataTable({
                        responsive: true,
                        bAutoWidth:false,

                        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                            nRow.setAttribute('data-id',aData.row_id);
                            nRow.setAttribute('class','ref_provider_info_class');
                        },

                        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                            oSettings.jqXHR = $.ajax( {
                                "dataType": 'json',
                                "type": "GET",
                                "url": sSource + urls,
                                "data": aoData,
                                "success": function (data) {
                                    feeTableData = data;
                                    console.log(feeTableData);
                                    fnCallback(feeTableData);
                                }
                            });
                        },

                        "sAjaxSource": "/sms/setup/billing/get-payments-list?",
                        "sAjaxDataProp": "",
                        "iDisplayLength": 10,
                        "scrollCollapse": false,
                        "paging":         true,
                        "searching": true,

                        "columns": [


                            { "mData": "get_grade_level.grade_level", sDefaultContent: ""},
                            { "mRender" : function ( data, type, full ) {
                                return full.get_student.last_name+ ", " + full.get_student.first_name + " " +full.get_student.middle_name;
                            }
                            },
                            { "mData": "title", sDefaultContent: ""},
                            { "mData": "amount", sDefaultContent: ""},


                        ]
                    });

                }
            </script>


@stop