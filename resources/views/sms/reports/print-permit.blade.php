<!DOCTYPE html>
<html>
<head>
    <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/css/custom/rhitsReports.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="http://{{$_SERVER['HTTP_HOST']}}/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

       
       
        .payments{
            position:relative;
            margin-top:20px;
            top:0px;
        }
        body{
            padding:30px;

        }
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
            margin-top:20px;
        }
        th,td{
            border: 1px solid #000000;
            text-align: left;
            padding-left:10px;
            padding-top:2px;
            /*padding-bottom: 2px;*/
            font-size: 15px;
            text-transform: capitalize;

        }
       
        .paddings{
            padding-top:5px;
            padding-bottom: 5px;
            font-size: 14px;
        }
        .statement{
            position:relative;
            float: left;
            width: 50%;
        }
        .payment-sched{
            position: relative;
            float: left;
            width: 45%;
            left:40px;
        }
        .white{
            background-color:white !important;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #F5F5F6;
            border-bottom-width: 1px;

        }
        .borders{
            
          
            border-left: : 1px solid black;
        }
        .border{
            border:1px solid #000000;
        }
        .billing{
            height:620px;
            margin-bottom: 30px;
        }
        .padding{
            padding-left: 10px;
        }
        .holder{
            border: 1px solid #DDDDDD;
            margin-bottom: 20px;
            padding:20px;
        }

        .bordered{
            border: 1px solid #DDDDDD;
        }
        .no-border table,td,th{
            border: 1px solid #ffffff;
        }
        .border-top{
            /*border-top: 1px solid #000000 !important;*/
            border-top: 1px;
            border-top: 1px solid ;
            border-top: medium solid #414446;
        }
        p{
            font-size: 13px;
        }
	td{
	  font-size:11px;
	  padding-top:2px;
	  padding-bottom:2px;
	}
    </style>
    <title>General Report</title>
</head>


<body>

   @foreach($sched->StudentSchedule as $st)
    <div class="col-xs-6" style="margin-bottom:210px; left:-30px; padding-left:15; padding-right:15px;">
        <div class="col-md-6">
    <div class ="header col-md-12">
        <div class="col-md-12">
            <h5 class="t-center bold">CFC SCHOOL OF THE MORNING STAR</h5>
            <h6 class="t-center bold"> Villa Kananga Road, BUTUAN CITY</h6>
            <h4 class="t-center">EXAMINATION ADMISSION PERMIT</h4>
        </div>
    </div>
 
      <div class="col-md-12" style="margin-top: 30px;">
          <p>Name: <b>{{strtoupper($st->Students->first_name)}} {{strtoupper($st->Students->last_name)}}</b> </p>
      <p>Level: <b>{{$sched->RfSection->getGradeLevel->grade_level}}</b></p>
      <p>Section: <b>{{$sched->RfSection->section_name}}</b></p>
      <p>Time:<b>{{$sched->start_time}} - {{$sched->end_time}}</b></p>
      </div>
      </div>

      <div class="col-md-6 " style="margin-top: 40px;">
          <h3 class="t-center">Schedule of Examination</h3>

        <table class="border table-bordered">
            <thead>
                <tr>
                    <td class="text-center">Quater</th>
                    <td class="text-center">Date</th>
                    <td class="text-center">Remarks</th>
                    <td class="text-center">Cashier's Signature</th>
                    <td class="text-center">Teacher's Signature</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Diagnostic</td>
                    <td>06/20-21</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1st Midterm</td>
                    <td>07/18-20</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>1st Final</td>
                    <td>08/16-19</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>2nd Midterm</td>
                    <td>09/19-21</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>2nd Final</td>
                    <td>10/18-20</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>3rd Midterm</td>
                    <td>11/20-22</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>3rd Final</td>
                    <td>12/13-15</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>4th Midterm</td>
                    <td>01/22-24</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>4th Final</td>
                    <td>02/19-21</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Diagnostic</td>
                    <td>03/10</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    
    </div>
    </div>

   @endforeach

</body>

</html>

