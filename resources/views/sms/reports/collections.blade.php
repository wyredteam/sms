@extends('sms.main.index')

@section('css_filtered')
    @include('admin.csslinks.css_crud')
    <link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
@stop

@section('content')



    <div class="col-md-12">
        <div class="portlet box wyred">
            <div class="portlet-title">
                <div class="caption">

                    <i class="fa fa-desktop text-white"></i> Generate Collections

                </div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom:20px;">
                        <div class="col-xs-12">
                            <div class="col-md-5">
                                <div class="form-goup">
                                    <label for="">Filter Date:</label>
                                    <p> From :<input type="text" class="date form-control" id="date_from" name="from_date"> To : <input type="text" id="date_to" class="form-control date" name="to_date"></p>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#loading" id="generate">Generate</button>
                                </div>
                            </div>
                        </div>

                        <div class="tabbable-line">
                            <h3>Collections/Expenses</h3>
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#tab_15_1" data-toggle="tab">
                                        <i class="fa fa-desktop"></i> Income </a>
                                </li>
                                <li>
                                    <a href="#studentDiscount" data-toggle="tab">
                                        <i class="fa fa-dollar"></i> Expenses </a>
                                </li>
                                <li>
                                    <a href="#tab_15_2" data-toggle="tab">
                                        <i class="fa fa-list-alt"></i> Other Income </a>
                                </li>
                                <li>
                                    <a href="#tab_15_3" data-toggle="tab">
                                        <i class="fa fa-copy"></i>  General</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_15_1">

                                    <!-- new tab -->
                                    <div class="col-md-12">
                                        <div class="well">
                                            <div class="col-md-12" style="padding-top:10px">
                                                <a href="#" class="btn blue-madison btn-sm pull-right" id="print-income" target="_blank"> Print<i class="fa fa-print"></i></a>
                                            </div>
                                            <div style="background:#FFFFFF;padding:10px">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="assessment">
                                                        <table id="income_record" class="table table-striped" >
                                                            <thead>
                                                            <tr>
                                                                <th class="text-center">Date</th>
                                                                <th class="text-center">Name</th>
                                                                <th class="text-center">Fee</th>
                                                                <th class="text-center">OR No</th>
                                                                <th class="text-center">Type</th>
                                                                <th class="text-center">Amount</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($payments as $pay)
                                                                    <tr class="income">
                                                                        <td>{{$pay->date_of_payment}}</td>
                                                                        <td>{{ucfirst($pay->getAccount->getStudent->last_name)}}, {{ucfirst($pay->getAccount->getStudent->first_name)}}</td>
                                                                        <td class="text-center">{{$pay->getFees->title}}</td>
                                                                        <td class="text-center">{{$pay->or_no}}</td>
                                                                        <td class="text-center">{{$pay->getPaymentOption->payment_option}}</td>
                                                                        <td class="text-right">P{{number_format($pay->amount,2,'.',',')}}</td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end new tab -->


                                </div>
                                <div class="tab-pane" id="studentDiscount">
                                    <div class="col-md-12">
                                        <div class="well">
                                            <div class="col-md-12" style="padding-top:10px">
                                                <a href="#" class="btn blue-madison btn-sm pull-right" id="print-expenses" target="_blank"> Print<i class="fa fa-print"></i></a>
                                            </div>
                                            <div style="background:#FFFFFF;padding:20px">
                                                <table id="expenses_record" class="table table-striped " >
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">Date</th>
                                                        <th class="text-center">Name</th>
                                                        <th class="text-center">Particulars</th>
                                                        <th class="text-center">Voucher Type</th>
                                                        <th class="text-center">Check Voucher #</th>
                                                        <th class="text-center">Amount</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($expenses as $expense)
                                                        <tr class="expenses">
                                                            <td>{{$expense->date}}</td>
                                                            <td>{{$expense->name}}</td>
                                                            <td class="text-center">{{$expense->getFees->title}}</td>
                                                            <td class="text-center">{{$expense->getPaymentOption->payment_option}}</td>
                                                            <td class="text-center">{{$expense->check_no}}</td>
                                                            <td class="text-right">P{{number_format($expense->amount,2,'.',',')}}</td>
                                                        </tr>
                                                     @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab_15_2">
                                    <div class="well">
                                        <div class="col-md-12" style="padding-top:10px">
                                            <a href="#" class="btn blue-madison btn-sm pull-right" id="print-others" target="_blank"> Print<i class="fa fa-print"></i></a>
                                        </div>
                                        <div style="background:#FFFFFF;padding:20px">
                                            <table id="other_record" class="table table-striped" >
                                                <thead>
                                                <tr>
                                                    <th class="text-center">Name</th>
                                                    <th class="text-center">Fee</th>
                                                    <th class="text-center">OR No</th>
                                                    <th class="text-center">Type</th>
                                                    <th class="text-center">Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($other as $pay)
                                                    <tr class="other">
                                                        <td class="text-center">{{ucfirst($pay->payers_name)}}</td>
                                                        <td class="text-center">{{$pay->getFees->title}}</td>
                                                        <td class="text-center">{{$pay->or_no}}</td>
                                                        <td class="text-center">{{$pay->getPaymentOption->payment_option}}</td>
                                                        <td class="text-right">P{{number_format($pay->amount,2,'.',',')}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>

                                </div>
                                <div class="tab-pane" id="tab_15_3">
                                    <div class="well">
                                        <div class="col-md-12" style="padding-top:10px">
                                            <a href="#" class="btn blue-madison btn-sm pull-right" id="print-general" target="_blank"> Print<i class="fa fa-print"></i></a>
                                            <a href="#" class="btn blue-madison btn-sm pull-right" id="print-general-by-month" target="_blank"> Print by Month <i class="fa fa-print"></i></a>
                                        </div>
                                        <div style="background:#FFFFFF;padding:20px;" >
                                            <h3>INCOME</h3>
                                            <table id="general_income" class="table table-striped" >
                                                <thead>
                                                <tr>
                                                    <th>Account</th>
                                                    <th class="text-right">Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                        <div style="background:#FFFFFF;padding:20px;margin-top:20px">
                                            <h3>EXPENSES</h3>
                                            <table id="general_expenses" class="table table-striped" >
                                                <thead>
                                                <tr>
                                                    <th>Account</th>
                                                    <th class="text-right">Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="loading" class="modal fade security-mo-z" tabindex="-1" style="z-index:99999" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Processing</h4>
                    </div>
                    <div class="modal-body modalFadeBody" >
                        <div class="progress loading">
                            <div class="progress-bar progress-bar-striped active" role="progressbar"
                                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                                <label>Processing your request. Please wait.</label>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                  </div>
                </div>
            </div>
        </div>

        @stop
        @section('js_filtered')
            @include('admin.jslinks.js_crud')
            <script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
            <!-- Clock picker -->
            <script src="/assets/admin/pages/scripts/table-advanced.js"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    Number.prototype.format = function(n, x) {
                        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                    };
                    $('.date').datepicker({
                        dateFormat: "yyyy/mm/dd",
                        startView: 2,
                        todayBtn: "linked",
                        keyboardNavigation: false,
                        forceParse: false,
                        autoclose: true
                    });
                });
                function changeGradeLevel(){

                    var selValue = $('#gradeType').val();
                    $('#gradeLevels').select_binder(selValue);
                }
                $('.gradelevel').change(function(){
                    var selValue = $(this).val();
                    $('.sectionName').select_binder(selValue);

                });

                $('#generate').click(function(){

                    var date_from = $('#date_from').val();
                    var date_to   = $('#date_to').val();
                    $('#print-income').attr('href','/sms/reports/print-income?from='+date_from+'&to='+date_to);
                    $('#print-expenses').attr('href','/sms/reports/print-expenses?from='+date_from+'&to='+date_to);
                    $('#print-others').attr('href','/sms/reports/print-others?from='+date_from+'&to='+date_to);
                    $('#print-general').attr('href','/sms/reports/print-general?from='+date_from+'&to='+date_to);
                    $('#print-general-by-month').attr('href','/sms/reports/print-general-by-month?from='+date_from+'&to='+date_to);
                    $.get('/sms/billing/get-collection?from='+date_from+'&to='+date_to ,function(data){
                        $('.income').remove();
                        $('.other').remove();
                        $('.expenses').remove();
                        $(data[0]).each(function(){
                            $('#income_record > tbody:last-child').append("<tr class='income'><td class='text-center'>"+this.date_of_payment+"</td><td>"+this.get_account.get_student.last_name+", "+this.get_account.get_student.first_name+"</td><td class='text-center'>"+this.get_fees.title+"</td><td class='text-center'>"+this.or_no+"</td><td class='text-center'>"+this.get_payment_option.payment_option+" "+this.get_bank.bank+" "+this.check_no+"</td><td class='text-right'>P"+this.amount+"</td></tr>");
                        });
                        $(data[2]).each(function(){
                            $('#other_record > tbody:last-child').append("<tr class='other'><td class='text-center'>"+this.date_of_payment+"</td><td>"+this.payers_name+"</td><td class='text-center'>"+this.get_fees.title+"</td><td class='text-center'>"+this.or_no+"</td><td class='text-center'>"+this.get_payment_option.payment_option+"</td><td class='text-right'>P"+this.amount+"</td></tr>");
                        });
                        $(data[1]).each(function(){
                            $('#expenses_record > tbody:last-child').append("<tr class='expenses'><td class='text-center'>"+this.date+"</td><td>"+this.name+"</td><td class='text-center'>"+this.get_fees.title+"</td><td class='text-center'>"+this.get_payment_option.payment_option+"</td><td class='text-center'>"+this.check_no+"</td><td class='text-right'>P"+this.amount+"</td></tr>");
                        });
                        $(data[3]).each(function(){
                            var amount = Number(this.amount);
                            if(amount != ''){
                                $('#general_income > tbody:last-child').append("<tr class='income'><td>"+this.account_desc+"</td><td class='text-right'>P"+this.amount+"</td></tr>");
                            }
                        });

                        $(data[4]).each(function(){
                            var amount = Number(this.amount);
                            if(amount != ''){
                                $('#general_expenses > tbody:last-child').append("<tr class='income'><td>"+this.account_desc+"</td><td class='text-right'>P"+this.amount+"</td></tr>");
                            }
                        });
                        $('#general_expenses > tbody:last-child').append("<tr class='income'><td class='text-center'><b>TOTAL</b></td><td class='text-right'>P"+data[5]+"</td></tr>");
                        $('#general_income > tbody:last-child').append("<tr class='income'><td class='text-center'>TOTAL</td><td class='text-right'>P"+data[6]+"</td></tr>");
                        $('#loading').modal('hide');
                    });

                });
                function generateBalances(){
                    var data = $('#formData').serialize();
                    window.open('/sms/billing/generate-balances?'+data, 'name'); 
                }
            </script>


@stop
