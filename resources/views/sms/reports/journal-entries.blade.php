@extends('sms.main.index')

@section('css_filtered')
    @include('admin.csslinks.css_crud')
    <link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
@stop

@section('content')



    <div class="col-md-12">
        <div class="portlet box wyred">
            <div class="portlet-title">
                <div class="caption">

                    <i class="fa fa-desktop text-white"></i> Journal Entries

                </div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-5" style="margin-bottom:20px;">
                        <div class="form-group date">
                            <label for="">Select Date for Filter:</label>
                            <div class="input-group date">
                        <span class="input-group-addon input-sm">
                         <i class="fa fa-calendar"></i>
                        </span>
                                <input type="text" class="form-control input-sm" name="date" id="dateFilter"  required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" id="generate-report">Generate</button>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <button class="btn btn-primary pull-right" id="edit">Edit</button>
                        <form id="editJournal">
                        <table id="journalTable" class="table table-striped table-hover" >
                            <thead>
                            <tr>
                                <th>Reference</th>
                                <th>Account Code</th>
                                <th>Description</th>
                                <th>Debit</th>
                                <th>Credit</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        </form>
                        <button class="btn blue-madison pull-right wyredModalCallback save-edit" data-toggle="modal" data-url="/sms/setup/billing/edit-journal" data-form="editJournal" data-target="#wyredSaveModal" disabled> Save <i class="fa fa-cube"></i></button>
                    </div>
                </div>
            </div>
        </div>


        @stop
        @section('js_filtered')
            @include('admin.jslinks.js_crud')
            @include('admin.jslinks.js_datatables')
            <script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
            <!-- Clock picker -->
            <script src="/assets/admin/pages/scripts/table-advanced.js"></script>
            <script type="text/javascript">

                $(document).ready(function(){
                    $('.date .input-group.date').datepicker({
                        startView: "months",
                        minViewMode:"months",
                        format: "mm-yyyy",
                        todayBtn: "linked",
                        keyboardNavigation: false,
                        forceParse: false,
                        autoclose: true
                    });

                });
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };
                $('#generate-report').click(function(){
                    $('.trjournal').remove();
                    $('.save-edit').attr('disabled',true);
                    var $date = $('#dateFilter').val();
                    $.get('/sms/setup/billing/get-journal-list?date='+$date,function(data){
                        var $totaldebit = 0;
                        var $totalcredit = 0;
                        $(data).each(function(){
                            $totaldebit = Number($totaldebit) + Number(this.debit);
                            $totalcredit = Number($totalcredit) + Number(this.credit);
                            $('#journalTable > tbody:last-child').append('<tr class="trjournal"><td ><input type="hidden" name="id[]" value="'+this.journal_id+'" ><input type="hidden" class="form-control hiddenclass" name="reference[]" value="'+this.reference_no+'"> ' +
                                    '<span class="tabledata">'+this.reference_no+'</span></td><td><input type="hidden" class="form-control hiddenclass" name="account_code[]" value="'+this.account_code+'"><span class="tabledata">'+this.account_code+'</span></td><td><input type="hidden" class="form-control hiddenclass" name="description[]" value="'+this.description+'"><span class="tabledata">'+this.description+'</span></td><td><input type="hidden" class="form-control hiddenclass debs mask-money" onkeyup="addThis()" name="debit[]" value="'+this.debit+'"><span class="tabledata"> '+this.debit+' </span></td><td><input type="hidden" class="form-control hiddenclass creds mask-money" onkeyup="addThis()" name="credit[]" value="'+this.credit+'"><span class="tabledata">'+this.credit+'</span></td></tr>');
                        });
                        $('.mask-money').maskMoney();
                        Number.prototype.format = function(n, x) {
                            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                            return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                        };
                        $('#journalTable > tbody:last-child').append('<tr class="trjournal"><td colspan="3" class="text-center">TOTAL</td><td class="debits">'+($totaldebit).format(2,3)+'</td><td class="credits">'+($totalcredit).format(2,3)+'</td></tr>');
                    });
                });
                function addThis(){
                    var totalcred = 0;
                    var totaldeb = 0;

                    $('.creds').each(function(){
                        var cred = $(this).val();

                        totalcred = Number(totalcred) + Number(cred);
                    });
                    $('.debs').each(function(){
                        var deb = $(this).val();
                        totaldeb = Number(totaldeb) + Number(deb);

                    });

                    $('.debits').html((totaldeb).format(2,3));
                    $('.credits').html((totalcred).format(2,3));

                }


                $('#edit').click(function(){
                    $('.save-edit').attr('disabled',false);
                    $('.hiddenclass').attr('type','text');
                    $('.tabledata').remove();
                });



//                function feesTableFunc(date){
//
//
//
//                    $('#journalTable').dataTable().fnClearTable();
//                    $("#journalTable").dataTable().fnDestroy();
//
//                    var feeTable = $('#journalTable').DataTable({
//                        responsive: true,
//                        bAutoWidth:false,
//
//                        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
//                            nRow.setAttribute('data-id',aData.row_id);
//                            nRow.setAttribute('class','ref_provider_info_class');
//                        },
//
//                        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
//                            oSettings.jqXHR = $.ajax( {
//                                "dataType": 'json',
//                                "type": "GET",
//                                "url": sSource + date,
//                                "data": aoData,
//                                "success": function (data) {
//                                    feeTableData = data;
//                                    console.log(feeTableData);
//                                    fnCallback(feeTableData);
//                                }
//                            });
//                        },
//
//                        "sAjaxSource": "/sms/setup/billing/get-journal-list?date=",
//                        "sAjaxDataProp": "",
//                        "iDisplayLength": 10,
//                        "scrollCollapse": false,
//                        "paging":         true,
//                        "searching": true,
//
//                        "columns": [
//
//
//                            { "mData": "get_grade_level.grade_level", sDefaultContent: ""},
//                            { "mData": "title", sDefaultContent: ""},
//                            { "mData": "amount", sDefaultContent: ""},
//
//
//                        ]
//                    });
//
//                }
            </script>


@stop