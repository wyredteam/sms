<!DOCTYPE html>
<html>
	<head>
		<link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('/assets/css/custom/rhitsReports.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
		<link href="http://{{$_SERVER['HTTP_HOST']}}/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
		 <style type="text/css">

        .table > thead > tr > th {
          border-bottom: 1px solid #DDDDDD;
          vertical-align: bottom;

         }
         body{
          padding-top:20px;
         }
         table{
          width: 100%;
          border:1px solid #000000;
          border-collapse: collapse;
         }
         th,td{
          border: 1px solid #000000;
         }
         .white{
          background-color:white !important; 
         }

         .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
          background-color: #F5F5F6;
          border-bottom-width: 1px;

        }
        .padding{
          padding-left: 10px;
        }
        .holder{
          border: 1px solid #DDDDDD;
          margin-bottom: 20px;
          padding:20px;
        }

        .bordered{
          border: 1px solid #DDDDDD;
        }

    </style>
		<title>Schedule Report</title>
	</head>


<body>

	<div class="col-md-12" >
			  	<div class="body-legal col-100" >
			    	<div class ="header col-md-12">
			      		<div class="col-md-12">
				          <h4 class="t-center bold">SCHOOL OF THE MORNING STAR</h4>
				          <h5 class="t-center bold">BUTUAN CITY</h5>
				          <p class="t-center"></p>
				          <p class="t-center"><i></i></p>
				          <h4 class="text-center">SCHEDULE REPORT</h4>
				      	</div>
			    	</div>

            <div class="row" style="margin-top: 20px;margin-bottom: 20px;">
              
              <div class="col-md-12">
                  <h5 class="text-center">School Year: {{$schedule->RfSchoolYear->school_year}}</h5>
              </div>

              <div class="col-xs-6">
                
                  <h5>Section : {{$schedule->RfSection->section_name}}</h5>
                  <h5>Adviser : {{$schedule->getAdviser->last_name}}, {{$schedule->getAdviser->first_name}} {{$schedule->getAdviser->middle_name}}</h5>
              </div>

              <div class="col-xs-6 ">

                  <h5 >Time : {{$schedule->start_time}} - {{$schedule->end_time}}</h5>
               
                  <h5 >Slot: {{$schedule->slot}}</h5>

              </div>
            </div>


            @foreach($handlesubjects as $week)
            <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                        <label class="text-center">{{$week->weekdays}}</label>
                    </div>
                    <table>

                    
                    <tr class="bg-primary">
                      <th class="text-center" width="30"></th>
                      <th class="text-center" width="180">TIME</th>
                      <th class="text-center">SUBJECT</th>
                      <th class="text-center">TEACHER</th>
                    </tr>
                    
                    @foreach($week->HandleSubjects as $subjects )
                    <?php $count = 1; ?>
                    <tr>
                        <td>{{$count}}.</td>
                        <td>{{$subjects->start_time}} - {{$subjects->end_time}}</td>
                        <td>{{$subjects->DtAssignSubject->getSubjects->subject_name}}</td>
                        <td>{{$subjects->getEmployee->last_name}},
                        {{$subjects->getEmployee->first_name}}
                        {{$subjects->getEmployee->middle_name}}

                        </td>
                    </tr>
                    <?php $count++; ?>
                    @endforeach

                    </table>

                  </div>
            </div>
				</div>
        @endforeach             

			
	</div>
</body>

</html>

