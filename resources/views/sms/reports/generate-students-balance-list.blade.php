<!DOCTYPE html>
<html>
<head>
    <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/css/custom/rhitsReports.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="http://{{$_SERVER['HTTP_HOST']}}/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: bottom;

        }
        .payments{
            position:relative;
            margin-top:40px;
            top:0px;
        }
        h4{
            line-height:10px;
        }
        h5{
            line-height:10px;
        }
        body{
            padding-top:20px;
        }
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
            margin-top:20px;
        }
        th,td{
            border: 1px solid #000000;
            text-align: left;
            padding-left:10px;

            font-size: 13px;
            padding-right: 10px;
        }
        .statement{
            position:relative;
            float: left;
            width: 50%;
        }
        .payment-sched{
            position: relative;
            float: left;
            width: 45%;
            left:40px;
        }
        .white{
            background-color:white !important;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #F5F5F6;
            border-bottom-width: 1px;

        }
        .border{
            border:1px solid #FFFFFF;
        }
        .billing{

            height:465px;
            margin-bottom: 190px;

        }
        .padding{
            padding-left: 10px;
        }
        .holder{
            border: 1px solid #DDDDDD;
            margin-bottom: 20px;
            padding:20px;
        }

        .bordered{
            border: 1px solid #DDDDDD;
        }
        .no-border table,td,th{
            border: 1px solid #000000;
		padding-top:4px;
		padding-bottom:4px;
        }
        .border-top{
            /*border-top: 1px solid #000000 !important;*/
            border-top: 1px;
            border-top: 1px solid ;
            border-top: medium solid #414446;
        }
        .break{
            page-break-after: always;
        }
	.yellow{
	   background-color:yellow;
	}


    </style>

    <style type="text/css" media="print">
        p.break
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
    </style>
    <title>Student Assessment</title>
</head>


<body>


<div class="page-num">Page 1</div>
<div class="body-letter col-100"  >

    <h4 class="text-center" style="color:red;">CFC - School of the Morning Star</h4>
    <h5 class="text-center">Villa Kananga Road,Butuan City</h5>

                 <h4 class="text-center">List of Students with Balance</h4>

    <h3>Section: {{$secs->section_name}}</h3>
    <table class="no-border">
		<tr>
			<th>Names</th>
			@foreach($feez as $fee)
			@if($fee->title != 'SUMMER' && $fee->title != 'JOURNALISM TRAINING' && $fee->title != 'Old Account' && $fee->title != 'Books - Nursery and Kinder')
			<th>{{$fee->title}}</td>
			@endif
			@endforeach

			<th>Balance</th>
		</tr>

    @foreach($billing as $bill)
            <?php $bal_due = 0; $bal=0; ?>

            @foreach($bill->getStudentBill as $student_bill)
                @if($student_bill->getBilling->getFees->getCategory->title == 'Assessment')
                        <?php $bal = $bal+$student_bill->bal; ?>
                @endif
            @endforeach


                @if($bal > 0)

                    <tr class=" @if($bill->promise > 0) yellow @endif" >
                       
                        <th>{{ucfirst($bill->getStudent->first_name)}} {{ucfirst($bill->getStudent->last_name)}}</th>
			
	@foreach($feez as $fee)
			@if($fee->title != 'SUMMER' && $fee->title != 'JOURNALISM TRAINING' && $fee->title != 'Old Account' && $fee->title != 'Books - Nursery and Kinder')	
			<?php $count =0; ?>
			@foreach($bill->getStudentBill as $student_bill)
              		  	@if($student_bill->getBilling->getFees->fees_id == $fee->fees_id)
                      			 <th> {{number_format($student_bill->bal,2,'.',',')}} </th>
					<?php $count = 1; ?>
            			    @endif
          		@endforeach
			
			@if($count == 0)
				<th>-</th>
			@endif
					
			@endif
			@endforeach

			   <th>{{number_format($bal,2,'.',',')}}</th>                 
</tr>


                @endif

@endforeach

                </table>
</body>

</html>

