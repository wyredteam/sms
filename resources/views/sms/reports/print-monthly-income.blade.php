<!DOCTYPE html>
<html>
<head>
    <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/css/custom/rhitsReports.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="http://{{$_SERVER['HTTP_HOST']}}/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: bottom;

        }
        table{
            width:100%;
        }
        .payments{
            position:relative;
            margin-top:20px;
            top:0px;
        }
        body{
            padding:30px;

        }
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
            margin-top:20px;
        }
        th,td{
            border: 1px solid #000000;
            text-align: left;
            padding-left:10px;
            padding-top:2px;
            /*padding-bottom: 2px;*/
            font-size: 12px;
            text-transform: capitalize;

        }
        .paddings{
            padding-top:5px;
            padding-bottom: 5px;
            font-size: 14px;
        }
        .statement{
            position:relative;
            float: left;
            width: 50%;
        }
        .payment-sched{
            position: relative;
            float: left;
            width: 45%;
            left:40px;
        }
        .white{
            background-color:white !important;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #F5F5F6;
            border-bottom-width: 1px;

        }
        .border{
            border:1px solid #000000;
        }
        .billing{
            height:620px;
            margin-bottom: 30px;
        }
        .padding{
            padding-left: 10px;
        }
        .holder{
            border: 1px solid #DDDDDD;
            margin-bottom: 20px;
            padding:20px;
        }

        .bordered{
            border: 1px solid #DDDDDD;
        }
        .no-border table,td,th{
            border: 1px solid #ffffff;
        }
        .border-top{
            /*border-top: 1px solid #000000 !important;*/
            border-top: 1px;
            border-top: 1px solid ;
            border-top: medium solid #414446;
        }


    </style>
    <title>General Report</title>
</head>


<body>


<div class="page-num">Page 1</div>
<div class="body-letter col-100" >
    <div class ="header col-md-12">
        <div class="col-md-12">
            <h4 class="t-center bold">SCHOOL OF THE MORNING STAR</h4>
            <h5 class="t-center bold">BUTUAN CITY</h5>
            <h4 class="text-center">Income Statement from {{date('F Y', strtotime($from))}} to {{date('F Y', strtotime($to))}}</h4>
        </div>
    </div>
    <div class="col-md-12" style="margin-bottom: 10px;">
        <div style="background:#FFFFFF;padding:20px;" >
            <div class="col-xs-12">
            </div>
            <h4>INCOME</h4>
            <table id="general_income" class="table table-bordered" >
                <thead>

                <tr>
                    <th width="25%">Account</th>
                    <?php $counter = 0; ?>
                    @foreach($all_date as $dates)
                        <?php $acc_total[$counter]=0;$acc_exp_total[$counter]=0; $counter++; ?>
                        <th class="text-center">{{date("F-Y",strtotime($dates))}}</th>
                    @endforeach
                    <th class="text-center">TOTAL</th>
                </tr>
                </thead>
                <tbody>

                    @foreach($acc_income as $income)
                        @if($income->amount > 0)
                        <tr>
                            <td>{{$income->account_desc}}</td>
                            <?php $date_total = 0; $counter = 0;?>
                            @foreach($income_date[$income->account_code] as $date_amount)
                                <?php $date_total = $date_total + $date_amount;
                                        $acc_total[$counter] += $date_amount;
                                    $counter++;
                                ?>
                             <td class="text-right">P{{number_format($date_amount,2,'.',',')}}</td>
                            @endforeach
                            <td class="text-right">P{{number_format($date_total,2,'.',',')}}</td>
                        </tr>
                        @endif
                    @endforeach
                <tr class="bg-info">

                        <td></td>
                        <?php $x=0; ?>
                        @foreach($acc_total as $income_total)
                            <?php $x = $x + $income_total; ?>
                            <td class="text-right">P{{number_format($income_total,2,'.',',')}}</td>
                        @endforeach
                        <td class="text-right"><b>P{{number_format($x,2,'.',',')}}</b></td>

                </tr>
                </tbody>
            </table>
        </div>
        <div style="background:#FFFFFF;padding:20px;margin-top:0px">
            <h4>EXPENSES</h4>
            <table id="general_expenses" class="table table-bordered" >
                <thead>
                <tr>
                    <th width="25%">Account</th>
                    @foreach($all_date as $dates)
                        <th class="text-center">{{date("F-Y",strtotime($dates))}}</th>
                    @endforeach
                    <th class="text-center">TOTAL</th>
                </tr>
                </thead>
                <tbody>
                @foreach($acc_expenses as $expenses)
                    @if($expenses->amount > 0)
                        <tr>
                            <td>{{$expenses->account_desc}}</td>
                            <?php $date_total = 0; $counter=0; ?>
                            @foreach($expenses_date[$expenses->account_code] as $date_amount)
                                <?php $acc_exp_total[$counter] += $date_amount; ?>
                                <?php $date_total = $date_total + $date_amount; $counter++; ?>
                                <td class="text-right">P{{number_format($date_amount,2,'.',',')}}</td>
                            @endforeach
                            <td class="text-right">P{{number_format($date_total,2,'.',',')}}</td>
                        </tr>
                    @endif
                @endforeach
                <tr class="bg-info">
                <td></td>
                <?php $x=0; ?>
                @foreach($acc_exp_total as $expe_total)
                    <?php $x = $x + $expe_total; ?>
                    <td class="text-right">P{{number_format($expe_total,2,'.',',')}}</td>
                @endforeach
                <td class="text-right"><b>P{{number_format($x,2,'.',',')}}</b></td>
                </tr>
                </tbody>
            </table>
            <table class="table table-bordered">
                <tr class="bg-warning">
                    <?php $t = 0; $counter=0; ?>
                    <th width="25%">GROSS NET</th>
                    @foreach($acc_exp_total as $net)
                        <?php $am = 0; $am = $acc_total[$counter] - $net; $t += $am; $counter++; ?>
                        <td class="text-right">P{{number_format($am,2,'.',',')}}</td>
                    @endforeach
                    <td class="text-right"><b>P{{number_format($t,2,'.',',')}}</b></td>
                </tr>
            </table>
        </div>
    </div>

</body>

</html>

