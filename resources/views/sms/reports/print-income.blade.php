<!DOCTYPE html>
<html>
<head>
    <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/css/custom/rhitsReports.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="http://{{$_SERVER['HTTP_HOST']}}/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: bottom;

        }
        table{
            width: 80%;
        }
        .payments{
            position:relative;
            margin-top:20px;
            top:0px;
        }
        body{
            padding-top:10px;
        }
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
            margin-top:20px;
        }
        th,td{
            border: 1px solid #000000;
            text-align: left;
            padding-left:10px;
            padding-top:2px;
            /*padding-bottom: 2px;*/
            font-size: 15px;
            text-transform: capitalize;

        }
        .paddings{
            padding-top:5px;
            padding-bottom: 5px;
            font-size: 14px;
        }
        .statement{
            position:relative;
            float: left;
            width: 50%;
        }
        .payment-sched{
            position: relative;
            float: left;
            width: 45%;
            left:40px;
        }
        .white{
            background-color:white !important;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #F5F5F6;
            border-bottom-width: 1px;

        }
        .border{
            border:1px solid #000000;
        }
        .billing{
            height:620px;
            margin-bottom: 30px;
        }
        .padding{
            padding-left: 10px;
        }
        .holder{
            border: 1px solid #DDDDDD;
            margin-bottom: 20px;
            padding:20px;
        }

        .bordered{
            border: 1px solid #DDDDDD;
        }
        .no-border table,td,th{
            border: 1px solid #ffffff;
        }
        .border-top{
            /*border-top: 1px solid #000000 !important;*/
            border-top: 1px;
            border-top: 1px solid ;
            border-top: medium solid #414446;
        }


    </style>
    <title>Income Report</title>
</head>


<body>


<div class="page-num">Page 1</div>
<div class="body-letter col-100" >
    <div class ="header col-md-12">
        <div class="col-md-12">
            <h4 class="t-center bold">SCHOOL OF THE MORNING STAR</h4>
            <h5 class="t-center bold">BUTUAN CITY</h5>
            <h4 class="text-center">Income Report {{date('F d, Y', strtotime($from))}} to {{date('F d, Y', strtotime($to))}}</h4>
        </div>
    </div>
    <div class="col-xs-12">
    </div>
    <div class="col-md-12" style="margin-bottom: 10px;">
        <table id="income_record" class="table table-bordered" >
            <thead>
            <tr>
                <th class="text-center">Date</th>
                <th class="text-center">Name</th>
                <th class="text-center">Fee</th>
                <th class="text-center">OR No</th>
                <th class="text-center">Type</th>
                <th class="text-center">Amount</th>
            </tr>
            </thead>
            <tbody>
	<?php $total = 0; ?>
            @foreach($payments as $pay)
                <tr class="income">
                    <td>{{$pay->date_of_payment}}</td>
                    <td>{{ucfirst($pay->getAccount->getStudent->last_name)}}, {{ucfirst($pay->getAccount->getStudent->first_name)}}</td>
                    <td class="text-center">{{$pay->getFees->title}}</td>
                    <td class="text-center">{{$pay->or_no}}</td>
                    <td class="text-center">{{$pay->getPaymentOption->payment_option}} {{$pay->getBank->bank}} {{$pay->check_no}}</td>
                    <td class="text-right">P{{number_format($pay->amount,2,'.',',')}}</td>
                </tr>
		<?php $total = $total + $pay->amount; ?>
            @endforeach
		<tr>
		  <td colspan="5" class="text-center">TOTAL</td>
		  <td class="text-right"> <b> P{{number_format($total,2,'.',',')}} </b></td>
		</tr>
            </tbody>
        </table>
    </div>

</body>

</html>

