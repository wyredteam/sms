@extends('sms.main.index')

@section('css_filtered')
    @include('admin.csslinks.css_crud')
    <link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
@stop

@section('content')



    <div class="col-md-12">
        <div class="portlet box wyred">
            <div class="portlet-title">
                <div class="caption">

                    <i class="fa fa-user text-white"></i> Attendance

                </div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body" style="padding:30px;">
                <div class="row">
                    <div class="col-md-12">
                        <table id="gradeFeesTable" class="table table-striped table-hover" >
                            <thead>
                              <tr>
                                  <th>Student ID</th>
                                  <th>Student Name</th>
                                  <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                             
                            <tr>
                                    <td>16-000001</td>
                                    <td>Ralph Degoma</td>
                                    <td>
                                        <input type="checkbox" name="" checked="" >
                                    </td>
                            </tr>
                            <tr>
                                    <td>16-000002</td>
                                    <td>Aldwin Filoteo</td>
                                    <td>
                                        <input type="checkbox" name="" checked="" >
                                    </td>
                            </tr>
                            <tr>
                                    <td>16-000003</td>
                                    <td>Leonardo Empuesto</td>
                                    <td>
                                        <input type="checkbox" name="" checked="" >
                                    </td>
                            </tr>
                      
                            </tbody>
                         </table>
                   </div>

                 <div class="col-md-6 pull-right">
                       <button class="btn red-sunglo"><i class="fa fa-file-excel-o"></i> Download Attendance</button>
                       <button class="btn blue-madison "><i class="fa fa-copy"></i> Submit Attendance</button>
                 </div>
             </div>
            </div>
        </div>


        @stop
        @section('js_filtered')
            @include('admin.jslinks.js_crud')
            <script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
            <!-- Clock picker -->
            <script src="/assets/admin/pages/scripts/table-advanced.js"></script>
            <script type="text/javascript">
                function changeGradeLevel(){

                    var selValue = $('#gradeType').val();
                    $('#gradeLevels').select_binder(selValue);
                }
                $('.gradelevel').change(function(){
                    var selValue = $(this).val();
                    $('.sectionName').select_binder(selValue);

                });

            </script>


@stop