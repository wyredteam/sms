@extends('sms.main.index')

@section('css_filtered')
@include('admin.csslinks.css_crud')
<link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

@stop

@section('content')



<div class="col-md-12">
  <div class="portlet box wyred">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-copy text-white"></i> Welcome to Grading System Module
      </div>
      <div class="tools">
         
      </div>
    </div>
    <div class="portlet-body">
      <div class="row">
          
          <div class="col-xs-12">
                    <div class="col-xs-12"><h3>Student List Filter</h3></div>
                   <div class="col-xs-6">
                       <form id="formFilter">
                        
                        <div class="form-group">
                            <label for="sy">School Year:</label>
                            <select class="form-control input-sm schoolYear" name="schoolYear" id="schoolYear" required="">
                               <option></option>
                               @foreach($sy as $year)
                                    <option value="{{$year->school_year_id}}">{{$year->sy_from}}-{{$year->sy_to}}</option>
                                @endforeach
                            </select>
                        </div>
                       
                    </div>
                    <div class="col-xs-6">
                        
                        <div class="form-group">
                            <label for="sy">Student:</label>
                            <select class="form-control input-sm students_schedule_id" data-id="students_schedule_id" data-name="full_name" data-url="/select-binder/get-students-by-section" name="student_id" id="student_id">
                                <option></option>
                            </select>
                      </div>

                        </form>
                        <div class="form-group col-md-12">
                            <button class="btn blue-madison pull-right" onclick="submitFormFilter()"><i class="fa fa-search"></i> Find</button>
                        </div>
                        <div class="form-group col-md-12" >
                            <button class="btn red-sunglo pull-right" onclick="printCustom()"><i class="fa fa-print"></i> Print Preview</button>
                        </div>
                    </div>


                    
          
        <div class="card-content">            
            
        </div>


     </div>
   
   
  </div>
</div>

@stop
@section('js_filtered')
@include('admin.jslinks.js_crud')
@include('admin.jslinks.js_datatables')
<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Clock picker -->
<script src="/assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="/assets/admin/pages/scripts/table-advanced.js"></script>


<script>

$('.schoolYear').change(function(){
    var selValue = $(this).val();
    $('.students_schedule_id').select_binder(selValue);
});

$('.students_schedule_id').change(function(){

    var selValue = $(this).val();

    $.get("/sms/card/get-card",{students_schedule_id:selValue}, function(result, status){
        $('.card-content').html('');
        $('.card-content').html(result);
        success("Card has been Retrieved!!")
    });
});


</script>

    
@stop