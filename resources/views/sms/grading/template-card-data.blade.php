<table id="grading-table" class="table table-striped table-bordered" >
    <tr>
        <td>Learning Areas</td>
    </tr>


    @foreach($templateCard as $templateCard)

            <tr>
                <td>
                    <input type="hidden" name="subject_id[]" value="{{$templateCard->getSubjects->subject_id}}">
                    {{$templateCard->getSubjects->subject_name}}
                </td>
                
            </tr>

    @endforeach
</table>
<div class="form-group col-md-12">
    <button type="button" class="btn blue-madison pull-right" onclick="saveCardOrder()"><i class="fa fa-save"></i> Save Card Order </button>
</div>
<script>
    $("#grading-table tbody").sortable();  
</script>