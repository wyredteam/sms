@extends('sms.main.index')

@section('css_filtered')
@include('admin.csslinks.css_crud')
<link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

@stop

@section('content')



<div class="col-md-12">
  <div class="portlet box wyred">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-copy text-white"></i> Welcome to Grading System Module
      </div>
      <div class="tools">
         
      </div>
    </div>
    <div class="portlet-body">
      <div class="row">
          
          <div class="col-xs-12">
                    <div class="col-xs-12"><h3>Student List Filter</h3></div>
                   <div class="col-xs-6">
                       <form id="formFilter">
                        
                        <div class="form-group">
                            <label for="sy">School Year:</label>
                            <select class="form-control input-sm" name="schoolYear" id="schoolYear" required="">
                                @foreach($sy as $year)
                                    <option value="All">All</option>
                                    <option value="{{$year->school_year_id}}">{{$year->sy_from}}-{{$year->sy_to}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sy">Student Status:</label>
                            <select class="form-control input-sm" name="student_status" id="student_status">
                                <option value="All">All</option>
                                @foreach($student_status as $student_status)
                                    <option value="{{$student_status->student_status_id}}">{{$student_status->student_status}}</option>
                                @endforeach
                            </select>
                        </div>
                        
                       <div class="form-group">
                           <label for="sy">Grade Type:</label>
                           <select class="form-control input-sm gradeType" required  name="gradeType"  id="gradeType">
                               <option value="All">All</option>
                               @foreach($gradeType as $type)
                                   <option value="{{$type->grade_type_id}}">{{$type->grade_type}}</option>
                                @endforeach
                           </select>
                       </div>
                       <div class="form-group">
                           <label for="sy">Grade Level:</label>
                           <select class="form-control input-sm input-sm gradelevel" required name="grade_level" data-id="grade_level_id" data-name="grade_level" data-url="/select-binder/get-gradeLevel" required>
                               <option >All</option>
                           </select>
                       </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Section:</label>
                            <select class="form-control input-sm input-sm sectionName edit_section_id"  name="sectionName" required  data-id="section_id" data-name="section_name" data-url="/select-binder/get-sectionName" >
                                <option>All</option>
                            </select>
                        </div>

                        </form>
                        <div class="form-group col-md-12">
                            <button class="btn blue-madison pull-right" onclick="submitFormFilter()"><i class="fa fa-search"></i> Find</button>
                        </div>
                        <div class="form-group col-md-12" >
                            <button class="btn red-sunglo pull-right" onclick="printCustom()"><i class="fa fa-print"></i> Print Preview</button>
                        </div>
                    </div>
          

          <table id="non-assessment" class="table table-striped table-bordered" >


     </div>
   
   
  </div>
</div>

@stop
@section('js_filtered')
@include('admin.jslinks.js_crud')
@include('admin.jslinks.js_datatables')
<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Clock picker -->
<script src="/assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="/assets/admin/pages/scripts/table-advanced.js"></script>


<script>



</script>

    
@stop