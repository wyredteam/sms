<table id="grading-table" class="table table-striped table-bordered" >
    <tr>
        <td>Learning Areas</td><td colspan="4">Grading Type</td><td>Final Grade</td><td>Remarks</td>
    </tr>
    {{dd($gradingType)}}
    <tr>
        <td></td>
            @foreach($gradingType as $gradeType)
            <td>{{$gradeType->on_card}}</td>
            @endforeach
        <td></td><td></td>
    </tr>

    @foreach($subjects as $subject)
        @if($subject->isSubject == '1')
            <tr>
                <td>{{$subject->getSubjects->isSubject}}</td><td>81</td><td>82</td><td>83</td><td>84</td><td></td><td></td>
            </tr>
        @endif
    @endforeach
</table>