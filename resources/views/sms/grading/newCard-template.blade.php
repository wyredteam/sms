@extends('sms.main.index')

@section('css_filtered')
@include('admin.csslinks.css_crud')
<link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

@stop

@section('content')



<div class="col-md-12">
  <div class="portlet box wyred">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-copy text-white"></i> Welcome to Grading System Module
      </div>
      <div class="tools">
         
      </div>
    </div>
    <div class="portlet-body">
      <div class="row">
          <form id="formFilter">
          <div class="col-xs-12">
                    <div class="col-xs-12"><h3>Student List Filter</h3></div>
                   <div class="col-xs-6">
                       
                        
                        <div class="form-group">
                            <label for="sy">School Year:</label>
                            <select class="form-control input-sm schoolYear" name="school_year_id" id="school_year_id" required="">
                               <option></option>
                               @foreach($sy as $year)
                                    <option value="{{$year->school_year_id}}">{{$year->sy_from}}-{{$year->sy_to}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="sy">Description:</label>
                            <input type="text" class="form-control input-sm" name="description">
                        </div>
                       
                    </div>
                    <div class="col-xs-6">
                        
                        <div class="form-group">
                            <label for="sy">Grade Level:</label>
                            <select class="form-control input-sm" name="grade_level_id" id="grade_level_id">
                                <option></option>
                               @foreach($RfGradeLevel as $RfGradeLevel)
                                    <option value="{{$RfGradeLevel->grade_level_id}}">{{$RfGradeLevel->grade_level}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="sy">Section Type:</label>
                            <select class="form-control input-sm" name="section_type_id" id="section_type_id">
                                <option></option>
                               @foreach($sectionType as $sectionType)
                                    <option value="{{$sectionType->section_type_id}}">{{$sectionType->section_type}}</option>
                                @endforeach
                            </select>
                        </div>
                        
                        <div class="form-group col-md-12">
                            <button type="button" class="btn blue-madison pull-right" onclick="getCardTemplate()"><i class="fa fa-search"></i> Generate Template Card </button>
                        </div>
                 
                    </div>

            </div>
            </form>

            <div class="col-md-12">
                <div class="template">

                </div>
                
            </div>
   
  </div>
</div>

@stop
@section('js_filtered')
@include('admin.jslinks.js_crud')
@include('admin.jslinks.js_datatables')
<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Clock picker -->
<script src="/assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="/assets/admin/pages/scripts/table-advanced.js"></script>


<script>


function getCardTemplate(){

    $.get("/sms/card/get-template-card",{section_type_id:section_type_id.value,grade_level_id:grade_level_id.value,school_year_id:school_year_id.value}, function(result, status){
        $('.template').html('');
        $('.template').html(result);
        success("Card template has been Retrieved!!")
    });
}

function saveCardOrder(){

    var subject_id = $("input[name='subject_id[]']").map(function(){return $(this).val();}).get();
    console.log(subject_id); 

    $.get("/sms/card/save-template-card",{section_type_id:section_type_id.value,grade_level_id:grade_level_id.value,school_year_id:school_year_id.value,subject_id:subject_id,}, function(result, status){
        success(result[1])
    });
}


</script>

    
@stop