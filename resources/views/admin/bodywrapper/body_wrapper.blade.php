
<?php
    $secure = new WRestrict();
?>


@include('admin.modal-forms.security')
<div class="page-header navbar navbar-fixed-top" >
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo text-center">
            <a href="index.html">
             <img src="/assets/img/athena.png" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE ACTIONS -->
        <!-- DOC: Remove "hide" class to enable the page header actions -->
        
        <!-- END PAGE ACTIONS -->
        <!-- BEGIN PAGE TOP -->
        <h3 class="pull-left" style="color:#FFFFFF;padding-top:3px;">School Management System</h3>
        <div class="page-top">
            <!-- BEGIN HEADER SEARCH BOX -->
          
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="separator hide">
                    </li>
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <!-- <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-bell"></i>
                        <span class="badge badge-success">
                        0 </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3><span class="bold">12 pending</span> notifications</h3>
                                <a href="extra_profile.html">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                    <li>
                                        <a href="javascript:;">
                                        <span class="time">just now</span>
                                        <span class="details">
                                        <span class="label label-sm label-icon label-success">
                                        <i class="fa fa-plus"></i>
                                        </span>
                                        New user registered. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                        <span class="time">3 mins</span>
                                        <span class="details">
                                        <span class="label label-sm label-icon label-danger">
                                        <i class="fa fa-bolt"></i>
                                        </span>
                                        Server #12 overloaded. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                        <span class="time">10 mins</span>
                                        <span class="details">
                                        <span class="label label-sm label-icon label-warning">
                                        <i class="fa fa-bell-o"></i>
                                        </span>
                                        Server #2 not responding. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                        <span class="time">14 hrs</span>
                                        <span class="details">
                                        <span class="label label-sm label-icon label-info">
                                        <i class="fa fa-bullhorn"></i>
                                        </span>
                                        Application error. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                        <span class="time">2 days</span>
                                        <span class="details">
                                        <span class="label label-sm label-icon label-danger">
                                        <i class="fa fa-bolt"></i>
                                        </span>
                                        Database overloaded 68%. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                        <span class="time">3 days</span>
                                        <span class="details">
                                        <span class="label label-sm label-icon label-danger">
                                        <i class="fa fa-bolt"></i>
                                        </span>
                                        A user IP blocked. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                        <span class="time">4 days</span>
                                        <span class="details">
                                        <span class="label label-sm label-icon label-warning">
                                        <i class="fa fa-bell-o"></i>
                                        </span>
                                        Storage Server #4 not responding dfdfdfd. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                        <span class="time">5 days</span>
                                        <span class="details">
                                        <span class="label label-sm label-icon label-info">
                                        <i class="fa fa-bullhorn"></i>
                                        </span>
                                        System Error. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                        <span class="time">9 days</span>
                                        <span class="details">
                                        <span class="label label-sm label-icon label-danger">
                                        <i class="fa fa-bolt"></i>
                                        </span>
                                        Storage server failed. </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li> -->
                    <!-- END NOTIFICATION DROPDOWN -->
                    <li class="separator hide">
                    </li>
                    <!-- BEGIN INBOX DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark" id="header_inbox_bar">
                        <a href="javascript:;" onclick="loadDropDownMessages()" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-envelope-open"></i>
                        <span class="badge badge-danger" id="new-message-count">
                         </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>You have <span class="bold">7 New</span> Messages</h3>
                                <a href="inbox.html">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller conversations" style="height: 275px;" data-handle-color="#637283">
                                    <li>
                                        <a href="inbox.html?a=view">
                                        <span class="photo">
                                        <img src="../../assets/admin/layout3/img/avatar2.jpg" class="img-circle" alt="">
                                        </span>
                                        <span class="subject">
                                        <span class="from">
                                        Lisa Wong </span>
                                        <span class="time">Just Now </span>
                                        </span>
                                        <span class="message">
                                        Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- END INBOX DROPDOWN -->
                    <li class="separator hide">
                    </li>
                    <!-- BEGIN TODO DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    
                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <span class="username username-hide-on-mobile">
                        {{ Auth::user()->username }} </span>
                        <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                        <!-- <img alt="" class="img-circle" src=""/> -->
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="extra_profile.html">
                                <i class="icon-user"></i> My Profile </a>
                            </li>
                            <li>
                                <a href="page_calendar.html">
                                <i class="icon-calendar"></i> My Calendar </a>
                            </li>
                            <li>
                                <a href="inbox.html">
                                <i class="icon-envelope-open"></i> My Inbox <span class="badge badge-danger">
                                3 </span>
                                </a>
                            </li>
                            <li>
                                <a href="page_todo.html">
                                <i class="icon-rocket"></i> My Tasks <span class="badge badge-success">
                                7 </span>
                                </a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="extra_lock.html">
                                <i class="icon-lock"></i> Lock Screen </a>
                            </li>
                            <li>
                                <a href="/admin/security/logout">
                                <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
           
            <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="start ">
                    <a href="/">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    </a>
                </li>
                <li class="active open">
                    <a href="javascript:;">
                    <i class="fa fa-user"></i>
                    <span class="title">Registrar</span>
                    <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        @if($secure->hasAccess('1'))
                        <li>
                            <a href="/sms/registrar/student-registration">
                            <i class="icon-user"></i>
                            Registration</a>
                        </li>
                        @endif

                        @if($secure->hasAccess('2'))
                        <li>
                            <a href="/sms/registrar/registered-list">
                            <i class="fa fa-list-alt"></i>
                            Student List</a>
                        </li>
                        @endif

                        @if($secure->hasAccess('3'))
                        <li>
                            <a href="/sms/registrar/enrollment">
                            <i class="fa fa-group"></i>
                            Enrollment</a>
                        </li>
                        @endif

                        @if($secure->hasAccess('6'))
                        <li>
                            <a href="/sms/registrar/teacher-schedule">
                                <i class="fa fa-clipboard"></i>
                                Teacher's Schedule</a>
                        </li>
                        @endif
                        

                        <li>
                            <a href="/sms/registrar/student-requirements">
                            <i class="fa fa-copy"></i>
                            Student Requirements</a>
                        </li>
                        <li>
                            <a href="#">
                            <i class="fa fa-star-o"></i>
                            Grading
                            <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li id="">
                                    <a href="#">
                                    <i class="fa fa-star"></i>
                                    <span class="title">Grading System</span>
                                    <span class="arrow open"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        
                                        <li id="schoolyearMenu">
                                        <a href="/sms/grading/new-card">
                                            <i class="fa fa-circle"></i>
                                            Create Card Template</a>
                                        </li>

                                        <li id="schoolyearMenu">
                                        <a href="/sms/grading/list">
                                            <i class="fa fa-circle"></i>
                                            Grading Reports</a>
                                        </li>

                                        <li id="schoolyearMenu">
                                        <a href="/sms/grading/setup">
                                            <i class="fa fa-circle"></i>
                                            Grading Setup</a>
                                        </li>
                                       
                                       
                                    </ul>
                                </li>
                            </ul>
                        </li>
                     
                    </ul>
                </li>


<!-- 
                <li id="">
                    <a href="#">
                    <i class="fa fa-graduation-cap"></i>
                    <span class="title">e-Class Room</span>
                    <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                      
                        <li id="schoolyearMenu">
                        <a href="/sms/e-class-room/attendance">
                            <i class="fa fa-circle"></i>
                            Attendance</a>
                        </li>
                   
                 
                      
                        <li id="gradelevelMenu">
                            <a href="">
                            <i class="fa fa-circle"></i>
                            Student Bill </a>
                        </li>
                       
                    </ul>
                </li> -->

                
                 <li id="">
                    <a href="#">
                    <i class="fa fa-desktop"></i>
                    <span class="title">Billing</span>
                    <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        
                        <li id="schoolyearMenu">
                        <a href="/sms/billing/assessment">
                            <i class="fa fa-circle"></i>
                            Assessment</a>

                        </li>
                        
                        <!-- 
                         <li id="gradelevelMenu">

                            <a href="/sms/billing/assign-fees">
                            <i class="fa fa-circle"></i>
                            Assign Fees </a>
                        </li> -->
                        
                        <li id="gradelevelMenu">
                            <a href="/sms/billing/student-bill">
                            <i class="fa fa-circle"></i>
                            Student Bill </a>
                        </li>

                            <li id="gradelevelMenu">
                                <a href="/sms/billing/other-income">
                                    <i class="fa fa-circle"></i>
                                    Other Income </a>
                            </li>
                            <li id="gradelevelMenu">
                                <a href="/sms/billing/expenses">
                                    <i class="fa fa-circle"></i>
                                    Expenses </a>
                            </li>
                              <li id="gradelevelMenu">
                                <a href="/sms/billing/permit">
                                    <i class="fa fa-circle"></i>
                                    Student Permit </a>
                            </li>
                        <li id="gradelevelMenu">
                            <a href="/sms/billing/generate-balances-list">
                                <i class="fa fa-circle"></i>
                                Student w/ Balance List </a>
                        </li>
                    </ul>
                </li>

                    

                <li id="setupMenu">
                    <a href="#">
                    <i class="fa fa-cog"></i>
                    <span class="title">Setup</span>
                    <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li id="academicsMenu">
                            <a href="/sms/setup/academics">
                            Academics
                            <span class="arrow open"></span>
                            </a>
                             <ul class="sub-menu">
                               
                                <li id="schoolyearMenu">
                                    <a href="/sms/setup/academics/school-year">
                                    <i class="fa fa-circle"></i>
                                    School Year </a>
                                </li>
                                

                                 <!-- <li id="syMonthTemp">
                                    <a href="/sms/setup/academics/sy-month-template">
                                    <i class="fa fa-circle"></i>
                                    SY Month Template </a>

                                </li> -->

                                <li id="gradelevelMenu">
                                    <a href="/sms/setup/academics/grade-level">
                                    <i class="fa fa-circle"></i>
                                    Grade Level </a>
                                </li>
                                

                                
                                <li id="sectionMenu">
                                    <a href="/sms/setup/academics/section">
                                    <i class="fa fa-circle"></i>
                                    Section</a>
                                </li>
                               

                               
                                <li id="subjectMenu">
                                    <a href="/sms/setup/academics/subject">
                                    <i class="fa fa-circle"></i>
                                    Subject</a>
                                </li>
                               

                                
                                <li id="assignMenu">
                                    <a href="/sms/setup/academics/assign-subject">
                                    <i class="fa fa-circle"></i>
                                    Assign Subjects</a>
                                </li>
                                

                                
                                <li id="scheduleMenu">
                                    <a href="/sms/setup/academics/schedule">
                                    <i class="fa fa-circle"></i>
                                    Schedule</a>
                                </li>
                               

                                <li id="">
                                    <a href="/sms/setup/academics/requirements">
                                    <i class="fa fa-circle"></i>
                                    Requirements</a>
                                </li>
                            </ul>
                        </li>
                        <li id="billingMenu">
                            <a href="#">
                            Billing
                            <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li id="feesMenu">
                                    <a href="#">
                                     <i class="fa fa-circle"></i>
                                     <span class="arrow open"></span>
                                    Setup Fees </a>
                                    <ul class="sub-menu">
                                        
                                      <li id="gradelevelfeeMenu">
                                        <a href="/sms/setup/billing/fee-category">
                                        <i class="fa fa-circle"></i>
                                          Fee Category</a>
                                      </li>
                                      

                                      
                                      <li id="gradelevelfeeMenu">
                                        <a href="/sms/setup/billing/fees">
                                        <i class="fa fa-circle"></i>
                                          Fees </a>
                                      </li>
                                     
                                    </ul>
                                </li>

                                
                                <li id="gradelevelfeeMenu">
                                    <a href="/sms/setup/billing/grade-level-fees">
                                    <i class="fa fa-circle"></i>
                                    Grade Level Fees </a>
                                </li>


                                <li id="gradelevelfeeMenu">
                                    <a href="/sms/setup/billing/accounts">
                                        <i class="fa fa-circle"></i>
                                        Accounts </a>
                                </li>
                                <li id="gradelevelfeeMenu">
                                    <a href="/sms/billing/journal-entry">
                                        <i class="fa fa-circle"></i>
                                        Journal Entry </a>
                                </li>
                                <!--  <li id="paymenttypeMenu">
                                    <a href="/sms/setup/billing/payment-type-schedule">
                                    <i class="fa fa-circle"></i>
                                    <span class="arrow open"></span>
                                    Rental</a>
                                     <ul class="sub-menu">
                                        
                                      <li id="">
                                        <a href="/sms/setup/billing/lessee">
                                        <i class="fa fa-circle"></i>
                                          Lessee / Tenant </a>
                                      </li>
                                      <li id="">
                                        <a href="/sms/setup/billing/tenant-fees">
                                        <i class="fa fa-circle"></i>
                                          Tenant Fees </a>
                                      </li>
                                     
                                    </ul>
                                 </li>
                                <!-- <li id="tuitionrefMenu">
                                    <a href="/sms/setup/billing/tuition-reference">
                                    <i class="fa fa-circle"></i>
                                    Tuition Reference </a>
                                </li> -->
                                <!-- <li id="duedatesMenu">
                                    <a href="/sms/setup/billing/due-dates">
                                    <i class="fa fa-circle"></i>
                                    Due Dates </a>
                                </li> -->
                            </ul>
                        </li>
                    </ul>
                </li>



                      <li id="">
                             <a href="#">
                                <i class="fa fa-file-excel-o"></i>
                                <span class="title">Reports</span>
                                <span class="arrow open"></span>
                                </a>
                            <ul class="sub-menu">
                                <li id="feesMenu">
                                    <a href="#">
                                     <i class="fa fa-circle"></i>
                                     <span class="arrow open"></span>
                                    Enrolled Students </a>
                                    <ul class="sub-menu">
                                      <li id="gradelevelfeeMenu">
                                        <a href="/sms/reports/enrolled-students" target="_blank">
                                        <i class="fa fa-circle"></i>
                                          Population</a>
                                      </li>
                                      <li id="gradelevelfeeMenu">
                                        <a href="/sms/reports/masterlist">
                                        <i class="fa fa-circle"></i>
                                            Masterlist
                                        </a>
                                      </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="/sms/reports/grade-level-billing" target="_blank">
                                        <i class="fa fa-circle"></i>
                                       Grade Level Billing
                                    </a>
                                </li>
                                <li>
                                    <a href="/sms/monthly-billing">
                                        <i class="fa fa-circle"></i>
                                        Monthly Billing
                                    </a>
                                </li>
                                <li>
                                    <a href="/reports/collections">
                                        <i class="fa fa-circle"></i>
                                        Collection
                                    </a>
                                </li>
                                <li>
                                    <a href="/reports/payment-list">
                                        <i class="fa fa-circle"></i>
                                        Payment List
                                    </a>
                                </li>
                                <li>
                                    <a href="/reports/journal-entries">
                                        <i class="fa fa-circle"></i>
                                        Journal Entries
                                    </a>
                                </li>
                                <li>
                                    <a href="/reports/overall-balances" target="_blank">
                                        <i class="fa fa-circle"></i>
                                        Overall Balances
                                    </a>
                                </li>
                               <!--  <li id="gradelevelfeeMenu">
                                    <a href="/sms/setup/billing/grade-level-fees">
                                    <i class="fa fa-circle"></i>
                                    Grade Level Fees </a>
                                </li> -->
                            </ul>
                        </li>
             

                <!-- <li>
                    <a href="http://192.168.1.20:83/" target="_blank">
                    <i class="fa fa-clock-o"></i>
                    <span class="title">Kronos</span>
                    </a>
                </li>
                <li>
                    <a href="{{substr_replace(Request::root(), "", -2)}}84" target="_blank">
                    <i class="fa fa-bar-chart-o"></i>
                    <span class="title">Pythagoras</span>
                    </a>
                </li>

                   -->
                
              
               
                <li id="securityMenu">
                    
                    <a href="/admin/security/security-menu">
                    <i class="fa fa-shield"></i>
                    <span class="title">Setup</span>
                    </a>
                </li>
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                             Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE HEAD -->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title pull-right">
                    <a href="{{ URL::previous() }}"><button class="btn red-sunglo btn-sm"><i class="fa fa-arrow-left"></i> BACK</button></a>

                    <a onclick="location.reload();"><button class="btn blue-madison btn-sm"><i class="fa fa-refresh"></i> Refresh</button></a>
                    <!-- <h1>Blank Page <small>blank page</small></h1> -->
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD -->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
             <!--    <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Page Layouts</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Blank Page</a>
                </li> -->
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    @yield('content')

                    
                      <div class="chat_box " id="chatBox">
                        <div class="chat_head"> Chat Box List</div>
                        <div class="chat_body"> 
                          @foreach($employees as $employees)
                            <div class="user"> 
                            <img style="WIDTH:30px;height:auto" src="http://bootdey.com/img/Content/user_2.jpg" class="pull-left" alt="Dmitry Ivaniuk">
                             <div style="padding-left:20px">
                                {{$employees->KronosEmployee->full_name}} 
                                <br> 
                                <i class="fa fa-envelope-o text-red"></i> 
                                {{$employees->username}}
                             </div>
                            </div>
                          @endforeach
                        </div>
                      </div>
                   
                    <div class="msg_box" style="right:290px">
                        <div class="msg_head">Krishna Teja
                        <div class="close">x</div>
                        </div>
                        <div class="msg_wrap">
                            <div class="msg_body">
                                <div class="msg_a">
                                <img style="WIDTH:30px;height:auto" src="http://bootdey.com/img/Content/user_2.jpg" class="pull-left" alt="Dmitry Ivaniuk">
                                This is from A   </div>
                                <div class="msg_b">This is from B, and its amazingly kool nah... i know it even i liked it :)</div>
                                <div class="msg_a">Wow, Thats great to hear from you man </div> 
                                <div class="msg_push"></div>
                            </div>
                        <div class="msg_footer"><textarea class="msg_input" rows="2"></textarea></div>
                    </div>
                    </div>

                
                </div>


            
            <!-- END PAGE CONTENT-->
            </div>

        
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<!-- <div class="page-footer">
    <div class="page-footer-inner">
         Powered by: WYRED INNOVATIONS
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div> -->

