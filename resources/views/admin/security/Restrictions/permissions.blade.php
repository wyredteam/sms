
@extends('sms.main.index')

@section('css_filtered')
@include('admin.csslinks.css_crud')
<link href="/assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="/assets/css/plugins/switchery/switchery.css" rel="stylesheet">
<link href="//rubaxa.github.io/Ply/ply.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/sortable/sortable.css" rel="stylesheet">
<style type="text/css">
      /* CSS REQUIRED */
    .state-icon {
        left: -5px;
    }
    .list-group-item-primary {
        color: rgb(255, 255, 255);
        background-color: rgb(66, 139, 202);
    }

    /* DEMO ONLY - REMOVES UNWANTED MARGIN */
    .well .list-group {
        margin-bottom: 0px;
    }
</style>
@stop

@section('content')

<div class="col-md-12">
  <div class="wyred-box-header">
    <h3 class="wyred-box-title"><i class="fa fa-user"></i> Security Permissions</h3>
  </div>
  <div class="wyred-box-body">
  <div class="row">

          <div class="col-lg-12">
                    <div class="tabs-container">

                        <div class="tabs-left">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-6">Permissions</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-7"></a></li>
                            </ul>
                            <div class="tab-content ">
                                <div id="tab-6" class="tab-pane active">
                                    <div class="panel-body">
                                           
                                            <div class="col-xs-12">

                                                <div class="form-group">
                                                    <label> Roles </label>
                                                    <select class="form-control input-sm" onchange="getPermissions()" id="role_id" name="role_id">
                                                        <option></option>
                                                        @foreach($roles as $roles)
                                                          <option value="{{$roles->role_id}}">{{$roles->role_name}}</option>
                                                        @endforeach  
                                                    </select>
                                                </div>

                                                <h3 class="text-center">Basic Example</h3>
                                                <div class="well"  style="max-height: 700px;overflow: auto;">
                                                    <ul  class="list-group check-list-box">
                                     
                                                    </ul>
                                                </div>

                                                <button class="btn btn-info" id="savePermission">Save Permission</button>
                                            </div>
                                          
                                    </div>
                                </div>
                               
                            </div>

                        </div>

                    </div>
                </div>
  </div>
</div>

</div>




@stop
@section('js_filtered')
@include('admin.jslinks.js_crud')
@include('admin.jslinks.js_datatables')

<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>




<script>


    $('#wyredSaveModal').on('hidden.bs.modal',function(){
        rolesDatable();
        
    });

    $(document).ready(function(){
        initializeCheckBoxes();
    });

    function rolesDatable(){

        $('#role-table').dataTable().fnClearTable();
        $("#role-table").dataTable().fnDestroy();

        assesstmentTable = $('#role-table').DataTable({
            responsive: true,
            bAutoWidth:false,

            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        studentList = data;
                        console.log(studentList);
                        fnCallback(studentList);
                    }
                });
            },

            "sAjaxSource": "/admin/security/get-sub-modules",
            "sAjaxDataProp": "",
            "iDisplayLength": 10,
            "scrollCollapse": false,
            "paging":         true,
            "searching": true,
            "columns": [



                { "mData": "role_id", sDefaultContent: ""},
                { "mData": "role_name", sDefaultContent: ""},
                { "mData": "role_description", sDefaultContent: ""},
                { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a  class="btn btn-info btn-block" target="_blank"><i class="fa fa-pencil"></i> Edit</a>');
                  }
                },
                { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a  class="btn btn-danger btn-block" target="_blank"><i class="fa fa-remove"></i> Remove</a>');
                  }
                },

            ]
        });

    }

    function initializeCheckBoxes(){

          $('.list-group.check-list-box .list-group-item').each(function () {
              
              // Settings
              var $widget = $(this),
                  $checkbox = $('<input type="checkbox" class="hidden" />'),
                  color = ($widget.data('color') ? $widget.data('color') : "primary"),
                  style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
                  settings = {
                      on: {
                          icon: 'glyphicon glyphicon-check'
                      },
                      off: {
                          icon: 'glyphicon glyphicon-unchecked'
                      }
                  };
                  
              $widget.css('cursor', 'pointer')
              $widget.append($checkbox);

              // Event Handlers
              $widget.on('click', function () {
                  $checkbox.prop('checked', !$checkbox.is(':checked'));
                  $checkbox.triggerHandler('change');
                  updateDisplay();
              });
              $checkbox.on('change', function () {
                  updateDisplay();
              });
                

              // Actions
              function updateDisplay() {
                  var isChecked = $checkbox.is(':checked');

                  // Set the button's state
                  $widget.data('state', (isChecked) ? "on" : "off");

                  // Set the button's icon
                  $widget.find('.state-icon')
                      .removeClass()
                      .addClass('state-icon ' + settings[$widget.data('state')].icon);

                  // Update the button's color
                  if (isChecked) {
                      $widget.addClass(style + color + ' active');
                  } else {
                      $widget.removeClass(style + color + ' active');
                  }
              }

              // Initialization
              function init() {
                  
                  if ($widget.data('checked') == true) {
                      $checkbox.prop('checked', !$checkbox.is(':checked'));
                  }
                  
                  updateDisplay();

                  // Inject the icon if applicable
                  if ($widget.find('.state-icon').length == 0) {
                      $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
                  }
              }
              init();
          });
          

          
}
    
    function getPermissions(){
        var id = $('#role_id').val();
        $.post("/admin/security/get-permissions-by-id",{role_id: id}, function(data, status){
              console.log(data);
              $('.check-list-box').html('');

              data.forEach(function(obj) {
                  if(obj.exist == true){
                      var li = '<li class="list-group-item" data-checked="true" data-id="'+obj.sub_module_id+'">'+obj.sub_module_name+' - '+obj.description+'</li>'; 
                  }else{
                      var li = '<li class="list-group-item" data-id="'+obj.sub_module_id+'">'+obj.sub_module_name+' - '+obj.description+'</li>'; 

                  }
                  $('.check-list-box').append(li);
                  initializeCheckBoxes();
              });
             
        });

        
    }


    $('#savePermission').on('click', function(event) {
        event.preventDefault(); 
        var checkedItems = {}, counter = 0;
        $(".check-list-box li.active").each(function(idx, li) {
            checkedItems[counter] = $(li).attr('data-id');
            counter++;
        });
        
        var data = checkedItems;
        console.log(data);
        savePermissionData(data);
    });

    function savePermissionData(data){
          var role_id = $('#role_id').val();
          $.post("/admin/security/save-new-permissions",{data: data, role_id:role_id}, function(data, status){
              swal("Good job!", "Permission has been updated!", "success")
          });

           
    }

</script>


@stop