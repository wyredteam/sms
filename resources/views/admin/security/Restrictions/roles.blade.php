
@extends('sms.main.index')

@section('css_filtered')
@include('admin.csslinks.css_crud')
<link href="/assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="/assets/css/plugins/switchery/switchery.css" rel="stylesheet">
@stop

@section('content')

<div class="col-md-12">
  <div class="wyred-box-header">
    <h3 class="wyred-box-title"><i class="fa fa-user"></i> Security Roles</h3>
  </div>
  <div class="wyred-box-body">
  <div class="row">

          <div class="col-lg-12">
                    <div class="tabs-container">

                        <div class="tabs-left">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-6">Roles</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-7"></a></li>
                            </ul>
                            <div class="tab-content ">
                                <div id="tab-6" class="tab-pane active">
                                    <div class="panel-body">
                                            
                                            <div class="col-md-12" style="margin-bottom: 30px">
                                                <a data-toggle="modal" data-target="#modal-roles" class="btn btn-primary btn-rounded pull-right" href="#"> + Add New Role</a>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="role-table" class="table table-striped table-bordered table-hover dataTables-example" >
                                                    <thead>
                                                    <tr>
                                                        <th>Role Id</th>
                                                        <th>Role Name</th>
                                                        <th>Role Description</th>
                                                        <th>Edit</th>
                                                        <th>Remove</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="gradeX">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="center"></td>
                                                        <td class="center"></td>
                                                    </tr>
                                                    
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Role Id</th>
                                                        <th>Role Name</th>
                                                        <th>Role Description</th>
                                                        <th>Edit</th>
                                                        <th>Remove</th>
                                                    </tfoot>
                                                    </table>
                                                    </div>
                                            </div>


                                    </div>
                                </div>
                               
                            </div>

                        </div>

                    </div>
                </div>

  </div>
</div>

</div>


<div class="modal fade draggable-modal mo-z drag-me" id="modal-roles" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Role Creation</h4>
      </div>
      <div class="modal-body">
        <form id="setupSubject">
          <div class="form-group">
           <label>Role Name</label>
             <input type="text" class="form-control input-sm" id="role_name" name="role_name" required>
          </div>

          <div class="form-group">
           <label>Role Description</label>
             <textarea  class="form-control input-sm" id="role_description" name="role_description" required></textarea>
          </div>
   
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button class="btn btn-info wyredModalCallback" data-toggle="modal" data-url="/admin/security/new-role" data-form="setupSubject" data-target="#wyredSaveModal">Save Fees</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


@stop
@section('js_filtered')
@include('admin.jslinks.js_crud')
@include('admin.jslinks.js_datatables')

<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>


<script>

    $(document).ready(function(){
        rolesDatable();
    });


    $('#wyredSaveModal').on('hidden.bs.modal',function(){
        rolesDatable();
    });


    function rolesDatable(){

        $('#role-table').dataTable().fnClearTable();
        $("#role-table").dataTable().fnDestroy();

        assesstmentTable = $('#role-table').DataTable({
            responsive: true,
            bAutoWidth:false,

            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        studentList = data;
                        console.log(studentList);
                        fnCallback(studentList);
                    }
                });
            },

            "sAjaxSource": "/admin/security/get-roles",
            "sAjaxDataProp": "",
            "iDisplayLength": 10,
            "scrollCollapse": false,
            "paging":         true,
            "searching": true,
            "columns": [



                { "mData": "role_id", sDefaultContent: ""},
                { "mData": "role_name", sDefaultContent: ""},
                { "mData": "role_description", sDefaultContent: ""},
                { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a  class="btn btn-info btn-block" target="_blank"><i class="fa fa-pencil"></i> Edit</a>');
                  }
                },
                { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a  class="btn btn-danger btn-block" target="_blank"><i class="fa fa-remove"></i> Remove</a>');
                  }
                },

            ]
        });

    }

</script>


@stop