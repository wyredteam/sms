
@extends('sms.main.index')

@section('css_filtered')
@include('admin.csslinks.css_crud')
<link href="/assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="/assets/css/plugins/switchery/switchery.css" rel="stylesheet">
@stop

@section('content')

<div class="col-md-12">
  <div class="wyred-box-header">
    <h3 class="wyred-box-title"><i class="fa fa-user"></i> Security Modules</h3>
  </div>
  <div class="wyred-box-body">
  <div class="row">

          <div class="col-lg-12">
                    <div class="tabs-container">

                        <div class="tabs-left">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-6">Main Modules</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-7">Sub Modules</a></li>
                            </ul>
                            <div class="tab-content ">
                                <div id="tab-6" class="tab-pane active">
                                    <div class="panel-body">
                                            
                                            <div class="col-md-12" style="margin-bottom: 30px">
                                                <a data-toggle="modal" data-target="#modal-module-creation" class="btn btn-primary btn-rounded pull-right" href="#"> + Add New Module</a>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="module-table" class="table table-striped table-bordered table-hover dataTables-example" >
                                                    <thead>
                                                    <tr>
                                                        <th>Module Id</th>
                                                        <th>Module Name</th>
                                                        <th>Edit</th>
                                                        <th>Remove</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="gradeX">
                                                        <td></td>
                                                        <td></td>
                                                        <td class="center"></td>
                                                        <td class="center"></td>
                                                    </tr>
                                                    
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Module Id</th>
                                                        <th>Module Name</th>
                                                        <th>Edit</th>
                                                        <th>Remove</th>
                                                    </tfoot>
                                                    </table>
                                                    </div>
                                            </div>


                                    </div>
                                </div>
                                <div id="tab-7" class="tab-pane">
                                    <div class="panel-body">
                                            
                                            <div class="col-md-12" style="margin-bottom: 30px">
                                                <a data-toggle="modal" data-target="#modal-submodule-creation" class="btn btn-primary btn-rounded pull-right" href="#"> + Add New Sub-Module</a>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="sub-module-table" class="table table-striped table-bordered table-hover dataTables-example" >
                                                    <thead>
                                                    <tr>
                                                        <th>Sub-Module Id</th>
                                                        <th>Module Name</th>
                                                        <th>Sub-Module Name</th>
                                                        <th>Edit</th>
                                                        <th>Remove</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="gradeX">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="center"></td>
                                                        <td class="center"></td>
                                                    </tr>
                                                    
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Sub-Module Id</th>
                                                        <th>Module Name</th>
                                                        <th>Sub-Module Name</th>
                                                        <th>Edit</th>
                                                        <th>Remove</th>
                                                    </tfoot>
                                                    </table>
                                                    </div>
                                            </div>


                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

  </div>
</div>

</div>


<div class="modal fade draggable-modal mo-z drag-me" id="modal-module-creation" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Module Creation</h4>
      </div>
      <div class="modal-body">
        <form id="setupSubject">
         <div class="form-group">
           <label>Module Name</label>
             <input type="text" class="form-control input-sm" id="module_name" name="module_name" required>
          </div>
   
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button class="btn btn-info wyredModalCallback" data-toggle="modal" data-url="/admin/security/new-module" data-form="setupSubject" data-target="#wyredSaveModal">Save Fees</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<div class="modal fade draggable-modal mo-z drag-me" id="modal-submodule-creation" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Sub Module Creation</h4>
      </div>
      <div class="modal-body">
        <form id="subModuleForm">
        <div class="form-group">
           <label>Module Name</label>
             <select class="form-control input-sm" id="module_id" name="module_id" required>
                <option></option>
                @foreach($modules as $modules)
                <option value="{{$modules->module_id}}">{{$modules->module_name}}</option>
                @endforeach
             </select>
          </div>

         <div class="form-group">
           <label>Sub Module Name</label>
             <input type="text" class="form-control input-sm" id="sub_module_name" name="sub_module_name" required>
          </div>

          <div class="form-group">
           <label>Description</label>
                <textarea class="form-control input-sm" id="description" name="description" required></textarea>
          </div>
   
   
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button class="btn btn-info wyredModalCallback" data-toggle="modal" data-url="/admin/security/new-submodule" data-form="subModuleForm" data-target="#wyredSaveModal">Save Fees</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


@stop
@section('js_filtered')
@include('admin.jslinks.js_crud')
@include('admin.jslinks.js_datatables')

<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>


<script>

    $(document).ready(function(){
        moduleDatable();
        subModuleDatable();
    });


    $('#wyredSaveModal').on('hidden.bs.modal',function(){
        moduleDatable();
        subModuleDatable();
    });

    
    function moduleDatable(){

        $('#module-table').dataTable().fnClearTable();
        $("#module-table").dataTable().fnDestroy();

        assesstmentTable = $('#module-table').DataTable({
            responsive: true,
            bAutoWidth:false,

            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        studentList = data;
                        console.log(studentList);
                        fnCallback(studentList);
                    }
                });
            },

            "sAjaxSource": "/admin/security/get-modules",
            "sAjaxDataProp": "",
            "iDisplayLength": 10,
            "scrollCollapse": false,
            "paging":         true,
            "searching": true,
            "columns": [



                { "mData": "module_id", sDefaultContent: ""},
                { "mData": "module_name", sDefaultContent: ""},
                { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a  class="btn btn-info btn-block" target="_blank"><i class="fa fa-pencil"></i> Edit</a>');
                  }
                },
                { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a  class="btn btn-danger btn-block" target="_blank"><i class="fa fa-remove"></i> Remove</a>');
                  }
                },

            ]
        });

    }

    function subModuleDatable(){

        $('#sub-module-table').dataTable().fnClearTable();
        $("#sub-module-table").dataTable().fnDestroy();

        assesstmentTable = $('#sub-module-table').DataTable({
            responsive: true,
            bAutoWidth:false,

            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        studentList = data;
                        console.log(studentList);
                        fnCallback(studentList);
                    }
                });
            },

            "sAjaxSource": "/admin/security/get-sub-modules",
            "sAjaxDataProp": "",
            "iDisplayLength": 10,
            "scrollCollapse": false,
            "paging":         true,
            "searching": true,
            "columns": [

                { "mData": "sub_module_id", sDefaultContent: ""},
                { "mData": "module.module_name", sDefaultContent: ""},
                { "mData": "sub_module_name", sDefaultContent: ""},
                { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a  class="btn btn-info btn-block" target="_blank"><i class="fa fa-pencil"></i> Edit</a>');
                  }
                },
                { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a  class="btn btn-danger btn-block" target="_blank"><i class="fa fa-remove"></i> Remove</a>');
                  }
                },

            ]
        });

    }

</script>


@stop