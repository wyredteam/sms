

@foreach($messages as $new_messages)
    @if($logUser->user_id == $new_messages->UserFrom->user_id)

     
        <div class="item in">
            <div class="image">
                <img src="/assets/img/default.png" style="width:30px;height:auto" >
            </div>
            <div class="text" style="background-color:#4080ff;color:white">
                <div class="heading">
                    <input type="hidden" value="{{$new_messages->message_id}}" class="last_message_id">
                    <a href="#">Me</a>
                    <span class="date">08:33</span>
                </div>
                {{$new_messages->message}}
            </div>
        </div>



    @else 

      <div class="item">
            <div class="image">
                <img src="/assets/img/default.png" style="width:30px;height:auto" alt="John Doe">
            </div>
            <div class="text">
                <div class="heading">
                    <input type="hidden" value="{{$new_messages->message_id}}" class="last_message_id">
                    <a href="#">{{$new_messages->UserFrom->KronosEmployee->full_name}}</a>
                    <span class="date">08:33</span>
                </div>
                {{$new_messages->message}}
            </div>
        </div>

    @endif
      
@endforeach