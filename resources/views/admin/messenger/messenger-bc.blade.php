@extends('sms.main.index')


@section('css_filtered')
@include('admin.csslinks.css_crud')
<link href="/assets/messenger/messenger.css" rel="stylesheet">

<style type="text/css">
	input{
		text-transform: none !important;
	}
</style>

@stop


@section('content')






    <div class="row">
    
        
        <!--=========================================================-->
        <!-- selected chat -->
      <div class="col-md-9 bg-white message-list" id="message-list">
            <div class="chat-message">
                <input type="hidden" id="partner_user_id">
                <ul id="chatbox" class="chat">




                </ul>

            </div>
                       
    </div>  

    

    <div class="col-md-3 bg-white employees-list">
            <div class=" row border-bottom padding-sm" style="height: 40px;">
              Member
            </div>

            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#members">Members</a></li>
              <li><a data-toggle="tab" href="#messages">Messages</a></li>
            </ul>
            
            <!-- =============================================================== -->
            <!-- member list -->
            <ul class="friend-list">
                
                <!-- <li class="active animated bounceInDown">
                  <a href="#" class="clearfix">
                    <img src="http://bootdey.com/img/Content/user_1.jpg" alt="" class="img-circle">
                    <div class="friend-name"> 
                      <strong>John Doe</strong>
                    </div>
                    <div class="last-message text-muted">Hello, Are you there?</div>
                    <small class="time text-muted">Just now</small>
                    <small class="chat-alert label label-danger">1</small>
                  </a>
                </li> -->
                <div class="tab-content">
                  <div id="members" class="tab-pane fade in active">
                      
                        @foreach($employees as $employees)
                        <li>
                          <a  class="clearfix" onclick="loadMessages({{$employees->user_id}})">
                            <img src="http://bootdey.com/img/Content/user_2.jpg" alt="" class="img-circle">
                            <div class="friend-name"> 
                              <strong>{{$employees->KronosEmployee->full_name}}</strong>

                            </div>
                            <small class="last-message  text-muted">Online</small>
                          </a>
                        </li> 


                        @endforeach

                  </div>

                  <div id="messages" class="tab-pane fade in active">
                      
            <!--            <li>
                          <a href="#" class="clearfix">
                            <img src="http://bootdey.com/img/Content/user_2.jpg" alt="" class="img-circle">
                            <div class="friend-name"> 
                              <strong></strong>
                            </div>
                            <div class="last-message text-muted">Lorem ipsum dolor sit amet.</div>
                            <small class="time text-muted">5 mins ago</small>
                          <small class="chat-alert text-muted"><i class="fa fa-check"></i></small>
                          </a>
                        </li>  -->

                  </div>
                </div>


                
                              
            </ul>
    </div>  


    <div class="col-md-12 bg-white">
      <div class="form-group">
        <input id="new-message" class="form-control border no-shadow no-rounded" placeholder="Type your message here">
        <span class="input-group-btn">
          <button class="btn btn-success no-rounded" id="sendMesage" type="button">Send</button>
        </span>
      </div><!-- /input-group --> 
    </div>     
  </div>







@foreach($messages as $new_messages)

    @if($logUser->user_id == $new_messages->user_id)

     <li class="right clearfix " style="border-radius:20px">
          <span class="chat-img pull-right">
            <img src="/assets/img/athena.png" alt="User Avatar">
          </span>
          <div class="chat-body-me  pull-right" style="width:300px;">
            <div class="">
            <input type="hidden" value="{{$new_messages->message_id}}" class="last_message_id">
              <small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 13 mins ago</small>
            </div>
            <p>
              {{$new_messages->message}}
            </p>
          </div>
        </li>


    @else 

  
      <div class="item">
        <div class="image">
            <img src="assets/images/users/user.jpg" alt="Dmitry Ivaniuk">
        </div>                                
        <div class="text">
            <div class="heading">
            <input type="hidden" value="{{$new_messages->message_id}}" class="last_message_id">
                <a href="#">{{$new_messages->User->KronosEmployee->full_name}}</a>
                <span class="date">08:39</span>
            </div> 
            {{$new_messages->message}}                                   
        </div>
        </div>

    @endif
      
@endforeach

@stop

@section('js_filtered')
@include('admin.jslinks.js_crud')
@include('admin.jslinks.js_datatables')

<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

<script type="text/javascript">
    
    $(document).ready(function(){
        window.setInterval(function(){
          checkMessages();
        }, 2000);
    });

    $('#message-list').on('scroll', function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
            console.log('end reach up');
        }
        else if($(this).scrollTop() <= 0)
        {
            console.log('Top reached');
            loadOldMessages();
        }
    
    });


    function loadOldMessages(){
        var ref_message_id = $(".last_message_id").first().val();
        console.log("oldest id: "+ref_message_id);
        var user_id = $('#partner_user_id').val();
        console.log(user_id);
        $.ajax({url: "/admin/messenger/old-convo-messages/"+user_id+"?ref_message_id="+ref_message_id, success: function(result){
            $("#chatbox").prepend(result);
        }});
    }
  
    function loadMessages(user_id){

        $("#chatbox").html('');
        $('#partner_user_id').val(user_id);
        var last_message_id = $(".last_message_id").last().val();
        console.log(last_message_id);
        $.ajax({url: "/admin/messenger/load-convo/"+user_id,
        success: function(result){
            $("#chatbox").append(result);
            scrollDown();
        }});


    }

    function loadNewMessages(user_id){

        //$("#chatbox").html('');
        $('#partner_user_id').val(user_id);
        var last_message_id = $(".last_message_id").last().val();
        console.log(last_message_id);
        $.ajax({url: "/admin/messenger/convo/"+user_id+"?last_message_id="+last_message_id, success: function(result){
            $("#chatbox").append(result);
            scrollDown();
        }});
    }

    $('#sendMesage').click(function(){

        var new_message = $('#new-message').val();
        var partner_user_id = $('#partner_user_id').val();
        $.ajax({
            url: '/admin/messenger/new-message',
            type: 'POST',
            data: {new_message:new_message,partner_user_id:partner_user_id} ,
            success: function (response) {
                //your success code
                console.log("Message has been sent!");
                $('#new-message').val('');
                loadNewMessages(partner_user_id);
                
                scrollDown();
            },
            error: function () {
                //your error code
                alert("errr ajax!")
            }
        });

    });

    function checkMessages(){
        var partner_user_id = $('#partner_user_id').val();
        var last_message_id = $(".last_message_id").last().val();

        if(partner_user_id == "" || last_message_id == ""){
            console.log("User not specified");
            return;
        }
        if(partner_user_id != undefined){
            var last_message_id = $(".last_message_id").last().val();
            console.log(last_message_id);
            $.ajax({url: "/admin/messenger/convo/"+partner_user_id+"?last_message_id="+last_message_id, success: function(result){
                var last_mes_id_result = $(result).find('.last_message_id').val();
                console.log(last_mes_id_result);

                if(last_message_id < last_mes_id_result){
                    var snd = new Audio("/assets/music/newmessage.mp3"); 
                    snd.play();
                    $("#chatbox").append(result);
                    scrollDown();
                }else{
                    console.log("No new Messages");
                }
                
                
            }});
        }
    }


function scrollDown(){
    //$(".message-list").animate({ scrollTop: $('.message-list').prop("scrollHeight")}, 100);
    $("#message-list").scrollTop($("#message-list")[0].scrollHeight);
}
</script>


@stop