@extends('sms.main.index')


@section('css_filtered')
@include('admin.csslinks.css_crud')
<link rel="stylesheet" type="text/css" id="theme" href="/assets/css/theme-default.css"/>

<link rel="stylesheet" type="text/css" id="theme" href="/assets/global/plugins/jquery-ui/jquery-ui.min.css"/>

<style type="text/css">
	input{
		text-transform: none !important;
	}

  .employees-list{
    height:100vh;
    overflow-y: scroll;
}

  #message-list{
      height:60vh;
      overflow-y: scroll;
  }
</style>

@stop


@section('content')






    <div class="row">   
        

        <div class="content-frame">                                    
                    
                    <!-- END CONTENT FRAME TOP -->
                    
                    
                    <!-- END CONTENT FRAME RIGHT -->
                
                    <!-- START CONTENT FRAME BODY -->
                    <div class="col-md-12 content-frame-body content-frame-body-left" id="message-list" >
                        <input type="hidden" id="partner_user_id">

                        <div class="messages messages-img chatbox" id="chatbox">


                        </div>   

                    </div>

                    <div class="panel panel-default push-up-10">
                            <div class="panel-body panel-body-search">
                                <div class="form-group">
                                   
                                    <input type="text" class="form-control new-message" placeholder="Your message...">
                                    <div class="form-group-btn">
                                        <button class="btn btn-default sendMesage" >Send</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- END CONTENT FRAME BODY -->      
                </div>


    </div>






@stop

@section('js_filtered')
@include('admin.jslinks.js_crud')
@include('admin.jslinks.js_datatables')

<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

<script type="text/javascript">
    
   
</script>


@stop