<link href="{{ URL::asset('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ URL::asset('/assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ URL::asset('/assets/css/wyred.css') }}" rel="stylesheet">
<!-- <link href="{{ URL::asset('/assets/css/theme-default.css') }}" rel="stylesheet"> -->
<link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>

<link href="/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="/assets/admin/layout4/css/themes/default.css" rel="stylesheet" type="text/css"/>
<link href="/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="/assets/css/custom/chat.css" rel="stylesheet" type="text/css"/>
<!-- <link rel="stylesheet" type="text/css" id="theme" href="/assets/messenger/messenger.css"/>
<link rel="stylesheet" type="text/css" id="theme" href="/assets/css/theme-default.css"/>
<link rel="stylesheet" type="text/css"  href="/assets/messenger/emoticons.css"/>

 -->

<style type="text/css">
	.student-body{
		border: 1px solid rgb(121, 126, 131);
		background:white;
		padding:15px;
	}
	.student-body:hover{
		box-shadow: 0 0 15px rgba(81, 203, 238, 1);
		border: 1px solid rgba(81, 203, 238, 1);
		z-index: 99999;
	}

	.box-gui{
		border-radius: 0px;
	}

	input, select  , textarea ,label , option{
    	text-transform: capitalize;

	}

	.list-group-contacts{
		text-overflow: ellipsis;
	}

	#employees-chat{
		position: fixed;
		top: 30px;
	    bottom: 0px;
	    right: 0px;
	    background-color: #ffffff;
	    z-index: 999999;
	 	padding-top: 10px;
	 	height:600px;
	 	padding-bottom: 0px;
	 	border-radius:0px;
	 	margin: 0px;
	 	overflow-y: scroll;
	}
	.list-group-item:last-child {
		margin-bottom: 0px;
		border-bottom-left-radius: 0px;
		border-radius: 0px; 
		border-color: transparent;
	}
	#chat-activator{
		position: fixed;
		right: 0px;
		bottom: 0px;
		z-index: 99999999;
	}


	label.error {
	    height:17px;
	    color: red;
	    font-size:small;
	    list-style-type: none;
	}

</style>