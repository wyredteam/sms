<!DOCTYPE html>
<html>
<head>
    <link href="<?php echo e(URL::asset('/assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('/assets/css/custom/rhitsReports.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('/assets/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet">
    <link href="http://<?php echo e($_SERVER['HTTP_HOST']); ?>/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: bottom;

        }
        .payments{
            position:relative;
            margin-top:20px;
            top:0px;
        }
        body{
            padding-top:20px;
        }
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
            margin-top:20px;
        }
        th,td{
            border: 1px solid #000000;
            text-align: left;
            padding-left:10px;
            padding-top:5px;
            padding-bottom: 5px;
            font-size: 15px;

        }
        .paddings{
            padding-top:2px;
            padding-bottom:2px;
            font-size: 15px;
        }
        .statement{
            position:relative;
            float: left;
            width: 50%;
        }
        .payment-sched{
            position: relative;
            float: left;
            width: 45%;
            left:40px;
        }
        .white{
            background-color:white !important;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #F5F5F6;
            border-bottom-width: 1px;

        }
        .border{
            border:1px solid #000000;
        }
        .billing{
            height:620px;
            margin-bottom: 30px;
        }
        .padding{
            padding-left: 10px;
        }
        .holder{
            border: 1px solid #DDDDDD;
            margin-bottom: 20px;
            padding:20px;
        }

        .bordered{
            border: 1px solid #DDDDDD;
        }
        .no-border table,td,th{
            border: 1px solid #ffffff;
        }
        .border-top{
            /*border-top: 1px solid #000000 !important;*/
            border-top: 1px;
            border-top: 1px solid ;
            border-top: medium solid #414446;
        }


    </style>
    <title>Student Ledger</title>
</head>


<body>


<div class="page-num">Page 1</div>
<div class="body-letter col-100" >
    <div class ="header col-md-12">
        <div class="col-md-12">
            <h3 class="t-center bold">SCHOOL OF THE MORNING STAR</h3>
            <h4 class="t-center bold">BUTUAN CITY</h4>
            <p class="t-center"></p>
            <p class="t-center"><i></i></p>
            <h3 class="text-center">Student Ledger</h3>
        </div>
    </div>
    <div class="col-md-12" style="margin-bottom: 10px;">
        <table class="table table-bordered">
            <tr class="bg-primary">
                <th colspan="10">Student Information</th>
            </tr>
            <tr>
                <td width="10%">Last Name:</td>
                <th class="paddings"><?php echo e(ucfirst($student->last_name)); ?></th>
                <td width="10%">First Name:</td>
                <th class="paddings"><?php echo e(ucfirst($student->first_name)); ?></th>
                <td>Middle Name:</td>
                <th width="20%" class="paddings"><?php echo e(ucfirst($student->middle_name)); ?></th>
                <td width="10%">Suffix:</td>
                <th class="paddings"><?php echo e(ucfirst($student->name_extension)); ?></th>
            </tr>
        </table>

        <table class="table table-striped paddings">
            <tr>
                <th>School Year</th>
                <th>Date</th>
                <th>Fees</th>
                <th>OR</th>
                <th>Payments</th>
                <th>Balance</th>
            </tr>
            <?php foreach($accounts as $acc): ?>
                <tr>
                    <td><?php echo e($acc->getSchoolYear->sy_from); ?> - <?php echo e($acc->getSchoolYear->sy_to); ?></td><td></td><td>TOTAL AMOUNT DUE</td><td></td><td></td><td class="text-right"><?php echo e($acc->amount_due); ?></td>
                </tr>
                <?php foreach($acc->getPayments as $payments): ?>
                 <tr>
                     <td></td>
                     <td><?php echo e($payments->date_of_payment); ?></td>
                     <td><?php echo e($payments->getPaymentOption->payment_option); ?> - <?php echo e($payments->getFees->title); ?></td>
                     <td><?php echo e($payments->or_no); ?></td>
                     <td class="text-right"><?php echo e(number_format($payments->amount,2,'.',',')); ?></td>
                     <td class="text-right"><?php echo e($payments->bal); ?></td>
                 </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </table>
    </div>
    </div>
</body>

</html>

