<?php $__env->startSection('css_filtered'); ?>
<?php echo $__env->make('admin.csslinks.css_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style type="text/css">
      
      .grid_space{
          margin-top:6px;
          margin-bottom: 3px;
      }
      .student-panel{
          margin-top: 10px;
      }
      

      .profile-pic{
          cursor: pointer;
          border: 10px solid #fff;
          border-bottom: 45px solid #fff;
          -webkit-box-shadow: 3px 3px 3px #777;
             -moz-box-shadow: 3px 3px 3px #777;
                  box-shadow: 3px 3px 3px #777;
      }

      #studentlist{
        text-transform: capitalize; 
        text-align: center;
      }

</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>



<div class="col-md-12">
      <div class="portlet box wyred">
          <div class="portlet-title">
              <div class="caption">
                  <i class="fa fa-group text-white"></i> Student List
              </div>
              <div class="tools">

              </div>
          </div>
          <div class="portlet-body">
              <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-12">
                     <h3>Filter By:</h3>
                    </div>

                  <form id="formFilter">
                    <div class="col-md-6">
                      
                        <div class="form-group">
                            <label for="sy">School Year:</label>
                            <select class="form-control input-sm" name="schoolYear" id="schoolYear" required="">
                                <?php foreach($sy as $year): ?>
                                    <option value="All">All</option>
                                    <option value="<?php echo e($year->school_year_id); ?>"><?php echo e($year->sy_from); ?>-<?php echo e($year->sy_to); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sy">Student Status:</label>
                            <select class="form-control input-sm" name="student_status" id="student_status">
                                <option value="All">All</option>
                                <?php foreach($student_status as $student_status): ?>
                                    <option value="<?php echo e($student_status->student_status_id); ?>"><?php echo e($student_status->student_status); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                     
                    </div>
                    <div class="col-md-6">
                          <div class="form-group">
                           <label for="sy">Grade Type:</label>
                           <select class="form-control input-sm gradeType" required  name="gradeType"  id="gradeType">
                               <option value="All">All</option>
                               <?php foreach($gradeType as $type): ?>
                                   <option value="<?php echo e($type->grade_type_id); ?>"><?php echo e($type->grade_type); ?></option>
                                <?php endforeach; ?>
                           </select>
                       </div>
                       <div class="form-group">
                           <label for="sy">Grade Level:</label>
                           <select class="form-control input-sm input-sm gradelevel" required name="grade_level" data-id="grade_level_id" data-name="grade_level" data-url="/select-binder/get-gradeLevel" required>
                               <option >All</option>
                           </select>
                       </div>
                        <div class="form-group">
                            <label>Section:</label>
                            <select class="form-control input-sm input-sm sectionName edit_section_id"  name="sectionName" required  data-id="section_id" data-name="section_name" data-url="/select-binder/get-sectionName" >
                                <option>All</option>
                            </select>
                        </div>
                   </div>
                    </form>

                    <div class="col-md-12">
                      <div class="col-md-6 pull-right">
                         <button class="btn btn-primary " onclick="submitFormFilter()"><i class="fa fa-search"></i> Find</button>
                         <button class="btn btn-danger" onclick="printCustom()"><i class="fa fa-print"></i> Print Preview</button>
                      </div>
                    </div>

                    <div class="col-md-12" style="padding-top:50px">
                     <table id="studentlist" class="table table-striped  table-hover" >
                      <thead>
                      <tr>
                          <th class="text-center">ID Number</th>
                          <th class="text-center">Full Name</th>
                          <th class="text-center">Grade Level</th>
                          <th class="text-center">Section</th>
                          <th class="text-center">Time In</th>
                          <th class="text-center">Time Out</th>
                          <th class="text-center">Gender</th>
                          <th class="text-center">Status</th>
                          <th class="text-center">Action</th>
                          <th class="text-center">Action</th>
                          <th class="text-center">Action</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
             
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                           <td></td>


                      </tr>

                      </tbody>
                  </table>
                </div>   
                  </div>
              </div>

          </div>
      </div>
      <!-- BEGIN SAMPLE FORM PORTLET-->
  </div>

   


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js_filtered'); ?>
<?php echo $__env->make('admin.jslinks.js_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin.jslinks.js_datatables', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>



    
<script>
    $('.gradeType').change(function(){
        var selValue = $(this).val();
        $('.gradelevel').select_binder(selValue);
        $('.gradelevel').append('<option value="All">All</option>');
    });

    $('.gradelevel').change(function(){
        var selValue = $(this).val();
      $('.sectionName').select_binder(selValue);
        $('.sectionName').append('<option value="All">All</option>');
    });


//    $('.sectionType').change(function(){
//        var secType = $(this).val();
//        var gradeLevel = $('.gradelevel').val();
//        var selValue = secType+'-'+gradeLevel;
//        $('.sectionName').select_binder(selValue);
//        $('.sectionName').append('<option value="All">All</option>');
//    });

/*    $(document).ready(function() {
*//*        studentTable();

    });*/

    function submitFormFilter(){
        var Formdata = $("form").serialize();


        formValidate = $("form");

        formValidate.validate({

              ignoreTitle: true,
              debug:true,
           
              errorPlacement: function(error, element) {
              
                  error.insertAfter(element);
    

              

          },


        });
     

          if(!formValidate.valid()){
             
              message = 'Some input fields is highly required. Please write something to resume.';
              var errorInput = $(formValidate).find("input.error")[0];
              modalError(message);
            try{
              $('html, body').animate({
               //scrollTop: ($(errorInput).offset().top - 300)
          }, 2000);
            }catch(e){

            }
              

              return false;

          }
        studentTable(Formdata);
    }

function printCustom(){
    var Formdata = $("form").serialize();

    if($('.edit_section_id').val() == ""){
        alert("Section cannot be blank");
        return false;
    }
    formValidate = $("form");

        formValidate.validate({

              ignoreTitle: true,
              debug:true,
           
              errorPlacement: function(error, element) {
              
                  error.insertAfter(element);
    

              

          },


        });
     

          if(!formValidate.valid()){
             
              message = 'Some input fields is highly required. Please write something to resume.';
              var errorInput = $(formValidate).find("input.error")[0];
              modalError(message);
            try{
              $('html, body').animate({
               //scrollTop: ($(errorInput).offset().top - 300)
          }, 2000);
            }catch(e){

            }
              

              return false;

          }


    var win = window.open('/sms/registrar/get-students/print?'+Formdata, '_blank');
    if (win) {
        //Browser has allowed it to be opened
        win.focus();
    } else {
        //Browser has blocked it
        alert('Please allow popups for this software');
    }
}
    function studentTable(Formdata){
      
      $('#studentlist').dataTable().fnClearTable();
      $("#studentlist").dataTable().fnDestroy();

          studentTableData = $('#studentlist').DataTable({
          responsive: true,
          bAutoWidth:false,

          "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            nRow.setAttribute('data-id',aData.row_id);
            nRow.setAttribute('class','ref_provider_info_class');
          },

          "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            oSettings.jqXHR = $.ajax( {
              "dataType": 'json',
              "type": "GET",
              "url": sSource,
              "data": aoData,
              "success": function (data) {
                studentList = data;
                console.log(studentList);
                fnCallback(studentList);           
              }
            });
          },
                     
          "sAjaxSource": "/sms/registrar/get-students?"+Formdata,
          "sAjaxDataProp": "",
          "iDisplayLength": 10,
          "scrollCollapse": false,
          "paging":         true,
          "searching": true,
          "columns": [
             


              { "mData": "id_no", sDefaultContent: ""},
              { "mRender" : function ( data, type, full ) { 
                        return full.last_name+ ", " + full.first_name + " " +full.middle_name; 
              }
              },
              
              { "mData": "grade_level", sDefaultContent: ""},
              { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      if(oData.schedule_id != null){
                          $(nTd).html('<a href="/sms/setup/academics/schedule/edit-schedule?schedule_id='+oData.schedule_id+'">'+oData.section_name+'</a>');
                      }
                }
              },
              { "mData": "start_time", sDefaultContent: ""},
              { "mData": "end_time", sDefaultContent: ""},
              { "mData": "gender", sDefaultContent: ""},
              { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      
                      if(oData.schedule_id == null){
                          $(nTd).html('<label class="text-warning">NOT ENROLLED</label>');
                      }else{
                          $(nTd).html('<label class="text-info">ENROLLED</label>');
                      }
                  }
              },
              { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<button data-url="/sms/registrar/remove-section/'+oData.seminar_id+'" class="btn red-sunglo btn-block btn-sm laddaRemove" data-seminar-id="'+oData.seminar_id+'" id="ladda"><i class="fa fa-trash"></i> REMOVE</button>');
                }
              },  

              { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a href="/admin/student/'+oData.student_id+'/edit" class="btn green-meadow btn-block btn-sm"><i class="fa fa-pencil-square"></i> EDIT</a>');
                }
              },
              { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a href="/admin/student/print-info?student_id='+oData.student_id+'" class="btn blue-madison btn-block btn-sm" target="_blank"><i class="fa fa-print"></i> Print</a>');
                  }
              },
              
          ]
      });

    }


    


</script>




<?php $__env->stopSection(); ?>
<?php echo $__env->make('sms.main.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>