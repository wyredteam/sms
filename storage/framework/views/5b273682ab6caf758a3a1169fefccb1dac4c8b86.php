<!DOCTYPE html>
<html>
<head>
    <link href="<?php echo e(URL::asset('/assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('/assets/css/custom/rhitsReports.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('/assets/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet">
    <link href="http://<?php echo e($_SERVER['HTTP_HOST']); ?>/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: bottom;

        }
        .payments{
            position:relative;
            margin-top:40px;
            top:0px;
        }
        h4{
            line-height:10px;
        }
        h5{
            line-height:10px;
        }
        body{
            padding-top:20px;
        }
        table{
            width: 30%;
            border:1px solid #000000;
            border-collapse: collapse;
            margin-top:20px;
	    margin-left:50px;
        }
        th,td{
            border: 1px solid #000000;
            text-align: left;
            padding-left:10px;

            font-size: 13px;
            padding-right: 10px;
        }
        .statement{
            position:relative;
            float: left;
            width: 50%;
        }
        .payment-sched{
            position: relative;
            float: left;
            width: 45%;
            left:40px;
        }
        .white{
            background-color:white !important;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #F5F5F6;
            border-bottom-width: 1px;

        }
        .border{
            border:1px solid #FFFFFF;
        }
        .billing{

            height:465px;
            margin-bottom: 190px;

        }
        .padding{
            padding-left: 10px;
        }
        .holder{
            border: 1px solid #DDDDDD;
            margin-bottom: 20px;
            padding:20px;
        }

        .bordered{
            border: 1px solid #DDDDDD;
        }
        .no-border table,td,th{
            border: 1px solid #000000;
		padding-top:4px;
		padding-bottom:4px;
        }
        .border-top{
            /*border-top: 1px solid #000000 !important;*/
            border-top: 1px;
            border-top: 1px solid ;
            border-top: medium solid #414446;
        }
        .break{
            page-break-after: always;
        }
	.yellow{
	   background-color:yellow;
	}


    </style>

    <style type="text/css" media="print">
        p.break
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
    </style>
    <title>Student Assessment</title>
</head>


<body>


<div class="page-num">Page 1</div>
<div class="body-letter col-100"  >

    <h4 class="text-center" style="color:red;">CFC - School of the Morning Star</h4>
    <h5 class="text-center">Villa Kananga Road,Butuan City</h5>

                 <h4 class="text-center">List of Students with Balance</h4>

    <h3 style="margin-left:50px;">Section: <?php echo e($secs->section_name); ?></h3>
    <table class="no-border">
		<tr>

			<th colspan="2" style="text-align:center;">Names</th>
			
		</tr>
	<?php $counter = 1; ?>
    <?php foreach($billing as $bill): ?>
            <?php $bal_due = 0; $bal=0;  ?>

            <?php foreach($bill->getStudentBill as $student_bill): ?>
                <?php if($student_bill->getBilling->getFees->getCategory->title == 'Assessment'): ?>
                        <?php $bal = $bal+$student_bill->bal; ?>
                <?php endif; ?>
            <?php endforeach; ?>


                <?php if($bal > 0): ?>

                    <tr class=" <?php if($bill->promise > 0): ?> yellow <?php endif; ?>" >
                       	<th><?php echo e($counter); ?>.</th>
                        <th><?php echo e(ucfirst($bill->getStudent->first_name)); ?> <?php echo e(ucfirst($bill->getStudent->last_name)); ?></th>
		

		<?php $counter++; ?>	              
</tr>

		
                <?php endif; ?>

<?php endforeach; ?>

                </table>
</body>

</html>

