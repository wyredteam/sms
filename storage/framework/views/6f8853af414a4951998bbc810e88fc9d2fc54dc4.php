<?php $__env->startSection('css_filtered'); ?>
<?php echo $__env->make('admin.csslinks.css_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<style type="text/css">
	input{
		text-transform: none !important;
	}
</style>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>


<div > 	
	<div class="col-md-12 ">	
		
		<div class="col-md-12">
  <div class="wyred-box-header">
    <h3 class="wyred-box-title"><i class="fa fa-user"></i> Account Management</h3>
  </div>
  <div class="wyred-box-body">
  <div class="row">

     	<div class="col-lg-12">
                <div class="tabs-container">

                    <div class="tabs-left">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-6">Account</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-7"></a></li>
                        </ul>
                        <div class="tab-content ">
                            <div id="tab-6" class="tab-pane active">
                                <div class="panel-body">


						<div class="col-md-12" style="margin-bottom: 30px">
					        <a data-toggle="modal" onclick="resetForm()" data-target="#modal-roles" class="btn btn-primary btn-rounded pull-right" href="#"> + Add New Account</a>
					    </div>

					    <div class="col-md-12">
					        <div class="table-responsive">
					            <table id="accountTable" class="table table-striped table-bordered table-hover dataTables-example" >
					            <thead>
					            <tr>
					                <th>Employee</th>
					                <th>Email</th>
					                <th>Role Name</th>
					                <th>Expiration Date</th>
					                <th>Edit</th>
					                <th>Remove</th>
					            </tr>
					            </thead>
					            <tbody>
					            <tr>
					                <td></td>
					                <td></td>
					                <td></td>
					                <td></td>
					                <td></td>
					                <td></td>
					            </tr>
					            
					            </tbody>
					            <tfoot>
					            <tr>
					                <th>Employee</th>
					                <th>Email</th>
					                <th>Role Name</th>
					                <th>Expiration Date</th>
					                <th>Edit</th>
					                <th>Remove</th>
					              </tr>
					            </tfoot>
					            </table>
					            </div>
					    </div>

                                            
	</div>      
	</div>         
</div>
</div>
</div>      
	</div>         
</div>
</div>
</div>
</div>
</div>





<div class="modal fade draggable-modal mo-z drag-me" id="modal-roles" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Role Creation</h4>
      </div>
      <div class="modal-body">
        <form id="accountForm">
      
        		<form id="accountRegistration" >
					<div class="registration-form">  

						<p class="text-success"><?php if(isset($message)){ echo $message; } ?></p>
					    <h4>Registration Form</h4>
		                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" />
		                <input type="hidden" name="user_id" id="user_id" />

					    <div class="form-group">
					      	<label>Employee</label>
					      	<select class="form-control" id="employee_id" name="employee_id" required>
					      		<option></option>
					      		<?php foreach($employee as $employee): ?>
					      		<option value="<?php echo e($employee->employee_id); ?>"><?php echo e($employee->last_name); ?>, <?php echo e($employee->first_name); ?> <?php echo e($employee->middle_name); ?></option>
					      		<?php endforeach; ?>
					      	</select>
					    </div>

					    <div class="form-group">
					      	<label>Role</label>
					      	<select class="form-control" id="role_id" name="role_id" required>
					      		<option></option>
					      		<?php foreach($roles as $roles): ?>
					      		<option value="<?php echo e($roles->role_id); ?>"><?php echo e($roles->role_name); ?></option>
					      		<?php endforeach; ?>
					      	</select>
					    </div>

					    <div class="form-group">
					      	<label>Email</label>
					      	<input type="email" id="email" class="form-control" placeholder="Email/Username" required data-toggle="tooltip" title="This will be your username" name="email">
					    </div>

					    <div class="form-group">
					      	<label>Password</label>
					      	<input type="password"  class="form-control" placeholder="Password"  data-toggle="tooltip" title="Remember this is your password" name="password">
					    </div>

					    <div class="form-group">
					      	<label>Expiration Date</label>
					      	<input type="text" id="date_exp" class="form-control date" placeholder="Expiration Date" required data-toggle="tooltip" title="This will be your username" name="date_exp">
					    </div> 


					</div>
				</form>	

   
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button class="btn btn-info wyredModalCallback" data-toggle="modal" data-url="/admin/save/account" data-form="accountForm" data-target="#wyredSaveModal">Save Fees</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('js_filtered'); ?>
<?php echo $__env->make('admin.jslinks.js_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin.jslinks.js_datatables', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

<script type="text/javascript">
	
	$(document).ready(function(){
		accountDatable();
	});

	$('.date').datepicker({
        startView: 2,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });


    function accountDatable(){

        $('#accountTable').dataTable().fnClearTable();
        $("#accountTable").dataTable().fnDestroy();

        assesstmentTable = $('#accountTable').DataTable({
            responsive: true,
            bAutoWidth:false,

            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        accountList = data;
                        console.log(accountList);
                        fnCallback(accountList);
                    }
                });
            },

            "sAjaxSource": "/admin/security/get-accounts-list",
            "sAjaxDataProp": "",
            "iDisplayLength": 10,
            "scrollCollapse": false,
            "paging":         true,
            "searching": true,
            "columns": [

            	{ "mData": "kronos_employee.full_name", sDefaultContent: ""},
                { "mData": "username", sDefaultContent: ""},
                { "mData": "roles.role_name", sDefaultContent: ""},
                { "mData": "date_exp", sDefaultContent: ""},
                { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a  class="btn btn-info btn-block" data-toggle="modal" data-target="#modal-roles" onclick="editAccount('+iRow+')"><i class="fa fa-pencil"></i> Edit</a>');
                  }
                },
                { sDefaultContent: "" ,
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                      $(nTd).html('<a  class="btn btn-danger btn-block" target="_blank"><i class="fa fa-remove"></i> Remove</a>');
                  }
                },

            ]
        });

    }

    function editAccount(id){
    	$('#user_id').val(accountList[id].user_id);
    	$('#employee_id').val(accountList[id].employee_id);
    	$('#role_id').val(accountList[id].role_id);
    	$('#email').val(accountList[id].username);
    	$('#date_exp').val(accountList[id].date_exp);
    }

    function resetForm(){
    	$('#accountRegistration')[0].reset();
    }
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('sms.main.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>