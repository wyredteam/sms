  
<!-- HAHAHAH -->
<p></p>
<?php if($update == "false"): ?>
      <?php foreach($weekdays as $week): ?>


            <input type="hidden" name="weekdays[]"  value="<?php echo e($week->weekdays_id); ?>">
              <div class="text-center weekday-header">
                <h3><?php echo e($week->weekdays); ?></h3>
              </div>
              <div class="col-md-12" id="section_subjects" >

                  <div class="table-responsive">
                          <table id="subjectTable" class="table table-striped table-bordered table-hover " >
                          <thead>
                            <tr>
                                <th class="text-center">SUBJECT NAME</th>
                                <th class="text-center">SUBJECT TEACHER</th>
                                <th class="text-center">START TIME</th>
                                <th class="text-center">END TIME</th>
                            </tr>
                          </thead>
                          <tbody>
                 
                    
                    <?php foreach($HandleSubjectsMain as $HandleSubjects): ?>

                          <?php if(count($HandleSubjects) != 0 ): ?>

                              <?php if($HandleSubjects->weekdays_id  ==  $week->weekdays_id): ?>
                                  <tr>
                                      <td>
                                        <input type="hidden" name="assign_subject_id<?php echo e($week->weekdays_id); ?>[]" value="<?php echo e($HandleSubjects->assign_subject_id); ?>"> <?php echo e($HandleSubjects->DtAssignSubject->getSubjects->subject_name); ?></td>
                                      <td>
                                         <div class="form-group">
                                          <select disabled class="form-control input-sm" name="employee_id<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjects->assign_subject_id); ?>" style="width:240px">
                                            <?php foreach($KronosEmployee as $employee): ?>
                                            <option <?php if($HandleSubjects->employee_id == $employee->employee_id): ?> selected  <?php endif; ?> value="<?php echo e($employee->employee_id); ?>"><?php echo e($employee->last_name); ?>,<?php echo e($employee->first_name); ?> <?php echo e($employee->middle_name); ?></option>
                                            <?php endforeach; ?>
                                          </select>
                                       </div>
                                      </td>
                                      <td class="text-center">

                                        <div class="form-group">
                                              <?php echo e($HandleSubjects->start_time); ?> <?php echo e($HandleSubjects->start_time_type); ?>

                                        </div>
                                      </td>
                                      <td class="text-center">
                                          <div class="form-group">
                                              <?php echo e($HandleSubjects->end_time); ?> <?php echo e($HandleSubjects->end_time_type); ?>

                                          </div>
                                      </td>
                                  </tr>
                             
                              <?php endif; ?>
                
                          <?php endif; ?>
                  <?php endforeach; ?>

                  <!-- FOR DEFAULT VALUES -->
                  <?php foreach(App\DtAssignSubject::getUnassignedSubject($week->weekdays_id,$schedule_id)->get() as $HandleSubjectsEmpty): ?>  
                    

                                <?php /*<tr>*/ ?>
                                    <?php /*<td>*/ ?>
                                      <?php /*<input type="hidden" name="assign_subject_id<?php echo e($week->weekdays_id); ?>[]" value="<?php echo e($HandleSubjectsEmpty->assign_subject_id); ?>"> <?php echo e($HandleSubjectsEmpty->getSubjects->subject_name); ?></td>*/ ?>
                                    <?php /*<td>*/ ?>
                                       <?php /*<div class="form-group">*/ ?>
                                            <?php /*None / Not yet assigned*/ ?>
                                     <?php /*</div>*/ ?>
                                    <?php /*</td>*/ ?>
                                    <?php /*<td class="text-center">*/ ?>

                                      <?php /*<div class="form-group">*/ ?>
                                            <?php /*None / Not yet assigned*/ ?>
                                      <?php /*</div>*/ ?>
                                    <?php /*</td>*/ ?>
                                    <?php /*<td class="text-center">*/ ?>
                                        <?php /*<div class="form-group">*/ ?>
                                            <?php /*None / Not yet assigned*/ ?>
                                        <?php /*</div>*/ ?>
                                    <?php /*</td>*/ ?>
                                <?php /*</tr>*/ ?>
                              
                  <?php endforeach; ?>    

                    </tbody>
                          </table>
                        </div>
                </div>               
      <?php endforeach; ?>
            

      <?php else: ?>
          

           <?php 
                if(count($HandleSubjectsMain)  > 0){
                    $start_time_type = $HandleSubjectsMain[0]->Schedule->start_time_type;
                    $start_time = $HandleSubjectsMain[0]->Schedule->start_time;
                }else{
                    $start_time_type = "";
                    $start_time = "";
                }

                if(count($HandleSubjectsMain)  > 0){
                    $end_time_type = $HandleSubjectsMain[0]->Schedule->end_time_type;
                    $end_time = $HandleSubjectsMain[0]->Schedule->end_time;
                }else{
                    $end_time_type = "";
                    $end_time = "";
                }

                

            ?>

            <div class="col-md-12">
                     <div class="class-schedule" style="z-index: 99999">
                <div class="form-group">
                 <label>Start Time</label>
                   <label>
                    <input type="radio" <?php if($start_time_type == "AM"): ?> checked  <?php endif; ?>  value="AM" name="section_start_time_type">
                    AM 
                  </label>
                  <label>
                    <input type="radio" <?php if($start_time_type == "PM"): ?> checked  <?php endif; ?> value="PM" name="section_start_time_type" >
                    PM 
                  </label>
                  <div class="input-group clockpicker" data-autoclose="true">
                      <input type="text" class="form-control" value="<?php echo e($start_time); ?>" name="section_start_time">
                      <span class="input-group-addon">
                          <span class="fa fa-clock-o"></span>
                      </span>
                  </div>
                </div>
                 <div class="form-group">
                 <label>End Time</label>
                   <label>
                    <input type="radio" <?php if($end_time_type == "AM"): ?> checked  <?php endif; ?> value="AM" name="section_end_time_type">
                    AM 
                  </label>
                  <label>
                    <input type="radio" <?php if($end_time_type == "PM"): ?> checked  <?php endif; ?> value="PM" name="section_end_time_type">
                    PM 
                  </label>
                  <div class="input-group clockpicker" data-autoclose="true">
                      <input type="text" class="form-control" value="<?php echo e($end_time); ?>" name="section_end_time">
                      <span class="input-group-addon">
                          <span class="fa fa-clock-o"></span>
                      </span>
                  </div>
                </div>
                </div>
              </div>



        <?php foreach($weekdays as $week): ?>


            <input type="hidden" name="weekdays[]"  value="<?php echo e($week->weekdays_id); ?>">
              <div class="text-center weekday-header">
                <h3><?php echo e($week->weekdays); ?></h3>
              </div>
              <div class="col-md-12" id="section_subjects" >

                  <div class="table-responsive">
                          <table id="subjectTable" class="table table-striped table-bordered table-hover " >
                          <thead>
                            <tr>
                                <th class="text-center">ACTIONS</th>
                                <th class="text-center">SUBJECT NAME</th>
                                <th class="text-center">SUBJECT TEACHER</th>
                                <th class="text-center">START TIME</th>
                                <th class="text-center">END TIME</th>
                            </tr>
                          </thead>
                          <tbody>
                 
                    
                    <?php foreach($HandleSubjectsMain as $HandleSubjects): ?>

                          <?php if(count($HandleSubjects) != 0 ): ?>

                              <?php if($HandleSubjects->weekdays_id  ==  $week->weekdays_id): ?>
                                  <tr>
                                      <td>
                                          <input type="hidden" name="check<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjects->assign_subject_id); ?>"  value="false">
                                          <input type="checkbox" <?php if($HandleSubjects->assign_subject_id != ""): ?> checked  <?php endif; ?> name="check<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjects->assign_subject_id); ?>" value="true" class="">
                                      </td>
                                      <td>
                                        <input type="hidden" name="assign_subject_id<?php echo e($week->weekdays_id); ?>[]" value="<?php echo e($HandleSubjects->assign_subject_id); ?>"> <?php echo e($HandleSubjects->DtAssignSubject->getSubjects->subject_name); ?></td>
                                      <td>
                                         <div class="form-group">
                                          <select  class="form-control input-sm" name="employee_id<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjects->assign_subject_id); ?>" style="width:240px">
                                            <?php foreach($KronosEmployee as $employee): ?>
                                            <option <?php if($HandleSubjects->employee_id == $employee->employee_id): ?> selected  <?php endif; ?> value="<?php echo e($employee->employee_id); ?>"><?php echo e($employee->last_name); ?>,<?php echo e($employee->first_name); ?> <?php echo e($employee->middle_name); ?></option>
                                            <?php endforeach; ?>
                                          </select>
                                       </div>
                                      </td>
                                      <td class="text-center">

                                          <div class="form-group">
                                              <label>
                                          <input type="radio" <?php if($HandleSubjects->start_time_type == "AM"): ?> checked  <?php endif; ?> class="" value="AM" name="start_time_type<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjects->assign_subject_id); ?>">
                                              AM 
                                          </label>
                                          <label>
                                              <input type="radio" <?php if($HandleSubjects->start_time_type == "PM"): ?> checked  <?php endif; ?> class="" value="PM" name="start_time_type<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjects->assign_subject_id); ?>">
                                              PM 
                                          </label>
                                         
                                          <div class="input-group clockpicker" data-autoclose="true">
                                              <input type="text" class="form-control" value="<?php echo e($HandleSubjects->start_time); ?>" name="start_time<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjects->assign_subject_id); ?>">
                                              <span class="input-group-addon">
                                                  <span class="fa fa-clock-o"></span>
                                              </span>
                                          </div>
                                          </div>
                                      </td>
                                      <td class="text-center">
                                           <div class="form-group">
                                               <label>
                                            <input type="radio" <?php if($HandleSubjects->end_time_type == "AM"): ?> checked  <?php endif; ?> class="" value="AM" name="end_time_type<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjects->assign_subject_id); ?>">
                                            AM 
                                            </label>
                                            <label>
                                            <input type="radio" <?php if($HandleSubjects->end_time_type == "PM"): ?> checked  <?php endif; ?> class="" value="PM" name="end_time_type<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjects->assign_subject_id); ?>">
                                            PM 
                                            </label>
                                           
                                            <div class="input-group clockpicker" data-autoclose="true">
                                                <input type="text" class="form-control" value="<?php echo e($HandleSubjects->end_time); ?>"  name="end_time<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjects->assign_subject_id); ?>">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                                </span>
                                            </div>
                                            </div>
                                      </td>
                                  </tr>
                             
                              <?php endif; ?>
                
                          <?php endif; ?>
                  <?php endforeach; ?>

                  <!-- FOR DEFAULT VALUES -->
                  <?php foreach(App\DtAssignSubject::getUnassignedSubject($week->weekdays_id,$schedule_id)->get() as $HandleSubjectsEmpty2): ?>


                                <tr>
                                  <td>
                                          <input type="hidden" name="check<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjectsEmpty2->assign_subject_id); ?>"  value="false">
                                          <input type="checkbox"  name="check<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjectsEmpty2->assign_subject_id); ?>" value="true" class="">
                                      </td>
                                    <td>
                                      <input type="hidden" name="assign_subject_id<?php echo e($week->weekdays_id); ?>[]" value="<?php echo e($HandleSubjectsEmpty2->assign_subject_id); ?>"> <?php echo e($HandleSubjectsEmpty2->getSubjects->subject_name); ?></td>
                                    <td>
                                       <div class="form-group">
                                          <div class="form-group">
                                          <select  class="form-control input-sm" name="employee_id<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjectsEmpty2->assign_subject_id); ?>" style="width:240px">
                                            <?php foreach($KronosEmployee as $employee): ?>
                                            <option  value="<?php echo e($employee->employee_id); ?>"><?php echo e($employee->last_name); ?>,<?php echo e($employee->first_name); ?> <?php echo e($employee->middle_name); ?></option>
                                            <?php endforeach; ?>
                                          </select>
                                       </div>
                                     </div>
                                    </td>
                                    <td class="text-center">


                                           <div class="form-group">
                                               <label>
                                              <input type="radio"  class="" value="AM" name="start_time_type<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjectsEmpty2->assign_subject_id); ?>">
                                              AM
                                              </label>
                                              <label>
                                              <input type="radio"  class="" value="PM" name="start_time_type<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjectsEmpty2->assign_subject_id); ?>">
                                              PM
                                              </label>

                                              <div class="input-group clockpicker" data-autoclose="true">
                                                  <input type="text" class="form-control" value="" name="start_time<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjectsEmpty2->assign_subject_id); ?>">
                                                  <span class="input-group-addon">
                                                      <span class="fa fa-clock-o"></span>
                                                  </span>
                                              </div>
                                              </div>

                                    </td>
                                    <td class="text-center">
                                        <div class="form-group">
                                         <label>
                                        <input type="radio"  class="" value="AM" name="end_time_type<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjectsEmpty2->assign_subject_id); ?>">
                                        AM
                                        </label>
                                        <label>
                                        <input type="radio"  class="" value="PM" name="end_time_type<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjectsEmpty2->assign_subject_id); ?>">
                                        PM
                                        </label>

                                        <div class="input-group clockpicker" data-autoclose="true">
                                            <input type="text" class="form-control" value=""  name="end_time<?php echo e($week->weekdays_id); ?><?php echo e($HandleSubjectsEmpty2->assign_subject_id); ?>">
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                        </div>
                                    </td>
                                </tr>

                  <?php endforeach; ?>

                    </tbody>
                          </table>
                        </div>
                </div>               
      <?php endforeach; ?>
           

<?php endif; ?>


<script type="text/javascript">
    $('.clockpicker').clockpicker({
        defaultTime: 'value',
        minuteStep: 1,
        disableFocus: true,
        template: 'dropdown',
        showMeridian:false
    });
</script>