<?php $__env->startSection('css_filtered'); ?>
<?php echo $__env->make('admin.csslinks.css_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin.csslinks.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<link href="/assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<style type="text/css">

  .popover.bottom {
    margin-top: 10px;
    z-index: 999999;
  }
  .modal-lar{
      width: 90%;
  }
  td{
    text-align: center;
  }

  .day{
      padding:30px;
      background-color: #ffffff;
      border:1PX solid #e8e8e8;
      margin: 5px;
  }

</style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>



<div class="col-md-12" >
  <div class="portlet box wyred">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-calendar text-white"></i> ADD NEW SCHEDULE
      </div>
      <div class="tools">
            
      </div>
    </div>
    <div class="portlet-body" style="padding:30px">
      <div class="row">
       <div class="col-md-12">
              

        <form id="subject_saving_with_section">
         <div class="row">
          <div class="col-md-6">
               <div class="form-group">
                 <label>SCHOOL YEAR</label>
                  <select class="form-control input-sm" name="school_year_id">
                   <?php foreach($schoolYear as $keyVal): ?>
                    <option value="<?php echo e($keyVal->school_year_id); ?>"><?php echo e($keyVal->sy_from); ?> - <?php echo e($keyVal->sy_to); ?></option>
                   <?php endforeach; ?>
                  </select>
              </div>
               <div class="form-group">
                 <label>CLASS TYPE</label>
                  <select class="form-control input-sm" name="class_type_id" id="classType">
                    <?php foreach($classType as $keyVal): ?>
                      <option value="<?php echo e($keyVal->class_type_id); ?>"><?php echo e($keyVal->class_type); ?></option>
                     <?php endforeach; ?>
                  </select>
                </div>
               <div class="form-group">
                 <label>GRADE TYPE</label>
                  <select class="form-control input-sm gradeType" >
                     <?php foreach($gradeType as $keyVal): ?>
                      <option value="<?php echo e($keyVal->grade_type_id); ?>"><?php echo e($keyVal->grade_type); ?></option>
                     <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                 <label>GRADE LEVEL</label>
                  <select class="form-control input-sm gradelevel" data-id="grade_level_id" data-name="grade_level" data-url="/select-binder/get-gradeLevel">
                    <option ></option>
                  </select>
                </div>
               </div>
               <div class="col-md-6">
                <div class="form-group">
                 <label>SECTION NAME</label>
                  <select class="form-control input-sm sectionName" name="section_id" data-id="section_id" data-name="section_name" data-url="/select-binder/get-sectionName"> 
                    <option ></option>
                  </select>
               </div>
               <div class="form-group">
                 <label>CLASS ADVISER</label>
                  <select class="form-control input-sm kronosEmployees" name="adviser" required="">
                    <option ></option>
                    <?php foreach($employees as $employees2): ?>
                      <option value="<?php echo e($employees2->employee_id); ?>"><?php echo e($employees2->last_name); ?>, <?php echo e($employees2->first_name); ?> <?php echo e($employees2->middle_name); ?></option>
                    <?php endforeach; ?>
                  </select>
               </div>
                <div class="form-group">
                 <label>SLOT</label>
                  <input type="number" class="form-control input-sm"  name="slot" id="slot" required>
                </div>
              
              </div>

              <div class="col-md-12">
          

               <div class="class-schedule" style="z-index: 99999">
                <div class="col-md-6">
                <div class="form-group">
                  <label>TIME IN</label>
                    <div class="input-group clockpicker" data-autoclose="true">
                        <input type="text" class="form-control input-sm" value="" name="section_start_time" >
                        <span class="input-group-addon input-sm">
                            <span class="fa fa-clock-o"></span>
                        </span>
                     </div>
                  </div>
                 </div>
                 <div class="col-md-6">
                   <div class="form-group">
                    <label>TIME OUT</label>
                      <div class="input-group clockpicker" data-autoclose="true">
                          <input type="text" class="form-control input-sm" value="" name="section_end_time" >
                          <span class="input-group-addon input-sm">
                              <span class="fa fa-clock-o"></span>
                          </span>
                      </div>
                    </div>
                </div>
                </div>

              </div>
               <div class="col-md-12" id="section_subjects" style="margin-top:20px;max-height: 500px; overflow-y:scroll;background-color: #FAFAFA ">
               </div> 


        </div>
         

        <div class="col-md-12" >

            <div class="alert alert-success">
              <ul class="fa-ul">
                <li>
                  <i class="fa fa-info-circle fa-lg fa-li"></i>
                  You can drag and drop the  <code>Schedule Panel</code> to switch Schedule.
                </li>
              </ul>
            </div>

            <div class="sortable" >
              <input type="hidden" value="" name="schedule_id">

                <div class="col-md-12 day" >
                

                <div class="alert alert-info col-md-3">
                  <ul class="fa-ul">
                    <li>
                      <i class="fa fa-calendar fa-lg fa-li"></i>
                      <b>Schedule Panel</b>
                    </li>
                  </ul>
                </div>

                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                       <label>Weekdays</label>
                        <select class="form-control input-sm weekdays"  required>
                          <option></option>
                          <?php foreach($weekdays as $weekdays): ?>
                          <option value="<?php echo e($weekdays->weekdays_id); ?>"><?php echo e($weekdays->weekdays); ?></option>
                          <?php endforeach; ?>
                        </select>
                     </div>
                    </div>   


                    <div class="col-md-6">
                      <div class="form-group">
                           <a class="btn grey-cascade btn-sm  pull-right remove-day"><i class="fa fa-trash"></i> REMOVE DAY</a>
                       </div>
                    </div>   
                </div>

                   <div class="col-md-12">
                    <div class="table-scrollable">
                      <table class="table table-hover schedule-table" >
                      <thead>
                      <tr>
                        <th>SUBJECT</th>
                        <th>TEACHER</th>
                        <th>IN</th>
                        <th>OUT</th>
                        <th>ACTION</th>
                      </tr>

                      </thead>
                      <tbody id="tbody">
                      <tr>
                        <td>
                            
                            <div class="form-group">
                                <input type="hidden" class="form-control counter"   required>
                                <select class="form-control input-sm subjects"  name="subject[][]" data-id="assign_subject_id" data-name="subject_name" data-url="/select-binder/get-subjects">
                                  <option ></option>
                                </select>
                             </div>

                        </td>
                        <td>
                            
                            <div class="form-group">
                                <select class="form-control input-sm teacher" name="teacher" required>
                                  <option ></option>
                                  <?php foreach($employees as $employees): ?>
                                    <option value="<?php echo e($employees->employee_id); ?>"><?php echo e($employees->last_name); ?>, <?php echo e($employees->first_name); ?> <?php echo e($employees->middle_name); ?></option>
                                  <?php endforeach; ?>
                                </select>
                             </div>

                        </td>
                        <td>
                            
                            <div class="input-group clockpicker" data-autoclose="true" style="width:200px;margin: auto">
                                <input type="text"  class="form-control input-sm time_in" value="" name="" >
                                <span class="input-group-addon input-sm">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>

                        </td>

                        <td>
                          

                              <div class="input-group clockpicker" data-autoclose="true" style="width:200px;margin: auto">
                                <input type="text"  class="form-control input-sm time_out" value="" name="" >
                                <span class="input-group-addon input-sm">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>


                        </td>

                        <td>
                            
                            <a class="btn red-sunglo btn-sm btn-block remove-subject"><i class="fa fa-trash"></i> REMOVE</a>

                        </td>
                      </tr>
                        </tbody>
                      </table>
                    </div>  
                    </div>

                    <div class="col-md-12">
                      <div class="col-md-10">
                        <div class="form-group">
                            <a > <button class="btn btn-default btn-sm new-subject" ><i class="fa fa-plus"></i> ADD NEW SUBJECT</button></a>                        
                        </div>  
                       

                        <div class="form-group">
                            <a > <button class="btn btn-primary btn-sm clone-day" ><i class="fa fa-plus"></i> CLONE DAY</button></a>                        
                        </div>  
                      </div>
                      </div>
                    </div>
            </div>

            </div>
            </div>


          </form>


            <div class="col-md-12" style="margin-top:20px">
              <div class="col-md-12">
                <div class="form-group">
                     <button class="btn blue-madison wyredModalCallback" onclick ="addNames()" data-toggle="modal" data-url="/sms/academics/assign-subjects/to-sections" data-form="subject_saving_with_section" data-target="#wyredSaveModal"><i class="fa fa-calendar"></i> Save New Schedule</button>
                 </div>
              </div>
            </div>  




          </div>
        </div>






<?php $__env->stopSection(); ?>
<?php $__env->startSection('js_filtered'); ?>
<?php echo $__env->make('admin.jslinks.js_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin.jslinks.js_datatables', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Clock picker -->
<script src="/assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="/assets/admin/pages/scripts/table-advanced.js"></script>


<script>

function addNames(){

  
    $('.day').each(function(i, obj) {

          $(this).find('.weekdays').each(function(i, weekdays) {
              weekdays_id = this.value;
              console.log(weekdays);
              $(this).attr('name', 'weekdays['+weekdays_id+']');
          });

          $ct = 0;
          $(this).find('.counter').each(function(i, subjects) {
              
              console.log(subjects);
              $(this).attr('name', 'counter['+weekdays_id+']['+$ct+']');

              $ct++;
          });


          $(this).find('.subjects').each(function(i, subjects) {
            console.log(subjects);
            $(this).attr('name', 'subjects['+weekdays_id+'][]');
          });

          $(this).find('.teacher').each(function(i, teacher) {
            console.log(teacher);
            $(this).attr('name', 'teacher['+weekdays_id+'][]');
          });

          $(this).find('.time_in').each(function(i, time_in) {
            console.log(time_in);
            $(this).attr('name', 'time_in['+weekdays_id+'][]');
          });

          $(this).find('.time_out').each(function(i, time_out) {
            console.log(time_out);
            $(this).attr('name', 'time_out['+weekdays_id+'][]');
          });
    });

    
}
  

$(document).ready(function(){
    clock();
    removeSub();
    newSubject();
    cloneDay();
    removeDay();
    $('tbody').sortable();
    $('.sortable').sortable();
})


function cloneDay(){

    $('.clone-day').click(function(event){

        event.stopPropagation();
        event.preventDefault();
        event.stopImmediatePropagation();

        var rowSched = $(this).parents('.day').first();
        var rowClone = $(this).parents('.day').first().clone();  

        event.stopPropagation();
        event.preventDefault();
        event.stopImmediatePropagation();
        $(rowClone).insertAfter(rowSched);


        clock();
        removeSub();
        newSubject();
        cloneDay();
        removeDay();
    })
    
}

function clock(){

    var clock = $('.clockpicker').clockpicker({
        defaultTime: 'value',
        minuteStep: 1,
        disableFocus: true,
        template: 'dropdown',
        showMeridian:false,
        twelvehour: true
      
    });
}


function removeSub(){

    $('.remove-subject').click(function(){

        event.stopPropagation();
        event.preventDefault();
        event.stopImmediatePropagation();
        var rowCount = $('.schedule-table >tbody >tr').length;

          var confirm = window.confirm("Remove subject? click yes to proceed");
          if (confirm == true) {
              if(rowCount != 1){
                $(this).closest("tr").remove();
              }
          }

            
    
    })
}
function removeDay(){

    $('.remove-day').click(function(event){

        event.stopPropagation();
        event.preventDefault();
        event.stopImmediatePropagation();

        var rowCount = $('.day').length;

          var confirm = window.confirm("Remove day? click yes to proceed");
          if (confirm == true) {
              if(rowCount != 1){
                $(this).parents('.day').remove();
              }
          }

            
    
    })
}


function newSubject(){
    
    $('.new-subject').click(function(event){
        event.stopPropagation();
        event.preventDefault();
        event.stopImmediatePropagation();

        var rowSched = $(this).parents('.day').find('.schedule-table tbody tr').first();
        var rowClone = $(this).parents('.day').find('.schedule-table tbody tr').last().clone();  
        $(rowClone).insertAfter(rowSched);

        /*var row = $(this).parents('.day').find('.schedule-table tbody tr').html();


        $('#tbody').append('<tr>'+row+'</tr>');*/
        removeSub();
        clock();
    })
}




$('.gradeType').change(function(){
      var selValue = $(this).val();
      $('.gradelevel').select_binder(selValue);
});

$('.gradelevel').change(function(){
      var selValue = $(this).val();
      $('.sectionName').select_binder(selValue);
});

$('.sectionName').change(function(){
    
    $('.subjects').select_binder(this.value);

});

</script>

    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('sms.main.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>