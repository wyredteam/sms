<!DOCTYPE html>
<html>
	<head>
		<link href="<?php echo e(URL::asset('/assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
		<link href="<?php echo e(URL::asset('/assets/css/custom/rhitsReports.css')); ?>" rel="stylesheet">
		<link href="<?php echo e(URL::asset('/assets/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet">
		<link href="http://<?php echo e($_SERVER['HTTP_HOST']); ?>/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
		 <style type="text/css">

        .table > thead > tr > th {
          border-bottom: 1px solid #DDDDDD;
          vertical-align: bottom;

         }
         body{
          padding-top:20px;
         }
         table{
          width: 100%;
          border:1px solid #000000;
          border-collapse: collapse;
         }
         th,td{
          border: 1px solid #000000;
         }
         .white{
          background-color:white !important; 
         }

         .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
          background-color: #F5F5F6;
          border-bottom-width: 1px;

        }
        .padding{
          padding-left: 10px;
        }
        .holder{
          border: 1px solid #DDDDDD;
          margin-bottom: 20px;
          padding:20px;
        }

        .bordered{
          border: 1px solid #DDDDDD;
        }

    </style>
		<title>Population Report</title>
	</head>


<body>

	<div class="col-md-12" >
		<div class ="header col-md-12">
			<div class="col-md-12">
					<h3 class="t-center bold">SCHOOL OF THE MORNING STAR</h3>
					<h4 class="t-center bold">BUTUAN CITY</h4>
					<p class="t-center"></p>
					<p class="t-center"><i></i></p>
					<h4 class="text-center"><?php echo e($sy->sy_from); ?>-<?php echo e($sy->sy_to); ?> Population Enrollment Report</h4>
			</div>
		</div>

		<div class="col-md-12 body-letter" >
			<?php 

				$final_total_male = 0;
				$final_total_female = 0
			 ?>
			<?php foreach($populations as $gradetypes): ?>
			<div class=" col-md-12 bg-primary text-center" style="padding:1px"><h4><?php echo e($gradetypes->grade_type); ?></h4></div>
			<table class="table table-bordered ">
				
				<thead>
					<tr>
						<th></th>
						
						<?php foreach($gradetypes->getGradeLevel as $grade_level): ?>
						<th class="text-center"><?php echo e($grade_level->grade_level); ?> </th>
						<?php endforeach; ?>
						<th class="text-center">TOTAL: </th>
					</tr>
				</thead>

				<tbody>
					<tr>

						<td>Male</td>
							<?php $total = 0; ?>
							<?php foreach($gradetypes->getGradeLevel as $grade_level): ?>
							
								<?php 
									$male_count = 0;
								?>
								<?php foreach($grade_level->Accounts as $accounts): ?>
										<?php if(isset($accounts)): ?>
											<?php if($accounts->getStudent->gender == "Male"): ?>
													<?php $male_count++; ?>
											<?php endif; ?>
										<?php endif; ?>
								<?php endforeach; ?>

							
							<td class="white"><?php echo e($male_count); ?></td>
							<?php 
								$total += $male_count; 
							?>
							<?php endforeach; ?>
						<td class="white"><?php echo e($total); ?></td>
						<?php 
								$final_total_male += $total; 
						?>
						<?php $total = 0; ?>
					</tr>
					<tr>
						<td>Female</td>
						<?php $total = 0; ?>
						<?php foreach($gradetypes->getGradeLevel as $grade_level): ?>
							
								<?php 
									$female_count = 0;
								?>
								<?php foreach($grade_level->Accounts as $accounts): ?>
										<?php if(isset($accounts)): ?>
											<?php if($accounts->getStudent->gender == "Female"): ?>
													<?php $female_count++; ?>
											<?php endif; ?>
										<?php endif; ?>
								<?php endforeach; ?>

						<td><?php echo e($female_count); ?></td>
						<?php 
							$total += $female_count; 
						?>
						<?php endforeach; ?>
						
						<td class="white"><?php echo e($total); ?></td>
						<?php 
								$final_total_female += $total; 
						?>
						<?php $total = 0; ?>
					</tr>
					<tr>
						<td>TOTAL</td>
						<?php $total = 0; ?>
						<?php foreach($gradetypes->getGradeLevel as $grade_level): ?>
							<?php 
								$female_count = 0;
								$male_count = 0;
							?>
							<?php foreach($grade_level->Accounts as $accounts): ?>
									<?php if(isset($accounts)): ?>
										<?php if($accounts->getStudent->gender == "Female"): ?>
												<?php $female_count++; ?>
										<?php endif; ?>
									<?php endif; ?>
									
							<?php endforeach; ?>

							<?php foreach($grade_level->Accounts as $accounts): ?>
									<?php if(isset($accounts)): ?>
										<?php if($accounts->getStudent->gender == "Male"): ?>
												<?php $male_count++; ?>
										<?php endif; ?>
									<?php endif; ?>

							<?php endforeach; ?>
						<td class="white"><?php echo e($female_count + $male_count); ?></td>
						<?php 
							$total += $female_count + $male_count; 
						?>
						<?php endforeach; ?>
						<td class="white"><?php echo e($total); ?></td>
						
						<?php $total = 0; ?>
					</tr>
				</tbody>
			
			</table>
			<?php endforeach; ?>
			
			</div>
			
			<div class="col-xs-12 text-center" style="margin-top:30px">
				<div class="col-xs-12">
					<div class=" col-md-12 bg-primary text-center" style="padding:1px"><h4>SUMMARY REPORT</h4></div>

					<table class="table table-bordered">
				
							<tr>
								<th></th>
								<td>
								


								</td>
							</tr>
						<tr>
							<th class="text-center">Female</th>
							<th class="text-center"><?php echo e($final_total_female); ?></th>
						</tr>
						<tr>
							<th class="text-center">Male</th>
							<th class="text-center"><?php echo e($final_total_male); ?></th>
						</tr>
						<tr>
							<th class="text-center">TOTAL</th>
							<th class="text-center"><?php echo e($final_total_male + $final_total_female); ?></th>
						</tr>
					</table>
				</div>
			</div>	
			</div>
</body>

</html>

