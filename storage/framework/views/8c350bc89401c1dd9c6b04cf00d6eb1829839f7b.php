<?php $__env->startSection('css_filtered'); ?>
<?php echo $__env->make('admin.csslinks.css_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>



<div class="col-md-12">
  <div class="portlet box wyred">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-alt-th text-white"></i> Student Requirements
      </div>
      <div class="tools">
         <div class="pull-right" style="margin-top:-5px;">
              <button class="btn red-sunglo btn-sm " data-toggle="modal" data-target="#add-fees"><i class="fa fa-plus"></i> Add Requirements</button>
         </div>
      </div>
    </div>
    <div class="portlet-body">
      <div class="row">
       <div class="col-md-12">
           <table id="" class="table table-striped table-bordered table-hover" >
                <thead>
                  <tr>
                      <th></th>
                      <th>Student ID</th>
                      <th>Student Name</th>
                      
                  </tr>
                </thead>
                <tbody>
                 
                <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                </tr>
          
                </tbody>
          </table>
        </div>
         
     </div>
   
   
  </div>
</div>
<!-- END Portlet PORTLET-->
<div class="modal fade draggable-modal mo-z drag-me" id="add-fees" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Students Requirement</h4>
      </div>
      <div class="modal-body">
        <form id="setupSubject">
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                 <label>Grade Type</label>
                  <select class="form-control input-sm">
                    <option>1</option>
                    <option>2</option>
                  </select>
                </div>
                <div class="form-group">
                 <label>Grade Level</label>
                  <select class="form-control input-sm">
                    <option>1</option>
                    <option>2</option>
                  </select>
                </div>
               <div class="form-group">
                 <label>Student Name</label>
                  <select class="form-control input-sm">
                    <option>1</option>
                    <option>2</option>
                  </select>
                </div>
                <div class="form-group">
                 <label>Requirements</label>
                  <select class="form-control input-sm">
                    <option>1</option>
                    <option>2</option>
                  </select>
                </div>
          </div>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button class="btn btn-info wyredModalCallback" data-toggle="modal" data-url="/sms/registrar/save-subject" data-form="setupSubject" data-target="#wyredSaveModal">Save Requirement</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js_filtered'); ?>
<?php echo $__env->make('admin.jslinks.js_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin.jslinks.js_datatables', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Clock picker -->
<script src="/assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="/assets/admin/pages/scripts/table-advanced.js"></script>


<script>
 $(document).ready(function(){

     $('#setupMenu').addClass('active');
     $('#academicsMenu').addClass('active');
     $('#Schedule').addClass('active');




     $(".drag-me").draggable({
       handle: ".modal-header"
      });

    $('#customizeSched').hide();
    $('#setEmpFlexySched').hide();

    $('#fixedOption').change(function(){
      if(this.checked == true){
         $('#customizeSched').hide();
         $('#fixedSched').show();
      }else {
         $('#customizeSched').show();
         $('#fixedSched').hide();
      }
    });

     $('#customizeOption').change(function(){
      if(this.checked == true){
         $('#customizeSched').show();
         $('#fixedSched').hide();
      }else {
         $('#customizeSched').hide();
         $('#fixedSched').show();
      }
    });

    $('#schedType').change(function(){
      if(this.value == '1'){
         $('#setEmpFlexySched').show();
      }else if(this.value == '2'){
        $('#setEmpFlexySched').hide();
      }
    });

      $('.clockpicker').clockpicker({
        defaultTime: 'value',
        minuteStep: 1,
        disableFocus: true,
        template: 'dropdown',
        showMeridian:false
    });

    $('.customize .input-group.date').datepicker({
      startView: 2,
      todayBtn: "linked",
      keyboardNavigation: false,
      forceParse: false,
      autoclose: true
    });

    $('.default .input-group.date').datepicker({
    format: "mm/dd"
    });
  

 });
</script>

    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('sms.main.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>