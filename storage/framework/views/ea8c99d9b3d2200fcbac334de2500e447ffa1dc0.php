<!DOCTYPE html>
<html>
<head>
    <link href="<?php echo e(URL::asset('/assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('/assets/css/custom/rhitsReports.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('/assets/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet">
    <link href="http://<?php echo e($_SERVER['HTTP_HOST']); ?>/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: bottom;

        }
        body{
            padding-top:20px;
        }
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
        }
        th,td{
            border: 1px solid #000000;
            border-top: 1px solid #000000;
        }
        .white{
            background-color:white !important;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #F5F5F6;
            border-bottom-width: 1px;

        }
        .padding{
            padding-left: 10px;
        }
        .holder{
            border: 1px solid #DDDDDD;
            margin-bottom: 20px;
            padding:20px;
        }

        .bordered{
            border: 1px solid #DDDDDD;
        }

    </style>
    <title>Population Report</title>
</head>


<body>


<div class="page-num">Page 1</div>
<div class="body-legal col-100" >
    <div class ="header col-md-12">
        <div class="col-md-12">
            <h3 class="t-center bold">SCHOOL OF THE MORNING STAR</h3>
            <h4 class="t-center bold">BUTUAN CITY</h4>
            <p class="t-center"></p>
            <p class="t-center"><i></i></p>
            <h3 class="text-center"><?php echo e($sy->sy_from); ?>-<?php echo e($sy->sy_to); ?> - ASSESSMENTS</h3>
        </div>
    </div>

    <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-xs-12">
            <?php foreach($bill as $gradelevel): ?>
            <div class="col-xs-4">
                <h3><?php echo e($gradelevel->grade_level); ?></h3>
                <table class="table">
                    <tr>
                        <th>FEES</th>
                        <th>AMOUNT</th>
                    </tr>
                    <?php $total = 0; ?>
                    <?php foreach($gradelevel->getBilling()->where('school_year_id',$sy_id)->get() as $fee): ?>
                    <?php if($fee->getFees->getCategory->title == 'Assessment'): ?>
                     <?php $total = $total + $fee->amount; ?>
                    <tr>
                         <td><?php echo e($fee->getFees->title); ?></td>
                         <td><?php echo e($fee->amount); ?></td>
                     </tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    <tr>
                        <th>TOTAL</th>
                        <th class="text-center"><?php echo e(number_format($total,2,'.',',')); ?></th>
                    </tr>
                </table>
            </div>
            <?php endforeach; ?>
        </div>
    </div>

</body>

</html>

