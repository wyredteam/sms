<?php $__env->startSection('css_filtered'); ?>
<?php echo $__env->make('admin.csslinks.css_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<link href="/assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="/assets/css/plugins/switchery/switchery.css" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="col-md-12">
  <div class="wyred-box-header">
    <h3 class="wyred-box-title"><i class="fa fa-rocket"></i> SECURITY MENUS</h3>
  </div>
  <div class="wyred-box-body">
  <div class="row">

          <a href="/admin/security/security-modules">
            <div class="col-md-3" >
                   <div class="widget yellow-bg p-lg text-center" style="height:220px">
                          <div class="m-b-md text-success">
                              <i class="fa fa-shield fa-4x"></i>
                              <h1 class="m-xs">Security Modules</h1>
                              
                              <small>Here, the developer creates unique permissions in your system.</small><br/>
                              <small>Access Level : Developer </small>
                          </div>
                    </div>
            </div>
          </a>

          <a href="/admin/security/security-roles">
          <div class="col-md-3" >
                 <div class="widget red-bg p-lg text-center text-success" style="height:220px">
                        <div class="m-b-md">
                            <i class="fa fa-warning fa-4x"></i>
                            <h1 class="m-xs">Roles</h1>
                            
                            <small>Every Account has a specific roles in order to have an access to specific modules. </small><br/>
                        </div>
                  </div>
          </div>
          </a>

          <a href="/admin/security/security-permissions">
          <div class="col-md-3" >
                 <div class="widget navy-bg p-lg text-center text-success" style="height:220px">
                        <div class="m-b-md">
                            <i class="fa fa-key fa-4x"></i>
                            <h1 class="m-xs">Permissions</h1>
                            
                            <small>This module is created for account restriction and allowing or disallowing specific accounts to have an access to the system.</small><br/>
                        </div>
                  </div>
          </div>
          </a>


          <a href="/admin/security/security-account">
          <div class="col-md-3" >
                 <div class="widget lazur-bg p-lg text-center text-success" style="height:220px">
                        <div class="m-b-md">
                            <i class="fa fa-users fa-4x"></i>
                            <h1 class="m-xs">Account Management</h1>
                            
                            <small>Module for account creation.</small><br/>
                        </div>
                  </div>
          </div>
          </a>

  </div>
</div>

</div>



<?php $__env->stopSection(); ?>
<?php $__env->startSection('js_filtered'); ?>
<?php echo $__env->make('admin.jslinks.js_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('sms.main.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>