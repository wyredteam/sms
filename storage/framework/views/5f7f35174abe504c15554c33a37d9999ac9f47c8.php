<?php $__env->startSection('css_filtered'); ?>
    <?php echo $__env->make('admin.csslinks.css_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
    <style>
        .bills{
            background: #5d91dd;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="row border-bottom white-bg dashboard-header">
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content animated fadeIn">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">

                                    <div class="ibox-tools">
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="panel panel-default">
                                                <div class="panel-heading red-sunglo">
                                                   GENERATE BALANCES
                                                </div>
                                                <div class="panel-body">
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="sy">GRADE LEVEL</label>
                                                            <select class="form-control input-sm input-sm gradelevel" id="grade-level" required name="grade_level" required>
                                                                <option></option>
                                                                <?php foreach($levels as $level): ?>
                                                                    <option value="<?php echo e($level->grade_level_id); ?>"><?php echo e($level->grade_level); ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>SECTION</label>
                                                            <select class="form-control input-sm input-sm sectionName edit_section_id" id="section_field" name="sectionName" required  data-id="section_id" data-name="section_name" data-url="/select-binder/get-sectionName" >

                                                                <option value=""></option>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12">
                                                        <div class="hr-line-dashed"></div>
                                                        <div class="col-sm-3 pull-right">
                                                            <button type="button" class="btn btn-block btn-outline btn-primary" id="print"><i class="fa fa-print"></i> Print W/ Balance</button>
 <button type="button" class="btn btn-block btn-outline btn-primary" id="print-name"><i class="fa fa-print"></i> Print Names</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js_filtered'); ?>
    <?php echo $__env->make('admin.jslinks.js_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('admin.jslinks.js_datatables', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <script>
        $('#grade-level').on('change',function(){
            $.get('/bind/select-section?grade='+ $(this).val(),function(data){
                $('#section_field').find('option').remove();
                $('#section_field').append($('<option>', {
                    value: '',
                    text: ''
                }));
                $(data).each(function(){
                    $('#section_field').append($('<option>', {
                        value: this.section_id,
                        text: this.section_name
                    }));
                });
            });
        });

        $('#section_field').on('change',function(){

            $.get('/bind/select-student?section='+ $(this).val(),function(data){
                $('#student_field').find('option').remove();
                $('#student_field').append($('<option>', {
                    value: 'All',
                    text: 'All'
                }));
                $(data.student_schedule).each(function(){
                    $('#student_field').append($('<option>', {
                        value: this.students.student_id,
                        text: this.students.last_name+", "+this.students.first_name
                    }));
                });
            });
        });

        $('#print').on('click',function(){
            var url = 'section_name='+$('#section_field').val();
            window.open('/print/student-balance-list?'+url,'_blank');
        });
$('#print-name').on('click',function(){
            var url = 'section_name='+$('#section_field').val();
            window.open('/print/student-balance-list-name?'+url,'_blank');
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('sms.main.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>