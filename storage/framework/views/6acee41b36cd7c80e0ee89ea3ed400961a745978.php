<?php $__env->startSection('css_filtered'); ?>
    <?php echo $__env->make('admin.csslinks.css_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<style>
    .bills{
        background: #5d91dd;
    }
    .acc:hover{
        cursor: pointer;
    }

</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    
          
        <div class="col-md-12" style="margin-top:20px;">
             
            <div class="portlet box wyred">
               
       
                <div class="portlet-body">
                <form id="journalForm">
                <div class="row">
                <!-- <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info-circle text-white"></i>Journal Entry
                    </div>
                    <div class="actions">
                     <div class="form-group">
                       <label>JOURNAL ENTRIES</label>
                        <select class="form-control input-sm" name="syFrom" id="syFrom">
                            <option>08/18/2016</option>
                        </select>
                      </div>
                    </div>

                </div> -->
                 <div class="col-md-4">
                     
                     <div class="form-group date">
                      <label>DATE</label>
                      <div class="input-group date">
                        <span class="input-group-addon input-sm">
                         <i class="fa fa-calendar"></i>
                        </span>
                        <input type="text" class="form-control input-sm" name="date" id="syFrom"  required="">
                      </div>
                      </div>
                       <div class="form-group">
                       <label>REFERENCE</label>
                        <input type="text" class="form-control input-sm" name="reference" id="syFrom" required="">
                      </div>
                 </div>  
                <div class="col-md-12">
                   <table id="" class="table" style="margin-top:10px" >
                        <thead>
                          <tr>
                              <th class="text-center">Accounts</th>
                              <th class="text-center">Description</th>
                              <th class="text-center">Debit</th>
                              <th class="text-center">Credit</th>
                              <th class="text-center">Job</th>
                              <th class="text-center">Action</th>
                          </tr>
                        </thead>
                        <tbody id="journal-body">
                         
                        <tr class="tr_clone" >
                                <td style="width:20%">
                                      <div class="input-group">

                                        <input type="text" class="form-control input-sm ac" id="ac1"  name="account_c[]" maxlength="6" required="">

                                        <a class="input-group-addon input-sm search-btn ab" id="1"  data-toggle="modal" data-target="#account-list">
                                         <i class="fa fa-search"></i>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" class="form-control input-sm ad" id="ad1" name="account_desc[]"  required="">
                                </td>
                                <td>
                                    <input type="text" class="form-control input-sm debs mask-money" onkeyup="addThis()" name="debit[]" id="account_desc">
                                </td>
                                <td>
                                    <input type="text" class="form-control input-sm creds mask-money" onkeyup="addThis()"  name="credit[]" id="credit">
                                </td>
                                <td>
                                    <input type="text" class="form-control input-sm" name="job[]" id="job">
                                </td>
                                <td>
                                    <a class="btn red-sunglo btn-sm pull-right del"> <i class="fa fa-minus"></i></a>
                                </td>
                        </tr>

                        <tr>
                            <th colspan="1"></th>
                            <td class=""><b style="padding-left:100px">TOTAL</b></td>
                            <td class="totaldebit"></td>
                            <td class="totalcredit"></td>
                            <td class=""></td>
                            <td>

                                <a class="btn green-meadow btn-sm add-journal"> Add Row <i class="fa fa-plus"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="1"></th>
                            <td class=""><b style="padding-left:25px">OUT OF BALANCE</b></td>
                            <td class="">0.00</td>
                            <td class="">0.00</td>
                        </tr>
                        </tbody>
                  </table>

                
                    

                 </div>

                    </div>
                    </form>
                    <button class="btn blue-madison pull-right wyredModalCallback" data-toggle="modal" data-url="/sms/setup/billing/save-journal" data-form="journalForm" data-target="#wyredSaveModal"> Save Entry <i class="fa fa-cube"></i></button>
                </div>
            </div>
            
            
        </div>
        

        <!-- END Portlet PORTLET-->


<div class="modal fade draggable-modal mo-z drag-me" id="account-list"  role="dialog"   aria-hidden="true" data-backdrop="" data-keyboard="false">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Account List</h4>
      </div>
      <div class="modal-body">

   

           <div class="row">
             <div class="col-md-12">
                <table id="AccountTable" class="table table-striped table-hover" style="margin-top:10px" >
                        <thead>
                          <tr>
                              <th id="head-acc">Account No</th>
                              <th id="head-desc">Description</th>
                              <th>Account Type</th>
                          </tr>
                        </thead>
                        <tbody class="accountBody">
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                  </table>
             </div>
           </div>
          
        
    
      </div>
   
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>





        <?php $__env->stopSection(); ?>
        <?php $__env->startSection('js_filtered'); ?>
            <?php echo $__env->make('admin.jslinks.js_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('admin.jslinks.js_datatables', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
            <!-- Clock picker -->
            <script src="/assets/js/plugins/clockpicker/clockpicker.js"></script>
            <script src="/assets/admin/pages/scripts/table-advanced.js"></script>


            <script type="text/javascript">

                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                $('#journal-body').on('click','tr .del',function(e){
                    e.preventDefault();
                    $(this).closest('tr').remove();
                    addThis();
                });

                function addThis(){
                    var totalcred = 0;
                    var totaldeb = 0;

                    $('.creds').each(function(){
                       var cred = $(this).val();
                            cred = cred.replace(',','');
                        totalcred = Number(totalcred) + Number(cred);
                    });
                    $('.debs').each(function(){
                        var deb = $(this).val();
                        deb = deb.replace(',','');
                        totaldeb = Number(totaldeb) + Number(deb);

                    });

                    $('.totaldebit').html((totaldeb).format(2,3));
                    $('.totalcredit').html((totalcred).format(2,3));

                }

                $('.search-btn').click(function(){
                    var id = $(this).attr('id');

                    $('#head-acc').attr('class',id+'1');
                    $('#head-desc').attr('class',id+'2');

                });


                

            var global_obj;
            var unique_id = 2;
              $('.add-journal').click(function(){


                  var $tr    = $('#journal-body tr').first();
                  
                  var $clone = $tr.clone(true,true);

                  $clone.find(':text').val('');

                  $($clone).find('.search-btn').attr('id',unique_id);
                  $($clone).find('.ac').attr('id',"ac"+unique_id);
                  $($clone).find('.ad').attr('id',"ad"+unique_id);

                  console.log($clone);
                  $tr.before($clone);
                  $('.mask-money').maskMoney();
                  $('.search-btn').click(function(){

                      var id = $(this).attr('id');
                      $('#head-acc').attr('class',id+'1');
                      $('#head-desc').attr('class',id+'2');

                      global_obj = $(this).attr('id');


                  });

                  unique_id++;
                  

              });

              $('.search-btn').click(function(){
                  global_obj = $(this).attr('id');
              });



                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };


                $(document).ready(function(){


                    accountTableFunc();


                    $(".drag-me").draggable({
                        handle: ".modal-header"
                    });
                    $('.checks').fadeOut();
                    $('#customizeSched').hide();
                    $('#setEmpFlexySched').hide();

                    $('#fixedOption').change(function(){
                        if(this.checked == true){
                            $('#customizeSched').hide();
                            $('#fixedSched').show();
                        }else {
                            $('#customizeSched').show();
                            $('#fixedSched').hide();
                        }
                    });

                    $('#customizeOption').change(function(){
                        if(this.checked == true){
                            $('#customizeSched').show();
                            $('#fixedSched').hide();
                        }else {
                            $('#customizeSched').hide();
                            $('#fixedSched').show();
                        }
                    });

                    $('#schedType').change(function(){
                        if(this.value == '1'){
                            $('#setEmpFlexySched').show();
                        }else if(this.value == '2'){
                            $('#setEmpFlexySched').hide();
                        }
                    });

                    $('.clockpicker').clockpicker({
                        defaultTime: 'value',
                        minuteStep: 1,
                        disableFocus: true,
                        template: 'dropdown',
                        showMeridian:false
                    });

                    $('.date .input-group.date').datepicker({
                        startView: 2,
                        todayBtn: "linked",
                        keyboardNavigation: false,
                        forceParse: false,
                        autoclose: true
                    });

                    $('.default .input-group.date').datepicker({
                        format: "mm/dd"
                    });


                });




              function getThisAcc(row){

                  var desc = $('#'+global_obj).closest('.tr_clone').find('.ad').attr('id');
                  var account = $('#'+global_obj).closest('.tr_clone').find('.ac').attr('id');
                  
                  $('#'+account).val(accountTabledata[row].account_code);
                  $('#'+desc).val(accountTabledata[row].account_desc);
           
                
                  $('#account-list').modal('hide');
                  console.log(global_obj);

              }


              function accountTableFunc(){



                  $('#AccountTable').dataTable().fnClearTable();
                  $("#AccountTable").dataTable().fnDestroy();

                  var accountTable = $('#AccountTable').DataTable({
                      responsive: true,
                      bAutoWidth:false,

                      "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                          nRow.setAttribute('data-id',aData.row_id);
                          nRow.setAttribute('id',aData.row_id);
                          nRow.setAttribute('class','ref_provider_info_class acc');
                          nRow.setAttribute('onclick','getThisAcc('+aData.row_id+')');
                      },

                      "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                          oSettings.jqXHR = $.ajax( {
                              "dataType": 'json',
                              "type": "GET",
                              "url": sSource,
                              "data": aoData,
                              "success": function (data) {
                                  accountTabledata = data;
                                  console.log(accountTabledata);
                                  fnCallback(accountTabledata);
                              }
                          });
                      },

                      "sAjaxSource": "/sms/setup/billing/get-account-list",
                      "sAjaxDataProp": "",
                      "iDisplayLength": 10,
                      "scrollCollapse": false,
                      "paging":         true,
                      "searching": true,

                      "columns": [


                          { "mData": "account_code", sDefaultContent: "", "className": "code"},
                          { "mData": "account_desc", sDefaultContent: "", "className": "desc"},
                          { "mData": "get_account_type.acccount_type", sDefaultContent: "","className":"type"},
                      ]
                  });

              }



            </script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('sms.main.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>