<?php $__env->startSection('css_filtered'); ?>
    <?php echo $__env->make('admin.csslinks.css_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>



    <div class="col-md-12">
        <div class="portlet box wyred">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar text-white"></i> Teacher's Schedule
                </div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-5" style="margin-bottom:20px;">
                        <form method="get" action="/sms/reports/generate-masterlist">
                            <div class="form-group">
                                <label for="">Select Teacher:</label>
                                <select name="teacher" id="teacher" class="form-control select2">
                                    <?php foreach($teachers as $teacher): ?>
                                        <option value="<?php echo e($teacher->employee_id); ?>"><?php echo e($teacher->last_name); ?>, <?php echo e($teacher->first_name); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </form>
                        <div class="form-group">
                            <button class="btn blue-madison" id="submit"><i class="fa fa-copy"></i> Get Schedule</button>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-6">
                            <h4>Teacher's Schedule</h4>
                        </div>
                         <div class="col-xs-6">
                             <a class="btn red-sunglo pull-right" id="print-sched" target="_blank" href=""><i class="fa fa-print"></i> Print Preview</a>
                         </div>
                      <table class="table table-bordered">
                          <tr>
                              <th colspan="6">Advisory:</th>

                          </tr>
                          <tr>
                              <td>Section:</td>
                              <th class="section"></th>
                              <td>Start Time:</td>
                              <th class="start-time"></th>
                              <td>End Time</td>
                              <th class="end-time"></th>
                          </tr>
                      </table>

                        <table class="table table-bordered" id="schedule">
                            <thead>
                                <tr>
                                    <th>Day</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                    <th>Subject</th>
                                    <th>Section</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php $__env->stopSection(); ?>
        <?php $__env->startSection('js_filtered'); ?>
            <?php echo $__env->make('admin.jslinks.js_crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('admin.jslinks.js_datatables', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <script src ="/assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
            <!-- Clock picker -->
            <script src="/assets/admin/pages/scripts/table-advanced.js"></script>
            <script>
                function changeGradeLevel(){

                    var selValue = $('#gradeType').val();
                    $('#gradeLevels').select_binder(selValue);
                }
                $('.gradelevel').change(function(){
                    var selValue = $(this).val();
                    $('.sectionName').select_binder(selValue);
                    $('.sectionName').append('<option val="All">All</option>');
                    getSubjectsWithFilters();//call change of subjects
                });

                $('#submit').click(function(){
                    var emp_id = $('#teacher').val();
                    $('.section').html('');
                    $('.start-time').html('');
                    $('.end-time').html('');
                    teacherSched(emp_id);
                    getAdvisory(emp_id);
                    $('#print-sched').attr('href','/sms/registrar/print-teacher-sched?teacher_id='+emp_id);
                });
                function getAdvisory(emp_id){
                    $.get('/sms/registrar/get-advisory?teacher_id='+emp_id, function(data){
                        $(data).each(function(){
                            $('.section').html(this.rf_section.section_name);
                            $('.start-time').html(this.start_time);
                            $('.end-time').html(this.end_time);
                        });
                    });
                }

                function teacherSched(emp_id){

                    $('#schedule').dataTable().fnClearTable();
                    $("#schedule").dataTable().fnDestroy();

                    assesstmentTable = $('#schedule').DataTable({
                        responsive: true,
                        bAutoWidth:false,

                        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                            nRow.setAttribute('data-id',aData.row_id);
                            nRow.setAttribute('class','ref_provider_info_class');
                        },

                        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                            oSettings.jqXHR = $.ajax( {
                                "dataType": 'json',
                                "type": "GET",
                                "url": sSource,
                                "data": aoData,
                                "success": function (data) {
                                    studentList = data;
                                    console.log(studentList);
                                    fnCallback(studentList);
                                }
                            });
                        },

                        "sAjaxSource": "/sms/registrar/get-teacher-schedule?teacher_id="+emp_id,
                        "sAjaxDataProp": "",
                        "iDisplayLength": 10,
                        "scrollCollapse": false,
                        "paging":         true,
                        "searching": true,
                        "columns": [



                            { "mData": "weekdays", sDefaultContent: ""},
                            { "mData": "start_time", sDefaultContent: ""},
                            { "mData": "end_time", sDefaultContent: ""},
                            { "mData": "subject_name", sDefaultContent: ""},
                            { "mData": "section_name", sDefaultContent: ""},

                        ]
                    });

                }

            </script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('sms.main.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>