<!DOCTYPE html>
<html>
<head>
    <link href="<?php echo e(URL::asset('/assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('/assets/css/custom/rhitsReports.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('/assets/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet">
    <link href="http://<?php echo e($_SERVER['HTTP_HOST']); ?>/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: bottom;

        }
        ul{
            list-style-type: none;
        }
        .payments{
            position:relative;
            margin-top:20px;
            top:0px;
        }
	h4{
	line-height:10px;
	}
	h5{
	line-height:10px;
	}
        body{
            padding-top:20px;
        }
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
            margin-top:20px;
        }
        th,td{
            border: 1px solid #000000;
            text-align: left;
            padding-left:10px;
            
            font-size: 13px;
            padding-right: 10px;
        }
        .statement{
            position:relative;
            float: left;
            width: 50%;
        }
        .payment-sched{
            position: relative;
            float: left;
            width: 45%;
            left:40px;
        }
        .white{
            background-color:white !important;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #F5F5F6;
            border-bottom-width: 1px;

        }
        .border{
            border:1px solid #FFFFFF;
        }
        .billing{
            height:465px;
            margin-bottom: 40px;
        }
        .padding{
            padding-left: 10px;
        }
        .holder{
            border: 1px solid #DDDDDD;
            margin-bottom: 20px;
            padding:20px;
        }

        .bordered{
            border: 1px solid #DDDDDD;
        }
        .no-border table,td,th{
            border: 1px solid #ffffff;
        }
        .border-top{
            /*border-top: 1px solid #000000 !important;*/
            border-top: 1px;
            border-top: 1px solid ;
            border-top: medium solid #414446;
        }
        td{
            text-transform: uppercase;
        }
        .head-start { 
            page-break-inside: avoid;
        }
        .table>tbody>tr>td{
            border-top: 0px;
        }
    </style>
    <title>Billing Balances</title>
</head>


<body>


<div class="body-letter col-100"  >

    <div class ="header col-md-12">
            <div class="col-md-12">
                    <h3 class="t-center bold">SCHOOL OF THE MORNING STAR</h3>
                    <h4 class="t-center bold">BUTUAN CITY</h4>
                    <p class="t-center"></p>
                    <p class="t-center"><i></i></p>
            </div>
        </div>
    <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="col-xs-6">
                <h5>Grade Level : <?php echo e($data[0]->RfSection->getGradeLevel->grade_level); ?></h5>
                <h5>Section: <?php echo e($data[0]->RfSection->section_name); ?></h5>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-12 border billing">


                    <h4 class="text-center">Student Balances</h4>
                <table class="table table-responsive ">
                    
                    <?php foreach($billing as $bill): ?>
                       
                                <tbody  style="margin-bottom:30px">
                            
                                    
                                    <tr >
                                        <tr >
                               
                                            
                                            <td colspan="5" >
                                                <?php echo e(ucfirst($bill->getStudent->first_name)); ?> <?php echo e(ucfirst($bill->getStudent->last_name)); ?>

                                            </td>
                                        </tr>
                                        <tr>
                                        <td class="head-start" >
                                                
                                                <?php foreach($bill->getStudentBill as $student_bill): ?>
                                                    <?php if($student_bill->getBilling->getFees->getCategory->title == 'Assessment'): ?>
                                                            <td class="text-left">
                                                                            
                                                                        <label><?php echo e($student_bill->getBilling->getFees->title); ?></label>
 
                                                            </td>

                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                
                                        </td>
                                        </tr>
                                        <td >
                                                <?php $counter = 0;?>
                                                <?php $bal_due = 0; $bal=0; ?>

                                                <?php foreach($bill->getStudentBill as $student_bill): ?>
                                                    <?php if($student_bill->getBilling->getFees->getCategory->title == 'Assessment'): ?>
                                                            <?php $bal_due = $bal_due + $student_bill->balance_due; $bal = $bal+$student_bill->bal; ?>
                                                            
                                                            <td class="text-left ">
                                                                            

                                                                        <?php if($student_bill->bal > 0): ?>

                                                                            <label class="text-danger text-center">P<?php echo e(number_format($student_bill->bal,2,'.',',')); ?></label>
                                                                        <?php else: ?>
                                                                            <label >P<?php echo e(number_format($student_bill->bal,2,'.',',')); ?></label>
                                                                        <?php endif; ?>
                                                                            
                                                                        
                                                                        
                                                                        
                                                            </td>

                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                        </td>
                                        
                                    </tr>

                                    <tr>
                                        <td colspan="1"></td>
                                        <td >total bal.</td>
                                        <td class="bold" colspan="7">P<?php echo e(number_format($bal,2,'.',',')); ?></td>
                                        
                                    </tr>

                                </tbody>

                    <?php $counter++; ?>
                  
                   

                    <?php endforeach; ?>


                </table>    
              	 
           
		  
            </div>
            </div>
	   
    </div>

    

</body>

</html>

