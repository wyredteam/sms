<!DOCTYPE html>
<html>
<head>
    <link href="<?php echo e(URL::asset('/assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('/assets/css/custom/rhitsReports.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('/assets/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet">
    <link href="http://<?php echo e($_SERVER['HTTP_HOST']); ?>/assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    <style type="text/css">

        .table > thead > tr > th {
            border-bottom: 1px solid #DDDDDD;
            vertical-align: bottom;

        }
        .payments{
            position:relative;
            margin-top:20px;
            top:0px;
        }
        body{
            padding-top:20px;
        }
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
            margin-top:20px;
        }
        th,td{
            border: 1px solid #000000;
            text-align: left;
            padding-left:10px;
            padding-top:2px;
            padding-bottom: 2px;
            font-size: 15px;
            padding-right: 10px;
        }
        .statement{
            position:relative;
            float: left;
            width: 50%;
        }
        .payment-sched{
            position: relative;
            float: left;
            width: 45%;
            left:5px;
        }
        .white{
            background-color:white !important;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #F5F5F6;
            border-bottom-width: 1px;

        }
        .border{
            border:2px solid #000000;
        }
        .padding{
            padding-left: 10px;
        }
        .holder{
            border: 1px solid #DDDDDD;
            margin-bottom: 20px;
            padding:20px;
        }

        .bordered{
            border: 1px solid #DDDDDD;
        }


    </style>
    <title>Student Assessment</title>
</head>


<body>


<div class="page-num">Page 1</div>
<div class="body-legal col-100" >
    <div class ="header col-md-12">
        <div class="col-md-12">
            <h3 class="t-center bold">SCHOOL OF THE MORNING STAR</h3>
            <h4 class="t-center bold">BUTUAN CITY</h4>
            <p class="t-center"></p>
            <p class="t-center"><i></i></p>
            <h3 class="text-center">ASSESSMENT</h3>
        </div>
    </div>
    <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-xs-12">
            <table>
                <tr>
                    <td width="15%">NAME:</td>
                    <td><b><?php echo e($student->first_name); ?> <?php echo e($student->middle_name[0]); ?>. <?php echo e($student->last_name); ?></b></td>
                    <td width="15%">School Year:</td>
                    <td><b><?php echo e($sy->sy_from); ?>-<?php echo e($sy->sy_to); ?></b></td>
                </tr>
                <tr>
                    <td>Grade Level:</td>
                    <td><b>
                        <?php foreach($student->getStudentSchedule as $sched): ?>
                            <?php echo e($sched->getSchedule->RfSection->section_name); ?>

                        <?php endforeach; ?>
                        </b></td>
                    <td>Section:</td>
                    <td><b><?php foreach($student->getStudentSchedule as $sched): ?>
                            <?php echo e($sched->getSchedule->RfSection->getGradeLevel->grade_level); ?>

                        <?php endforeach; ?></b></td>
                </tr>
            </table>
            <table class="statement">
                <tr>
                    <th colspan="2">Statement of Account</th>
                </tr>
                <tr>
                    <th>Fees</th>
                    <th>Amount</th>
                </tr>
                <?php foreach($assessment as $assesst): ?>
                 <tr>
                     <td><?php echo e($assesst->title); ?></td>
                     <td class="text-right"><?php echo e(number_format($assesst->t_amount,2,'.',',')); ?></td>
                 </tr>
                <?php endforeach; ?>
                <tr>
                    <th class="text-center">TOTAL</th>
                    <td class="text-right">P<?php echo e(number_format($total,2,'.',',')); ?></td>
                </tr>
            </table>
            <table class="payment-sched">
                <tr>
                    <th colspan="4">Payment Schedule</th>
                </tr>
                <tr>
                    <th>Fees</th>
                    <th>Schedule</th>
                    <th>Number of Months</th>
                    <th>Amount</th>
                </tr>
                <tr>
                    <td>Tuition</td>
                    <td>Monthly</td>
                    <td class="text-center">10</td>
                    <td class="text-right"><?php echo e(number_format($tui,2,'.',',')); ?></td>
                </tr>
                <tr>
                    <td>Miscellaneous</td>
                    <td>Monthly</td>
                    <td class="text-center">5</td>
                    <td class="text-right"><?php echo e(number_format($misc,2,'.',',')); ?></td>
                </tr>
            </table>
                <div class="col-xs-12 text-center" style="margin-top:20px;"><h4>Assessment Fees</h4></div>
            <table class="payments">
                <tr>

                    <th colspan="4">Payments</th>
                </tr>
                <tr>
                    <th>Date of Payment</th>
                    <th>Fees</th>
                    <th>OR No.</th>
                    <th>Amount</th>
                </tr>
                <?php foreach($payment_assessment as $payment): ?>
                    <tr>
                        <td><?php echo e($payment->date_of_payment); ?></td>
                        <td><?php echo e($payment->payment_option); ?>-<?php echo e($payment->title); ?></td>
                        <td><?php echo e($payment->or_no); ?></td>
                        <td class="text-right"><?php echo e(number_format($payment->amount,2,'.',',')); ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <th colspan="3" class="text-right">Remaining Balance:</th>
                    <th class="text-right"><?php echo e(number_format($total_bal,2,'.',',')); ?></th>
                </tr>
            </table>
            <div class="col-xs-12 text-center" style="margin-top:20px;"><h4>Non-Assessment Fees</h4></div>
            <table class="payments">
                <tr>

                    <th colspan="4">Payments</th>
                </tr>
                <tr>
                    <th>Date of Payment</th>
                    <th>Fees</th>
                    <th>OR No.</th>
                    <th>Amount</th>
                </tr>
                <?php foreach($payment_non_assessment as $payment): ?>
                    <tr>
                        <td><?php echo e($payment->date_of_payment); ?></td>
                        <td><?php echo e($payment->payment_option); ?>-<?php echo e($payment->title); ?></td>
                        <td><?php echo e($payment->or_no); ?></td>
                        <td class="text-right"><?php echo e(number_format($payment->amount,2,'.',',')); ?></td>
                    </tr>
                <?php endforeach; ?>

            </table>
        </div>

    </div>

</body>

</html>

