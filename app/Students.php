<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    protected $table = 'dt_students';
    protected $primaryKey = 'student_id';
    public $incrementing = false;
    protected $appends = array('id_no','full_name');


    public function getbirthdayAttribute($value) {
        $value = $value;
        $birthday = date("m/d/Y", strtotime($value));
        return $birthday;
    }

    public function getAdjustment(){
        return $this->hasMany('App\AdjustmentBilling','student_id','student_id');
    }
    public function getStudentSchedule(){
        return $this->hasMany('App\StudentSchedule','student_id','student_id');
    }


    public function Parents_Students(){
    	return $this->hasMany('App\Parents_Students','student_id','student_id');
    }

    public function setbirthdayAttribute($value) {
        $birthday = date("Y/m/d", strtotime($value));
        $this->attributes['birthday'] = strtolower($birthday);
    }

    public function getGender($gender = null){
        return  $this->where('gender',$gender)->count();
       
    }
    public function getidNoAttribute($student_id){

        $student_id = sprintf("%06d",$this->attributes['student_id']);
        $final_student_id = $this->batch_id."-".$student_id;
        return  $final_student_id;
       
    }
 
    
    public function getfullNameAttribute(){
            return $this->last_name. ", ".$this->first_name." ".$this->middle_name;
    }
                                            
                                    
    public function getAccounts(){
        return $this->hasMany('App\DtAccounts','student_id','student_id');
    }
}
