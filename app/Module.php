<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'rf_modules';
    protected $primaryKey = 'module_id';
    protected $fillable = ['module_name'];
}
