<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DtSyMonthTemplate extends Model
{
    //
    protected $table = "dt_sy_month_template";
    protected $primaryKey = "template_id";

    public function getMonth(){
        return $this->belongsTo('App\RfMonth','month_id','month_id');
    }
}
