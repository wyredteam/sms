<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RfPaymentOption extends Model
{
    //
    protected $table = "rf_payment_option";
    protected $primaryKey = "payment_option_id";
}
