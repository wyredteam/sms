<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DtOtherPayment extends Model
{
    //
    use SoftDeletes;
    protected $table = 'dt_other_payment';
    protected $primaryKey = 'other_payment_id';

    public function getFees(){
        return $this->belongsTo('App\RfFees','fees_id','fees_id');
    }
    public function getPaymentOption(){
        return $this->belongsTo('App\RfPaymentOption','payment_option_id','payment_option_id');
    }
}
