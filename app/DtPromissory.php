<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DtPromissory extends Model
{
    //
    protected $table = 'dt_promissory';
    protected $primaryKey = 'promissory_id';

    public function getpromisedDateAttribute($value) {
        $value = $value;
        $promised_date = date("m/d/Y", strtotime($value));
        return $promised_date;
    }
    public function setpromisedDateAttribute($value) {
        $promised_date = date("Y/m/d", strtotime($value));
        $this->attributes['promised_date'] = strtolower($promised_date);
    }
    public function getFees(){
        return $this->belongsTo('App\RfFees','fees_id','fees_id');
    }
    public function setamountAttribute($value) {
        $price = str_replace( ',', '', $value );
        $this->attributes['amount'] = $price;
    }
}
