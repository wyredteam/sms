<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentSchedule extends Model
{
    protected $table = 'dt_students_schedule';
    protected $primaryKey = 'student_schedule_id';
    protected $appends = array('id_no');

    public function Students(){
        return $this->belongsTo('App\Students','student_id','student_id')->orderby('last_name','asc');
    }

    public function getSchedule(){
        return $this->belongsTo('App\Schedule','schedule_id','schedule_id');
    }

    

    public function getidNoAttribute($student_id){

        $student_id = sprintf("%06d",$this->student_id);
        $final_student_id = $this->Students->batch_id."-".$student_id;
        return  $final_student_id;
       
    }
}
