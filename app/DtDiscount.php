<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DtDiscount extends Model
{
    //
    protected $table = "dt_discount";
    protected $primaryKey = "discount_id";

    public function getFees(){
        return $this->belongsTo('App\RfFees','fees_id','fees_id');
    }
}
