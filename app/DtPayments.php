<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DtPayments extends Model
{
    //
    protected $table = "dt_payments";
    protected $primaryKey = "payment_id";

    public function getFees(){
        return $this->belongsTo('App\RfFees','fees_id','fees_id');
    }
    public function getAccount(){
        return $this->belongsTo('App\DtAccounts','account_id','account_id');
    }
    public function getPaymentOption(){
        return $this->belongsTo('App\RfPaymentOption','payment_option_id','payment_option_id');
    }
    public function setamountAttribute($value) {
        $price = str_replace( ',', '', $value );
        $this->attributes['amount'] = $price;
    }
    public function getBank(){
	return $this->belongsTo('App\RfBank','bank_id','bank_id');
	}
}
