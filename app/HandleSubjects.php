<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HandleSubjects extends Model
{
    use SoftDeletes;

    protected $table = 'dt_handle_subject';
    protected $primaryKey = 'handle_subject_id';


    public function setstartTimeAttribute($value) {
       
        $type = substr($value, -2);
        $time_input = substr_replace($value, "", -2);
        $final_format = $time_input." ".$type;
        $date = \DateTime::createFromFormat( 'H:i A', $final_format);
        $formatted = $date->format( 'H:i:s');
        $this->attributes['start_time'] = $formatted;
    }

    public function setendTimeAttribute($value) {

        $type = substr($value, -2);
        $time_input = substr_replace($value, "", -2);
        $final_format = $time_input." ".$type;
        $date = \DateTime::createFromFormat( 'H:i A', $final_format);
        $formatted = $date->format( 'H:i:s');
        $this->attributes['end_time'] = $formatted;
    }

    public function getstartTimeAttribute($value) {

        $startTime = date('g:i A', strtotime($value));
        return $startTime;
    }

    public function getstartTimeTypeAttribute() {

        $startTimeType = date('A', strtotime($this->attributes['start_time']));
        return $startTimeType;
    }

    public function getendTimeTypeAttribute() {

        $endTimeType = date('A', strtotime($this->attributes['end_time']));
        return $endTimeType;
    }


    public function getendTimeAttribute($value) {
        $endTime = date('g:i A', strtotime($value));
        return $endTime;
    }

    



    public function DtAssignSubject(){
        return $this->belongsTo('App\DtAssignSubject','assign_subject_id','assign_subject_id');
    }

    public function Schedule(){
        return $this->belongsTo('App\Schedule','schedule_id','schedule_id');
    }
    public function getEmployee(){
        return $this->belongsTo('App\KronosEmployee','employee_id','employee_id');
    }
    public function Weekdays(){
        return $this->belongsTo('App\Weekdays','weekdays_id','weekdays_id');
    }


}
