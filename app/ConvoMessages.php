<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConvoMessages extends Model
{
    protected $table = 'ms_messages';
    protected $primaryKey = 'message_id';

   	public function User(){
        return $this->belongsTo('App\User','user_id','user_id');
    }

    public function UserFrom(){
        return $this->belongsTo('App\User','users_from','user_id');
    }

    public function UserTo(){
        return $this->belongsTo('App\User','to','user_id');
    }
}
