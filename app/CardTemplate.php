<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardTemplate extends Model
{
    //
    protected $table = "dt_card_template";
    protected $primaryKey = "template_id";

}
