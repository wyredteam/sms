<?php namespace App\Wyred\Security\ModuleRestriction;

use App\Http\Requests;
use App\Http\Controllers\controller_retrieve;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Request;
use Fpdf;
use Log;

//custom
use datatableFormat;
use Validator;
//=====data=====

use App\User;
use App\SubModule;
use App\Permissions;
use Auth;
use DB;
use Session;

Class WRestrict{
	
	public function hasAccess($sub_id){
		
		$logUser = Auth::user();
		$Permissions = Permissions::where('sub_module_id',$sub_id)
								->where('role_id',$logUser->role_id)
								->get();

		if(count($Permissions) > 0){
			return true;
		}else{
			return false;
		}
	}
	
}


?>