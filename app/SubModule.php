<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubModule extends Model
{
    protected $table = 'dt_sub_modules';
    protected $primaryKey = 'sub_module_id';
    protected $fillable = ['sub_module_name'];

    public function Module(){
        return $this->belongsTo('App\Module','module_id','module_id');
    }
}
