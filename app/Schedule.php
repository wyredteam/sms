<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'dt_schedule';
    protected $primaryKey = "schedule_id";

    public function setstartTimeAttribute($value) {
  
        $type = substr($value, -2);
        $time_input = substr_replace($value, "", -2);
        $final_format = $time_input." ".$type;
        $date = \DateTime::createFromFormat( 'H:i A', $final_format);
        $formatted = $date->format( 'H:i:s');
        $this->attributes['start_time'] = $formatted;
    }

    public function setendTimeAttribute($value) {
        
        $type = substr($value, -2);
        $time_input = substr_replace($value, "", -2);
        $final_format = $time_input." ".$type;
        $date = \DateTime::createFromFormat( 'H:i A', $final_format);
        $formatted = $date->format( 'H:i:s');
        $this->attributes['end_time'] = $formatted;
    }

    public function getstartTimeAttribute($value) {
        $startTime = date('g:i A', strtotime($value));
        return $startTime;
    }

    public function getendTimeAttribute($value) {
        $endTime = date('g:i A', strtotime($value));
        return $endTime;
    }

    public function getstartTimeTypeAttribute() {

        $startTimeType = date('A', strtotime($this->attributes['start_time']));
        return $startTimeType;
    }

    public function getendTimeTypeAttribute() {

        $endTimeType = date('A', strtotime($this->attributes['end_time']));
        return $endTimeType;
    }

    public function RfSection(){
        return $this->belongsTo('App\RfSection','section_id','section_id');
    }

    public function RfGradeLevel(){
        return $this->belongsTo('App\RfGradeLevel','grade_level_id','grade_level_id');
    }
    public function RfSchoolYear(){
        return $this->belongsTo('App\RfSchoolYear','school_year_id','school_year_id');
    }

    public function StudentSchedule(){
        return $this->hasMany('App\StudentSchedule','schedule_id','schedule_id');
    }
    public  function getAdviser(){
        return $this->belongsTo('App\KronosEmployee','employee_id','employee_id');
    }
    public function getHandleSubjects(){
        return $this->hasMany('App\HandleSubjects','schedule_id','schedule_id');
    }
    
}

