<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdjustBilling extends Model
{
    //
    protected $table = 'adjust_billing';
    protected  $primaryKey = 'adjust_billing_id';

    public function getMonth(){
        return $this->belongsTo('App\RfMonth','month_id','month_id');
    }
}
