<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DtAccounts extends Model
{
    //
    protected $table = "dt_accounts";
    protected $primaryKey = "account_id";
    protected $fillable = array('adviser', 'st','et');



    public function getPayments(){
        return $this->hasMany('App\DtPayments','account_id','account_id');
    }
    public function getStudentBill(){
        return $this->hasMany('App\DtStudentBill','account_id','account_id');
    }
    public function getStudent(){
        return $this->belongsTo('App\Students','student_id','student_id');
    }
    public function getSchoolYear(){
        return $this->belongsTo('App\RfSchoolYear','school_year_id','school_year_id');
    }
    public function getGradeLevel(){
        return $this->belongsTo('App\RfGradeLevel','grade_level_id','grade_level_id');
    }
    public function getDiscount(){
        return $this->hasMany('App\DtDiscount','account_id','account_id');
    }
    public function getPromissory(){
	return $this->hasMany('App\DtPromissory','account_id','account_id');
    }
}
