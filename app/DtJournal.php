<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DtJournal extends Model
{
    //
    protected $table = 'dt_journal';
    protected $primaryKey = 'journal_id';


    public function getAccount(){
        return $this->belongsTo('App\RfAccount','account_code','account_code');
    }

    public function setdebitAttribute($value) {
        $price = str_replace( ',', '', $value );
        $this->attributes['debit'] = $price;
    }
    public function setcreditAttribute($value) {
        $price = str_replace( ',', '', $value );
        $this->attributes['credit'] = $price;
    }
}
