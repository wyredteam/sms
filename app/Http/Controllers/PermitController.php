<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\RfGradeLevel;
use App\RfSchoolYear;
use Illuminate\Support\Facades\Input;
use Pdf;
use App\RfFees;
use App\RfSection;
use App\DtDiscount;
use App\RfMonth;
use App\AdjustBilling;
use App\DtSyMonthTemplate;
use App\DtPayments;
use App\DtAccounts;
use DB;
use App\DtPromissory;
use App\Schedule;
class PermitController extends Controller
{
    //
    public function index(){
    		$levels = RfGradeLevel::orderBy('grade_level')->get();
    	return view('sms.billing.permit',compact('levels'));
    }
    public function selectSection(){
    	$sy = RfSchoolYear::Current();
    	$grade_level = Input::get('grade');

    	$sections = RfSection::whereHas('Schedule',function($q) use ($sy){
    		$q->where('school_year_id',$sy->school_year_id);
    	})->where('grade_level_id',$grade_level)->get();

    	return $sections;
    }

    public function getStudents(){
    	$sy = RfSchoolYear::Current();
    	$sections = Input::get('section');

    	$sched = Schedule::with('StudentSchedule.Students')->where('section_id',$sections)->where('school_year_id',$sy->school_year_id)->first();

    	return $sched;
    }

    public function printPermit(){
    	$section = Input::get('section');
    	$student = Input::get('student');

    	$sy = RfSchoolYear::Current();
    	
    	if($student == 'All'){

    	$sched = Schedule::with('StudentSchedule.Students')->where('section_id',$section)->where('school_year_id',$sy->school_year_id)->first();
    	}
    	else{

    	$sched = Schedule::with(['StudentSchedule' => function($q) use ($student){
    		$q->where('student_id',$student);
    	}, 'StudentSchedule.Students','RfSection.getGradeLevel'])->where('section_id',$section)->where('school_year_id',$sy->school_year_id)->first();
    	}

    		
    	


	
    	 $pdf = Pdf::loadView('sms.reports.print-permit',compact('sched'));

        return $pdf->setPaper('letter')->setOrientation('landscape')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 0)
            ->setOption('margin-right',0)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }


	public function printStudentBal(){
		$request = Input::all();
		$feez = RfFees::where('fee_categories_id','8')->get();
		
		$sy = RfSchoolYear::where('is_current', '1')->first();
		$sy_id = $sy->school_year_id;
		$date_now = date('F');


		$m = RfMonth::where('month_name', $date_now)->first();
		$month_id = $m->month_id;
		$ranks = DtSyMonthTemplate::where('month_id', $month_id)->first();
		$aw = DtSyMonthTemplate::where('rankings', '<=', $ranks->rankings)->get();
		foreach ($aw as $ew) {
			$dates[] = $ew->month_id;
		}

		$date_payments = DtPayments::with('getFees.getCategory')
			->where(DB::raw('MONTH(date_of_payment)'), $dates[1])
			->get();



//        $billing = DtAccounts::with('getStudent','getStudentBill.getBilling.getFees.getCategory')
//                            ->where('school_year_id', $sy_id)
//                            ->where('grade_level_id',$grade_level_id)
//                            ->get();
		$billings = DtAccounts::query();
		$billings->with('getGradeLevel.getSection','getStudent','getStudentBill.getBilling.getFees.getCategory');
		$billings->where('dt_accounts.school_year_id', $sy_id);
		if(Input::has('student_id')){
			$billings->where('dt_students.student_id',Input::get('student_id'));
			$billings->join('dt_students','dt_accounts.student_id','=','dt_students.student_id');
			$billings->join('dt_students_schedule','dt_students.student_id','=','dt_students_schedule.student_id');
			$billings->join('dt_schedule','dt_students_schedule.schedule_id','=','dt_schedule.schedule_id');
			$billings->join('rf_section','dt_schedule.section_id','=','rf_section.section_id');
		}
		if(Input::has('section_name') != ''){

			$billings->join('dt_students','dt_accounts.student_id','=','dt_students.student_id');
			$billings->join('dt_students_schedule','dt_students.student_id','=','dt_students_schedule.student_id');
			$billings->join('dt_schedule','dt_students_schedule.schedule_id','=','dt_schedule.schedule_id');
			$billings->join('rf_section','dt_schedule.section_id','=','rf_section.section_id');
			$billings->where('dt_schedule.school_year_id',$sy_id);
			$billings->where('dt_schedule.section_id',Input::get('section_name'));
//            $billings->whereHas('getGradeLevel', function($q){
//                 $q->whereHas('getSection',function($q){
//                  return $q->where('section_id',Input::get('section_name'));
//                });
//            });
		}
		$billing = $billings->get();

		

		foreach ($billing as $bill) {

			$emp = Schedule::with('getAdviser')->where('schedule_id',$bill->schedule_id)->first();
			
			$promise_from = date('Y-m-6');
			$promise_to   = date('Y-m-d', strtotime('+1 month',strtotime($promise_from)));
			
			$bill->promise = DtPromissory::where('promised_date','>=',$promise_from)->where('promised_date','<=',$promise_to)->where('account_id',$bill->account_id)->count();
			
			$bill->adviser = $emp->getAdviser->first_name.' '.$emp->getAdviser->last_name;
			$bill->st = $emp->start_time;
			$bill->et = $emp->end_time;
			foreach ($bill->getStudentBill as $naks) {
				$total=0;
				$naks->account_iddd = $naks->account_id;
				$fees_id = $naks->getBilling->fees_id;
				$date_counter = 0;
				foreach($dates as $date){
					//$dates_payments = DtPayments::with('getFees.getCategory')
					// ->where(DB::raw('MONTH(date_of_payment)'), $date)
					// ->where('account_id',$naks->account_iddd)
					// ->where('fees_id',$fees_id)
					//     ->get();
					//  foreach($dates_payments as $paid){
					//      $total= $total + $paid->amount;
					//  }
					$date_counter++;
				}

				$dates_payments = DtPayments::with('getFees.getCategory')
					->where('account_id',$naks->account_iddd)
					->where('fees_id',$fees_id)
					->get();
				foreach($dates_payments as $paid){
					$total= $total + $paid->amount;
				}

				$discount = DtDiscount::with('getFees.getCategory')
					->where('fees_id',$fees_id)
					->where('account_id',$naks->account_iddd)
					->get();
				$dis = 0;
				if(!empty($discount)){
					foreach($discount as $count){
						$dis = $dis + $count->amount;
					}
				}


				$stud_id = DtAccounts::find($naks->account_iddd);


				if($naks->getBilling->getFees->title == 'Tuition'){


					$tuition_divider = 10;
					$ranking = 0;
					$adjustment = AdjustBilling::where('student_id',$stud_id->student_id)->where('school_year_id',$sy_id)->first();

					if($adjustment != ''){
						$rank = DtSyMonthTemplate::where('month_id',$adjustment->month_id)->first();

						$tuition_divider = $tuition_divider - ($rank->template_id - 1);
						$ranking = $rank->rankings - 3;

					}

					$date_counter = $date_counter - $ranking;
					$tuition_amount = ($naks->getBilling->amount - $dis) / $tuition_divider;

					$over_all = $tuition_amount * ($date_counter - 2);

					$naks->balance_due = ($naks->getBilling->amount - $dis) - $total;

					$bal = $over_all - $total;

					if($bal <= 0){
						$bal=0;
					}
				}
				elseif($naks->getBilling->getFees->title == 'Miscellaneous'){
					$tuition_amount = ($naks->getBilling->amount - $dis) / 10;
					$over_all = $tuition_amount * ($date_counter - 3);
					$naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
					$bal = $over_all - $total;
					if($bal <= 0){
						$bal=0;
					}
				}
				elseif($naks->getBilling->getFees->title == 'SCHOOL BUS'){
                        	$tuition_amount = ($naks->getBilling->amount - $dis) / 9;
                      		 $over_all = $tuition_amount * ($date_counter - 3);
                       		 $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                       		 $bal = $over_all - $total;
                       		 if($bal <= 0){
                       		     $bal=0;
                      			  }
                   		 }
				elseif($naks->getBilling->getFees->title == 'PTA'){
                    		    $tuition_amount = ($naks->getBilling->amount - $dis) / 3;
                     		   $over_all = $tuition_amount * ($date_counter - 3);
                     		   $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                     		   $bal = $over_all - $total;
		
                    		    if($bal <= 0){
                     		       $bal=0;
                     		   }
                  		  }
				elseif($naks->getBilling->getFees->title == 'Books'){

					$bks = DtPayments::with('getFees.getCategory')
						->where('account_id',$naks->account_iddd)
						->where('fees_id',$fees_id)
						->first();

					$amp = 0;
					if($bks != ''){
						$amp = $bks->amount;
					}
					
					$tuition_amount = ($naks->getBilling->amount - $amp - $dis) / 3;
					$over_all = $tuition_amount * ($date_counter - 3);
					$naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
					$bal = $over_all + $amp - $total;
					if($bal <= 0){
						$bal=0;
					}
				}
				elseif($naks->getBilling->getFees->title == 'Registration Fee'){

					$bks = DtPayments::with('getFees.getCategory')
						->where('account_id',$naks->account_iddd)
						->where('fees_id',$fees_id)
						->first();
					$amp = 0;
					if($bks != ''){
						$amp = $bks->amount;
					}

					$tuition_divider = 10;
					$ranking = 0;
					$adjustment = AdjustBilling::where('student_id',$stud_id->student_id)->where('school_year_id',$sy_id)->first();

					if($adjustment != ''){
						$rank = DtSyMonthTemplate::where('month_id',$adjustment->month_id)->first();

						$tuition_divider = $tuition_divider - ($rank->template_id - 1);
						$ranking = $rank->rankings - 3;

					}

					$date_counter = $date_counter - $ranking;
					$tuition_amount = ($naks->getBilling->amount - $amp - $dis) / $tuition_divider;

					$over_all = $tuition_amount * ($date_counter - 2);

					$naks->balance_due = ($naks->getBilling->amount - $dis) - $total;

					$bal = $over_all + $amp - $total;

					if($bal <= 0){
						$bal=0;
					}
				}
				elseif($naks->getBilling->getFees->title == 'Tutorial' || $naks->getBilling->getFees->title == 'TUTORIAL'){
			
			$divider = ($naks->getBilling->amount - $dis) / 1500;
			$date_minus = 8 - $divider;
			
			if($divider == 0){$divider = 8;}	
                        $tuition_amount = ($naks->getBilling->amount - $dis) / $divider;
                        $over_all = $tuition_amount * ($date_counter - (3 + $date_minus));
                        $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                        $bal = $over_all - $total;
                        if($bal <= 0){
                            $bal=0;
                        }
                    }
				else{
					$naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
					$bal = ($naks->getBilling->amount - $dis) - $total;
				}

				$total_bal = $bal - $total;

				$naks->bal = $bal;

			}



		}


		$secs = RfSection::find(Input::get('section_name'));

		$pdf = Pdf::loadView('sms.reports.generate-students-balance-list',compact('secs','date_now','billing','feez'));

		return $pdf->setPaper('letter')->setOrientation('landscape')
			->setOption('margin-bottom', 2)
			->setOption('margin-left', 2)
			->setOption('margin-right', 2)
			->setOption('margin-top', 2)
			->stream('report.pdf');
	}

	public function printBalanceNames(){
		$request = Input::all();
		$feez = RfFees::where('fee_categories_id','8')->get();
		
		$sy = RfSchoolYear::where('is_current', '1')->first();
		$sy_id = $sy->school_year_id;
		$date_now = date('F');


		$m = RfMonth::where('month_name', $date_now)->first();
		$month_id = $m->month_id;
		$ranks = DtSyMonthTemplate::where('month_id', $month_id)->first();
		$aw = DtSyMonthTemplate::where('rankings', '<=', $ranks->rankings)->get();
		foreach ($aw as $ew) {
			$dates[] = $ew->month_id;
		}

		$date_payments = DtPayments::with('getFees.getCategory')
			->where(DB::raw('MONTH(date_of_payment)'), $dates[1])
			->get();



//        $billing = DtAccounts::with('getStudent','getStudentBill.getBilling.getFees.getCategory')
//                            ->where('school_year_id', $sy_id)
//                            ->where('grade_level_id',$grade_level_id)
//                            ->get();
		$billings = DtAccounts::query();
		$billings->with('getGradeLevel.getSection','getStudent','getStudentBill.getBilling.getFees.getCategory');
		$billings->where('dt_accounts.school_year_id', $sy_id);
		if(Input::has('student_id')){
			$billings->where('dt_students.student_id',Input::get('student_id'));
			$billings->join('dt_students','dt_accounts.student_id','=','dt_students.student_id');
			$billings->join('dt_students_schedule','dt_students.student_id','=','dt_students_schedule.student_id');
			$billings->join('dt_schedule','dt_students_schedule.schedule_id','=','dt_schedule.schedule_id');
			$billings->join('rf_section','dt_schedule.section_id','=','rf_section.section_id');
		}
		if(Input::has('section_name') != ''){

			$billings->join('dt_students','dt_accounts.student_id','=','dt_students.student_id');
			$billings->join('dt_students_schedule','dt_students.student_id','=','dt_students_schedule.student_id');
			$billings->join('dt_schedule','dt_students_schedule.schedule_id','=','dt_schedule.schedule_id');
			$billings->join('rf_section','dt_schedule.section_id','=','rf_section.section_id');
			$billings->where('dt_schedule.school_year_id',$sy_id);
			$billings->where('dt_schedule.section_id',Input::get('section_name'));
//            $billings->whereHas('getGradeLevel', function($q){
//                 $q->whereHas('getSection',function($q){
//                  return $q->where('section_id',Input::get('section_name'));
//                });
//            });
		}
		$billing = $billings->get();

		

		foreach ($billing as $bill) {

			$emp = Schedule::with('getAdviser')->where('schedule_id',$bill->schedule_id)->first();
			
			$promise_from = date('Y-m-6');
			$promise_to   = date('Y-m-d', strtotime('+1 month',strtotime($promise_from)));
			
			$bill->promise = DtPromissory::where('promised_date','>=',$promise_from)->where('promised_date','<=',$promise_to)->where('account_id',$bill->account_id)->count();
			
			$bill->adviser = $emp->getAdviser->first_name.' '.$emp->getAdviser->last_name;
			$bill->st = $emp->start_time;
			$bill->et = $emp->end_time;
			foreach ($bill->getStudentBill as $naks) {
				$total=0;
				$naks->account_iddd = $naks->account_id;
				$fees_id = $naks->getBilling->fees_id;
				$date_counter = 0;
				foreach($dates as $date){
					//$dates_payments = DtPayments::with('getFees.getCategory')
					// ->where(DB::raw('MONTH(date_of_payment)'), $date)
					// ->where('account_id',$naks->account_iddd)
					// ->where('fees_id',$fees_id)
					//     ->get();
					//  foreach($dates_payments as $paid){
					//      $total= $total + $paid->amount;
					//  }
					$date_counter++;
				}

				$dates_payments = DtPayments::with('getFees.getCategory')
					->where('account_id',$naks->account_iddd)
					->where('fees_id',$fees_id)
					->get();
				foreach($dates_payments as $paid){
					$total= $total + $paid->amount;
				}

				$discount = DtDiscount::with('getFees.getCategory')
					->where('fees_id',$fees_id)
					->where('account_id',$naks->account_iddd)
					->get();
				$dis = 0;
				if(!empty($discount)){
					foreach($discount as $count){
						$dis = $dis + $count->amount;
					}
				}


				$stud_id = DtAccounts::find($naks->account_iddd);


				if($naks->getBilling->getFees->title == 'Tuition'){


					$tuition_divider = 10;
					$ranking = 0;
					$adjustment = AdjustBilling::where('student_id',$stud_id->student_id)->where('school_year_id',$sy_id)->first();

					if($adjustment != ''){
						$rank = DtSyMonthTemplate::where('month_id',$adjustment->month_id)->first();

						$tuition_divider = $tuition_divider - ($rank->template_id - 1);
						$ranking = $rank->rankings - 3;

					}

					$date_counter = $date_counter - $ranking;
					$tuition_amount = ($naks->getBilling->amount - $dis) / $tuition_divider;

					$over_all = $tuition_amount * ($date_counter - 2);

					$naks->balance_due = ($naks->getBilling->amount - $dis) - $total;

					$bal = $over_all - $total;

					if($bal <= 0){
						$bal=0;
					}
				}
				elseif($naks->getBilling->getFees->title == 'Miscellaneous'){
					$tuition_amount = ($naks->getBilling->amount - $dis) / 10;
					$over_all = $tuition_amount * ($date_counter - 3);
					$naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
					$bal = $over_all - $total;
					if($bal <= 0){
						$bal=0;
					}
				}
				elseif($naks->getBilling->getFees->title == 'SCHOOL BUS'){
                        	$tuition_amount = ($naks->getBilling->amount - $dis) / 9;
                      		 $over_all = $tuition_amount * ($date_counter - 3);
                       		 $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                       		 $bal = $over_all - $total;
                       		 if($bal <= 0){
                       		     $bal=0;
                      			  }
                   		 }
				elseif($naks->getBilling->getFees->title == 'PTA'){
                    		    $tuition_amount = ($naks->getBilling->amount - $dis) / 3;
                     		   $over_all = $tuition_amount * ($date_counter - 3);
                     		   $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                     		   $bal = $over_all - $total;
		
                    		    if($bal <= 0){
                     		       $bal=0;
                     		   }
                  		  }
				elseif($naks->getBilling->getFees->title == 'Books'){

					$bks = DtPayments::with('getFees.getCategory')
						->where('account_id',$naks->account_iddd)
						->where('fees_id',$fees_id)
						->first();

					$amp = 0;
					if($bks != ''){
						$amp = $bks->amount;
					}
					
					$tuition_amount = ($naks->getBilling->amount - $amp - $dis) / 3;
					$over_all = $tuition_amount * ($date_counter - 3);
					$naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
					$bal = $over_all + $amp - $total;
					if($bal <= 0){
						$bal=0;
					}
				}
				elseif($naks->getBilling->getFees->title == 'Registration Fee'){

					$bks = DtPayments::with('getFees.getCategory')
						->where('account_id',$naks->account_iddd)
						->where('fees_id',$fees_id)
						->first();
					$amp = 0;
					if($bks != ''){
						$amp = $bks->amount;
					}

					$tuition_divider = 10;
					$ranking = 0;
					$adjustment = AdjustBilling::where('student_id',$stud_id->student_id)->where('school_year_id',$sy_id)->first();

					if($adjustment != ''){
						$rank = DtSyMonthTemplate::where('month_id',$adjustment->month_id)->first();

						$tuition_divider = $tuition_divider - ($rank->template_id - 1);
						$ranking = $rank->rankings - 3;

					}

					$date_counter = $date_counter - $ranking;
					$tuition_amount = ($naks->getBilling->amount - $amp - $dis) / $tuition_divider;

					$over_all = $tuition_amount * ($date_counter - 2);

					$naks->balance_due = ($naks->getBilling->amount - $dis) - $total;

					$bal = $over_all + $amp - $total;

					if($bal <= 0){
						$bal=0;
					}
				}
				elseif($naks->getBilling->getFees->title == 'Tutorial' || $naks->getBilling->getFees->title == 'TUTORIAL'){
			
			$divider = ($naks->getBilling->amount - $dis) / 1500;
			$date_minus = 8 - $divider;
			
			if($divider == 0){$divider = 8;}	
                        $tuition_amount = ($naks->getBilling->amount - $dis) / $divider;
                        $over_all = $tuition_amount * ($date_counter - (3 + $date_minus));
                        $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                        $bal = $over_all - $total;
                        if($bal <= 0){
                            $bal=0;
                        }
                    }
				else{
					$naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
					$bal = ($naks->getBilling->amount - $dis) - $total;
				}

				$total_bal = $bal - $total;

				$naks->bal = $bal;

			}



		}


		$secs = RfSection::find(Input::get('section_name'));

		$pdf = Pdf::loadView('sms.reports.generate-students-balance-list-names',compact('secs','date_now','billing','feez'));

		return $pdf->setPaper('letter')->setOrientation('portrait')
			->setOption('margin-bottom', 2)
			->setOption('margin-left', 2)
			->setOption('margin-right', 2)
			->setOption('margin-top', 2)
			->stream('report.pdf');

	}
}
