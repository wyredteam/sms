<?php

namespace App\Http\Controllers;

use App\AdjustBilling;
use App\DtExpenses;
use App\DtJournal;
use App\DtOtherPayment;
use App\DtPromissory;
use App\RfAccount;
use App\RfGradeLevel;
use App\Students;
use App\StudentSchedule;
use Illuminate\Http\Request;
use DatatableFormat;
use App\Http\Requests;
use App\RfFeeCategories;
use Validator;
use App\RfFees;
use App\DtBilling;
use App\DtPaymentTypeSched;
use App\DtDueDates;
use App\RfSchoolYear;
use Illuminate\Support\Facades\DB;
use App\DtStudentBill;
use App\DtAccounts;
use App\DtPayments;
use App\RfPaymentOption;
use App\RfBank;
use Illuminate\Support\Facades\Input;
use App\DtDiscount;
class BillingController extends Controller
{
    //
    public function saveCategory(Request $request){

        if($request['category_id'] != ""){
            $id=$request['category_id'];
            $title = $request['title'];
            return $this->updateCategory($id,$title);
        }

    	$return = new rrdReturn();
    	$validator = RfFeeCategories::where('title',$request['title'])
                  ->where('deleted_at','0000-00-00')
                  ->count();
         if ($validator > 0) {
				return $return->status(false)
	                      ->message("Title Already been Created!.")
	                      ->show();
         }
         else{
         	$title = new RfFeeCategories;
         	$title->title = $request['title'];
         	$title->save();
         }
		return $return->status(true)
	                      ->message("That is amazing! User has been Created!.")
	                      ->show();
    }

    public function saveFees(Request $request){

        if($request['fee_id'] != ""){
            $id=$request['fee_id'];
            $title = $request['title'];
            $description = $request['description'];
            $account_code = $request['account'];
            $category = $request['category'];
            return $this->updateFee($id,$title,$description,$account_code,$category);
        }
    	$return = new rrdReturn();
    	$checker = RfFees::where('title',$request['title'])
    						->where('description',$request['description'])
    						->where('fee_categories_id',$request['category'])
    						->where('account_code',$request['account'])
                ->where('deleted_at','=','0000-00-00')
    						->count();

    		if($checker > 0){
    			return $return->status(false)
	                      ->message("Fee Already been Created!.")
	                      ->show();
    		}
    		else{
    			$new = new RfFees;
    			$new->title 	  = $request['title'];
    			$new->description = $request['description'];
    			$new->fee_categories_id = $request['category'];
    			$new->account_code = $request['account'];
    			$new->save();

    			return $return->status(true)
	                      ->message("Fee has been Created!.")
	                      ->show();
    		}				

    }

    public function saveGradeFees(Request $request){


        $sy = RfSchoolYear::where('is_current','1')->first();
        $sy_id = $sy->school_year_id;

        if($request['billing_id'] != ""){
              $id = $request['billing_id'];
              $grade_level = $request['grade_level'];
              $fees_id = $request['fees'];
              $amount = $request['amount'];
              return $this->updateGradeFees($id,$grade_level,$fees_id,$amount);
        }

    		$return = new rrdReturn();
    		$checker = DtBilling::where('fees_id',$request['fees'])
    							->where('grade_level_id',$request['grade_level'])
                    ->where('school_year_id',$sy_id)                      
    								->count();

    		
$checker = 0;
if($checker > 0){
    				return $return->status(false)
	                      ->message("Grade Level Fee Has Already Been Created!.")
	                      ->show();
    		}
    		else{
    			$new = new DtBilling;
    			$new->amount = $request['amount'];
    			$new->fees_id = $request['fees'];
    			$new->grade_level_id = $request['grade_level'];
                $new->school_year_id = $sy_id;
    			$new->save();

    			return $return->status(true)
	                      ->message("Successfull!.")
	                      ->show();

    		}

    }
    public function updateGradeFees($id,$grade_level,$fees_id,$amount){
        $sy = RfSchoolYear::where('is_current','1')->first();
        $sy_id = $sy->school_year_id;
        $return = new rrdReturn();
          $checker = DtBilling::where('fees_id',$fees_id)
                    ->where('grade_level_id',$grade_level)
                    ->where('school_year_id',$sy_id) 
                    ->where('billing_id','!=',$id)
                    ->where('school_year_id',$sy_id)
                    ->count();
        if($checker > 0){
            return $return->status(false)
                        ->message("Grade Level Fee Has Already Been Created!.")
                        ->show();
        }
        else{
          $update = DtBilling::find($id);
          $update->amount = $amount;
          $update->fees_id = $fees_id;
          $update->grade_level_id = $grade_level;
          $update->save();

          return $return->status(true)
                        ->message("Successfull!.")
                        ->show();

        }

    }
    public function getCategory(){
    		$category = RfFeeCategories::all();

    		$datatableFormat = new DatatableFormat();
      	return $datatableFormat->format($category);
    }
      public function getFees(){
    		$category = RfFees::with('getAccount','getCategory')->get();

    		$datatableFormat = new DatatableFormat();
      	return $datatableFormat->format($category);
    }
    public function getGradeFees(){

            $sy = RfSchoolYear::where('is_current','1')->first();
            $sy_id = $sy->school_year_id;
            $grade_id = Input::get('grade_id');

            $categories= DtBilling::query();
    		$categories->with('getFees.getCategory','getGrade.getGradeType');
            if($grade_id != 'null'){
                $categories->where('grade_level_id',$grade_id);
            }
            $categories->where('school_year_id',$sy_id);
            $category = $categories->get();

    		$datatableFormat = new DatatableFormat();
      	return $datatableFormat->format($category);
    }


    public function recallFees(){

        $gradeFee = DtBilling::where('school_year_id',Input::get('sy_recall'))->get();
        $current = RfSchoolYear::where('is_current','1')->first();

        foreach($gradeFee as $fees){
            $new = new DtBilling();
            $new->amount = $fees->amount;
            $new->fees_id = $fees->fees_id;
            $new->grade_level_id =  $fees->grade_level_id;
            $new->school_year_id = $current->school_year_id;
            $new->save();
        }

        $return = new rrdReturn();
        return $return->status(true)
            ->message("Fees has been recall.")
            ->show();
    }


    public function updateCategory($id,$title){
        $return = new rrdReturn();
        $checker  = RfFeeCategories::where('title',$title)->count();
         if ($checker >0) {
                return $return->status(false)
                          ->message("Fee Already been Created!.")
                          ->show();
         }
         else{
            $categ = RfFeeCategories::where("fee_categories_id",$id)->update(['title'=> $title]);
           
            return $return->status(true)
                          ->message("Successfull Updated!.")
                          ->show();
         }

    }

    public function updateFee($id,$title,$description,$account_code,$category){
             $return = new rrdReturn();
            $checker = RfFees::where('title',$title)
                            ->where('description',$description)
                            ->where('fee_categories_id',$category)
                            ->where('account_code',$account_code)
                            ->count();

        $checker = 0;


            if($checker > 0){
                return $return->status(false)
                          ->message("Fee Already been Created!.")
                          ->show();
            }
            else{
                $update = RfFees::where('fees_id',$id)
                        ->update(['title'=>$title,'description'=>$description,
                            'fee_categories_id'=> $category, 
                            'account_code'=> $account_code]);
                        return $return->status(true)
                          ->message("Successfull Updated!.")
                          ->show();
            }
    }
    public function savePaymentSched(Request $request){
          $return = new rrdReturn();
          $delete = DtPaymentTypeSched::truncate();
          foreach ($request['month'] as $month) {
              $month = explode('/', $month);
              $new = new DtPaymentTypeSched;
              $new->month_id = $month[0];
              $new->payment_type_id = $month[1];
              $new->save();  
          }

         return $return->status(true)
                          ->message("Successfully Save!.")
                          ->show(); 

    }

  public function saveDueDates(Request $request){
       $return = new rrdReturn();
      $delete = DtDueDates::truncate();
       $month = $request['month'];
       $counter = 0;
       foreach ($request['dues'] as $dues) {
          $new = new DtDueDates;
          $new->month_id  = $month[$counter];
          $new->due_dates = $dues;
          $new->save();
          $counter++;
       }

        return $return->status(true)
                          ->message("Successfully Save!.")
                          ->show(); 

  }
    public  function  getStudentInfo(Request $request){

//        $this->autoGenerateBill($request['student_id']);

        $sy = RfSchoolYear::where('is_current','1')->first();
        $sy_id = $sy->school_year_id;
        $id = $request['student_id'];

        $sched = StudentSchedule::with('Students','getSchedule.RfSection.getGradeLevel')
                                  ->whereHas('getSchedule',function($q){
                                      $sy = RfSchoolYear::where('is_current','1')->first();
                                      $sy_id = $sy->school_year_id;
                                        $q->where('school_year_id',$sy_id);
                                    })
                                    ->where('student_id',$id)
                                    ->first();
        return $sched;
    }

    public function getOverallBalance(){

        $sy=RfSchoolYear::where('is_current','1')->first();


        $accounts = DtAccounts::with(['getStudentBill' => function($q){
            $q->whereHas('getBilling.getFees.getCategory',function($query){
                $query->where('title','Assessment');
            });
        },'getStudentBill.getBilling.getFees.getCategory','getSchoolYear'])->where('student_id',Input::get('student_id'))
            ->where('school_year_id','!=',$sy->school_year_id)
            ->get();

        foreach($accounts as $acc){
                $overall = 0;
            foreach($acc->getStudentBill as $bill){

                $amount = $bill->getBilling->amount;
                $payment = DtPayments::where('account_id',$acc->account_id)->where('fees_id',$bill->getBilling->getFees->fees_id)->sum('amount');
                $discount = DtDiscount::where('account_id',$acc->account_id)->where('fees_id',$bill->getBilling->getFees->fees_id)->sum('amount');
                $total = $amount - $payment - $discount;
                $overall = $overall + $total;
                if($total > 0){
                    $bill->balance = $total;
                }
            }
            $acc->overall_bal = $overall;
        }


        return $accounts;

    }

  public function getAssessments(Request $request){
        $id = $request['student_id'];
        $sy = RfSchoolYear::where('is_current','1')->first();
             $sy_id = $sy->school_year_id;

      $option = RfPaymentOption::all();
      $sched = StudentSchedule::with('Students','getSchedule.RfSection.getGradeLevel')
              ->whereHas('getSchedule',function($q){
              $sy = RfSchoolYear::where('is_current','1')->first();
              $sy_id = $sy->school_year_id;
              $q->where('school_year_id',$sy_id);
               })
            ->where('student_id',$id)
            ->first();

        $student_accounts = DtAccounts::where('student_id',$id)->where('school_year_id',$sy_id)->first();
        if($student_accounts != ""){
            $account_id = $student_accounts->account_id;
        }
      else{
          $account_id='';
      }

        $billing = DtAccounts::with('getStudentBill.getBilling.getFees.getCategory')
                                ->where('account_id',$account_id)
                                ->first();

        foreach($billing->getStudentBill as $aw){

            $fees = $aw->getBilling->fees_id;
            $total = 0;
            $bal = DtPayments::with('getFees.getCategory')->where('fees_id',$fees)->where('account_id',$account_id)->get();

            $discount = DtDiscount::with('getFees.getCategory')
                ->where('fees_id',$fees)
                ->where('account_id',$account_id)
                ->get();
            $dis = 0;
            if(!empty($discount)){
                foreach($discount as $count){
                    $dis = $dis + $count->amount;
                }
            }

            foreach($bal as $balance){
                $total = $total + $balance->amount;
            }

            $amount_bal                 = $aw->getBilling->amount - $dis;
            $total_balance              = $amount_bal - $total;

            if($aw->getBilling->getFees->getCategory->title == "Non-Assessment"){
                $aw->getBilling->bal    = 0.0001;
            }

            else{
                $aw->getBilling->bal    = $total_balance;
            }


            $aw->getBilling->total      = $total;
            $aw->getBilling->dis_amount = $amount_bal;

      
              
        }
        if($sched == ''){
            $billing = '';
        }
      $bills[0] = $billing;
      $bills[1] = $option;
      return $bills;
  }
    public function generateBill(Request $request){


        $id = $request['student_id'];
        $return = new rrdReturn();
        $sy = RfSchoolYear::where('is_current','1')->first();
            $sy_id = $sy->school_year_id;

        $student = StudentSchedule::with('Students','getSchedule.RfSection.getGradeLevel')
          ->whereHas('getSchedule',function($q){
              $sy = RfSchoolYear::where('is_current','1')->first();
              $sy_id = $sy->school_year_id;
              $q->where('school_year_id',$sy_id);
          })
          ->where('student_id',$id)
          ->first();

        $grade_level = $student->getSchedule->RfSection->getGradeLevel->grade_level_id;

        $student_accounts = DtAccounts::where('student_id',$id)->where('school_year_id',$sy_id)->first();
        $grade_billing = DtBilling::where('grade_level_id',$grade_level)
                                    ->where('school_year_id',$sy_id)
                                    ->get();

        $student_accounts_id = $student_accounts->account_id;
	
        $delete = DtStudentBill::where('account_id',$student_accounts_id)->delete();


        foreach($grade_billing as $billing){
                 $billing_id = $billing->billing_id;
                 $acc_bill = new DtStudentBill();
                 $acc_bill->billing_id = $billing_id;
                 $acc_bill->account_id = $student_accounts_id;
                 $acc_bill->save();
        }
		
        return $return->status(true)
            ->message("Generate Successfull.")
            ->show();
    }

    public function autoGenerateBill($student_id){

        $id = $student_id;
        $return = new rrdReturn();
        $sy = RfSchoolYear::where('is_current','1')->first();
            $sy_id = $sy->school_year_id;

        $student = StudentSchedule::with('Students','getSchedule.RfSection.getGradeLevel')
          ->whereHas('getSchedule',function($q){
              $sy = RfSchoolYear::where('is_current','1')->first();
              $sy_id = $sy->school_year_id;
              $q->where('school_year_id',$sy_id);
          })
          ->where('student_id',$id)
          ->first();

        $grade_level = $student->getSchedule->RfSection->getGradeLevel->grade_level_id;

        $student_accounts = DtAccounts::where('student_id',$id)->where('school_year_id',$sy_id)->first();
        $grade_billing = DtBilling::where('grade_level_id',$grade_level)
                                    ->where('school_year_id',$sy_id)
                                    ->get();

        $student_accounts_id = $student_accounts->account_id;

        $delete = DtStudentBill::where('account_id',$student_accounts_id)->delete();

        foreach($grade_billing as $billing){
                 $billing_id = $billing->billing_id;
                 $acc_bill = new DtStudentBill();
                 $acc_bill->billing_id = $billing_id;
                 $acc_bill->account_id = $student_accounts_id;
                 $acc_bill->save();

        }
    }

    function savePayment(Request $request){
        $return  = new rrdReturn();
        $amount  = $request['payment'];
        $fees    = $request['fees'];
        $or_num  = $request['or_num'];
        $bank    = $request['bank'];
        $date_now = $request['date_now'];
        $check_date = $request['check_date'];
        $payment_option = $request['payment_option'];
        $check_no     = $request['check_no'];
        $counter = 0;

        foreach($fees as $fee){

            $check_bank = RfBank::where('bank',$bank[$counter])->first();
            if($check_bank == ''){
                $newbank = new RfBank();
                $newbank->bank = $bank[$counter];
                $newbank->save();
                $getbank = RfBank::where('bank',$bank[$counter])->first();
                $bank_id = $getbank->bank_id;
            }
            else{
                $bank_id = $check_bank->bank_id;
            }
            $billing = explode('-',$fee);
            $payment = new DtPayments();
            $payment->or_no      = $or_num;
            $payment->account_id = $billing[1];
            $payment->fees_id    = $billing[0];
            $payment->amount     = $amount[$counter];
            $payment->date_of_payment = $date_now;
            $payment->encoded_by  = '';
            $payment->payment_option_id = $payment_option[$counter];
            $payment->bank_id     = $bank_id;
            $payment->check_date  = $check_date[$counter];
            $payment->check_no    = $check_no[$counter];
            $payment->save();
            $counter++;
        }

        return $return->status(true)
            ->message("Payment Successfull.")
            ->show();
    }
    public function getPayments(Request $request){
        //GENERATE AUTO ASSESMENTS
        

        $id = $request['student_id'];
        $sy = RfSchoolYear::where('is_current','1')->first();
        $sy_id = $sy->school_year_id;
        $bal_assessment = DtAccounts::where('dt_accounts.student_id',$id)
                                  ->where('dt_accounts.school_year_id',$sy_id)
                                  ->join('dt_student_bill','dt_accounts.account_id','=','dt_student_bill.account_id')
                                  ->join('dt_billing','dt_student_bill.billing_id','=','dt_billing.billing_id')
                                  ->join('rf_fees','dt_billing.fees_id','=','rf_fees.fees_id')
                                  ->join('rf_fee_categories','rf_fees.fee_categories_id','=','rf_fee_categories.fee_categories_id')
                                  ->where('rf_fee_categories.title','Assessment')
                                  ->get();


        $payment_assessment = DtAccounts::where('dt_accounts.student_id',$id)
                                        ->where('dt_accounts.school_year_id',$sy_id)
                                        ->join('dt_payments','dt_accounts.account_id','=','dt_payments.account_id')
                                        ->join('rf_fees','dt_payments.fees_id','=','rf_fees.fees_id')
                                        ->join('rf_fee_categories','rf_fees.fee_categories_id','=','rf_fee_categories.fee_categories_id')
                                        ->join('rf_payment_option','dt_payments.payment_option_id','=','rf_payment_option.payment_option_id')
                                        ->where('rf_fee_categories.title','Assessment')
                                        ->select('dt_payments.amount','rf_fees.title','rf_payment_option.payment_option','dt_payments.date_of_payment','dt_payments.or_no','dt_payments.payment_id')
                                        ->get();
        $payment_non_assessment = DtAccounts::where('dt_accounts.student_id',$id)
                                        ->where('dt_accounts.school_year_id',$sy_id)
                                        ->join('dt_payments','dt_accounts.account_id','=','dt_payments.account_id')
                                        ->join('rf_fees','dt_payments.fees_id','=','rf_fees.fees_id')
                                        ->join('rf_fee_categories','rf_fees.fee_categories_id','=','rf_fee_categories.fee_categories_id')
                                        ->join('rf_payment_option','dt_payments.payment_option_id','=','rf_payment_option.payment_option_id')
                                        ->where('rf_fee_categories.title','Non-Assessment')
                                        ->select('dt_payments.amount','rf_fees.title','rf_payment_option.payment_option','dt_payments.date_of_payment','dt_payments.or_no','dt_payments.payment_id')
                                        ->get();
        $total=0;
        $balance = 0;
        foreach($bal_assessment as $bill){
                $balance = $balance + $bill->amount;
        }
        foreach($payment_assessment as $payment){
            $total = $total + $payment->amount;
        }

        $a_id = DtAccounts::where('student_id',$id)
            ->where('school_year_id',$sy_id)
            ->first();
        $account_id = $a_id->account_id;
        $discount = DtDiscount::with('getFees.getCategory')
            ->where('account_id',$account_id)
            ->get();
        $dis = 0;
        if(!empty($discount)){
            foreach($discount as $count){
                if($count->getFees->getCategory->title == 'Assessment') {
                    $dis = $dis + $count->amount;
                }
            }
        }

        $accounts = DtAccounts::with(['getStudentBill' => function($q){
            $q->whereHas('getBilling.getFees.getCategory',function($query){
                $query->where('title','Assessment');
            });
        },'getStudentBill.getBilling.getFees.getCategory','getSchoolYear'])->where('student_id',Input::get('student_id'))
            ->where('school_year_id','!=',$sy->school_year_id)
            ->get();
            $prevbal=0;
        foreach($accounts as $acc){
            $overall = 0;
            foreach($acc->getStudentBill as $bill){

                $amount = $bill->getBilling->amount;
                $payment = DtPayments::where('account_id',$acc->account_id)->where('fees_id',$bill->getBilling->getFees->fees_id)->sum('amount');
                $discount = DtDiscount::where('account_id',$acc->account_id)->where('fees_id',$bill->getBilling->getFees->fees_id)->sum('amount');
                $total = $amount - $payment - $discount;
                $overall = $overall + $total;
                if($total > 0){
                    $bill->balance = $total;
                }
            }
            $prevbal = $prevbal + $overall;
        }




        $bal = $balance - ($total + $dis) + $prevbal;

        $paid[0] = $payment_assessment;
        $paid[1] = $payment_non_assessment;
        $paid[2] = $bal;




        

        return $paid;

    }

    public function getPaymentId(){
        $payment = DtPayments::with('getFees')->find(Input::get('payment_id'));

        return $payment;
    }

    public function editPayment(){

        $payment_id      = Input::get('payment_id');
        $payment         = DtPayments::find($payment_id);
        $payment->or_no  = Input::get('payment_or_no');
        $payment->date_of_payment = Input::get('payment_date');
        $payment->amount = Input::get('payment_amount');
        $payment->save();

        $return = new rrdReturn();
        return $return->status(true)
            ->message("Payment has been updated!.")
            ->show();
    }
    public  function getFeeAmount(){
        $sy = RfSchoolYear::where('is_current','1')->first();
        $sy_id = $sy->school_year_id;

        $fee = DtAccounts::with('getStudentBill.getBilling')
                        ->whereHas('getStudentBill.getBilling',function($q){
                            $q->where('fees_id',Input::get('fees_id'));
                        })
                        ->where('student_id',Input::get('student_id'))
                        ->where('school_year_id',$sy_id)
                        ->first();
        $fee_amount = 0;
        foreach($fee->getStudentBill as $fees) {
            if($fees->getBilling->fees_id == Input::get('fees_id'))
            $fee_amount = $fees->getBilling->amount;
        }
        $account_id = $fee->account_id;
        $discount = DtDiscount::where('account_id',$account_id)
                                ->where('fees_id',Input::get('fees_id'))
                                ->get();
        $dis = 0;
        $discounted_fee = $fee_amount;
        if(!empty($discount)){
            foreach($discount as $count){
                $discounted_fee = $discounted_fee - $count->amount;
            }
        }

        return $discounted_fee;
    }
    public function adjustMonthlyBill(){
        $sy = RfSchoolYear::where('is_current','1')->first();
        $sy_id = $sy->school_year_id;
        AdjustBilling::where('school_year_id',$sy_id)->where('fees_id',Input::get('fees'))->where('student_id',Input::get('student_id'))->delete();

        $adjust                 = new AdjustBilling();
        $adjust->student_id     = Input::get('student_id');
	$adjust->fees_id	= Input::get('fees');
        $adjust->school_year_id = $sy_id;
        $adjust->month_id       = Input::get('month');
        $adjust->save();

        $return  = new rrdReturn();
        return $return->status(true)
            ->message("Adjustment has been made!.")
            ->show();

    }
    public function saveDiscount(){
        $sy = RfSchoolYear::where('is_current','1')->first();
        $sy_id = $sy->school_year_id;
        $id = DtAccounts::where('student_id',Input::get('student_id'))
                        ->where('school_year_id',$sy_id)
                        ->first();
        $account_id = $id->account_id;

                $discount = new DtDiscount();
                $discount->account_id = $account_id;
                $discount->fees_id    = Input::get('discount_fees');
                $discount->percentage = Input::get('discount_percentage');
                $discount->amount     = Input::get('discount_amount');
                $discount->description= Input::get('discount_description');
                $discount->save();

        $return = new rrdReturn();
        return $return->status(true)
            ->message("Discount has been updated!.")
            ->show();
    }
    public function getDiscount(Request $request){


        $sy = RfSchoolYear::where('is_current','1')->first();
        $sy_id = $sy->school_year_id;

        $id = DtAccounts::with('getDiscount.getFees','getSchoolYear')->where('student_id',Input::get('student_id'))
            ->orderBy('school_year_id','asc')
            ->get();
	
        $account_id = 1;

        $discount = DtDiscount::with('getFees')
                ->where('account_id',$account_id)->get();

        return $id;
    }

    public function addPromissory(){
        $sy      = RfSchoolYear::where('is_current','1')->first();
        $account = DtAccounts::where('student_id',Input::get('student_id'))
                                ->where('school_year_id',$sy->school_year_id)
                                ->first();
        $reason = ' ';
        if(Input::get('reason') != ''){
            $reason = Input::get('reason');
        }
        $promised                   = new DtPromissory();
        $promised->fees_id          = Input::get('fees_id');
        $promised->account_id       = $account->account_id;
        $promised->amount           = Input::get('promised_amount');
        $promised->promised_date    = Input::get('promised_date');
        $promised->reason           = $reason;
        $promised->save();

        $return = new rrdReturn();
        return $return->status(true)
            ->message("Promissory has been made!.")
            ->show();
    }
    public function getPromissory(){
        $sy      = RfSchoolYear::where('is_current','1')->first();
        $account = DtAccounts::where('student_id',Input::get('student_id'))
            ->where('school_year_id',$sy->school_year_id)
            ->first();
        $promissory = DtPromissory::with('getFees')
                                    ->where('account_id',$account->account_id)
                                    ->get();

        return $promissory;
    }

    public function getLedger(){

        $student_id = Input::get('student_id');

        $accounts   = DtAccounts::with('getSchoolYear','getPayments.getFees.getCategory','getPayments.getPaymentOption')
                                  ->where('student_id',$student_id)
                                  ->get();


        foreach($accounts as $bal){
            $balance = DtStudentBill::with('getBilling.getFees.getCategory')
                                    ->where('account_id',$bal->account_id)
                                    ->get();
                                    
            $total_balance    = 0;
            $total_discount   = 0;
            $total_amount_due = 0;

             foreach($balance as $getTotal){
                 if($getTotal->getBilling->getFees->getCategory->title == 'Assessment'){
                     $total_balance = $total_balance + $getTotal->getBilling->amount;
                 }
             }


            $discount = DtDiscount::where('account_id',$bal->account_id)->get();
            foreach($discount as $dis){
                $total_discount = $total_discount + $dis->amount;
            }

            $total_amount_due = $total_balance - $total_discount;
            $bal->amount_due = number_format($total_amount_due,2,'.',',');
            $total_balance = $total_balance - $total_discount;

            foreach($bal->getPayments as $getbal){
                        if($getbal->getFees->getCategory->title == 'Assessment'){
                            $total_balance = $total_balance - $getbal->amount;
                            $getbal->bal = number_format($total_balance,2,'.',',');
                        }
                        else{
                            $getbal->bal = '';
                        }
            }


        }

        return $accounts;

    }
    public function saveOtherPayment(){
        $sy      = RfSchoolYear::where('is_current','1')->first();

        $amount = Input::get('amount');
        $fees   = Input::get('fees_id');
        $counter = 0;

        $bank = Input::get('bank');
        $check_bank = RfBank::where('bank',$bank)->first();
        if($check_bank == ''){
            $newbank = new RfBank();
            $newbank->bank = $bank[$counter];
            $newbank->save();
            $getbank = RfBank::where('bank',$bank)->first();
            $bank_id = $getbank->bank_id;
        }
        else{
            $bank_id = $check_bank->bank_id;
        }

        foreach($fees as $fee){
            $payment = new DtOtherPayment();
            $payment->fees_id = $fee;
            $payment->amount  = $amount[$counter];
            $payment->date_of_payment    = Input::get('date');
            $payment->or_no   = Input::get('or_no');
            $payment->check_no = Input::get('check_no');
            $payment->payment_option_id = Input::get('payment_type');
            $payment->payers_name = Input::get('payer');
            $payment->check_date = Input::get('check_date');
            $payment->bank_id   = $bank_id;
            $payment->school_year_id = $sy->school_year_id;
            $payment->save();

            $counter++;
        }

        $return = new rrdReturn();
        return $return->status(true)
            ->message("Promissory has been made!.")
            ->show();
    }
    public function getOtherPayments(){

        $sy = RfSchoolYear::where('is_current','1')->first();

        $other = DtOtherPayment::with('getFees','getPaymentOption')->get();

        $datatableFormat = new DatatableFormat();
        return $datatableFormat->format($other);

    }
    public function getExpenses(){

        $expenses = RfFees::with('getCategory')->whereHas('getCategory',function($q){
                                        $q->where('title','Expenses');
                                    })->get();

        $datatableFormat = new DatatableFormat();
        return $datatableFormat->format($expenses);
    }
    public function saveExpenses(){

        $amount = Input::get('amount');
        $fees   = Input::get('fees_id');
        $sy      = RfSchoolYear::where('is_current','1')->first();
        $counter = 0;
        $bank = Input::get('bank');
        $check_bank = RfBank::where('bank',$bank)->first();
        if($check_bank == ''){
            $newbank = new RfBank();
            $newbank->bank = $bank[$counter];
            $newbank->save();
            $getbank = RfBank::where('bank',$bank)->first();
            $bank_id = $getbank->bank_id;
        }
        else{
            $bank_id = $check_bank->bank_id;
        }
            foreach($fees as $fee){
                $expenses                    = new DtExpenses();
                $expenses->fees_id           = $fee;
                $expenses->amount            = $amount[$counter];
                $expenses->bank_id           = $bank_id;
                $expenses->name              = Input::get('name');
                $expenses->school_year_id    = $sy->school_year_id;
                $expenses->voucher_no        = Input::get('voucher_no');
                $expenses->check_no          = Input::get('check_no');
                $expenses->payment_option_id = Input::get('payment_type');
                $expenses->check_date        = Input::get('check_date');
                $expenses->date              = Input::get('date');
                $expenses->save();
                $counter++;

            }

        $return = new rrdReturn();
        return $return->status(true)
            ->message("Expenses has been made!.")
            ->show();

    }
    public function getExp(){
        $expenses = DtExpenses::with('getFees','getPaymentOption')
                            ->get();

        $datatableFormat = new DatatableFormat();
        return $datatableFormat->format($expenses);
    }
    public function assignCustomFees(){
            $fees = Input::get('fees');
        $sy = RfSchoolYear::where('is_current','1')->first();

        $acc = DtAccounts::where('school_year_id',$sy->school_year_id)
                            ->where('student_id',Input::get('student_id'))
                            ->first();

        foreach($fees as $fee){
            $assign = new DtStudentBill();
            $assign->billing_id = $fee;
            $assign->account_id = $acc->account_id;
            $assign->save();
        }

        $return = new rrdReturn();
        return $return->status(true)
            ->message("Fees has been assigned.")
            ->show();
    }

    public function studentBalList(){

        $levels = RfGradeLevel::orderBy('grade_level')->get();
        return view('sms.billing.student-balances',compact('levels'));
    }

    public function getCollection(){
        ini_set('max_execution_time', 300);
        $from = Input::get('from');
        $to   = Input::get('to');
        $date_from = date("Y-m-d", strtotime($from));
        $date_to   = date("Y-m-d", strtotime($to));

        $payments = DtPayments::with('getFees','getAccount.getStudent','getPaymentOption','getBank')
            ->where('date_of_payment','>=', $date_from)
            ->where('date_of_payment','<=',$date_to)
            ->orderBy('date_of_payment','asc')
            ->orderBy('or_no','asc')
            ->get();

        $expenses = DtExpenses::with('getFees','getPaymentOption')
            ->where('date','>=', $date_from)
            ->where('date','<=',$date_to)
            ->orderBy('date','asc')
            ->get();
        $other = DtOtherPayment::with('getFees','getPaymentOption')
            ->where('date_of_payment','>=', $date_from)
            ->where('date_of_payment','<=',$date_to)
            ->orderBy('date_of_payment','asc')
            ->orderBy('or_no','asc')
            ->get();

        $acc_income = RfAccount::where('acccount_type_id','1')->get();
        $acc_expenses = RfAccount::where('acccount_type_id','2')->get();
        $total_income = 0;
        $total_expenses = 0;
        foreach($acc_income as $ai){
            $amount = 0;
            foreach($payments as $pay){
                if($pay->getFees->account_code == $ai->account_code){
                    $amount = $amount + $pay->amount;
                }
            }
            foreach($other as $others){
                if($others->getFees->account_code == $ai->account_code){
                    $amount = $amount + $others->amount;
                }
            }
            $total_income = $total_income + $amount;
            $ai->amount = number_format($amount,2,'.',',');
        }
        foreach($acc_expenses as $exp){
            $amount = 0;
            foreach($expenses as $ex){
                if($ex->getFees->account_code == $exp->account_code){
                    $amount = $amount + $ex->amount;
                }
            }
            $total_expenses = $total_expenses + $amount;
            $exp->amount = number_format($amount,2,'.',',');
        }

        $result[0] = $payments;
        $result[1] = $expenses;
        $result[2]  = $other;
        $result[3]  = $acc_income;
        $result[4]  = $acc_expenses;
        $result[5]  = number_format($total_expenses,2,'.',',');
        $result[6]  = number_format($total_income,2,'.',',');
        return $result;

    }
    public function getAccounts(){

        $accounts = RfAccount::with('getAccountType')->get();

        $datatableFormat = new DatatableFormat();
        return $datatableFormat->format($accounts);
    }
    public function saveAccount(){

        $checker = RfAccount::where('account_code',Input::get('account_code'))->count();
        if($checker > 0){
            $return = new rrdReturn();
            return $return->status(false)
                ->message("Sorry! Account Code is not Available.")
                ->show();
        }
        $acc = new RfAccount();
        $acc->account_code = Input::get('account_code');
        $acc->account_desc = Input::get('account');
        $acc->acccount_type_id = Input::get('account_type');
        $acc->status_id = '1';
        $acc->save();


        $return = new rrdReturn();
        return $return->status(true)
            ->message("Successful!")
            ->show();

    }
    public function getAcc(){

        $account = RfAccount::with('getAccountType')->get();

        $datatableFormat = new DatatableFormat();
        return $datatableFormat->format($account);
    }
    public function saveJournal(Request $rq){


        $date       = Input::get('date');
        $reference  = Input::get('reference');
        $code       = Input::get('account_c');
        $desc       = Input::get('account_desc');
        $debit      = Input::get('debit');
        $credit     = Input::get('credit');
        $job        = Input::get('job');

        $date = date('Y-m-d', strtotime($date));
        $counter = 0;
        foreach($code as $acc){
            $journal = new DtJournal();
            $journal->reference_no  = $reference;
            $journal->date_entry    = $date;
            $journal->account_code  = $acc;
            $journal->description   = $desc[$counter];
            $journal->debit         = $debit[$counter];
            $journal->credit        = $credit[$counter];
            $journal->job           = $job[$counter];
            $journal->save();
            $counter++;
        }

        $return = new rrdReturn();
        return $return->status(true)
            ->message("Successful!")
            ->show();
    }

    public function getJournals(){
        $date = Input::get('date');


        $dates = explode('-',$date);
        $month = $dates[0];
        $year  = $dates[1];

        $journals = DtJournal::where(DB::raw('MONTH(date_entry)'),$month)
                              ->where(DB::raw('Year(date_entry)'),$year)
                              ->get();


        return $journals;
    }
    public function editJournal(){
        $id           = Input::get('id');
        $reference    = Input::get('reference');
        $account_code = Input::get('account_code');
        $description  = Input::get('description');
        $debit        = Input::get('debit');
        $credit       = Input::get('credit');

        $counter = 0;
        foreach($id as $code){
            $journal = DtJournal::find($code);
            $journal->reference_no = $reference[$counter];
            $journal->account_code = $account_code[$counter];
            $journal->description  = $description[$counter];
            $journal->debit        = $debit[$counter];
            $journal->credit       = $credit[$counter];
            $journal->save();
            $counter++;
        }

        $return = new rrdReturn();
        return $return->status(true)
            ->message("Successful!")
            ->show();

    }
}
