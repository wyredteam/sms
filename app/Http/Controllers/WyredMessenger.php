<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Request;
use Fpdf;
use basicListing;
use LeadSoftModel;
use LeadAuth;
use ModuleRestriction;
use Storage;
use Response;
use datatableFormat;
use Validator;
use App\User;
use App\Module;
use App\SubModule;
use App\Roles;
use App\Permissions;
use App\KronosEmployee;
use App\ConvoUsers;
use App\Convo;
use App\ConvoMessages;
use Log;
use Auth;
use DB;
use Session;
use Hash;
use Mail;
use App\VerificationModel;

class WyredMessenger extends Controller
{

	public function messenger(){

		$logUser = Auth::user();

		$employees = User::with('KronosEmployee')
					->where('employee_id','<>', null)
					->where('user_id','<>',$logUser->user_id)
					->get();

		return view('admin.messenger.messenger',compact('employees'));
	}

	public function getConvo($user_id){

		$logUser = Auth::user();

		
		$convo_user = ConvoMessages::whereNotIn('convo_id',function($query)
					            {
					            	$logUser = Auth::user();
					                $query->select(DB::raw('convo_id'))
					                      ->from('ms_convo_trash')
					                      ->where('user_id',$logUser->user_id);
					            })
				 
								->where('users_from',$logUser->user_id)
								->where('to',$user_id)
								->first();

								
		if($convo_user == ""){

			

			$convo_user = ConvoMessages::whereNotIn('convo_id',function($query)
					            {
					            	$logUser = Auth::user();
					                $query->select(DB::raw('convo_id'))
					                      ->from('ms_convo_trash')
					                      ->where('user_id',$logUser->user_id);
					            })
				 
								->where('users_from',$user_id)
								->where('to',$logUser->user_id)
								->first();
			
			if($convo_user == ""){
				$convo_id = "";
			}else{
				$convo_id = $convo_user->convo_id;
			}

			
		}else{
			$convo_id = $convo_user->convo_id;
		}	


		if($convo_id == ""){
			return "No active conversation";
		}


		$messages = ConvoMessages::with('User','User.KronosEmployee')
									->where('convo_id',$convo_id)
									->whereNotIn('message_id',function($query)
						            {
						            	$logUser = Auth::user();
						                $query->select(DB::raw('message_id'))
						                      ->from('ms_message_trash')
						                      ->where('user_id',$logUser->user_id);
						            })
						            ->having('message_id', '>', Request::input('last_message_id'))
						            ->get();

		ConvoMessages::where('convo_id',$convo_id)
									->whereNotIn('message_id',function($query)
						            {
						            	$logUser = Auth::user();
						                $query->select(DB::raw('message_id'))
						                      ->from('ms_message_trash')
						                      ->where('user_id',$logUser->user_id);
						            })
						            ->where('message_id', '>', Request::input('last_message_id'))
						            ->where('users_from', '<>', $logUser->user_id)
						            ->update(['status' => 0]);

		return view('admin.messenger.new-messages',compact('messages','logUser'));				           

		
	}

	public function loadConvo($user_id){

		$logUser = Auth::user();

		
		$convo_user = ConvoMessages::whereNotIn('convo_id',function($query)
					            {
					            	$logUser = Auth::user();
					                $query->select(DB::raw('convo_id'))
					                      ->from('ms_convo_trash')
					                      ->where('user_id',$logUser->user_id);
					            })
				 
								->where('users_from',$logUser->user_id)
								->where('to',$user_id)
								->first();

								
		if($convo_user == ""){

			

			$convo_user = ConvoMessages::whereNotIn('convo_id',function($query)
					            {
					            	$logUser = Auth::user();
					                $query->select(DB::raw('convo_id'))
					                      ->from('ms_convo_trash')
					                      ->where('user_id',$logUser->user_id);
					            })
				 
								->where('users_from',$user_id)
								->where('to',$logUser->user_id)
								->first();
			
			if($convo_user == ""){
				$convo_id = "";
			}else{
				$convo_id = $convo_user->convo_id;
			}

			
		}else{
			$convo_id = $convo_user->convo_id;
		}	


		if($convo_id == ""){
			return "No active conversation";
		}


		$messages = ConvoMessages::with('User','UserFrom','UserTo','User.KronosEmployee')
									->where('convo_id',$convo_id)
									->whereNotIn('message_id',function($query)
						            {
						            	$logUser = Auth::user();
						                $query->select(DB::raw('message_id'))
						                      ->from('ms_message_trash')
						                      ->where('user_id',$logUser->user_id);
						            })
						            ->orderby('message_id','desc')
						            ->take(15)
						            ->get()->reverse();	

		ConvoMessages::where('convo_id',$convo_id)
									->whereNotIn('message_id',function($query)
						            {
						            	$logUser = Auth::user();
						                $query->select(DB::raw('message_id'))
						                      ->from('ms_message_trash')
						                      ->where('user_id',$logUser->user_id);
						            })
						            ->update(['status' => 0]);


		return view('admin.messenger.new-messages',compact('messages','logUser'));				           

		
	}

	public function newMessage(){
		

		DB::beginTransaction();

                $return = new rrdReturn();

                    try {

						$logUser = Auth::user();

						$convo_user = ConvoMessages::whereNotIn('convo_id',function($query)
					            {
					            	$logUser = Auth::user();
					                $query->select(DB::raw('convo_id'))
					                      ->from('ms_convo_trash')
					                      ->where('user_id',$logUser->user_id);
					            })
				 
								->where('users_from',$logUser->user_id)
								->where('to',Request::input('partner_user_id'))
								->first();

						if($convo_user == ""){
							$convo_user = ConvoMessages::whereNotIn('convo_id',function($query)
					            {
					            	$logUser = Auth::user();
					                $query->select(DB::raw('convo_id'))
					                      ->from('ms_convo_trash')
					                      ->where('user_id',$logUser->user_id);
					            })
				 
								->where('to',$logUser->user_id)
								->where('users_from',Request::input('partner_user_id'))
								->first();
						}

						if($convo_user == ""){
							
							//create convo
							$createConvo = new Convo();
							$createConvo->save();
							$convo_id = $createConvo->max('convo_id');

						}else{
							$convo_id = $convo_user->convo_id;
						}


						
						$newMessage = new ConvoMessages();
						$newMessage->convo_id = $convo_id;
						$newMessage->message = Request::input('new_message');
						$newMessage->users_from = $logUser->user_id;
						$newMessage->to = Request::input('partner_user_id');
						$newMessage->save();

						DB::commit();

					}    

                    catch (\Illuminate\Database\QueryException $e) {
                            

                            Log::error($e->getMessage());
                            DB::rollback();
                            return $return->status(false)
                                  ->message("An error has eccured, transactions immediately calls a database rollback.
                                            This feature was designed by the software developer to prevent data loss and
                                            database uninttended data.".$e->getMessage())
                                  ->show();
                    } 

						return "true";


	}

	public function loadOldMessages($user_id){

		$logUser = Auth::user();

		
		$convo_user = ConvoMessages::whereNotIn('convo_id',function($query)
					            {
					            	$logUser = Auth::user();
					                $query->select(DB::raw('convo_id'))
					                      ->from('ms_convo_trash')
					                      ->where('user_id',$logUser->user_id);
					            })
				 
								->where('users_from',$logUser->user_id)
								->where('to',$user_id)
								->first();

								
		if($convo_user == ""){

			

			$convo_user = ConvoMessages::whereNotIn('convo_id',function($query)
					            {
					            	$logUser = Auth::user();
					                $query->select(DB::raw('convo_id'))
					                      ->from('ms_convo_trash')
					                      ->where('user_id',$logUser->user_id);
					            })
				 
								->where('users_from',$user_id)
								->where('to',$logUser->user_id)
								->first();
			
			if($convo_user == ""){
				$convo_id = "";
			}else{
				$convo_id = $convo_user->convo_id;
			}

			
		}else{
			$convo_id = $convo_user->convo_id;
		}	


		if($convo_id == ""){
			return "No active conversation";
		}


		$messages = ConvoMessages::with('User','User.KronosEmployee')
									->where('convo_id',$convo_id)
									->whereNotIn('message_id',function($query)
						            {
						            	$logUser = Auth::user();
						                $query->select(DB::raw('message_id'))
						                      ->from('ms_message_trash')
						                      ->where('user_id',$logUser->user_id);
						            })
						            ->having('message_id', '<', Request::input('ref_message_id'))
						            ->orderby('message_id','desc')
						            ->take(5)
						            ->get()->reverse();	


		return view('admin.messenger.new-messages',compact('messages','logUser'));				
	}

	public function countNewMessages(){
		
		$logUser = Auth::user();

		$count = ConvoMessages::where('to',$logUser->user_id)
								->where('status',1)
								->count();

		

		$data  = [];
								
	    if($count > 0){

	    	
	    	$info = ConvoMessages::with('User','UserFrom.KronosEmployee')
								->where('to',$logUser->user_id)
								->where('status',1)
								->first();

			
			$data["count"]        = $count;
			$data['user_id']      = $info->users_from;
			$data["contact_name"] = $info->UserFrom->KronosEmployee->first_name;


			ConvoMessages::where('to',$logUser->user_id)
								->where('status',1)
								->update(['status' => 0]);
	    }else{

	    	$data == "";
	    }

		
		return $data;
	}

	public function loadDropDownMessages(){

		$logUser = Auth::user();


		$messages = Db::select("SELECT
									*
									from
									(
									select * from ms_messages
									where  status = 1 or status = 0
									and  convo_id NOT IN (SELECT message_id from ms_message_trash where user_id = '".$logUser->user_id."')
									 ORDER BY message_id desc
									) ms_messages
									LEFT JOIN tbl_users on tbl_users.user_id = ms_messages.users_from  
									LEFT JOIN dt_employee on dt_employee.employee_id = tbl_users.employee_id
									where users_from <> '".$logUser->user_id."'
									GROUP BY convo_id
									ORDER BY convo_id desc;");

		
		return view('admin.messenger.dropdown-messages',compact('messages'));

	}
}
