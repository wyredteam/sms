<?php

namespace App\Http\Controllers;

use App\RfGradeLevel;
use App\RfSchoolYear;
use App\RfSection;
use App\RfSectionType;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Students;
use DatatableFormat;
use App\RfGradeType;
use App\Student_Status;

use Illuminate\Support\Facades\DB;
class StudentListController extends Controller
{
    public function registeredList(){

    	$students = Students::with('Parents_Students','Parents_Students.Parents')->get();
        $gradeType = RfGradeType::where('grade_type','!=','All')->get();
        $sy         = RfSchoolYear::all();
        $sectionType = RfSectionType::all();
        $student_status = Student_Status::all();

      	return view('sms.registrar.registered-list',compact('student_status','gradeType','sectionType','sy'))->with('students',$students);
    }

    public function getStudents(Request $rq){

        $query = Students::select(db::raw('dt_students.*,dt_schedule.schedule_id,end_time,start_time,grade_level,section_name'))
                ->leftjoin('dt_students_schedule','dt_students_schedule.student_id','=','dt_students.student_id')
                ->leftjoin('dt_schedule','dt_students_schedule.schedule_id','=','dt_schedule.schedule_id')
                ->leftjoin('rf_section','dt_schedule.section_id','=','rf_section.section_id')
                ->leftjoin('rf_grade_level','rf_section.grade_level_id','=','rf_grade_level.grade_level_id');


        if($rq['schoolYear'] != "All"){
            $query->where('dt_schedule.school_year_id', $rq['schoolYear']);  
        }
        
        if($rq['student_status'] != "All"){
            $query->where('dt_students_schedule.student_status_id', $rq['student_status']);  
        }   
        if($rq['gradeType'] != "All"){
            $query->where('rf_grade_level.grade_type_id', $rq['gradeType']);  
        } 
        if($rq['grade_level'] != "All"){
            $query->where('rf_section.grade_level_id', $rq['grade_level']);  
        }
        if($rq['sectionName'] != "All"){
            $query->where('rf_section.section_id', $rq['sectionName']);  
        }
        
        
          

        

        $students = $query->get();

        $datatableFormat = new DatatableFormat();
        return $datatableFormat->format($students);
	    

    }
}
