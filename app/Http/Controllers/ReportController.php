<?php

namespace App\Http\Controllers;

use App\AdjustBilling;
use App\DtPayments;
use App\DtSyMonthTemplate;
use App\RfFees;
use App\RfMonth;
use App\StudentBill;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use DB;
use DatatableFormat;
use Validator;
use Pdf;
use App\DtExpenses;
use App\RfAccount;
use App\DtOtherPayment;
use App\RfSchoolYear;
use App\RfGradeLevel;
use App\RfGradeType;
use App\RfSection;
use App\StudentSchedule;
use App\Students;
use App\DtAccounts;
use App\Schedule;
use App\DtDiscount;
use App\Student_Status;
use App\DtStudentBill;
use App\RfClassType;
use App\Weekdays;
use App\KronosEmployee;
use App\DtAssignSubject;

class ReportController extends Controller
{

    public function enrolledStudents(){

        $sy = RfSchoolYear::where('is_current','1')->first();

        $populations = RfGradeType::where('grade_type_id','<>',"1")
            ->get();


       $pdf = Pdf::loadView('sms.reports.enrolled-students',compact('populations','sy'));
    
       return $pdf->setPaper('letter')->setOrientation('portrait')
                   ->setOption('margin-bottom', 2)
                   ->setOption('margin-left', 1)
                   ->setOption('margin-right', 1)
                   ->setOption('margin-top', 2)
                   ->stream('report.pdf');
    }
    
    public function generateMasterlist(){

            $request = Request::all();
            $sy = RfSchoolYear::where('is_current','1')->first();
            $grade_type = $request['gradeType'];
            $grade_level = $request['grade_level'];
            $section = $request['section_name'];

            $employee = Schedule::with('getAdviser','RfSection')->where('section_id',$request['section_name'])->first();

            if($section == 'All'){
                return $this->generateGradeList($grade_type,$grade_level);
            }

            $grade = RfGradeLevel::find($grade_level);
            $sec = RfSection::find($section);
                $populations = RfSection::with('Schedule.StudentSchedule.Students')
                        ->where('section_id',$section)
                        ->get();
                   

          $pdf = Pdf::loadView('sms.reports.masterlist-generate',compact('employee','grade_type','grade_level','section','grade','sec','populations','sy'));
        
            return $pdf->setPaper('letter')->setOrientation('portrait')
                        ->setOption('margin-bottom', 2)
                        ->setOption('margin-left', 2)
                        ->setOption('margin-right', 2)
                       ->setOption('margin-top', 2)
                        ->stream('report.pdf');
    }

    public function generateGradeList($grade_type, $grade_level ){

            $sy = RfSchoolYear::where('is_current','1')->first();
            $sy_id = $sy->school_year_id;
            $grade = RfGradeLevel::find($grade_level);
                $populations = RfGradeLevel::with('getSection.Schedule.StudentSchedule.Students')
                        ->where('grade_level_id',$grade_level)
                        ->get();
                   

          $pdf = Pdf::loadView('sms.reports.gradelevel-masterlist',compact('grade_type','grade_level','grade','sec','populations','sy_id'));
        
            return $pdf->setPaper('letter')->setOrientation('portrait')
                        ->setOption('margin-bottom', 2)
                        ->setOption('margin-left', 2)
                        ->setOption('margin-right', 2)
                       ->setOption('margin-top', 2)
                        ->stream('report.pdf');
    }

    public function printAssessment(){
            $id = Request::input('student_id');
        $sy = RfSchoolYear::where('is_current','1')->first();
        $sy_id = $sy->school_year_id;
        $student = Students::with('getStudentSchedule','getStudentSchedule.getSchedule','getStudentSchedule.getSchedule.RfSection','getStudentSchedule.getSchedule.RfSection.getGradeLevel')
                            ->whereHas('getStudentSchedule.getSchedule',function($q) use ($sy_id){
                                $q->where('school_year_id',$sy_id);
                            })
                            ->where('student_id',$id)
                            ->first();

        $assessment = DtAccounts::where('dt_accounts.student_id',$id)
                                ->where('dt_accounts.school_year_id',$sy_id)
                                ->join('dt_student_bill','dt_accounts.account_id','=','dt_student_bill.account_id')
                                ->join('dt_billing','dt_student_bill.billing_id','=','dt_billing.billing_id')
                                ->join('rf_fees','dt_billing.fees_id','=','rf_fees.fees_id')
                                ->join('rf_fee_categories','rf_fees.fee_categories_id','=','rf_fee_categories.fee_categories_id')
                                ->where('rf_fee_categories.title','Assessment')
                                ->select('dt_billing.amount','rf_fees.title','rf_fees.fees_id','dt_accounts.account_id')
                                ->get();

        $total = 0;
        $tui   = 0;
        $misc  = 0;
        foreach($assessment as $amount){

            $discount = DtDiscount::with('getFees.getCategory')
                ->where('fees_id',$amount->fees_id)
                ->where('account_id',$amount->account_id)
                ->get();
            $dis = 0;
            if(!empty($discount)){
                foreach($discount as $count){
                    $dis = $dis + $count->amount;

                }
            }

            $total = $total + $amount->amount - $dis;
            if($amount->title == 'Tuition'){
                $tui = $amount->amount - $dis;
            }
            elseif($amount->title == 'Miscellaneous'){
                $misc = $amount->amount - $dis;
            }
            $amount->t_amount = $amount->amount - $dis;

        }
        $tui    = $tui/10;
        $misc   = $misc/5;
        $paid    = 0;
        $total_bal = 0;
        $payment_assessment = DtAccounts::where('dt_accounts.student_id',$id)
                                    ->where('dt_accounts.school_year_id',$sy_id)
                                    ->join('dt_payments','dt_accounts.account_id','=','dt_payments.account_id')
                                    ->join('rf_fees','dt_payments.fees_id','=','rf_fees.fees_id')
                                    ->join('rf_fee_categories','rf_fees.fee_categories_id','=','rf_fee_categories.fee_categories_id')
                                    ->join('rf_payment_option','dt_payments.payment_option_id','=','rf_payment_option.payment_option_id')
                                    ->where('rf_fee_categories.title','Assessment')
                                    ->select('rf_payment_option.payment_option','dt_payments.amount','rf_fees.title','dt_payments.date_of_payment','dt_payments.or_no','dt_payments.payment_id','rf_fees.fees_id','dt_accounts.account_id')
                                    ->get();
        $payment_non_assessment = DtAccounts::where('dt_accounts.student_id',$id)
                                    ->where('dt_accounts.school_year_id',$sy_id)
                                    ->join('dt_payments','dt_accounts.account_id','=','dt_payments.account_id')
                                    ->join('rf_fees','dt_payments.fees_id','=','rf_fees.fees_id')
                                    ->join('rf_fee_categories','rf_fees.fee_categories_id','=','rf_fee_categories.fee_categories_id')
                                    ->join('rf_payment_option','dt_payments.payment_option_id','=','rf_payment_option.payment_option_id')
                                    ->where('rf_fee_categories.title','Non-Assessment')
                                    ->select('rf_payment_option.payment_option','dt_payments.amount','rf_fees.title','dt_payments.date_of_payment','dt_payments.or_no','dt_payments.payment_id')
                                    ->get();
            foreach($payment_assessment as $payment_amount){

                $paid = $paid + $payment_amount->amount;

            }

        $total_bal = $total - $paid;
        $pdf = Pdf::loadView('sms.reports.print-assesment',compact('student','sy','assessment','total','tui','misc','payment_assessment','payments_non_assessment','total_bal','payment_non_assessment'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 1)
            ->setOption('margin-right', 1)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }
    public function gradeLevelBilling(){

        $sy = RfSchoolYear::where('is_current','1')->first();
        $sy_id = $sy->school_year_id;

        $bill = RfGradeLevel::with('getBilling','getBilling.getFees.getCategory')
                                ->get();

        $pdf = Pdf::loadView('sms.reports.grade-level-billing',compact('bill','sy_id','sy'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }

    public function generateMonthlyBilling()
    {

        $request = Request::all();

        $sy = RfSchoolYear::where('is_current', '1')->first();
        $sy_id = $sy->school_year_id;
        $date_now = date('F');


        $m = RfMonth::where('month_name', $date_now)->first();
        $month_id = $m->month_id;
        $ranks = DtSyMonthTemplate::where('month_id', $month_id)->first();
        $aw = DtSyMonthTemplate::where('rankings', '<=', $ranks->rankings)->get();
        foreach ($aw as $ew) {
            $dates[] = $ew->month_id;
        }

        $date_payments = DtPayments::with('getFees.getCategory')
            ->where(DB::raw('MONTH(date_of_payment)'), $dates[1])
            ->get();



//        $billing = DtAccounts::with('getStudent','getStudentBill.getBilling.getFees.getCategory')
//                            ->where('school_year_id', $sy_id)
//                            ->where('grade_level_id',$grade_level_id)
//                            ->get();
        $billings = DtAccounts::query();
        $billings->with('getGradeLevel.getSection','getStudent','getStudentBill.getBilling.getFees.getCategory');
        $billings->where('dt_accounts.school_year_id', $sy_id);
        if(Input::has('student_id')){
            $billings->where('dt_students.student_id',Input::get('student_id'));
            $billings->join('dt_students','dt_accounts.student_id','=','dt_students.student_id');
            $billings->join('dt_students_schedule','dt_students.student_id','=','dt_students_schedule.student_id');
            $billings->join('dt_schedule','dt_students_schedule.schedule_id','=','dt_schedule.schedule_id');
	    $billings->where('dt_schedule.school_year_id',$sy_id);
            $billings->join('rf_section','dt_schedule.section_id','=','rf_section.section_id');
        }
        if(Input::has('grade_level') != ''){
            $billings->where('dt_accounts.grade_level_id',Input::get('grade_level'));
            $billings->join('dt_students','dt_accounts.student_id','=','dt_students.student_id');
            $billings->join('dt_students_schedule','dt_students.student_id','=','dt_students_schedule.student_id');
            $billings->join('dt_schedule','dt_students_schedule.schedule_id','=','dt_schedule.schedule_id');
            $billings->join('rf_section','dt_schedule.section_id','=','rf_section.section_id');
            $billings->where('dt_schedule.school_year_id',$sy_id);
            $billings->where('dt_schedule.section_id',Input::get('section_name'));
//            $billings->whereHas('getGradeLevel', function($q){
//                 $q->whereHas('getSection',function($q){
//                  return $q->where('section_id',Input::get('section_name'));
//                });
//            });
        }
        $billing = $billings->get();



        foreach ($billing as $bill) {
            
            $emp = Schedule::with('getAdviser')->where('schedule_id',$bill->schedule_id)->first();


            $bill->adviser = $emp->getAdviser->first_name.' '.$emp->getAdviser->last_name;
            $bill->st = $emp->start_time;
            $bill->et = $emp->end_time;
            foreach ($bill->getStudentBill as $naks) {
                $total=0;
                    $naks->account_iddd = $naks->account_id;
                        $fees_id = $naks->getBilling->fees_id;
                        $date_counter = 0;
                        foreach($dates as $date){
                            //$dates_payments = DtPayments::with('getFees.getCategory')
                               // ->where(DB::raw('MONTH(date_of_payment)'), $date)
                               // ->where('account_id',$naks->account_iddd)
                               // ->where('fees_id',$fees_id)
                           //     ->get();
                          //  foreach($dates_payments as $paid){
                          //      $total= $total + $paid->amount;
                          //  }
                            $date_counter++;
                        }
			
		$dates_payments = DtPayments::with('getFees.getCategory')
                                ->where('account_id',$naks->account_iddd)
                                ->where('fees_id',$fees_id)
                                ->get();
                            foreach($dates_payments as $paid){
                                $total= $total + $paid->amount;
                            }

                $discount = DtDiscount::with('getFees.getCategory')
                    ->where('fees_id',$fees_id)
                    ->where('account_id',$naks->account_iddd)
                    ->get();
                $dis = 0;
                if(!empty($discount)){
                    foreach($discount as $count){
                        $dis = $dis + $count->amount;
                    }
                }


                $stud_id = DtAccounts::find($naks->account_iddd);


                if($naks->getBilling->getFees->title == 'Tuition'){


                    $tuition_divider = 10;
                    $ranking = 0;
                    $adjustment = AdjustBilling::where('student_id',$stud_id->student_id)->where('fees_id','4')->where('school_year_id',$sy_id)->first();

                    if($adjustment != ''){
                        $rank = DtSyMonthTemplate::where('month_id',$adjustment->month_id)->first();

                        $tuition_divider = $tuition_divider - ($rank->template_id - 1);
                        $ranking = $rank->rankings - 3;

                    }

                        $date_counter = $date_counter - $ranking;
                       $tuition_amount = ($naks->getBilling->amount - $dis) / $tuition_divider;

                        $over_all = $tuition_amount * ($date_counter - 2);
			
                        $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
		
                        $bal = $over_all - $total;
		
                        if($bal <= 0){
                            $bal=0;
                        }
                    }
                    elseif($naks->getBilling->getFees->title == 'Miscellaneous'){
                        $tuition_amount = $naks->getBilling->amount - $dis;
                        $over_all = $tuition_amount * ($date_counter - 3);
                        $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                        $bal = $over_all - $total;
			$bal = $naks->getBilling->amount - $dis - $total;
                        if($bal <= 0){
                            $bal=0;
                        }
                    }
 		    elseif($naks->getBilling->getFees->title == 'SCHOOL BUS'){

$bus_divider = 9;
                    $ranking = 0;
                    $adjustment = AdjustBilling::where('student_id',$stud_id->student_id)->where('fees_id','241')->where('school_year_id',$sy_id)->first();

                    if($adjustment != ''){
                        $rank = DtSyMonthTemplate::where('month_id',$adjustment->month_id)->first();

                        $bus_divider = $bus_divider - ($rank->template_id - 2);
                        $ranking = $rank->rankings - 3;

                    }
			
                        $date_counter = $date_counter - $ranking;
                        $tuition_amount = ($naks->getBilling->amount - $dis) / $bus_divider;
                        $over_all = $tuition_amount * ($date_counter - 3);
                        $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                        $bal = $over_all - $total;

                        if($bal <= 0){
                            $bal=0;
                        }
                    }
		    elseif($naks->getBilling->getFees->title == 'PTA'){
                        $tuition_amount = ($naks->getBilling->amount - $dis) / 3;
                        $over_all = $tuition_amount * ($date_counter - 3);
                        $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                        $bal = $over_all - $total;

                        if($bal <= 0){
                            $bal=0;
                        }
                    }
                     elseif($naks->getBilling->getFees->title == 'Books'){

                         $bks = DtPayments::with('getFees.getCategory')
                             ->where('account_id',$naks->account_iddd)
                             ->where('fees_id',$naks->getBilling->getFees->fees_id)
                             ->first();


                         $amp = 0;
                     if($bks != ''){
                         $amp = $bks->amount;
                     }
                    $tuition_amount = ($naks->getBilling->amount - $amp - $dis) / 3;
			
                    $over_all = $tuition_amount * ($date_counter - 3);
                    $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                    $bal = $over_all + $amp - $total;
                    if($bal <= 0){
                        $bal=0;
                    }
                     }
                    elseif($naks->getBilling->getFees->title == 'Registration Fee'){

                        $bks = DtPayments::with('getFees.getCategory')
                            ->where('account_id',$naks->account_iddd)
                            ->where('fees_id',$naks->getBilling->getFees->fees_id)
                            ->first();

                    $tuition_divider = 10;
                    $ranking = 0;
                    $adjustment = AdjustBilling::where('student_id',$stud_id->student_id)->where('school_year_id',$sy_id)->first();

                    if($adjustment != ''){
                        $rank = DtSyMonthTemplate::where('month_id',$adjustment->month_id)->first();

                        $tuition_divider = $tuition_divider - ($rank->template_id - 1);
                        $ranking = $rank->rankings - 3;

                    }
                        $amp = 0;
                        if($bks != ''){
                            $amp = $bks->amount;
                        }
			
                        $date_counter = $date_counter - $ranking;
                       $tuition_amount = ($naks->getBilling->amount - $amp - $dis) / $tuition_divider;
				
                        $over_all = $tuition_amount * ($date_counter - 2);
            		
                        $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
        		
                        $bal = $over_all + $amp - $total;
			$bal = $naks->getBilling->amount - $dis - $total;      		
			
                        if($bal <= 0){
                            $bal=0;
                        }
                    }
                    elseif($naks->getBilling->getFees->title == 'Tutorial' || $naks->getBilling->getFees->title == 'TUTORIAL'){
			
			$divider = ($naks->getBilling->amount - $dis) / 1500;
			$date_minus = 8 - $divider;
			
			if($divider == 0){$divider = 8;}	
                        $tuition_amount = ($naks->getBilling->amount - $dis) / $divider;
                        $over_all = $tuition_amount * ($date_counter - (3 + $date_minus));
                        $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                        $bal = $over_all - $total;
                        if($bal <= 0){
                            $bal=0;
                        }
                    }
                    else{
                        $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                        $bal = ($naks->getBilling->amount - $dis) - $total;
                    }

                $total_bal = $bal - $total;

                $naks->bal = $bal;

                    }



            }




        $pdf = Pdf::loadView('sms.reports.generate-monthly-billing',compact('date_now','billing'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }

    public function generateBalances(){
        
        $request = Request::all();

        $sy = RfSchoolYear::where('is_current', '1')->first();
        $sy_id = $sy->school_year_id;
        $date_now = date('F');


        $m = RfMonth::where('month_name', $date_now)->first();
        $month_id = $m->month_id;
        $ranks = DtSyMonthTemplate::where('month_id', $month_id)->first();
        $aw = DtSyMonthTemplate::where('rankings', '<=', $ranks->rankings)->get();
        foreach ($aw as $ew) {
            $dates[] = $ew->month_id;
        }

        $date_payments = DtPayments::with('getFees.getCategory')
            ->where(DB::raw('MONTH(date_of_payment)'), $dates[1])
            ->get();


        $billings = DtAccounts::query();
        $billings->with('getGradeLevel.getSection','getStudent','getStudentBill.getBilling.getFees.getCategory');
        $billings->where('dt_accounts.school_year_id', $sy_id);

     
        if(Input::has('student_id')){
            $billings->where('student_id',Input::get('student_id'));
        }
        if(Input::has('grade_level') != ''){
            $billings->where('dt_accounts.grade_level_id',Input::get('grade_level'));
            $billings->join('dt_students','dt_accounts.student_id','=','dt_students.student_id');
            $billings->join('dt_students_schedule','dt_students.student_id','=','dt_students_schedule.student_id');
            $billings->join('dt_schedule','dt_students_schedule.schedule_id','=','dt_schedule.schedule_id');
            $billings->join('rf_section','dt_schedule.section_id','=','rf_section.section_id');
            $billings->where('dt_schedule.school_year_id',$sy_id);
            $billings->where('dt_schedule.section_id',Input::get('section_name'));

        }
        $billing = $billings->get();



        foreach ($billing as $bill) {
            $emp = Schedule::with('getAdviser')->where('schedule_id',$bill->schedule_id)->first();

            $bill->adviser = $emp->getAdviser->first_name.' '.$emp->getAdviser->last_name;
            $bill->st = $emp->start_time;
            $bill->et = $emp->end_time;
            foreach ($bill->getStudentBill as $naks) {
                $total=0;
                    $naks->account_iddd = $naks->account_id;
                        $fees_id = $naks->getBilling->fees_id;
                        $date_counter = 0;
                        foreach($dates as $date){
                            $dates_payments = DtPayments::with('getFees.getCategory')
                                ->where(DB::raw('MONTH(date_of_payment)'), $date)
                                ->where('account_id',$naks->account_iddd)
                                ->where('fees_id',$fees_id)
                                ->get();
                            foreach($dates_payments as $paid){
                                $total= $total + $paid->amount;
                            }
                            $date_counter++;
                        }

                $discount = DtDiscount::with('getFees.getCategory')
                    ->where('fees_id',$fees_id)
                    ->where('account_id',$naks->account_iddd)
                    ->get();
                $dis = 0;
                if(!empty($discount)){
                    foreach($discount as $count){
                        $dis = $dis + $count->amount;
                    }
                }

                $stud_id = DtAccounts::find($naks->account_iddd);


                    if($naks->getBilling->getFees->title == 'Tuition'){


                        $tuition_divider = 10;
                        $ranking = 0;
                        $adjustment = AdjustBilling::where('student_id',$stud_id->student_id)->where('school_year_id',$sy_id)->first();

                        if($adjustment != ''){
                            $rank = DtSyMonthTemplate::where('month_id',$adjustment->month_id)->first();

                            $tuition_divider = $tuition_divider - ($rank->template_id - 1);
                            $ranking = $rank->rankings - 3;

                        }

                        $date_counter = $date_counter - $ranking;

                       $tuition_amount = ($naks->getBilling->amount - $dis) / $tuition_divider;
                        $over_all = $tuition_amount * ($date_counter - 2);
            
                        $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
        
                        $bal = $over_all - $total;
        
                        if($bal <= 0){
                            $bal=0;
                        }
                    }
                elseif($naks->getBilling->getFees->title == 'Miscellaneous'){
                    $tuition_amount = ($naks->getBilling->amount - $dis) / 5;
                    $over_all = $tuition_amount * ($date_counter - 7);
                    $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                    $bal = $over_all - $total;
                    if($bal <= 0){
                        $bal=0;
                    }
                }
                    else{
                        $naks->balance_due = ($naks->getBilling->amount - $dis) - $total;
                        $bal = ($naks->getBilling->amount - $dis) - $total;
                    }

                $total_bal = $bal - $total;

                $naks->bal = $bal;

                    }



            }

        $data = Schedule::with('RfSection.getGradeLevel')->where('section_id',Input::get('section_name'))->get();    


        $pdf = Pdf::loadView('sms.reports.generate-balances',compact('date_now','billing','data'));
        //return view('sms.reports.generate-balances',compact('date_now','billing','data'));
        return $pdf->setPaper('letter')->setOrientation('landscape')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }

    public function overallBalances(){


        $fees = RfFees::whereHas('getCategory',function($q){
            $q->where('title','Assessment');
        })->get();

        foreach($fees as $fee){

            $recievables = StudentBill::whereHas('getBilling',function($q) use ($fee){
                $q->where('fees_id',$fee->fees_id);
            })->with('getBilling')->get();
            $total_recievables = 0;

            foreach($recievables as $recieve){
                    $total_recievables = $total_recievables + $recieve->getBilling->amount;
            }

            $fee->recievables = $total_recievables;
            $recieve_payments = DtPayments::where('fees_id',$fee->fees_id)->get();
            $total_payments = 0;

            foreach($recieve_payments as $pay){
                $total_payments = $total_payments + $pay->amount;
            }
            $fee->payments = $total_payments;
            $dis = DtDiscount::where('fees_id',$fee->fees_id)->get();
            $total_discount = 0;

            foreach($dis as $d){
                $total_discount = $total_discount + $d->amount;
            }

            $fee->discount = $total_discount;
        }

        $pdf = Pdf::loadView('sms.reports.overall-balances',compact('fees'));
        //return view('sms.reports.generate-balances',compact('date_now','billing','data'));
        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }

    public function getStudentsCustom(){


        $rq = Request::all();

        $query = Students::select(db::Raw('group_concat(parents_name) as parents_name,dt_students.first_name,dt_students.gender,dt_students.middle_name,
                                dt_students.last_name,dt_students.birthday,dt_students.home_address,dt_students.lrn,dt_students.name_extension'))
                ->leftjoin('dt_students_schedule','dt_students.student_id','=','dt_students_schedule.student_id')
                ->leftjoin('dt_schedule','dt_students_schedule.schedule_id','=','dt_schedule.schedule_id')
                ->leftjoin('rf_section','dt_schedule.section_id','=','rf_section.section_id')
                ->leftjoin('rf_grade_level','rf_section.grade_level_id','=','rf_grade_level.grade_level_id')
                ->leftjoin('dt_parents_students','dt_parents_students.student_id','=','dt_students.student_id')
                ->leftjoin('dt_parents','dt_parents.parent_id','=','dt_parents_students.parent_id')
                ->groupby('dt_students.student_id')
		->orderby('gender','desc')
                ->orderby('last_name','asc');


        $headers = [];

        if($rq['schoolYear'] != "All"){
            $query->where('dt_schedule.school_year_id', $rq['schoolYear']);
            $school_year = RfSchoolYear::find($rq['schoolYear']);
            $headers["school_year"] = $school_year->school_year; 
        }else{
            $headers["school_year"] = "All";
        }
        
        if($rq['student_status'] != "All"){
            $query->where('dt_students_schedule.student_status_id', $rq['student_status']);  
            $student_status = Student_Status::find($rq['student_status']);
            $headers["student_status"] = $student_status->student_status; 
        }else{
            $headers["student_status"] = "All";
        }   

        if($rq['gradeType'] != "All"){
            $query->where('rf_grade_level.grade_type_id', $rq['gradeType']);  
            $grade_type = RfGradeType::find($rq['gradeType']);
            $headers["grade_type"] = $grade_type->grade_type; 
        }else{
            $headers["grade_type"] = "All"; 
        }

        if($rq['grade_level'] != "All"){
            $query->where('rf_section.grade_level_id', $rq['grade_level']);  
            $grade_level = RfGradeLevel::find($rq['grade_level']);
            $headers["grade_level"] = $grade_level->grade_level; 
        }else{
            $headers["grade_level"] = "All"; 
        }

        if($rq['sectionName'] != "All"){
            $query->where('rf_section.section_id', $rq['sectionName']);  
            $section = RfSection::find($rq['sectionName']);
            $headers["sectionName"] = $section->section_name; 
        }else{
            $headers["sectionName"] = "All"; 
        }
        
        
          

        

        
        

        $students = $query->get();


        $pdf = Pdf::loadView('sms.reports.custom-students-report',compact('students','headers'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');


    }

    public function getInfo(){

        $student = Students::with('Parents_Students.Parents.Religion','Parents_Students.Parents.Occupation','Parents_Students.Parents.Nationality')
                            ->find(Input::get('student_id'));

        $pdf = Pdf::loadView('sms.reports.student-info',compact('student'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }

    public function printLedger(){
        $student_id = Input::get('student_id');
        $student = Students::find($student_id);

        $accounts   = DtAccounts::with('getSchoolYear','getPayments.getFees.getCategory','getPayments.getPaymentOption')
            ->where('student_id',$student_id)
            ->get();


        foreach($accounts as $bal){
            $balance = DtStudentBill::with('getBilling.getFees.getCategory')
                ->where('account_id',$bal->account_id)
                ->get();
            $total_balance    = 0;
            $total_discount   = 0;
            $total_amount_due = 0;

            foreach($balance as $getTotal){
                if($getTotal->getBilling->getFees->getCategory->title == 'Assessment'){
                    $total_balance = $total_balance + $getTotal->getBilling->amount;
                }
            }


            $discount = DtDiscount::where('account_id',$bal->account_id)->get();
            foreach($discount as $dis){
                $total_discount = $total_discount + $dis->amount;
            }

            $total_amount_due = $total_balance - $total_discount;
            $bal->amount_due = number_format($total_amount_due,2,'.',',');
            $total_balance = $total_balance - $total_discount;

            foreach($bal->getPayments as $getbal){
                if($getbal->getFees->getCategory->title == 'Assessment'){
                    $total_balance = $total_balance - $getbal->amount;
                    $getbal->bal = number_format($total_balance,2,'.',',');
                }
                else{
                    $getbal->bal = '';
                }
            }

        }

        $pdf = Pdf::loadView('sms.reports.student-ledger',compact('student','accounts'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }

    public function printSchedule($schedule_id){


        $classType    = RfClassType::all();
        $gradeType    = RfGradeType::all();
        $schoolYear   = RfSchoolYear::all();
        $weekdays     = Weekdays::all();
        $employees    = KronosEmployee::all();

        $schedule = Schedule::with('RfSchoolYear','getAdviser','RfSection.getSectionType','RfSection.getGradeLevel')->find($schedule_id);

        $section_type_id = $schedule->RfSection->getSectionType->section_type_id;

        $assignSubject = DtAssignSubject::with('getSubjects')->where('section_type_id',$section_type_id)->get();

        $handlesubjects = Weekdays::with('HandleSubjects.DtAssignSubject.getSubjects','HandleSubjects.getEmployee')
                        ->whereHas('HandleSubjects', function($q) use ($schedule_id){
                             $q->where('schedule_id', $schedule_id)
                               ->orderby('start_time');
                        })
                        ->get();



        $sched_id = $schedule_id;

        $pdf = Pdf::loadView('sms.reports.schedule',compact('handlesubjects','schedule','employees','weekdays','gradeType','schoolYear','classType','sched_id','assignSubject'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }

    public function printTeacherSched(){

        $sy = RfSchoolYear::where('is_current','1')->first();

        $schedule = Schedule::join('dt_handle_subject','dt_schedule.schedule_id','=','dt_handle_subject.schedule_id')
            ->join('dt_employee','dt_handle_subject.employee_id','=','dt_employee.employee_id')
            ->join('dt_assign_subject','dt_handle_subject.assign_subject_id','=','dt_assign_subject.assign_subject_id')
            ->join('rf_subject','dt_assign_subject.subject_id','=','rf_subject.subject_id')
            ->join('rf_weekdays','dt_handle_subject.weekdays_id','=','rf_weekdays.weekdays_id')
            ->join('rf_section','dt_schedule.section_id','=','rf_section.section_id')
            ->where('dt_employee.employee_id',Input::get('teacher_id'))
            ->where('dt_schedule.school_year_id',$sy->school_year_id)
            ->select('rf_weekdays.weekdays','rf_subject.subject_name','dt_handle_subject.start_time','dt_handle_subject.end_time','rf_section.section_name')
            ->orderBy('rf_weekdays.weekdays','asc')
            ->get();
        $teacher = KronosEmployee::find(Input::get('teacher_id'));
        $sched = Schedule::with('RfSection')
            ->where('employee_id',Input::get('teacher_id'))
            ->where('school_year_id',$sy->school_year_id)
            ->first();

        $pdf = Pdf::loadView('sms.reports.teacher-schedule',compact('schedule','sched','teacher'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');

    }

    public function printIncome(){

        $from = Input::get('from');
        $to   = Input::get('to');
        $date_from = date("Y-m-d", strtotime($from));
        $date_to   = date("Y-m-d", strtotime($to));

        $payments = DtPayments::with('getFees','getAccount.getStudent','getPaymentOption')
            ->where('date_of_payment','>=', $date_from)
            ->where('date_of_payment','<=',$date_to)
            ->orderBy('date_of_payment','asc')
            ->orderBy('or_no','asc')
            ->get();


        $pdf = Pdf::loadView('sms.reports.print-income',compact('payments','from','to'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }
    public function printExpenses(){

        $from = Input::get('from');
        $to   = Input::get('to');
        $date_from = date("Y-m-d", strtotime($from));
        $date_to   = date("Y-m-d", strtotime($to));



        $expenses = DtExpenses::with('getFees','getPaymentOption')
            ->where('date','>=', $date_from)
            ->where('date','<=',$date_to)
            ->orderBy('date','asc')
            ->get();



        $pdf = Pdf::loadView('sms.reports.print-expenses',compact('expenses','from','to'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }
    public function printOthers(){

        $from = Input::get('from');
        $to   = Input::get('to');
        $date_from = date("Y-m-d", strtotime($from));
        $date_to   = date("Y-m-d", strtotime($to));


        $other = DtOtherPayment::with('getFees','getPaymentOption')
            ->where('date_of_payment','>=', $date_from)
            ->where('date_of_payment','<=',$date_to)
            ->orderBy('date_of_payment','asc')
            ->orderBy('or_no','asc')
            ->get();



        $pdf = Pdf::loadView('sms.reports.print-others',compact('other','from','to'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 2)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 2)
            ->stream('report.pdf');
    }
    public function printGeneral(){
        $from = Input::get('from');
        $to   = Input::get('to');
        $date_from = date("Y-m-d", strtotime($from));
        $date_to   = date("Y-m-d", strtotime($to));

        $payments = DtPayments::with('getFees','getAccount.getStudent','getPaymentOption')
            ->where('date_of_payment','>=', $date_from)
            ->where('date_of_payment','<=',$date_to)
            ->orderBy('date_of_payment','asc')
            ->orderBy('or_no','asc')
            ->get();

        $expenses = DtExpenses::with('getFees','getPaymentOption')
            ->where('date','>=', $date_from)
            ->where('date','<=',$date_to)
            ->orderBy('date','asc')
            ->get();
        $other = DtOtherPayment::with('getFees','getPaymentOption')
            ->where('date_of_payment','>=', $date_from)
            ->where('date_of_payment','<=',$date_to)
            ->orderBy('date_of_payment','asc')
            ->orderBy('or_no','asc')
            ->get();

        $acc_income = RfAccount::where('acccount_type_id','1')->get();
        $acc_expenses = RfAccount::where('acccount_type_id','2')->get();
        $total_income = 0;
        $total_expenses = 0;
        foreach($acc_income as $ai){
            $amount = 0;
            foreach($payments as $pay){
                if($pay->getFees->account_code == $ai->account_code){
                    $amount = $amount + $pay->amount;
                }
            }
            foreach($other as $others){
                if($others->getFees->account_code == $ai->account_code){
                    $amount = $amount + $others->amount;
                }
            }
            $total_income = $total_income + $amount;
            $ai->amount = number_format($amount,2,'.',',');
        }
        foreach($acc_expenses as $exp){
            $amount = 0;
            foreach($expenses as $ex){
                if($ex->getFees->account_code == $exp->account_code){
                    $amount = $amount + $ex->amount;
                }
            }
            $total_expenses = $total_expenses + $amount;
            $exp->amount = number_format($amount,2,'.',',');
        }


        $total_expenses = number_format($total_expenses,2,'.',',');
        $total_income  = number_format($total_income,2,'.',',');

        $pdf = Pdf::loadView('sms.reports.print-general',compact('total_expenses','total_income','acc_expenses','acc_income','from','to'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 4)
            ->setOption('margin-left', 4)
            ->setOption('margin-right', 4)
            ->setOption('margin-top', 4)
            ->stream('report.pdf');
    }
    public function printGeneralByMonth(){

        $from = Input::get('from');
        $to   = Input::get('to');
        $date_from = date("Y-m", strtotime($from));
        $date_to   = date("Y-m", strtotime($to));


        $acc_income = RfAccount::where('acccount_type_id','1')->get();
        $acc_expenses = RfAccount::where('acccount_type_id','2')->get();
        $total_income = 0;
        $total_expenses = 0;
        foreach($acc_income as $ai){
            $date_from = date("Y-m", strtotime($from));
            $date_to   = date("Y-m", strtotime($to));
            $amount = 0;
            $counter = 0;
            while($date_from <= $date_to){
                $all_date[$counter]= $date_from;
                $d = date('m', strtotime($date_from));
                $y = date('Y', strtotime($date_from));
                $payments = DtPayments::join('rf_fees','dt_payments.fees_id','=','rf_fees.fees_id')
                    ->join('rf_account','rf_fees.account_code','=','rf_account.account_code')
                    ->where(DB::raw('MONTH(dt_payments.date_of_payment)'),$d)
                    ->where(DB::raw('YEAR(dt_payments.date_of_payment)'),$y)
                    ->where('rf_account.account_code',$ai->account_code)
                    ->sum('dt_payments.amount');
                $other = DtOtherPayment::join('rf_fees','dt_other_payment.fees_id','=','rf_fees.fees_id')
                    ->join('rf_account','rf_fees.account_code','=','rf_account.account_code')
                    ->where(DB::raw('MONTH(dt_other_payment.date_of_payment)'),$d)
                    ->where(DB::raw('YEAR(dt_other_payment.date_of_payment)'),$y)
                    ->where('rf_account.account_code',$ai->account_code)
                    ->sum('dt_other_payment.amount');
                $amount = $amount + $payments + $other;
                $income_date[$ai->account_code][$counter] = $payments + $other;
                $counter++;
                 $d++;
                if($d > 12){
                    $d = 1;
                    $y++;
                }
                $date_increment = $y.'-'.$d;
                $date_from = date('Y-m',strtotime($date_increment));

            }

            $total_income = $total_income + $amount;
            $ai->amount = number_format($amount,2,'.',',');
        }

        foreach($acc_expenses as $exp){
            $date_from = date("Y-m", strtotime($from));
            $date_to   = date("Y-m", strtotime($to));
            $amount = 0;
            $counter = 0;
            while($date_from <= $date_to){
                $d = date('m', strtotime($date_from));
                $y = date('Y', strtotime($date_from));
                $expen = DtExpenses::join('rf_fees','dt_expenses.fees_id','=','rf_fees.fees_id')
                    ->join('rf_account','rf_fees.account_code','=','rf_account.account_code')
                    ->where(DB::raw('MONTH(dt_expenses.date)'),$d)
                    ->where(DB::raw('YEAR(dt_expenses.date)'),$y)
                    ->where('rf_account.account_code',$exp->account_code)
                    ->sum('dt_expenses.amount');
                $amount = $amount + $expen;
                $expenses_date[$exp->account_code][$counter] = $expen ;
                $counter++;
                $d++;
                if($d > 12){
                    $d = 1;
                    $y++;
                }
                $date_increment = $y.'-'.$d;
                $date_from = date('Y-m',strtotime($date_increment));
            }

            $total_expenses = $total_expenses + $amount;
            $exp->amount = number_format($amount,2,'.',',');
        }



        $total_expenses = number_format($total_expenses,2,'.',',');
        $total_income  = number_format($total_income,2,'.',',');

        $pdf = Pdf::loadView('sms.reports.print-monthly-income',compact('expenses_date','income_date','all_date','total_expenses','total_income','acc_expenses','acc_income','from','to'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 4)
            ->setOption('margin-left', 4)
            ->setOption('margin-right', 4)
            ->setOption('margin-top', 4)
            ->stream('report.pdf');
    }
    public function getPaymentList(){
            $grade_type  = Input::get('grade_type');
            $grade_level = Input::get('grade_level');
            $fees        = Input::get('fees');
            $section     = Input::get('section');



            $sy = RfSchoolYear::where('is_current','1')->first();
            $list = DtAccounts::query();
            $list->with('getStudent','getGradeLevel');
            $list->join('dt_payments','dt_accounts.account_id','=','dt_payments.account_id');
             $list->join('dt_students','dt_accounts.student_id','=','dt_students.student_id');
            $list->join('rf_fees','dt_payments.fees_id','=','rf_fees.fees_id');
        if($section > 0){
            $list->join('dt_students_schedule','dt_students.student_id','=','dt_students_schedule.student_id');
            $list->join('dt_schedule','dt_students_schedule.schedule_id','=','dt_schedule.schedule_id');
            $list->where('dt_schedule.section_id',$section);
        }
            $list->where('dt_accounts.school_year_id',$sy->school_year_id);
            $list->where('dt_payments.fees_id',$fees);
            if($grade_level != 'All'){
                $list->where('dt_accounts.grade_level_id',$grade_level);
            }

            $payment_list = $list->get();

        $datatableFormat = new DatatableFormat();
        return $datatableFormat->format($payment_list);

    }
    public function printPaymentList(){

        $grade_type  = Input::get('grade_type');
        $grade_level = Input::get('grade_level');
        $fees        = Input::get('fees');
        $section     = Input::get('section');



        $sy = RfSchoolYear::where('is_current','1')->first();
        $list = DtAccounts::query();
        $list->join('dt_students','dt_accounts.student_id','=','dt_students.student_id');
        $list->join('rf_grade_level','dt_accounts.grade_level_id','=','rf_grade_level.grade_level_id');
        $list->join('dt_payments','dt_accounts.account_id','=','dt_payments.account_id');
        $list->join('rf_fees','dt_payments.fees_id','=','rf_fees.fees_id');
        if($section > 0){
         $list->join('dt_students_schedule','dt_students.student_id','=','dt_students_schedule.student_id');
         $list->join('dt_schedule','dt_students_schedule.schedule_id','=','dt_schedule.schedule_id');
         $list->where('dt_schedule.section_id',$section);
        }
        $list->where('dt_accounts.school_year_id',$sy->school_year_id);
        $list->where('dt_payments.fees_id',$fees);
        if($grade_level != 'All'){
            $list->where('dt_accounts.grade_level_id',$grade_level);
        }

        $list->orderBy('rf_grade_level.grade_level');
        $list->orderBy('dt_students.last_name');
        $fee = RfFees::find($fees);
        $payment_list = $list->get();

        $pdf = Pdf::loadView('sms.reports.print-payment-list',compact('payment_list','fee'));

        return $pdf->setPaper('letter')->setOrientation('portrait')
            ->setOption('margin-bottom', 4)
            ->setOption('margin-left', 4)
            ->setOption('margin-right', 4)
            ->setOption('margin-top', 4)
            ->stream('report.pdf');

    }
}

