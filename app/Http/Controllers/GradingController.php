<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Students;
use DatatableFormat;
use App\RfSection;
use App\StudentSchedule;
use App\Schedule;
use App\KronosEmployee;
use App\RfGradeLevel;
use App\RfSchoolYear;
use App\RfSectionType;
use App\RfGradeType;
use App\Student_Status;
use App\GradingType;
use App\StudentsProgress;
use App\DtAssignSubject;
use App\CardTemplate;


class GradingController extends Controller
{
    public function gradingList(Request $request){

    	$students = Students::with('Parents_Students','Parents_Students.Parents')->get();
        $gradeType = RfGradeType::where('grade_type','!=','All')->get();
        $sy         = RfSchoolYear::all();
        $sectionType = RfSectionType::all();
        $student_status = Student_Status::all();

    	return view('sms.grading.grading-list',compact('student_status','gradeType','sectionType','sy'))->with('students',$students);

	}

	public function gradingSetup(Request $request){

        $gradingType = GradingType::all();
		$students = Students::with('Parents_Students','Parents_Students.Parents')->get();
        $gradeType = RfGradeType::where('grade_type','!=','All')->get();
        $sy         = RfSchoolYear::all();
        $sectionType = RfSectionType::all();
        $student_status = Student_Status::all();

    	return view('sms.grading.grading-setup',compact('student_status','gradeType','sectionType','sy','students','gradingType'));

	}

    public function getCard(Request $request){

        return $request['students_schedule_id'];

        $check = StudentsProgress::where('students_schedule_id',$request['students_schedule_id'])
                                ->get();

        $gradingType = GradingType::all();
        

        if(count($check) > 0){



        }else{
           
            $schedule = StudentSchedule::with('getSchedule','getSchedule.RfSection')->where('students_schedule_id',$request['students_schedule_id'])
                                ->first();

            $subjects = DtAssignSubject::with('getSubjects')->where('grade_level_id',$schedule->getSchedule->RfSection->grade_level_id)
                                        ->groupby('subject_id')
                                        ->get();
        
            return view('sms.grading.new-card',compact('subjects','gradingType'));    
        }

    }

    public function newCardTemplate(Request $rq){


        $gradingType = GradingType::all();
        $RfGradeLevel = RfGradeLevel::all();
        $students = Students::with('Parents_Students','Parents_Students.Parents')->get();
        $gradeType = RfGradeType::where('grade_type','!=','All')->get();
        $sy         = RfSchoolYear::all();
        $sectionType = RfSectionType::all();
        $student_status = Student_Status::all();


        return view('sms.grading.newCard-template',compact('student_status','gradeType','sectionType','sy','students','gradingType','RfGradeLevel'));
    }

    public function getCardTemplate(Request $rq){


        $templateCard = CardTemplate::where('grade_level_id',$rq['grade_level_id'])
                                    ->where('school_year_id',$rq['school_year_id'])
                                    ->get();
        $gradingType = GradingType::all();

        if(count($templateCard) == ""){

            $templateCard = DtAssignSubject::with('getSubjects')
                                    ->where('grade_level_id',$rq['grade_level_id'])
                                    ->where('school_year_id',$rq['school_year_id'])
                                    ->where('section_type_id',$rq['section_type_id'])
                                    ->get();
        }

        return view('sms.grading.template-card-data',compact('templateCard','gradingType'));

    }

    public function saveTemplate(Request $rq){



        /*foreach($rq['subject_id'] as $subject_id){

            $cardTemplate    = new CardTemplate();
            $cardTemplate->grade_level_id  = $rq['grade_level_id'];
            $cardTemplate->subject_id      = $subject_id;
            $cardTemplate->school_year_id  = $rq['school_year_id'];
            $cardTemplate->section_type_id = $rq['section_type_id'];
            $cardTemplate->save();
        }*/


        
    }
}
