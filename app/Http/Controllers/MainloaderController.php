<?php

namespace App\Http\Controllers;

use App\DtExpenses;
use App\DtOtherPayment;
use App\DtPayments;
use App\KronosEmployee;
use App\RfAccountType;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\controller_retrieve;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


//=====data=====
use Auth;
use DB;
use Session;
use Mail;
use App\RfClassType;
use App\RfGradeType;
use App\RfSectionType;
use App\RfSchoolYear;
use App\RfSubjects;
use App\RfGradeLevel;
use App\RfAccount;
use App\RfFeeCategories;
use App\RfFees;
use App\Students;
use App\RfPaymentType;
use App\RfMonth;
use App\Student_Status;

class MainloaderController extends Controller
{
    
    public function __construct(){
      
      $this->middleware('auth');
      
    }
    
    public function view(){

    	return view('sms.main.index');
    }
    
    public function getTeachers(){
        $teachers = KronosEmployee::all();

        return view('sms.registrar.teacher-schedule',compact('teachers'));

    }
    
    public function home(){

      return view('sms.main.index');
    }

    public function studentRegistration(){
      
      return view('sms.registrar.student-registration');
    }

  
    public function enrollment(){

      $classType  = RfClassType::all();
      $gradeType  = RfGradeType::all();
      $subject    = RfSubjects::all();
      $student    = Students::all();
      $status     = Student_Status::all();


      return view('sms.registrar.enrollment', compact('student','classType','gradeType','subject','status'));
    }

    public function enrolledStudent(){
      return view('sms.registrar.enrolled-student');
    }

    public function studentRequirement(){
      return view('sms.registrar.student-requirements');
    }


    // A C A D E M I C S

    public function academicsSetup(){
      $classType    = RfClassType::all();
      $gradeType    = RfGradeType::all();
      $sectionType  = RfSectionType::all();
      $schoolYear   = RfSchoolYear::all();
      $subject      = RfSubjects::all();
      $gradeLevel   = RfGradeLevel::all();
      
      return view('sms.setup.academics', compact('classType','gradeType','sectionType','schoolYear','subject','gradeLevel'));
    }

    public function schoolYear(){
      return view('sms.setup.school-year');
    }

    public function symonthTemplate(){
      return view('sms.setup.sy-month-template');
    }

    public function gradeLevel(){

      $gradeType    = RfGradeType::all();

      return view('sms.setup.grade-level',compact('gradeType'));
    }

    public function section(){

      $gradeType    = RfGradeType::all();
      $sectionType  = RfSectionType::all();
      return view('sms.setup.section',compact('gradeType','sectionType'));
    }

    public function subject(){
      return view('sms.setup.subject');
    }

    public function assignSubject(){
      $gradeType    = RfGradeType::all();
      $sectionType  = RfSectionType::all();
      $subject      = RfSubjects::all();
      return view('sms.setup.assign-subject',compact('subject','gradeType','sectionType'));
    }

    public function schedule(){
      $classType    = RfClassType::all();
      $gradeType    = RfGradeType::all();
      $schoolYear   = RfSchoolYear::all();
      return view('sms.setup.schedule',compact('gradeType','schoolYear','classType'));
    }

    //B I L L I N G and F E E S

    public function studentAssessment(){
        $sy = RfSchoolYear::where('is_current','1')->first();
      $students = Students::all();
        $months = RfMonth::all();
        $tuition = RfFees::where('title','Tuition')->first();
	$schoolbus = RfFees::where('title','SCHOOL BUS')->first();

      return view('sms.billing.assessment',compact('sy','months','tuition','schoolbus'))
            ->with('students',$students);
    }
    public function assignFees(){
        $status = Student_Status::all();
        $students = Students::all();
        $sy = RfSchoolYear::where('is_current','1')->first();


      return view('sms.billing.assign-fees',compact('status','students','sy'));
    }
    public function studentBill(){
        $status = Student_Status::all();
        $students = Students::all();
        $sy = RfSchoolYear::where('is_current','1')->first();
        $date_now = date("Y-m-d");
      return view('sms.billing.student-bill',compact('status','students','sy','date_now'));
    }

    public function requirements(){
      return view('sms.setup.requirements');
    }



    // S E T U P - B I L L I N G


    public function categoryFee(){
      return view('sms.setup.fee-category');
    }

    public function setupFees(){

      $categories = RfFeeCategories::all();
      $accounts    = RfAccount::all();
      return view('sms.setup.fees',compact('categories','accounts'));
    }

    public function levelFees(){
        $gradeType    = RfGradeType::all();
        $fees = RfFees::all();
        $sy = RfSchoolYear::where('is_current','1')->first();
        $recall = RfSchoolYear::where('is_current','!=','1')->get();
        $categories = RfFeeCategories::all();
        $accounts    = RfAccount::all();
      return view('sms.setup.grade-level-fees',compact('gradeType','fees','categories','accounts','sy','recall'));
    }

    public function paymentTypeSchedules(){
      $months = RfMonth::all();
      $paymentTypes = RfPaymentType::all();
      return view('sms.setup.payment-type-schedule',compact('months','paymentTypes'));
    }

    public function tuitionReference(){
      return view('sms.setup.tuition-reference');
    }

    public function dueDates(){
         $months = RfMonth::all();
      return view('sms.setup.due-dates',compact('months'));
    }

    public function lesseeSetup(){    
      return view('sms.setup.rental');
    }

    public function tenantFees(){    
      return view('sms.setup.tenant-fees');
    }

    public function setupAccounts(){
            $type = RfAccountType::all();
        return view('sms.setup.accounts',compact('type'));
    }


    //E - C L A S S

    public function Attendance(){

      return view('sms.e-class-room.attendance');
    }


    // K R O N O S

    public function timeAttendance(){
      return view('sms.kronos.time-attendance');
    }

    public function schedulling(){
      return view('sms.kronos.schedulling');
    }

   public function hr(){
      return view('sms.kronos.human-resource');
    }
    

    public function masterlist(){
      $gradeType    = RfGradeType::all();
      return view('sms.reports.masterlist',compact('gradeType'));
    }
    public function monthlyBilling(){
        $gradeType    = RfGradeType::all();
        return view('sms.billing.monthly-billing',compact('gradeType'));
    }

    public function otherIncome(){

        $gradeType    = RfGradeType::all();
        $fees = RfFees::all();
        $sy = RfSchoolYear::where('is_current','1')->first();
        $categories = RfFeeCategories::all();
        $accounts    = RfAccount::all();
        return view('sms.billing.other-income',compact('gradeType','fees','categories','accounts','sy'));
    }
    public function Expenses(){

        $gradeType    = RfGradeType::all();
        $fees = RfFees::all();
        $sy = RfSchoolYear::where('is_current','1')->first();
        $categories = RfFeeCategories::all();
        $accounts    = RfAccount::with('getAccountType')->where('acccount_type_id','!=','1')->get();
        return view('sms.billing.expenses',compact('gradeType','fees','categories','accounts','sy'));
    }

    public function collections(){

            $date = date('Y-m-d');

            $payments = DtPayments::with('getFees','getAccount.getStudent','getPaymentOption')
                                    ->where('date_of_payment', $date)
                                    ->get();

            $expenses = DtExpenses::with('getFees','getPaymentOption')
                                    ->where('date', $date)
                                    ->get();
            $other = DtOtherPayment::with('getFees','getPaymentOption')
                            ->where('date_of_payment', $date)
                            ->get();

        return view('sms.reports.collections', compact('payments','expenses','other'));
    }

    public function paymentList(){
        $gradeType    = RfGradeType::all();
        $fees  = RfFees::all();
        return view('sms.reports.payment-list',compact('gradeType','fees'));
    }
    public function journalEntry(){

        $account = RfAccount::with('getAccountType')->get();
        return view('sms.billing.journal-entry',compact('account'));
    }
    public function journalEntries(){

      return view('sms.reports.journal-entries');
    }

}
