<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Request;
use Fpdf;
use basicListing;
use LeadSoftModel;
use LeadAuth;
use ModuleRestriction;
use Storage;
use Response;
use datatableFormat;
use Validator;
use App\User;
use App\Module;
use App\SubModule;
use App\Roles;
use App\Permissions;
use App\KronosEmployee;
use Auth;
use DB;
use Session;
use Hash;
use Mail;
use App\VerificationModel;

class WyredController extends Controller
{


	public function makeProfile()
	{	

		$return = new rrdReturn();

		if(Request::input('user_id') != ""){
			$check = User::where('username', Request::input('email'))
					 ->where('password',Hash::make(Request::input('password')))
					 ->where('user_id','<>',Request::input('user_id'))
					 ->get();
		}else{
			$check = User::where('username', Request::input('email'))
					 ->where('password',Hash::make(Request::input('password')))
					 ->get();
		}
		

		if(count($check) > 0){	 
			return $return->status(false)
	                      ->message("Sorry, username already exist. Try another one maybe .!.")
	                      ->show();
		}	

		if(Request::input('user_id') != ""){
				$users  = User::find(Request::input('user_id')); 

		}else{
				$users  = new User(); 
		}
		$users->username    = Request::input('email');
		if(Request::input('password') != ""){
			$users->password    = Hash::make(Request::input('password'));
		}
		$users->role_id     = Request::input('role_id');
		$users->date_exp    = Request::input('date_exp');
		$users->employee_id = Request::input('employee_id');
		$users->save();

		return $return->status(true)
	                      ->message("That is amazing! User has been Created!.")
	                      ->show();
	}

	public function createAccount(){

		return view('admin.security.account.accountRegistration');

	}

	public function forgotPassword(){

		return view('admin.security.login.forgotPassword');
	}

	public function requestVerificationCode(){

		return view('admin.security.login.verificationCode');
	}

	

	public function passwordReset(){


		$return = new rrdReturn();

		$date = date("Y/m/d");
		$thisDate = str_replace('/', '-', $date);
		$user = db::table('tbl_users')
				->where('username',Request::input("email"))
				->first();

		if(count($user) > 0){


			$verifyVCode = db::table('dat_verification_code')
				->where('verification_code',Request::input("verification_code"))
				->get();

			

			if(count($verifyVCode) == 0){

				return $return->status(false)
		                      ->message("Verification Code is invalid.Try again.")
		                      ->show();
			}else{

				$codeExpires = db::table('dat_verification_code')
				->where('verification_code',Request::input("verification_code"))			
				->where('date_expire','=',$thisDate)	
				->get();

				if(count($codeExpires) == 0){

					return $return->status(false)
			                      ->message("Verification Code expired. Please request another one.")
			                      ->show();
				}
			}

			$userVerification = db::table('tbl_users')
				->where('user_id',$user->user_id)
				->update([
							'password' => bcrypt(Request::input("new_password"))
						 ]);
		}else{

			return $return->status(false)
		                      ->message("Email is not registered. Try again.")
		                      ->show();
		}

		$deleteVerificationCode = db::table('dat_verification_code')
				->where('user_id',$user->user_id)
				->update([
							'verification_code' => ''
						 ]);

		return $return->status(true)
		                      ->message("Password has been updated!")
		                      ->show();

	}

	public function sendVerificationCode(){

		$return = new rrdReturn();

		$user = db::table('tbl_users')
				->where('username',Request::input("email"))
				->first();
		do {

			$verificationCode = str_random(20);

			$verify = db::table('dat_verification_code')
				->where('verification_code',$verificationCode)
				->get();

		} while (count($verify) > 0);


		if(count($user) == 0){

			return $return->status(false)
		                      ->message("Email is not a registered. Try again.")
		                      ->show();
		} 

		Mail::send('admin.mail.verificationCode', ['vcode' => $verificationCode], function($message)
        {
            $message->to(Request::input("email"), 'SMI')->subject('Verification Code');
        });


		$date = date("Y/m/d");
		$expire = str_replace('-', '/', $date);




		$userVerification = db::table('dat_verification_code')
				->where('user_id',$user->user_id)
				->get();

		if(count($userVerification) > 0){

			$userVerification = db::table('dat_verification_code')
				->where('user_id',$user->user_id)
				->update(['verification_code' => $verificationCode ,
						  'date_expire'		 => $expire
						 ]);
		}else{

			$verification                    = new VerificationModel; 
			$verification->user_id           = $user->user_id;
			$verification->verification_code = $verificationCode;
			$verification->date_expire       = $expire;
			$verification->save();
		}

		

		return $return->status(true)
		                      ->message("Verification Code has been sent!")
		                      ->show();

	}

	public function securityMenu(){

		return view('admin.security.Restrictions.security');
	}

	public function securityModules(){
		$modules = Module::all();
		return view('admin.security.Restrictions.security-modules',compact('modules'));
	}

	public function newModule(){

		$module = new Module();
		$module->module_name = Request::input('module_name');
		$module->save();

		$return = new rrdReturn();
      	return $return->status(true)
                            ->message("New module has been created!")
                            ->show(); 
	}

	

	public function getModules(){

		return Module::all();
	}

	public function newSubModule(){

		$sub_module                  = new SubModule();
		$sub_module->module_id       = Request::input('module_id');
		$sub_module->sub_module_name = Request::input('sub_module_name');
		$sub_module->description     = Request::input('description');
		$sub_module->save();


		$return = new rrdReturn();
      	return $return->status(true)
                            ->message("New Sub-module has been created!")
                            ->show(); 
	}

	public function getSubModules(){

		return SubModule::with('Module')->get();
	}

	public function newRoles(){
		return view('admin.security.Restrictions.roles');
	}

	public function newRole(){

		$roles                   = new Roles();
		$roles->role_name        = Request::input('role_name');
		$roles->role_description = Request::input('role_description');
		$roles->save();


		$return = new rrdReturn();
      	return $return->status(true)
                            ->message("New role has been created!")
                            ->show(); 
	}

	public function getRoles(){

		return Roles::all();
	}

	public function newPermissions(){

		$roles = Roles::all();
		return view('admin.security.Restrictions.permissions',compact('roles'));
	}

	public function savePermissions(){

		Permissions::where('role_id',Request::input('role_id'))->delete();

		if(Request::input('data') != ""){
			foreach(Request::input('data') as $sub_module){
				$permissions                = new Permissions();
				$permissions->role_id       = Request::input('role_id');
				$permissions->sub_module_id = $sub_module;
				$permissions->save();
			}
		}
		
		$return = new rrdReturn();
      	return $return->status(true)
                            ->message("Permissions has been successfully assigned!")
                            ->show(); 
	}

	public function getPermissions(){


		$SubModule = SubModule::all();


		foreach ($SubModule as $Sub) {

			$check = Permissions::where('sub_module_id',$Sub->sub_module_id) 
								->where('role_id',Request::input('role_id')) 
								->get();

			if(count($check) > 0){
				$Sub->exist = true;
			}else{
				$Sub->exist = false;
			}
		}

		return $SubModule;
	}

	public function securityAccount(){

		$employee = KronosEmployee::all();
		$roles = Roles::all();

		return view('admin.security.account.accountRegistration',compact('roles','employee'));

	}

	public function getAccountsList(){


		$user = User::with('KronosEmployee','Roles')
					->where('employee_id','<>' ,null)
					->get();
		return $user;
	}
}
