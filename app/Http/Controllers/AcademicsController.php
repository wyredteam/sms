<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;


use App\Http\Controllers\controller_retrieve;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


//=====data=====

use Auth;
use DB;
use Session;
use Mail;


use App\RfClassType;
use App\RfGradeType;
use App\RfSectionType;
use App\RfSchoolYear;
use App\RfSubjects;
use App\RfGradeLevel;
use App\Schedule;
use App\DtAssignSubject;
use App\HandleSubjects;
use App\Weekdays;
use App\KronosEmployee;
use DatatableFormat;

class AcademicsController extends Controller
{
    
    public function subjectAssigningSections(Request $rq){



        if($rq['schedule_id'] != ''){
            $Schedule = Schedule::find($rq['schedule_id']);
        }
        else {
            $checkifScheduleExists = Schedule::where('section_id', $rq['section_id'])
                ->where('school_year_id', $rq['school_year_id'])
                ->get();


            if (count($checkifScheduleExists) > 0) {
                $return = new rrdReturn();
                return $return->status(false)
                    ->message("Seems like this section already have a schedule, please go back to the table and check the section to edit. Thank you!")
                    ->show();
            }
            $Schedule             = new Schedule();
        }
        $Schedule->section_id     = $rq['section_id'];
        $Schedule->employee_id    = $rq['adviser'];
        $Schedule->slot           = $rq['slot'];
        $Schedule->start_time     = $rq['section_start_time'];
        $Schedule->end_time       = $rq['section_end_time'];
        $Schedule->class_type_id  = $rq['class_type_id'];
        $Schedule->school_year_id = $rq['school_year_id'];
        $Schedule->save();

        if($rq['schedule_id']){
            $ScheduleId = $rq['schedule_id'];
        }
        else{
            $ScheduleId = $Schedule->max('schedule_id');
        }
        HandleSubjects::where('schedule_id',$ScheduleId)->delete();
        foreach($rq['weekdays'] as $weekdays_id){

            $i = 0;
            foreach($rq['counter'][$weekdays_id] as $counter){
                    
                    
                    $HandleSubjects                    = new HandleSubjects();
                    $HandleSubjects->assign_subject_id = $rq['subjects'][$weekdays_id][$i];
                    $HandleSubjects->start_time        = $rq['time_in'][$weekdays_id][$i];
                    $HandleSubjects->end_time          = $rq['time_out'][$weekdays_id][$i];
                    $HandleSubjects->schedule_id       = $ScheduleId;
                    $HandleSubjects->weekdays_id       = $weekdays_id;
                    $HandleSubjects->employee_id       = $rq['teacher'][$weekdays_id][$i];
                    $HandleSubjects->save();
                    
                    $i++;

            }

        }

        $return = new rrdReturn();
        return $return->status(true)
                      ->message("Subjects has been successfully assigned!")
                      ->show();
    }

    public function updateSubjectAssigningSections(Request $request){


        $Schedule                 = Schedule::find($request['schedule_id']);
        $Schedule->section_id     = $request['section_id'];
        $Schedule->employee_id    = $request['adviser'];
        $Schedule->slot           = $request['slot'];
        $Schedule->start_time     = $request['section_start_time'];
        $Schedule->end_time       = $request['section_end_time'];
        $Schedule->class_type_id  = $request['class_type_id'];
        $Schedule->school_year_id = $request['school_year_id'];
        $Schedule->save();

        $ScheduleId = $request['schedule_id'];
        
        HandleSubjects::where('schedule_id',$ScheduleId)->delete();

        $count  = 0;

        for($h = 0; $h <= count($request['weekdays'])-1 ; $h++){

            for($i = 0; $i <= count($request['assign_subject_id'.$request['weekdays'][$h]])-1 ; $i++){
                
                $weekdays_id = $request['weekdays'][$h];
                $assignSubjectId = $request['assign_subject_id'.$weekdays_id][$i];
                $uniqueCombo = $weekdays_id.$assignSubjectId;
                $checkId = "check".$weekdays_id.$assignSubjectId;
                $employee_id = $request['employee_id'.$uniqueCombo];
                $start_time_type = $request['start_time_type'.$uniqueCombo];
                $start_time = $request['start_time'.$uniqueCombo].":00 ".$start_time_type;
                $end_time_type = $request['end_time_type'.$uniqueCombo];
                $end_time = $request['end_time'.$uniqueCombo].":00 ".$end_time_type;


                if($request[$checkId] == "true"){

                    $count++; 
                    $HandleSubjects                    = new HandleSubjects();
                    $HandleSubjects->assign_subject_id = $assignSubjectId;
                    $HandleSubjects->start_time        = $start_time;
                    $HandleSubjects->end_time          = $end_time;
                    $HandleSubjects->schedule_id       = $ScheduleId;
                    $HandleSubjects->weekdays_id       = $weekdays_id;
                    $HandleSubjects->employee_id       = $employee_id;
                    $HandleSubjects->save();

                }

                
            }
        }

        $return = new rrdReturn();
        return $return->status(true)
                      ->message("Section Subjects has been successfully updated!")
                      ->show();
    }

    public function getSchedules(){

        $data = Schedule::with('RfSection','RfSection.getGradeLevel','RfSchoolYear')->get();

        $datatableFormat = new DatatableFormat();
        return $datatableFormat->format($data);
    }


    public function addNewScheduleView(){

        $classType    = RfClassType::all();
        $gradeType    = RfGradeType::all();
        $schoolYear   = RfSchoolYear::all();
        $weekdays     = Weekdays::all();
        $employees    = KronosEmployee::all();


        return view('sms.setup.setup-schedule',compact('employees','weekdays','gradeType','schoolYear','classType'));
    }
            
    public function editSchedule(Request $rq){

        $classType    = RfClassType::all();
        $gradeType    = RfGradeType::all();
        $schoolYear   = RfSchoolYear::all();
        $weekdays     = Weekdays::all();
        $employees    = KronosEmployee::all();

        $schedule = Schedule::with('RfSection.getSectionType','RfSection.getGradeLevel')->find($rq['schedule_id']);

        $section_type_id = $schedule->RfSection->getSectionType->section_type_id;

        $assignSubject = DtAssignSubject::with('getSubjects')->where('section_type_id',$section_type_id)->get();

        $handlesubjects = Weekdays::with('HandleSubjects.DtAssignSubject.getSubjects','HandleSubjects.getEmployee')
                        ->whereHas('HandleSubjects', function($q) use ($rq){
                             $q->where('schedule_id', $rq['schedule_id']);
                        })
                        ->get();
        $sched_id = $rq['schedule_id'];

        return view('sms.setup.setup-schedule-edit',compact('handlesubjects','schedule','employees','weekdays','gradeType','schoolYear','classType','sched_id','assignSubject'));
    }


}
