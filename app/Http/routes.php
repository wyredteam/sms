<?php

/*
|--------------------------------------------------------------------------
| SMS
|--------------------------------------------------------------------------
|
|	BACKEND : Ralph Reigh Degoma
|	DATABASE: Aldwin Cafe Filoteo
| 	BACKEND: Jan Jabanes
*/




Route::get('/','MainloaderController@home');

Route::get('test', function(){
	$data = App\RfGradeLevel::with('getSection','getSection.Schedule','getSection.Schedule.StudentSchedule')
							->whereHas('getSection.Schedule' , function($q){
								$q->where('school_year_id','2');
							})
							->get();

	dd($data);
});



Route::post('editable', function(){
	return Request::input('value');
});



//SELECT BINDER
Route::get('/select-binder/get-father','SelectBinderController@getFather');
Route::get('/select-binder/get-mother','SelectBinderController@getMother');
Route::get('/select-binder/get-gradeLevel','SelectBinderController@getGradeLevel');
Route::get('/select-binder/get-guardian','SelectBinderController@getGuardian');
Route::get('/select-binder/get-sectionName','SelectBinderController@getSectionName');
Route::get('/select-binder/get-KronosEmployee','SelectBinderController@KronosEmployee');
Route::get('/select-binder/get-students','SelectBinderController@getStudents');
Route::get('/select-binder/get-section-time','SelectBinderController@getSectionTime');
Route::get('/select-binder/get-section','SelectBinderController@getSection');
Route::get('/select-binder/get-sectionTypeName','SelectBinderController@getSectionTypeName');
Route::get('/select-binder/get-students-by-section','SelectBinderController@getStudentBySection');



//AUTOSUGGESTS
Route::get('/autosuggest/getFather','AutoSuggestController@getFather');
Route::get('/autosuggest/getMother','AutoSuggestController@getMother');
Route::get('/autosuggest/getReligiion','AutoSuggestController@getReligiion');
Route::get('/autosuggest/getNationality','AutoSuggestController@getNationality');
Route::get('/autosuggest/getOccupation','AutoSuggestController@getOccupation');
Route::get('/autosuggest/getbank','AutoSuggestController@getBank');




//R E G I S T R A R
Route::get('/sms/registrar/student-registration','MainloaderController@studentRegistration');
Route::post('/admin/new-student/registration','StudentController@newStudentSave');
Route::get('/admin/student/{student_id}/edit','StudentController@editStudent');
Route::get('/sms/registrar/search-student','StudentController@searchStudent');
Route::get('/sms/registrar/search-student-alphabet','StudentController@searchStudentAlphabet');
Route::get('/sms/registrar/enrollees','StudentController@enrolleesList');
Route::get('/sms/registrar/teacher-schedule','MainloaderController@getTeachers');
Route::get('/sms/registrar/get-teacher-schedule','RegistrarController@getTeacherSchedule');
Route::get('/sms/registrar/get-advisory','RegistrarController@getAdvisory');
Route::get('/sms/registrar/print-teacher-sched','ReportController@printTeacherSched');


//R E G I S T E R E D | L I S T
Route::get('/sms/registrar/registered-list','StudentListController@registeredList');
Route::get('/sms/registrar/get-students','StudentListController@getStudents');




//E N R O L L M E NT
Route::get('/sms/registrar/enrollment','MainloaderController@enrollment');
Route::post('/admin/sms/new-enroll','RegistrarController@enrollStudent');
Route::get('/sms/registrar/enrolled-student','MainloaderController@enrolledStudent');

//S T U D E N T S | R E Q U I R E M E N T S
Route::get('/sms/registrar/student-requirements','MainloaderController@studentRequirement');



//B I L L I N G
Route::get('/sms/billing/get-overall-balance','BillingController@getOverallBalance');
Route::get('/sms/billing/assessment','MainloaderController@studentAssessment');
Route::get('/sms/billing/assign-fees','MainloaderController@assignFees');
Route::get('/sms/billing/student-bill','MainloaderController@studentBill');
Route::get('/sms/get-assessments','BillingController@getAssessments');
Route::get('sms/billing/get-student-info','BillingController@getStudentInfo');
Route::post('/sms/registrar/generate-billing','BillingController@generateBill');
Route::post('/sms/billing/save-payment','BillingController@savePayment');
Route::get('/sms/billing/get-payments','BillingController@getPayments');
Route::post('/sms/billing/edit-payment','BillingController@editPayment');
Route::get('/sms/billing/get-payment-id','BillingController@getPaymentId');
Route::get('/sms/billing/get-fee-amount','BillingController@getFeeAmount');
Route::post('/sms/billing/save-discount','BillingController@saveDiscount');
Route::get('/sms/billing/get-discount','BillingController@getDiscount');
Route::post('/sms/billing/add-promissory','BillingController@addPromissory');
Route::get('/sms/billing/get-promissory','BillingController@getPromissory');
Route::get('/sms/billing/get-ledger','BillingController@getLedger');
Route::get('/sms/billing/other-income','MainloaderController@otherIncome');
Route::get('/sms/billing/expenses','MainloaderController@Expenses');
Route::post('/sms/setup/billing/other-payments','BillingController@saveOtherPayment');
Route::get('/sms/setup/billing/get-other-payments','BillingController@getOtherPayments');
Route::get('/sms/setup/billing/get-expenses','BillingController@getExpenses');
Route::post('/sms/setup/billing/save-expenses','BillingController@saveExpenses');
Route::get('/sms/setup/billing/get-expenses-report','BillingController@getExp');
Route::post('/sms/billing/assign-custom-fee','BillingController@assignCustomFees');
Route::post('/sms/setup/billing/save-journal','BillingController@saveJournal');
Route::get('/reports/journal-entries','MainloaderController@journalEntries');
Route::get('/sms/setup/billing/get-journal-list','BillingController@getJournals');
Route::post('/sms/setup/billing/edit-journal','BillingController@editJournal');
Route::post('/sms/billing/adjust-monthly-bill','BillingController@adjustMonthlyBill');
Route::get('/sms/billing/generate-balances-list','BillingController@studentBalList');



//B I L L I N G   S E T U P
Route::get('/sms/setup/billing/fee-category','MainloaderController@categoryFee');
Route::get('/sms/setup/billing/fees','MainloaderController@setupFees');
Route::get('/sms/setup/billing/payment-type-schedule','MainloaderController@paymentTypeSchedules');
Route::get('/sms/setup/billing/grade-level-fees','MainloaderController@levelFees');
Route::get('/sms/setup/billing/tuition-reference','MainloaderController@tuitionReference');
Route::get('/sms/setup/billing/due-dates','MainloaderController@dueDates');
Route::get('/sms/setup/billing/lessee','MainloaderController@lesseeSetup');
Route::get('/sms/setup/billing/tenant-fees','MainloaderController@tenantFees');
Route::post('/sms/setup/billing/save-category','BillingController@saveCategory');
Route::get('/sms/setup/billing/get-category','BillingController@getCategory');
Route::post('/sms/setup/billing/save-fees','BillingController@saveFees');
Route::get('/sms/setup/billing/get-fees','BillingController@getFees');
Route::get('/sms/setup/billing/get-grade-fees','BillingController@getGradeFees');
Route::post('/sms/setup/billing/save-grade-fees','BillingController@saveGradeFees');
Route::post('/sms/setup/billing/save-payment-sched','BillingController@savePaymentSched');
Route::get('/sms/setup/billing/accounts','MainloaderController@setupAccounts');
Route::get('/sms/setup/billing/get-accounts','BillingController@getAccounts');
Route::post('/sms/setup/billing/save-account','BillingController@saveAccount');
Route::post('/sms/setup/billing/recall-fees','BillingController@recallFees');


Route::get('/sms/billing/get-collection','BillingController@getCollection');




//A C A D E M E I C    S E T U P
Route::get('sms/registrar/getgrade','RegistrarController@getGrade');
Route::get('sms/registrar/year','RegistrarController@getYear');
Route::get('sms/registrar/subject','RegistrarController@getSubject');
Route::get('sms/registrar/get-section','RegistrarController@getSection');
Route::get('/sms/registrar/get-assign-subject','RegistrarController@getAssignSubject');
Route::post('sms/registrar/assign-subject','RegistrarController@assignSubject');
Route::post('/sms/registrar/save-grade-level','RegistrarController@saveGradeLevel');
Route::post('/sms/registrar/save-school-year','RegistrarController@saveSchoolYear');
Route::post('/sms/registrar/save-subject','RegistrarController@saveSubject');
Route::get('/sms/registrar/grade','RegistrarController@gradeLevel');
Route::post('/sms/registrar/remove/{remove}/{id}', 'RegistrarController@Remove');
Route::post('/sms/registrar/edit/grade/{id}', 'RegistrarController@Edit');
Route::post('/sms/registrar/save-section','RegistrarController@saveSection');
Route::get('/sms/get-subjects/with-filters','RegistrarController@getSubjectsWithFilters');
Route::get('/sms/get-list-subject','RegistrarController@getListSubjects');
Route::get('/sms/get-list-subject-edit','RegistrarController@getListSubjectsEdit');
Route::get('/sms/get-list-schedules','RegistrarController@getListSchedules');
Route::get('/sms/setup/academics/schedule/add-schedule','AcademicsController@addNewScheduleView');
Route::get('/sms/setup/academics/requirements','RegistrarController@getRequirements');
Route::get('/sms/setup/academics/schedule/edit-schedule','AcademicsController@editSchedule');


//E N R O L L M E N T
Route::get('/sms/registrar/enrolled-students','EnrollmentController@getEnrollees');
Route::get('/sms/enrollment/get-adviser','EnrollmentController@getAdviser');

//E C L A S S

Route::get('/sms/e-class-room/attendance','MainloaderController@Attendance');












//R E P O R T S 
Route::get('/sms/reports/enrolled-students','ReportController@enrolledStudents');
Route::get('/sms/reports/masterlist','MainloaderController@masterlist');
Route::get('/sms/reports/generate-masterlist','ReportController@generateMasterlist');
Route::get('/sms/print-assessment','ReportController@printAssessment');
Route::get('/sms/reports/grade-level-billing','ReportController@gradeLevelBilling');
Route::get('/sms/monthly-billing','MainloaderController@monthlyBilling');
Route::get('/sms/reports/generate-monthly-billing','ReportController@generateMonthlyBilling');
Route::get('/admin/student/print-info','ReportController@getInfo');
Route::get('/sms/registrar/get-students/print','ReportController@getStudentsCustom');
Route::get('/sms/billing/print-ledger','ReportController@printLedger');
Route::get('/sms/setup/academics/schedule/{id}/print','ReportController@printSchedule');
Route::get('/sms/billing/generate-balances','ReportController@generateBalances');
Route::get('/reports/collections','MainloaderController@collections');
Route::get('/reports/payment-list','MainloaderController@paymentList');
Route::get('/sms/setup/billing/get-payments-list','ReportController@getPaymentList');
Route::get('/sms/setup/billing/print-payments-list','ReportController@printPaymentList');
Route::get('/sms/billing/journal-entry','MainloaderController@journalEntry');
Route::get('/sms/setup/billing/get-account-list','BillingController@getAcc');
Route::get('/reports/overall-balances','ReportController@overallBalances');
Route::get('/print/student-balance-list-name','PermitController@printBalanceNames');
//Route::post('/sms/reports/generate-payment-list','');


Route::get('/sms/reports/print-income','ReportController@printIncome');
Route::get('/sms/reports/print-expenses','ReportController@printExpenses');
Route::get('/sms/reports/print-others','ReportController@printOthers');
Route::get('/sms/reports/print-general','ReportController@printGeneral');
Route::get('/sms/reports/print-general-by-month','ReportController@printGeneralByMonth');

// K R O N O S
Route::get('/sms/kronos/time-attendance','MainloaderController@timeAttendance');
Route::get('/sms/kronos/schedulling','MainloaderController@schedulling');



// S E L E C T   B I N D E R
Route::post('/sms/select-binder/','RegistrarController@saveSubject');
Route::get('/select-binder/get-subjects','SelectBinderController@getSubjects');

Route::get('/bind/select-section','PermitController@selectSection');

Route::get('/sms/billing/permit','PermitController@index');
Route::get('/bind/select-student','PermitController@getStudents');
Route::get('/print/permit','PermitController@printPermit');
Route::get('/print/student-balance-list','PermitController@printStudentBal');

//A C A D E M I C S
Route::get('/sms/setup/academics','MainloaderController@academicsSetup');
Route::get('/sms/setup/academics/school-year','MainloaderController@schoolYear');
Route::get('/sms/setup/academics/sy-month-template','MainloaderController@symonthTemplate');
Route::get('/sms/setup/academics/grade-level','MainloaderController@gradeLevel');
Route::get('/sms/setup/academics/section','MainloaderController@section');
Route::get('/sms/setup/academics/subject','MainloaderController@subject');
Route::get('/sms/setup/academics/assign-subject','MainloaderController@assignSubject');
Route::get('/sms/setup/academics/schedule','MainloaderController@schedule');
Route::post('/sms/academics/assign-subjects/to-sections','AcademicsController@subjectAssigningSections');
Route::get('/sms/setup/academics/schedules','AcademicsController@getSchedules');




// S O F T D E L E T E 
Route::get('/softdelete/deleteSchoolYear','SoftdeleteController@deleteSchoolYear');
Route::get('/softdelete/deleteGradeLevel','SoftdeleteController@deleteGradeLevel');
Route::get('/softdelete/deleteSection','SoftdeleteController@deleteSection');
Route::get('/softdelete/deleteSubject','SoftdeleteController@deleteSubject');
Route::get('/softdelete/deleteAssignSubject','SoftdeleteController@deleteAssignSubject');
Route::get('/softdelete/delete-fee-categories','SoftdeleteController@deleteFeeCategories');
Route::get('/softdelete/delete-fees','SoftdeleteController@deleteFees');
Route::get('/softdelete/delete-billing','SoftdeleteController@deleteBilling');
Route::get('/softdelete/delist-student','SoftdeleteController@delistStudent');
Route::get('/softdelete/cancel-payment','SoftdeleteController@cancelPayment');
Route::get('/softdelete/cancel-discount','SoftdeleteController@cancelDiscount');
Route::get('/softdelete/cancel-promissory','SoftdeleteController@deletePromissory');
Route::get('/softdelete/delete-other-payment','SoftdeleteController@deletOtherPayment');
Route::get('/softdelete/delete-expenses','SoftdeleteController@deleteExpenses');
Route::get('/softdelete/remove-assessment','SoftdeleteController@removeAssessment');





/*  S E C U R I T Y  */
		
//AUTHENTICATION
Route::get('admin/security/login/Authentication','Auth\AuthController@Authenticate');
Route::get('admin/security/security-menu','WyredController@securityMenu');
Route::get('admin/security/security-modules','WyredController@securityModules');



Route::post('admin/security/login/Authentication','Auth\AuthController@postLogin');
Route::get('admin/security/logout','Auth\AuthController@logout');
		

//ACCOUNT MANAGEMENT
Route::get('/admin/security/create-account','WyredController@createAccount');
Route::post('/admin/save/account','WyredController@makeProfile');


//MODULE
Route::post('/admin/security/new-module','WyredController@newModule');
Route::get('/admin/security/get-modules','WyredController@getModules');
Route::post('/admin/security/new-submodule','WyredController@newSubModule');
Route::get('/admin/security/get-sub-modules','WyredController@getSubModules');


//ROLES
Route::get('admin/security/security-roles','WyredController@newRoles');
Route::post('admin/security/new-role','WyredController@newRole');
Route::get('admin/security/get-roles','WyredController@getRoles');


//Permissions
Route::get('admin/security/security-permissions','WyredController@newPermissions');
Route::post('admin/security/save-new-permissions','WyredController@savePermissions');
Route::post('admin/security/get-permissions-by-id','WyredController@getPermissions');


//accounts
Route::get('/admin/security/security-account','WyredController@securityAccount');
Route::get('/admin/security/get-accounts-list','WyredController@getAccountsList');

//messenger
Route::get('/admin/messenger','WyredMessenger@messenger');
Route::get('/admin/messenger/convo/{user_id}','WyredMessenger@getConvo');
Route::post('/admin/messenger/new-message','WyredMessenger@newMessage');
Route::get('/admin/messenger/load-convo/{user_id}','WyredMessenger@loadConvo');
Route::get('/admin/messenger/old-convo-messages/{user_id}','WyredMessenger@loadOldMessages');
Route::get('/admin/messenger/new-messages','WyredMessenger@countNewMessages');
Route::get('/admin/messenger/load-dropdown-messages','WyredMessenger@loadDropDownMessages');

//grading
Route::get('/sms/grading/list','GradingController@gradingList');
Route::get('/sms/grading/setup','GradingController@gradingSetup');
Route::get('/sms/card/get-card','GradingController@getCard');
Route::get('/sms/grading/new-card','GradingController@newCardTemplate');
Route::get('/sms/card/get-template-card','GradingController@getCardTemplate');
Route::get('/sms/card/save-template-card','GradingController@saveTemplate');



//PASSWORD
Route::get('/forgot-password','WyredController@forgotPassword');
Route::get('/request-verification-code','WyredController@requestVerificationCode');
Route::post('/send-verification-code','WyredController@sendVerificationCode');
Route::post('/password-reset','WyredController@passwordReset');


//facebook
Route::get('auth/facebook', 'Auth\AuthFacebookController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\AuthFacebookController@handleProviderCallback');




//MISC
Route::get('truncate', 'MiscController@truncate');
Route::get('add-columns', 'MiscController@addColumns');




//logs

Route::get('sms/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


Route::Post('/android/register','AndroidController@register');
