<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RfAccountType extends Model
{
    //
    protected $table = 'rf_acccount_type';
    protected $primaryKey = 'acccount_type_id';
}
