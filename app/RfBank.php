<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RfBank extends Model
{
    //
    protected $table = "rf_bank";
    protected $primaryKey = "bank_id";
}
