<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
     

        view()->composer('*', function($view) {


            if (Auth::check()) {
                $employees = User::with('KronosEmployee')
                    ->where('employee_id','<>', null)
                    ->where('user_id','<>',auth()->user()->user_id )
                    ->get();

                $view->with('employees', $employees);
            }

            
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
