<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradingType extends Model
{
    //
    protected $table = 'rf_grading_type';
    protected $primaryKey = 'grading_type_id';

}
