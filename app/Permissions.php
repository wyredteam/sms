<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    protected $table = 'dt_emp_assign_modules';
    protected $primaryKey = 'emp_assign_mod_id';
}
