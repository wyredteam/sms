<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DtExpenses extends Model
{
    //
    protected $table = 'dt_expenses';
    protected $primaryKey = 'expenses_id';

    public function getFees(){
        return $this->belongsTo('App\RfFees','fees_id','fees_id');
    }
    public function getPaymentOption(){
        return $this->belongsTo('App\RfPaymentOption','payment_option_id','payment_option_id');
    }
}
