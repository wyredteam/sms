<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RfAccount extends Model
{
    //

    protected $table = 'rf_account';


    public function getAccountType(){
        return $this->belongsTo('App\RfAccountType','acccount_type_id','acccount_type_id');
    }
}
